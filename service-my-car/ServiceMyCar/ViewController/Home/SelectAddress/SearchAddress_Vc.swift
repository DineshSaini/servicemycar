//
//  SearchAddress_Vc.swift
//  ServiceMyCar
//
//  Created by baps on 25/01/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import CoreLocation
import SwiftyJSON
import GoogleMaps
import GooglePlaces


class SearchAddress_Vc: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var searchBackView:UIView!
    @IBOutlet weak var tblPlaces: UITableView!
    
    var isPickUp = true
    var resultsArray:[Dictionary<String, AnyObject>] = Array()
    
    var placesGmMS =  [GMSPlace]()
    
    var filter = GMSAutocompleteFilter()
   
    
   let badgeButton : MIBadgeButton = MIBadgeButton(frame:CGRect(x: -10, y: 15, width: 40, height: 20))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
       // getsPlaceDetails
        txtSearch.addTarget(self, action: #selector(searchPlaceFromGoogle(_:)), for: .editingChanged)
     
        
       // txtSearch.addTarget(self, action: #selector(getsPlaceDetails(_:)), for: .editingChanged)

        tblPlaces.estimatedRowHeight = 44.0
        tblPlaces.dataSource = self
        tblPlaces.delegate = self
        self.navigationController?.isNavigationBarHidden = false
    AppHelper.makeViewCircularWithRespectToHeight(searchBackView, borderColor: .lightGray, borderWidth: 1.0)
        let lbNavTitle = UILabel(frame: CGRect(x: CGFloat(35), y:self.view.bounds.size.width/2-235, width: CGFloat(200), height: CGFloat(40)))
        lbNavTitle.textAlignment = .center
        lbNavTitle.text = "search_address".localized()
        txtSearch.placeholder = "search_address".localized()

        lbNavTitle.textColor = Colorblack
        lbNavTitle.font = UIFont(name: FontBold, size: 16)
        self.navigationItem.titleView = lbNavTitle
        self.navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.white
        
        let titleDict: NSDictionary = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [NSAttributedString.Key : Any]
        
        let button = UIButton.init(type: .custom)
        //backarrow , menu
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        
        if languageCode == "en" {
            button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
        }else {
            button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
            txtSearch.textAlignment = .right
        }
        button.addTarget(self, action:#selector(SideMenu), for: UIControl.Event.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        badgeButton.edgeInsetTop = 10
        badgeButton.setImage(UIImage(named : "notification"), for: .normal)
        print(appDelegate.NCount)
         badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        badgeButton.badgeString = appDelegate.NCount
        if appDelegate.NCount == "0"{
            badgeButton.badgeBackgroundColor = UIColor.clear
            badgeButton.badgeTextColor = UIColor.clear
        }else{
            badgeButton.badgeBackgroundColor = colorGreen
            badgeButton.badgeTextColor = UIColor.white
        }
        badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        let barButton2 : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton2
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)
//        filter.type = .establishment
//        filter.country = "AE"
        
        
        
       
        
    }
    
    
    // MARK: - Google Api Calling
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            //self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                
                badgeButton.badgeString = appDelegate.NCount
                if appDelegate.NCount == "0"
                {
                    badgeButton.badgeBackgroundColor = UIColor.clear
                    badgeButton.badgeTextColor = UIColor.clear
                }
                else
                {
                    badgeButton.badgeBackgroundColor = colorGreen
                    badgeButton.badgeTextColor = UIColor.white
                }
                
            }
        }
    }
    
    
    @objc func NotificationAlert()
    {
        guard let vcs = self.navigationController?.viewControllers else{return}
        let inStack = vcs.contains(where: { (vc) -> Bool in
            return vc is MyNotificationList_Vc
        })
        if inStack{
            self.performSegue(withIdentifier: unwindToNotificationController, sender: self)
        }else{
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        //        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
        //
        //        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
       self.navigationController?.setupNavigation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.setupNavigation()

    }
    
    
    // MARK:- goBack
    
    @objc func SideMenu()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- UITableViewDataSource and UItableViewDelegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return /*placesGmMS.count*/resultsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "placecell")
        if let lblPlaceName = cell?.contentView.viewWithTag(102) as? UILabel {
//            let place = self.placesGmMS[indexPath.row]
//            lblPlaceName.text = "\(place.name ?? "") \(place.formattedAddress ?? "")"
           let place = self.resultsArray[indexPath.row]
            lblPlaceName.text = "\(place["description"] as! String)"
        }
        return cell!
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
//        let place = self.placesGmMS[indexPath.row]
//        appDelegate.selectedAddress = "\(place.name ?? "") \(place.formattedAddress ?? "")"
//        appDelegate.Selectedplacemark = CLLocationCoordinate2D(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
//        DispatchQueue.main.async {
//            // self.items = downloadedItems
//            self.Back()
//        }
        let place = self.resultsArray[indexPath.row]
        print("\(place["description"] as! String)")
        GetLatlongFromAddress(address: place["description"] as! String)

    }
    
     func GetLatlongFromAddress(address:String) {
        let langcode = UserDefaults.standard.value(forKey: kAppLanguage) ?? "en"

            var strGoogleApi = "https://maps.googleapis.com/maps/api/geocode/json?key=\(googleApiKey)&language=\(langcode)&address=\(address)"
            strGoogleApi = strGoogleApi.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            print(strGoogleApi)
            var urlRequest = URLRequest(url: URL(string: strGoogleApi)!)
            urlRequest.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, resopnse, error) in
            if error == nil {
                
                if let responseData = data {
                    let jsonDict = try? JSONSerialization.jsonObject(with: responseData, options: .mutableContainers)
                    
                    if let dict = jsonDict as? Dictionary<String, AnyObject>{
                        
                        if let results = dict["results"] as? NSArray {
                            print("json == \(results)")
                            if let data = results.object(at: 0) as? NSDictionary{
                                if let nextvalue = data.value(forKey: "place_id") as? String{
                                    appDelegate.selectedAddress = address
                                    self.getaddressFromPlaceId(placeId: nextvalue)
                                }
                            }
                        }
                    }
                }
            } else {
                //we have error connection google api
            }
            
        }
        task.resume()
    }
    
    
    func Back(){
      self.navigationController?.popViewController(animated: true)
    }
    
    @objc func getsPlaceDetails(_ textfield:UITextField){
        guard let text = textfield.text else{return}
        if text.isEmpty {
            self.placesGmMS.removeAll()
            self.tblPlaces.reloadData()
            return
        }
        
        let placesClient = GMSPlacesClient.shared()
        placesClient.autocompleteQuery(text, bounds: nil, filter: filter) { (results, error) in
            self.placesGmMS.removeAll()
            if error != nil{
                self.tblPlaces.reloadData()
                return
            }
            guard let results = results else{
                self.tblPlaces.reloadData()
                return
            }
            for result in results {
                var placeId = ""
                if let tempId = result.placeID as? String {
                    placeId = tempId
                }else{
                    self.tblPlaces.reloadData()
                    return
                }
                placesClient.lookUpPlaceID(placeId, callback: { (place, error) in
                    if error != nil {
                        self.tblPlaces.reloadData()
                        return
                    }
                    if let place = place{
                        self.placesGmMS.append(place)
                    }
                    self.tblPlaces.reloadData()
                })
                self.tblPlaces.reloadData()
            }
        }
    }
    
    
    @objc func searchPlaceFromGoogle(_ textField:UITextField) {
        let langcode = UserDefaults.standard.value(forKey: kAppLanguage) ?? "en"

        if let searchQuery = textField.text {
         var strGoogleApi = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(searchQuery)&components=country:AE&language=\(langcode)&key=\(googleApiKey)"
         //   https://maps.googleapis.com/maps/api/place/autocomplete/json?input=Vict&types=geocode&language=fr&key=YOUR_API_KEY
           // var strGoogleApi = "https://maps.googleapis.com/maps/api/place/autocomplete/json?components=country:ae&key=\(googleApiKey)&hasNextPage=true&nextPage()=true&input=\(searchQuery)"
            strGoogleApi = strGoogleApi.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            print(strGoogleApi)
            var urlRequest = URLRequest(url: URL(string: strGoogleApi)!)
            urlRequest.httpMethod = "GET"
            let task = URLSession.shared.dataTask(with: urlRequest) { (data, resopnse, error) in
                if error == nil {
                    
                    if let responseData = data {
                        let jsonDict = try? JSONSerialization.jsonObject(with: responseData, options: .mutableContainers)
                        
                        if let dict = jsonDict as? Dictionary<String, AnyObject>{
                            
                            if let results = dict["predictions"] as? [Dictionary<String, AnyObject>] {
                                self.resultsArray.removeAll()
                                for dct in results {
                                    let dict = dct
                                    self.resultsArray.append(dict)
                                }
                                
                                DispatchQueue.main.async {
                                    self.tblPlaces.reloadData()
                                }
                                
                            }
                        }
                        
                    }
                } else {
                    //we have error connection google api
                }
            }
            task.resume()
        }
    }
    
    
    func getaddressFromPlaceId(placeId:String){
        let placesClient = GMSPlacesClient.shared()
        placesClient.lookUpPlaceID(placeId, callback: { (place, error) in
            if error != nil {
                return
            }
            if let place = place{
                let myLocation =  CLLocationCoordinate2D(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
                appDelegate.Selectedplacemark = myLocation
                //appDelegate.selectedAddress = "\(place.name ?? "") \(place.formattedAddress ?? "")"
                DispatchQueue.main.async {
                    self.Back()
                }
            }
        })
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

