//
//  SelectAddress_Vc.swift
//  ServiceMyCar
//
//  Created by baps on 24/01/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import GoogleMaps



protocol SelectAddress_VcDelegate {
    func getFullAddress(_ viewController:SelectAddress_Vc, getAddress address:String, getLatitude lat:String, getLongitude long:String, getState state:String)
}


class SelectAddress_Vc: UIViewController  {
    
    
    var isDropSelection:Bool = Bool()
    var isFrom = ""
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var btnCntFindAddress: UIButtonX!
    @IBOutlet weak var viewLocation: UIView!
    @IBOutlet weak var btnConfirmPickup: UIButtonX!
    @IBOutlet weak var txtViewMapIn: UITextView!
    @IBOutlet private weak var mapCenterPinImage: UIImageView!
    @IBOutlet private weak var pinImageVerticalConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewLocatoinHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var addressLabel2: UILabel!
    @IBOutlet weak var topspaceForMapToBottom: NSLayoutConstraint!
    private var searchedTypes = ["bakery", "bar", "cafe", "grocery_or_supermarket", "restaurant"]
    private let locationManager = CLLocationManager()
    private let dataProvider = GoogleDataProvider()
    private let searchRadius: Double = 1000
    var isPickUp = true
    var topSpace:Float = Float()
    var County:String = String()
    var myLocationButtonBottomSpace = 60
    var validationCity = "select_address_uae".localized()//"Sorry we do not operate in this location."//
    var Latitude:String = String()
    var Longitude:String = String()
    var findState:String = ""
    let badgeButton : MIBadgeButton = MIBadgeButton(frame:CGRect(x: -10, y: 15, width: 40, height: 20))
    
    
    var delegate:SelectAddress_VcDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        self.mapView.mapStyle(withFilename: "mapStyle", andType: "json")
        self.view.sendSubviewToBack(mapView)
        mapView.delegate = self
        self.navigationController?.isNavigationBarHidden = false
        let lbNavTitle = UILabel(frame: CGRect(x: CGFloat(35), y:self.view.bounds.size.width/2-235, width: CGFloat(200), height: CGFloat(40)))
        lbNavTitle.textAlignment = .center
        lbNavTitle.text = "select_address_title".localized()
        lbNavTitle.textColor = Colorblack
        lbNavTitle.font = UIFont(name: FontBold, size: 16)
     
        self.navigationItem.titleView = lbNavTitle
        self.navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.barTintColor = UIColor.clear
        
        let titleDict: NSDictionary = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [NSAttributedString.Key : Any]

        
        let button = UIButton.init(type: .custom)
        //backarrow , menu
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        
        if languageCode == "en" {
            button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
            
        }else {
            button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
            self.txtViewMapIn.textAlignment = .right

        }
        button.addTarget(self, action:#selector(SideMenu), for: UIControl.Event.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
 
        btnConfirmPickup.setTitle("confirm".localized(), for: .normal)
        AppHelper.makeViewCircularWithRespectToHeight(btnConfirmPickup, borderColor: .clear, borderWidth: 0.0)
        //btnCntFindAddress.setTitle("select_address_cant_find_your_address".localized(), for: .normal)
        self.viewLocatoinHeightConstant.constant = 35
        badgeButton.edgeInsetTop = 10
        badgeButton.setImage(UIImage(named : "notification"), for: .normal)
        print(appDelegate.NCount)
         badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        badgeButton.badgeString = appDelegate.NCount
        if appDelegate.NCount == "0"
        {
            badgeButton.badgeBackgroundColor = UIColor.clear
            badgeButton.badgeTextColor = UIColor.clear
        }
        else
        {
            badgeButton.badgeBackgroundColor = colorGreen
            badgeButton.badgeTextColor = UIColor.white
        }
        badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        let barButton2 : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton2
        //        getPackagesListByCategory()
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)

       
    }
    
    // Step 3
    
    @objc func NotificationAlert()
    {
        guard let vcs = self.navigationController?.viewControllers else{return}
        let inStack = vcs.contains(where: { (vc) -> Bool in
            return vc is MyNotificationList_Vc
        })
        if inStack{
            self.performSegue(withIdentifier: unwindToNotificationController, sender: self)
        }else{
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        //        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
        //
        //        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    // Step 4
    
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            //self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                
                badgeButton.badgeString = appDelegate.NCount
                if appDelegate.NCount == "0"
                {
                    badgeButton.badgeBackgroundColor = UIColor.clear
                    badgeButton.badgeTextColor = UIColor.clear
                }
                else
                {
                    badgeButton.badgeBackgroundColor = colorGreen
                    badgeButton.badgeTextColor = UIColor.white
                }
            }
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.makeTransparent()
        if appDelegate.selectedAddress != ""{
            self.addressLabel.text = appDelegate.selectedAddress
            self.txtViewMapIn.text = appDelegate.selectedAddress
            
        }

        if appDelegate.Selectedplacemark != nil{
            mapView.camera = GMSCameraPosition(target: (appDelegate.Selectedplacemark)!, zoom: 15, bearing: 0, viewingAngle: 0)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.makeTransparent()

    }
    
    
    // MARK:- goBack
    
    @objc func SideMenu()
    {
        appDelegate.Selectedplacemark = nil
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK:- onClickBtnPickUp
    @IBAction func onClickBtnPickUp(_ sender: Any) {
        
//        if self.County == self.validationCity
//        {
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "SearchAddress_Vc") as! SearchAddress_Vc
            nextViewController.isPickUp = true
            self.navigationController?.pushViewController(nextViewController, animated: true)
       // }
        
    }
    
    // MARK:- onClickBtnDropoff
    @IBAction func onClickBtnDropoff(_ sender: Any) {
//        if self.County == self.validationCity
//        {
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "SearchAddress_Vc") as! SearchAddress_Vc
            nextViewController.isPickUp = false
            self.navigationController?.pushViewController(nextViewController, animated: true)
        //}
        
    }
    // MARK:- onClickbtnCntFindAddress
    @IBAction func onClickbtnCntFindAddress(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        return
        
//        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "AddAddress_Vc") as! AddAddress_Vc
//        nextViewController.strPickupaddress = addressLabel.text!
//        nextViewController.isDropSelection = isDropSelection
//        nextViewController.strDropOffAddress = addressLabel2.text!
//        nextViewController.isFrom = isFrom
//        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    // MARK:- onClickBtnConfirmPickup
    @IBAction func onClickBtnConfirmPickup(_ sender: UIButton) {
        if self.County == self.validationCity{
            self.delegate?.getFullAddress(self, getAddress: addressLabel.text!, getLatitude: self.Latitude, getLongitude: self.Longitude, getState: self.findState)
        self.navigationController?.popViewController(animated: true)
        }else{
          locationManager.requestWhenInUseAuthorization()
        }
        
        return
        
        
//        if self.County == self.validationCity
//        {
//            if isDropSelection == false
//            {
//                let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "AddAddress_Vc") as! AddAddress_Vc
//                nextViewController.strPickupaddress = addressLabel.text!
//                nextViewController.isDropSelection = isDropSelection
//                nextViewController.strDropOffAddress = addressLabel2.text!
//                self.navigationController?.pushViewController(nextViewController, animated: true)
//            }
//            else
//            {
//                print(isFrom)
//                if isFrom == "3" || isFrom == "4" {
//                    let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "AddAddress_Vc") as! AddAddress_Vc
//                    nextViewController.strPickupaddress = addressLabel.text!
//                    nextViewController.isFrom = isFrom
//                    self.navigationController?.pushViewController(nextViewController, animated: true)
//                }
//                else {
//                    if sender.titleLabel?.text == "select_address_confirm_drop_off".localized()
//                    {
//
//                        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "AddAddress_Vc") as! AddAddress_Vc
//                        nextViewController.strPickupaddress = addressLabel.text!
//                        nextViewController.isDropSelection = isDropSelection
//                        nextViewController.strDropOffAddress = addressLabel2.text!
//                        self.navigationController?.pushViewController(nextViewController, animated: true)
//                    }
//                    else
//                    {
//                        if viewLocatoinHeightConstant.constant == 35
//                        {
//                            // btnCntFindAddress.isHidden = true
//
//                            UIView.animate(withDuration: 2, animations:{
//                                self.viewLocatoinHeightConstant.constant = 70
//                                //self.topspaceForMapToBottom.constant = 19
//                                self.addressLabel2.text = self.addressLabel.text
//                                self.isPickUp = false
//                                sender.setTitle("select_address_confirm_drop_off".localized(), for: .normal)
//                            })
//                        }
//                        else
//                        {
//                            //btnCntFindAddress.isHidden = false
//                            UIView.animate(withDuration: 2, animations:{
//                                // self.viewLocatoinHeightConstant.constant = 35
//                                self.topspaceForMapToBottom.constant = CGFloat(self.topSpace)
//                                self.isPickUp = true
//                                sender.setTitle("select_address_confirm_pickup".localized(), for: .normal)
//                            })
//                        }
//                    }
//                }
//            }
//        }
        
 //       locationManager.requestWhenInUseAuthorization()
    }
    
    
    private func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
        let geocoder = GMSGeocoder()
        
        Latitude = "\(coordinate.latitude)"
        Longitude = "\(coordinate.longitude)"
        
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            self.txtViewMapIn.isHidden = false
            if self.isPickUp == true
            {
                self.addressLabel.unlock()
            }
            else
            {
                self.addressLabel2.unlock()
            }
            
            
            guard let address = response?.firstResult(), let lines = address.lines else {
                return
            }
            self.County = (response?.firstResult()?.country)!
            print(self.County)
            if self.County == self.validationCity{
                if self.isPickUp == true{
                    if appDelegate.selectedAddress != ""{
                        self.addressLabel.text = appDelegate.selectedAddress
                        self.txtViewMapIn.text = appDelegate.selectedAddress
                        appDelegate.selectedAddress = ""
                    }else{
                       self.addressLabel.text = lines.joined(separator: "\n")
                         self.txtViewMapIn.text = lines.joined(separator: "\n")
                    }
                    
                    let labelHeight = self.addressLabel.intrinsicContentSize.height
                    self.mapView.padding = UIEdgeInsets(top: self.view.safeAreaInsets.top, left: 0,
                                                        bottom: 50, right: 0)
                    
                    UIView.animate(withDuration: 0.25) {
                        self.pinImageVerticalConstraint.constant = ((labelHeight - self.view.safeAreaInsets.top) * 0.5)
                        self.view.layoutIfNeeded()
                    }
                }else{
                    if appDelegate.selectedAddress != ""{
                        self.addressLabel2.text = appDelegate.selectedAddress
                        self.txtViewMapIn.text = appDelegate.selectedAddress
                        appDelegate.selectedAddress = ""
                    }else{
                        self.addressLabel2.text = lines.joined(separator: "\n")
                        self.txtViewMapIn.text = lines.joined(separator: "\n")
                    }
                    let labelHeight = self.addressLabel2.intrinsicContentSize.height
                    self.mapView.padding = UIEdgeInsets(top: self.view.safeAreaInsets.top, left: 0,
                                                        bottom: 50, right: 0)
                    
                    UIView.animate(withDuration: 0.25) {
                        self.pinImageVerticalConstraint.constant = ((labelHeight - self.view.safeAreaInsets.top) * 0.5)
                        self.view.layoutIfNeeded()
                    }
                }
            }else{
                self.addressLabel.text = "Sorry_we_do_not_operate_in_this_location".localized()
                self.addressLabel2.text = "Sorry_we_do_not_operate_in_this_location".localized()
                self.txtViewMapIn.text = "Sorry_we_do_not_operate_in_this_location".localized()
            }
        }
    }
    
    
    func nameOfPlaceWithCoordinates(coordinate : CLLocationCoordinate2D,complition:@escaping (_ address:String)->Void) {
        let geocoder = GMSGeocoder()
        
        var currentAddress = ""
        Latitude = "\(coordinate.latitude)"
        Longitude = "\(coordinate.longitude)"
        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
            
            self.txtViewMapIn.isHidden = false
            if self.isPickUp == true{
                self.addressLabel.unlock()
            }else{
                self.addressLabel2.unlock()
            }
            
            for address in (response?.results())!{
                self.County = address.country!
         //       print("get final address ==============     \(address)")
                if self.County == self.validationCity{
                    if self.isPickUp{
                        if appDelegate.selectedAddress != ""{
                            self.addressLabel.text = appDelegate.selectedAddress
                            self.txtViewMapIn.text = appDelegate.selectedAddress
                            appDelegate.selectedAddress = ""
                        }else{
                            currentAddress = self.setUpRemoveUnamedRoad(address)
                            complition(currentAddress)
                            self.addressLabel.text = currentAddress
                            self.txtViewMapIn.text = currentAddress
                        }
                        let labelHeight = self.addressLabel.intrinsicContentSize.height
                        self.mapView.padding = UIEdgeInsets(top: self.view.safeAreaInsets.top, left: 0,bottom: 50, right: 0)
                        UIView.animate(withDuration: 0.25) {
                            self.pinImageVerticalConstraint.constant = ((labelHeight - self.view.safeAreaInsets.top) * 0.5)
                            self.view.layoutIfNeeded()
                        }
                         if address.thoroughfare != nil && address.thoroughfare != "Unnamed Road"{
                            return
                        }
                        if address.subLocality != nil{
                            return
                        }
                        if address.locality != nil{
                            return
                        }
                    }else{
                        if appDelegate.selectedAddress != ""{
                            self.addressLabel2.text = appDelegate.selectedAddress
                            self.txtViewMapIn.text = appDelegate.selectedAddress
                            appDelegate.selectedAddress = ""
                        }else{
                            currentAddress = self.setUpRemoveUnamedRoad(address)
                            complition(currentAddress)
                            self.addressLabel2.text = currentAddress
                            self.txtViewMapIn.text = currentAddress
                        }
                        let labelHeight = self.addressLabel2.intrinsicContentSize.height
                        self.mapView.padding = UIEdgeInsets(top: self.view.safeAreaInsets.top, left: 0,bottom: 50, right: 0)
                        UIView.animate(withDuration: 0.25) {
                            self.pinImageVerticalConstraint.constant = ((labelHeight - self.view.safeAreaInsets.top) * 0.5)
                            self.view.layoutIfNeeded()
                         }
                        if address.thoroughfare != nil && address.thoroughfare != "Unnamed Road"{
                            return
                        }
                        if address.subLocality != nil{
                            return
                        }
                        if address.locality != nil{
                            return
                        }

                    }
                }else{
                    self.addressLabel.text = "Sorry_we_do_not_operate_in_this_location".localized()
                    self.addressLabel2.text = "Sorry_we_do_not_operate_in_this_location".localized()
                    self.txtViewMapIn.text = "Sorry_we_do_not_operate_in_this_location".localized()
                }
            }
          
            return
            
            if let address = response?.firstResult() {
                self.County = (response?.firstResult()?.country)!
                print(self.County)
                if self.County == self.validationCity{
                    if self.isPickUp == true{
                        if appDelegate.selectedAddress != ""{
                            self.addressLabel.text = appDelegate.selectedAddress
                            self.txtViewMapIn.text = appDelegate.selectedAddress
                            appDelegate.selectedAddress = ""
                        }else{
//                            let lines = address.lines! as [String]
//                            let filteredLines = lines.filter({ (line) -> Bool in
//                                return !line.isEmpty
//                            })
//                            print("throught fare ===========   \(address)")
//
//                            currentAddress = filteredLines.joined(separator: ",")
                            currentAddress = self.setUpRemoveUnamedRoad(address)

                            complition(currentAddress)
                            self.addressLabel.text = currentAddress //filteredLines.joined(separator: "\n")
                            self.txtViewMapIn.text = currentAddress //filteredLines.joined(separator: "\n")
                        }
                        
                        let labelHeight = self.addressLabel.intrinsicContentSize.height
                        self.mapView.padding = UIEdgeInsets(top: self.view.safeAreaInsets.top, left: 0,
                                                            bottom: 50, right: 0)
                        
                        UIView.animate(withDuration: 0.25) {
                            self.pinImageVerticalConstraint.constant = ((labelHeight - self.view.safeAreaInsets.top) * 0.5)
                            self.view.layoutIfNeeded()
                        }
                    }else{
                        if appDelegate.selectedAddress != ""{
                            self.addressLabel2.text = appDelegate.selectedAddress
                            self.txtViewMapIn.text = appDelegate.selectedAddress
                            appDelegate.selectedAddress = ""
                        }else{
//                            let lines = address.lines! as [String]
//                            let filteredLines = lines.filter({ (line) -> Bool in
//                                return !line.isEmpty
//                            })
//                            print("throught fare ===========   \(address.thoroughfare ?? "")")
//                            currentAddress = filteredLines.joined(separator: ",")
                            currentAddress = self.setUpRemoveUnamedRoad(address)
                            complition(currentAddress)
                            self.addressLabel2.text = currentAddress//filteredLines.joined(separator: "\n")
                            self.txtViewMapIn.text = currentAddress//filteredLines.joined(separator: "\n")
                        }
                        let labelHeight = self.addressLabel2.intrinsicContentSize.height
                        self.mapView.padding = UIEdgeInsets(top: self.view.safeAreaInsets.top, left: 0,
                                                            bottom: 50, right: 0)
                        
                        UIView.animate(withDuration: 0.25) {
                            self.pinImageVerticalConstraint.constant = ((labelHeight - self.view.safeAreaInsets.top) * 0.5)
                            self.view.layoutIfNeeded()
                        }
                    }
                }else{
                    self.addressLabel.text = "Sorry_we_do_not_operate_in_this_location".localized()
                    self.addressLabel2.text = "Sorry_we_do_not_operate_in_this_location".localized()
                    self.txtViewMapIn.text = "Sorry_we_do_not_operate_in_this_location".localized()
                }
            }
        }
    }
    
    
    func setUpRemoveUnamedRoad(_ address:GMSAddress)->String{
        
        var thruoghFare = ""
        var subLocality = ""
        var locality = ""
        var country = ""
        var currentAddress = ""
        var postalCode = ""
        var adminstrativeArea = ""
        if address.thoroughfare != nil && address.thoroughfare != "Unnamed Road"{
            thruoghFare = address.thoroughfare! + " - "
        }
        if address.subLocality != nil{
            subLocality = address.subLocality! + " - "
        }
        if address.locality != nil{
            locality = address.locality! + " - "
        }
        if  address.country != nil{
            country = address.country!
        }
        if address.locality == nil && address.administrativeArea != nil{
            adminstrativeArea = address.administrativeArea! + " - "
        }
        if address.administrativeArea != nil{
            self.findState = address.administrativeArea!
            adminstrativeArea = address.administrativeArea! + " - "

        }
        if  address.postalCode != nil{
            postalCode = address.postalCode! + " - "
        }
        if address.coordinate != nil{
            Latitude = "\(address.coordinate.latitude)"
            Longitude = "\(address.coordinate.longitude)"
        }

        currentAddress = thruoghFare+locality+subLocality+postalCode+adminstrativeArea+country
        if isPickUp{
            self.addressLabel.text = currentAddress
            self.txtViewMapIn.text = currentAddress
        }else{
            self.addressLabel2.text = currentAddress
            self.txtViewMapIn.text = currentAddress
        }
        return currentAddress
    }
    
    
    func fetchNearbyPlaces(coordinate: CLLocationCoordinate2D) {
        mapView.clear()
        
        dataProvider.fetchPlacesNearCoordinate(coordinate, radius:searchRadius, types: searchedTypes) { places in
            places.forEach {
                let marker = PlaceMarker(place: $0)
                marker.map = self.mapView
            }
        }
    }
    
    @IBAction func refreshPlaces(_ sender: Any) {
        fetchNearbyPlaces(coordinate: mapView.camera.target)
    }
}

//// MARK: - TypesTableViewControllerDelegate
//extension SelectAddress_Vc: TypesTableViewControllerDelegate {
//    func typesController(_ controller: TypesTableViewController, didSelectTypes types: [String]) {
//        searchedTypes = controller.selectedTypes.sorted()
//        dismiss(animated: true)
//        fetchNearbyPlaces(coordinate: mapView.camera.target)
//    }
//}

// MARK: - CLLocationManagerDelegate
extension SelectAddress_Vc: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse else {
            return
        }
        
        locationManager.startUpdatingLocation()
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        
        mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
        locationManager.stopUpdatingLocation()
        //fetchNearbyPlaces(coordinate: location.coordinate)
    }
}

// MARK: - GMSMapViewDelegate
extension SelectAddress_Vc: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        self.nameOfPlaceWithCoordinates(coordinate: position.target) { (address) in
            print (" idle position address   \(address)")
        }
        //reverseGeocodeCoordinate(position.target)
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        txtViewMapIn.isHidden = true
        if isPickUp == true{
            addressLabel.lock()
        }else{
            addressLabel2.lock()
        }
        
        if (gesture) {
            mapCenterPinImage.fadeIn(0.25)
            mapView.selectedMarker = nil
        }
    }
    
//    func mapView(_ mapView: GMSMapView, markerInfoContents marker: GMSMarker) -> UIView? {
//        guard let placeMarker = marker as? PlaceMarker else {
//            return nil
//        }
//        guard let infoView = UIView.viewFromNibName("MarkerInfoView") as? MarkerInfoView else {
//            return nil
//        }
//
//        infoView.nameLabel.text = placeMarker.place.name
//        if let photo = placeMarker.place.photo {
//            infoView.placePhoto.image = photo
//        } else {
//            infoView.placePhoto.image = UIImage()//(named: "generic")
//        }
//
//        return infoView
//    }
    
//    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
//        mapCenterPinImage.fadeOut(0.25)
//        return false
//    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        mapCenterPinImage.fadeIn(0.25)
        mapView.selectedMarker = nil
        return false
    }
}
