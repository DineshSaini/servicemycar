//
//  SelectService_cell.swift
//  ServiceMyCar
//
//  Created by baps on 21/01/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit

class SelectService_cell: UICollectionViewCell {
    
    @IBOutlet weak var viewWidthConstant: NSLayoutConstraint!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var lblPlateNo: UILabel!
    @IBOutlet weak var lblModel: UILabel!
    @IBOutlet weak var lblCarname: UILabel!
    @IBOutlet weak var imgCar: UIImageView!
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var shadowBackView:UIView!
}
