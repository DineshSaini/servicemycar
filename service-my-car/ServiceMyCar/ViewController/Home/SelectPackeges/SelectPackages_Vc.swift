//
//  SelectPackages_Vc.swift
//  ServiceMyCar
//
//  Created by baps on 21/01/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SelectPackages_Vc: UIViewController,UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout,HttpWrapperDelegate {
    
    
    @IBOutlet weak var collectionviewselect: UICollectionView!
    var arrService = ["service_packages_supply_own_parts".localized(),"service_packages_by_original_parts_from_service_my_car".localized(),"service_packages_by_after_market_parts_from_service_my_car".localized()]
    var arrImage = ["car-tools","accident","weel","battery","car-wash","car-option"]
    var objgetPackagesListByCategory = HttpWrapper()
    var isSelectparts = false
    var net:NetworkReachabilityManager = NetworkReachabilityManager()!
    var arrCategorylist:NSMutableArray = NSMutableArray()
    var isType = 2
    let badgeButton : MIBadgeButton = MIBadgeButton(frame:CGRect(x: -10, y: 15, width: 40, height: 20))
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
                let lbNavTitle = UILabel(frame: CGRect(x: CGFloat(35), y:self.view.bounds.size.width/2-235, width: CGFloat(200), height: CGFloat(40)))
                lbNavTitle.textAlignment = .center
        
        if isSelectparts == true
        {
            lbNavTitle.text = "service_packages_service_parts".localized()
        }
        else
        {
             lbNavTitle.text = "service_packages_title".localized()
        }
        
                lbNavTitle.textColor = Colorblack
                lbNavTitle.font = UIFont(name: Regular, size: 16)
        
//        let logo = UIImage(named: "AppTopIcon")
//        let imageView = UIImageView(image:logo)
//        imageView.contentMode = .scaleAspectFit
        // self.navigationItem.titleView = imageView
        self.navigationItem.titleView = lbNavTitle
        self.navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.white
        
        let titleDict: NSDictionary = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [NSAttributedString.Key : Any]
        
        let button = UIButton.init(type: .custom)
        //backarrow , menu
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        
        if languageCode == "en" {
            button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
        }
        else {
            button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
        }
        button.addTarget(self, action:#selector(SideMenu), for: UIControl.Event.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        let buttonLogout = UIButton.init(type: .custom)
        buttonLogout.setImage(UIImage.init(named: "notification"), for: UIControl.State.normal)
        buttonLogout.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        buttonLogout.frame = CGRect.init(x: self.view.frame.size.width-30, y: 0, width: 25, height: 25)
        // buttonLogout.titleLabel!.font =  UIFont(name:FontSemibold, size: 14)
        let barButtonLog = UIBarButtonItem.init(customView: buttonLogout)
       // self.navigationItem.rightBarButtonItem = barButtonLog
        
         getPackagesListByCategory()
        badgeButton.edgeInsetTop = 10
        badgeButton.setImage(UIImage(named : "notification"), for: .normal)
        print(appDelegate.NCount)
         badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        badgeButton.badgeString = appDelegate.NCount
        if appDelegate.NCount == "0"
        {
            badgeButton.badgeBackgroundColor = UIColor.clear
            badgeButton.badgeTextColor = UIColor.clear
        }
        else
        {
            badgeButton.badgeBackgroundColor = colorGreen
            badgeButton.badgeTextColor = UIColor.white
        }
        badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        let barButton2 : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton2
        //        getPackagesListByCategory()
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)
        
    }
    
    // Step 3
    
//    @objc func NotificationAlert()
//    {
//        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
//        
//        self.navigationController?.pushViewController(nextViewController, animated: true)
//        
//    }
//    
    // Step 4
    
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            //self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                
                badgeButton.badgeString = appDelegate.NCount
                if appDelegate.NCount == "0"
                {
                    badgeButton.badgeBackgroundColor = UIColor.clear
                    badgeButton.badgeTextColor = UIColor.clear
                }
                else
                {
                    badgeButton.badgeBackgroundColor = colorGreen
                    badgeButton.badgeTextColor = UIColor.white
                }
            }
        }
    }
    
    // MARK:- goBack
    
    @objc func SideMenu()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    // MARK:- goBack
    
    @objc func NotificationAlert()
    {
        guard let vcs = self.navigationController?.viewControllers else{return}
        let inStack = vcs.contains(where: { (vc) -> Bool in
            return vc is MyNotificationList_Vc
        })
        if inStack{
            self.performSegue(withIdentifier: unwindToNotificationController, sender: self)
        }else{
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        //        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
        //
        //        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    // MARK:- CollectionView Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isSelectparts == true
        {
           return arrService.count
        }
        else
        {
           return arrCategorylist.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if isSelectparts == true
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectService_cell", for: indexPath) as! SelectService_cell
            cell.viewBack.layer.borderColor = Colorblack.cgColor
            cell.viewBack.layer.cornerRadius = 5
            cell.viewBack.layer.borderWidth = 1
            cell.viewWidthConstant.constant = (collectionView.frame.size.width - 20) / 2.0
            cell.lblTitle.text = arrService[indexPath.row]
            cell.imgCar.image = UIImage.init(named: arrImage[indexPath.row])
            return cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectService_cell", for: indexPath) as! SelectService_cell
            cell.viewBack.layer.cornerRadius = 4.0
            cell.viewBack.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
            cell.viewBack.backgroundColor = UIColor.white
            cell.viewBack.layer.shadowOffset = CGSize.zero
            cell.viewBack.layer.shadowOpacity = 2.0
            cell.viewBack.layer.shadowRadius = 3.0
            cell.viewBack.layer.masksToBounds = false
            
            cell.viewWidthConstant.constant = (collectionView.frame.size.width - 20) / 2.0
            if let name = (arrCategorylist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varName") as? String,let Price = (arrCategorylist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varPrice") as? String
            {
                cell.lblTitle.text = name
                cell.lblPrice.text = "\(Price) \("app_currency".localized())"
            }
            
            if let UserProfilePic = (arrCategorylist.object(at: indexPath.row) as! NSDictionary).value(forKey: "icon") as? String
            {
                let url = "\(UserProfilePic)"
                print(url)
                
                if let url2 = URL(string: url) {
                    cell.imgCar.sd_setImage(with: url2, placeholderImage: UIImage(named: "img_plan1"))
                }
            }
            return cell
        }
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if isType == 1 || isType == 2
        {
            isType -= 1
            let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
            let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
            let size:CGFloat = (collectionView.frame.size.width - space) / 2.0
            return CGSize(width: size, height: size)
        }
        else
        {
            isType = 2
            let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
            let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
            let size:CGFloat = (collectionView.frame.size.width - space) / 2.0
            return CGSize(width: (collectionView.frame.size.width - space), height: size)
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if isSelectparts == true
        {
            
        }else
        {
            
            
            if let name = (arrCategorylist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varName") as? String,let Price = (arrCategorylist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varPrice") as? String,let PackageImageUrl = (arrCategorylist.object(at: indexPath.row) as! NSDictionary).value(forKey: "icon") as? String,let Id = (arrCategorylist.object(at: indexPath.row) as! NSDictionary).value(forKey: "intId") as? String
            {
                
                appDelegate.selectedPackagePrice = Float(Price)!
                appDelegate.Bookings.Package.PackageName = name
                appDelegate.Bookings.Package.PackegePrice = Price
                appDelegate.Bookings.Package.PackageImageUrl = PackageImageUrl
                appDelegate.Bookings.Package.Id = Id
                
            }
            
            
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "BasicService_Vc") as! BasicService_Vc
            if let data = arrCategorylist.object(at: indexPath.row) as? NSDictionary
            {
                nextViewController.DataDic = data
            }
            //let nextViewController = objPostJob.instantiateViewController(withIdentifier:"PostYourJob") as! PostYourJob
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        
        
        
    }
    
    // MARK: - getPackagesListByCategory API
    
    func getPackagesListByCategory()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            var languageCode = ""
            if UserDefaults.standard.value(forKey: kAppLanguage) != nil {
                languageCode = UserDefaults.standard.value(forKey: kAppLanguage) as! String
            }
            
            let dictParameters:[String:AnyObject] = ["chrCategory" : "S" as AnyObject,
                                                     "lang" : languageCode as AnyObject]
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objgetPackagesListByCategory = HttpWrapper.init()
            self.objgetPackagesListByCategory.delegate = self
            self.objgetPackagesListByCategory.requestWithparamdictParamPostMethodwithHeaderAndDictGet(url: kgetPackagesListByCategory, dictParams: dictParameters, headers: header)
        }
        
    }
    
    
    // MARK: - HttpWrapperfetchDataSuccess
    
    func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary)
    {
        AppHelper.hideLoadingView()
        print(dicsResponse)
        if wrapper == objgetPackagesListByCategory {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                arrCategorylist.removeAllObjects()
                
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray
                {
                    arrCategorylist = tempNames.mutableCopy() as! NSMutableArray
                }
                print(arrCategorylist)
                collectionviewselect.reloadData()
            }
            else
            {
               // AppHelper.showAlert("Alert", message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }
        
    }
    // MARK: - HttpWrapperfetchDataFail
    
    func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError)
    {
        AppHelper.hideLoadingView()
        print(error.debugDescription)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

