//
//  Profile_Vc.swift
//  ServiceMyCar
//
//  Created by baps on 24/01/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import SJSwiftSideMenuController
import Alamofire
import SwiftyJSON
import Localize_Swift
class Profile_Vc: UIViewController ,HttpWrapperDelegate{

    @IBOutlet weak var btnUpdateProfile: UIButtonX!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPhoneNo: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLanguage:UITextField!
    
    @IBOutlet weak var fristNameView:UIView!
    @IBOutlet weak var lastNameView:UIView!
    @IBOutlet weak var phoneView:UIView!
    @IBOutlet weak var emailView:UIView!
    @IBOutlet weak var languageView:UIView!
    
    
    @IBOutlet weak var logOutButton:UIButton!
    
    var language = ["English","Arabic"]
    var languagePicker = UIPickerView()
    var selectLanguage:String = ""
    var languageCode = ""

    
    let badgeButton : MIBadgeButton = MIBadgeButton(frame:CGRect(x: -10, y: 15, width: 40, height: 20))
    var objUpdateUserProfile = HttpWrapper()
    var objChangeLanguage = HttpWrapper()
    
    var net:NetworkReachabilityManager = NetworkReachabilityManager()!
    
 
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        
        self.setDrawView()
        let button = UIButton.init(type: .custom)
        //backarrow , menu
        
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        if languageCode == "en" {
            button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
            selectLanguage = "English"
            self.txtLanguage.text = "English".localized()
            
        }else {
            button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
            selectLanguage = "Arabic"
            self.txtLanguage.text = "Arabic".localized()
            txtFirstName.textAlignment = .right
            txtLastName.textAlignment = .right
            txtEmail.textAlignment = .right
            txtPhoneNo.textAlignment = .right
            txtLanguage.textAlignment = .right

        }
        
        button.addTarget(self, action:#selector(goBack), for: UIControl.Event.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        badgeButton.edgeInsetTop = 10
        badgeButton.setImage(UIImage(named : "notification"), for: .normal)
        print(appDelegate.NCount)
         badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        badgeButton.badgeString = appDelegate.NCount
        if appDelegate.NCount == "0"{
            badgeButton.badgeBackgroundColor = UIColor.clear
            badgeButton.badgeTextColor = UIColor.clear
        }else{
            badgeButton.badgeBackgroundColor = colorGreen
            badgeButton.badgeTextColor = UIColor.white
        }
        badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        let barButton2 : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton2
        //        getPackagesListByCategory()
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)
        txtFirstName.placeholder = "profile_firstname_placeholder".localized()
        txtLastName.placeholder = "profile_lastname_placeholder".localized()
        txtEmail.placeholder = "profile_phonenumber_placeholder".localized()
        txtPhoneNo.placeholder = "profile_email".localized()
        txtLanguage.placeholder = "Language".localized()
        btnUpdateProfile.setTitle("profile_update_profile".localized(), for: .normal)
        logOutButton.setTitle("menu_logout".localized(), for: .normal)
        self.setupLanguagePicker()
    }
    
    // Step 3
    
    @objc func NotificationAlert()
    {
        guard let vcs = self.navigationController?.viewControllers else{return}
        let inStack = vcs.contains(where: { (vc) -> Bool in
            return vc is MyNotificationList_Vc
        })
        if inStack{
            self.performSegue(withIdentifier: unwindToNotificationController, sender: self)
        }else{
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        //        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
        //
        //        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    // Step 4
    
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            //self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                
                badgeButton.badgeString = appDelegate.NCount
                if appDelegate.NCount == "0"
                {
                    badgeButton.badgeBackgroundColor = UIColor.clear
                    badgeButton.badgeTextColor = UIColor.clear
                }
                else
                {
                    badgeButton.badgeBackgroundColor = colorGreen
                    badgeButton.badgeTextColor = UIColor.white
                }
            }
        }
    }
    
    
    //Mark:- viewDraw
    
    func setDrawView(){
        AppHelper.makeViewCircularWithRespectToHeight(fristNameView, borderColor: .lightGray, borderWidth: 1.0)
        AppHelper.makeViewCircularWithRespectToHeight(lastNameView, borderColor: .lightGray, borderWidth: 1.0)
        AppHelper.makeViewCircularWithRespectToHeight(emailView, borderColor: .lightGray, borderWidth: 1.0)
        AppHelper.makeViewCircularWithRespectToHeight(phoneView, borderColor: .lightGray, borderWidth: 1.0)
        AppHelper.makeViewCircularWithRespectToHeight(languageView, borderColor: .lightGray, borderWidth: 1.0)
        AppHelper.makeViewCircularWithRespectToHeight(btnUpdateProfile, borderColor: .clear, borderWidth: 1.0)
        AppHelper.makeViewCircularWithRespectToHeight(logOutButton, borderColor: .clear, borderWidth: 1.0)
        
         self.txtFirstName.textColor = UIColor.lightGray
        self.txtLastName.textColor = UIColor.lightGray
        self.txtPhoneNo.textColor = UIColor.lightGray
        self.txtEmail.textColor = UIColor.lightGray
        self.txtLanguage.textColor = UIColor.lightGray

            
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
//        navigationController?.navigationBar.clipsToBounds = false
//        self.navigationItem.setHidesBackButton(true, animated:true)
//        self.navigationController?.makeTransparent()

       
        let UserLName = UserDefaults.standard.value(forKey: kUserName) as! String
       if let UserEmail = UserDefaults.standard.value(forKey: KUserLoginEmail) as? String{
         txtEmail.text = UserEmail
        }
        if let UserPhoneNo = UserDefaults.standard.value(forKey: kUserShortLoginPhone) as? String{
           txtPhoneNo.text = UserPhoneNo
        }
        
        let name = UserLName.split(separator: " ")
        txtFirstName.text = "\(name[0])"
        txtLastName.text = "\(name[1])"
        
        self.txtFirstName.isUserInteractionEnabled = false
        self.txtLastName.isUserInteractionEnabled = false
        self.txtEmail.isUserInteractionEnabled = false
        self.txtPhoneNo.isUserInteractionEnabled = false
        
        self.txtFirstName.textColor = UIColor.lightGray
        self.txtLastName.textColor = UIColor.lightGray
        self.txtPhoneNo.textColor = UIColor.lightGray
        self.txtEmail.textColor = UIColor.lightGray
        self.txtLanguage.textColor = UIColor.lightGray
        
        
        

    }
    // MARK:- goBack
    
    @objc func goBack()
    {
    
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - onClickBtnUpdateProfile actions
    
    @IBAction func onClickBtnUpdateProfile(_ sender: Any) {
        UpdateProile()
    }
    
    
    //Mark:- LogOutButton Action
    
    @IBAction func onClickLogOutButton(_ sender:UIButton){
        self.logOutUserFromApp()
    }
    
    
    
    func logOutUserFromApp(){
        let alertController = UIAlertController(title: "app_alert_title".localized(), message: "menu_logout_message".localized(), preferredStyle: .alert)
        let settingsAction = UIAlertAction(title: "app_net_alert_ok".localized(), style: .default) { (_) -> Void in
            let defaults = UserDefaults.standard
            
            defaults.removeObject(forKey: kUserLoginUserId)
            defaults.removeObject(forKey: kUserLoginPhone)
            defaults.removeObject(forKey: kUserName)
            defaults.removeObject(forKey: KUserLoginEmail)
            defaults.removeObject(forKey: klatitude)
            defaults.removeObject(forKey: kLongitude)
            defaults.removeObject(forKey: kUserShortLoginPhone)

            
            defaults.synchronize()
            
            
            
            let newLanguage:Languages!
            self.languageCode = "en"
            UserDefaults.standard.set(self.languageCode, forKey: kAppLanguage)
            if self.languageCode == "en" {
                newLanguage = .en
            }else{
                newLanguage = .ar
            }
            Localize.setCurrentLanguage(self.languageCode)
            LanguageManager.shared.setLanguage(language: newLanguage)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.arrCategorylist.removeAllObjects()
            appDelegate.logout()
        }
        let cancelAction = UIAlertAction(title: "app_alert_cancel".localized(), style: .default, handler: {(alert: UIAlertAction!) in print("Foo")
            //                     self.SwitchNotification.isOn = false
        })
        
        alertController.addAction(cancelAction)
        alertController.addAction(settingsAction)
        
        self.present(alertController, animated: true, completion: {
            print("Foo")
        })
    }
    
    
    // MARK: - UpdateProile API
    
    func UpdateProile()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else if isLoginDataValid()
        {
            let phoneText = "05"+txtPhoneNo.text!
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["userId" : nUserId as AnyObject,
                                                     "firstName" : txtFirstName.text as AnyObject,
                                                     "lastName" : txtLastName.text as AnyObject,
                                                     "varRegisterPhone" : phoneText as AnyObject,
                                                     "varRegisterEmail" : txtEmail.text as AnyObject,
                                                     "varRegisterPassword" : "" as AnyObject]
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objUpdateUserProfile = HttpWrapper.init()
            self.objUpdateUserProfile.delegate = self
            self.objUpdateUserProfile.requestWithparamdictParamPostMethodwithHeader(url: kupdateProfile, dicsParams: dictParameters, headers: header)
        }
        
    }
    
    
    
    func changeUserLanguage(_ language:String){
        if net.isReachable == false{
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["userId" : nUserId as AnyObject,
                                                     "lang" : language as AnyObject]
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objChangeLanguage = HttpWrapper.init()
            self.objChangeLanguage.delegate = self
            self.objChangeLanguage.requestWithparamdictParamPutMethodwithHeader(url: kSetUserLang, dicsParams: dictParameters, headers: header)
        }
        
    }

    
    
    
    
    
    
    // MARK: - HttpWrapperfetchDataSuccess
    
    func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary)
    {
        AppHelper.hideLoadingView()
        print(dicsResponse)
        if wrapper == objUpdateUserProfile {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                let data = dicsResponse.value(forKey: "data") as! NSDictionary
                let json = JSON(data)
                print(json["UserLoginUserId"])
                UserDefaults.standard.setValue("\(json["UserLoginUserId"])", forKey: kUserLoginUserId)
                // UserDefaults.standard.setValue((dicsResponse.value(forKey: "data") as! NSDictionary).value(forKey: "apiToken") as! String, forKey: "apiToken")
                
                UserDefaults.standard.setValue("\(json["UserLoginPhone"])", forKey: kUserLoginPhone)
                
                UserDefaults.standard.setValue("\(json["UserLoginName"])", forKey: kUserName)
                
                UserDefaults.standard.setValue("\(json["UserLoginEmail"])", forKey: KUserLoginEmail)
                
                UserDefaults.standard.setValue("\(json["UserLoginShortPhone"])", forKey: kUserShortLoginPhone)

                UserDefaults.standard.synchronize()
                
                let UserLName = UserDefaults.standard.value(forKey: kUserName) as! String
                let UserEmail = UserDefaults.standard.value(forKey: KUserLoginEmail) as! String
                let UserPhoneNo = UserDefaults.standard.value(forKey: kUserShortLoginPhone) as! String
                print(UserLName)
                let name = UserLName.split(separator: " ")
                 print(name)
                txtFirstName.text = "\(name[0])"
                txtLastName.text = "\(name[1])"
                txtEmail.text = UserEmail
                txtPhoneNo.text = UserPhoneNo
                self.txtFirstName.isUserInteractionEnabled = false
                self.txtLastName.isUserInteractionEnabled = false
                self.txtEmail.isUserInteractionEnabled = false
                self.txtPhoneNo.isUserInteractionEnabled = false
                
                self.txtFirstName.textColor = UIColor.darkGray
                self.txtLastName.textColor = UIColor.darkGray
                self.txtPhoneNo.textColor = UIColor.darkGray
                self.txtEmail.textColor = UIColor.darkGray
                

                showToast()
                

            }
            else{
                AppHelper.showAlert("app_alert_title".localized(), message:dicsResponse.value(forKey: "msg") as! String)
            }
            
        }else if wrapper == objChangeLanguage{
            if dicsResponse.value(forKey: "status") as! NSString == "success"{
                let newLanguage:Languages!
                languageCode = self.selectLanguage == "English" ? "en" : "ar"
                self.txtLanguage.text = selectLanguage.localized()
                UserDefaults.standard.set(languageCode, forKey: kAppLanguage)
                if languageCode == "en" {
                    newLanguage = .en
                }else{
                    newLanguage = .ar
                }
               // self.updateCookies()
                Localize.setCurrentLanguage(languageCode)
                LanguageManager.shared.setLanguage(language: newLanguage)
                
                appDelegate.arrCategorylist.removeAllObjects()
                appDelegate.loginSuccess()
                self.view.endEditing(true)

            }else{
                AppHelper.showAlert("app_alert_title".localized(), message:dicsResponse.value(forKey: "msg") as! String)
            }
        }
        
    }
    // MARK: - HttpWrapperfetchDataFail
    
    func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError){
        AppHelper.hideLoadingView()
        print(error.debugDescription)
    }
    
    
    func showToast(){
        let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
        loadingNotification?.mode = MBProgressHUDModeText
        loadingNotification?.labelText = "Sucsses".localized()
        loadingNotification?.margin = 10;
        loadingNotification?.yOffset = 250;
        
        loadingNotification?.isUserInteractionEnabled = true
        loadingNotification?.hide(true, afterDelay: 1.5)
    }
    // MARK: - isLoginDataValid
    func isLoginDataValid() -> Bool{
        
        if (txtFirstName.text?.trimmingCharacters(in: CharacterSet.whitespaces).count)! <= 0{
            AppHelper.showAlert("app_alert_title".localized(), message: "profile_firstname_validation".localized())
            return false
        }
        
        if (txtLastName.text?.trimmingCharacters(in: CharacterSet.whitespaces).count)! <= 0{
            AppHelper.showAlert("app_alert_title".localized(), message: "profile_lastname_validation".localized())
            return false
        }
        
        
        if (txtPhoneNo.text?.trimmingCharacters(in: CharacterSet.whitespaces).count)! <= 0{
            AppHelper.showAlert("app_alert_title".localized(), message: "profile_phone_number_validation".localized())
            return false
        }
        
        if !AppHelper.isValidEmail(txtEmail.text!){
            AppHelper.showAlert("app_alert_title".localized(), message: "profile_email_validation".localized())
            return false
        }
        return true
    }
    
    @IBAction func tappedOnNameEditButton(_ sender:UIButton){
        self.txtFirstName.isUserInteractionEnabled = true
        self.txtFirstName.textColor = UIColor.black

    }
    
    @IBAction func tappedOnPhoneNumberEditButton(_ sender:UIButton){
        self.txtPhoneNo.isUserInteractionEnabled = true
        self.txtPhoneNo.textColor = UIColor.black


    }
    
    @IBAction func tappedOnEmailAddressEditButton(_ sender:UIButton){
        self.txtEmail.isUserInteractionEnabled = true
        self.txtEmail.textColor = UIColor.black


    }
    
    @IBAction func tappedOnLastNameEditButton(_ sender:UIButton){
        self.txtLastName.isUserInteractionEnabled = true
        self.txtLastName.textColor = UIColor.black

    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension Profile_Vc:UIPickerViewDataSource,UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return language.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return language[row].localized()
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            let lang = language[row]
            self.txtLanguage.text = lang.localized()
            self.selectLanguage = lang
        self.languageCode = (self.selectLanguage == "English") ? "en" : "ar"
    }
    
    
    // Mark:- SetUp language Picker
    
    func setupLanguagePicker(){
        self.languagePicker = UIPickerView()
        self.languagePicker.backgroundColor = UIColor.lightText
        
        //let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.done, target: self, action:#selector(onClickDone(_:)))
        let doneButton = UIBarButtonItem(title: "Done".localized(), style: .plain, target: self, action: #selector(onClickDone(_:)))
        doneButton.tintColor = UIColor.white
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action:nil)
        let cancelButton = UIBarButtonItem(title: "Cancel".localized(), style: .plain, target: self, action: #selector(onClickCancel(_:)))
        //let cancelButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.cancel, target: nil, action: #selector(onClickCancel(_:)))
        cancelButton.tintColor =  UIColor.white
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let array = [cancelButton, spaceButton, doneButton]
        toolbar.setItems(array, animated: true)
        toolbar.barStyle = UIBarStyle.default
        toolbar.barTintColor = colorGreen
        toolbar.tintColor = colorGreen
        self.txtLanguage.inputView = self.languagePicker
        self.txtLanguage.inputAccessoryView = toolbar;
        self.languagePicker.dataSource = self
        self.languagePicker.delegate = self
        
    }
    
    @IBAction func onClickDone(_ sender: UIBarButtonItem){
        self.selectLanguage = language[languagePicker.selectedRow(inComponent: 0)]
//        let newLanguage:Languages!
        languageCode = self.selectLanguage == "English" ? "en" : "ar"
        self.txtLanguage.text = selectLanguage.localized()
        self.changeUserLanguage(languageCode)
//        UserDefaults.standard.set(languageCode, forKey: kAppLanguage)
//        if languageCode == "en" {
//            newLanguage = .en
//        }else{
//            newLanguage = .ar
//        }
//        Localize.setCurrentLanguage(languageCode)
//        LanguageManager.shared.setLanguage(language: newLanguage)
//
//        appDelegate.arrCategorylist.removeAllObjects()
//        appDelegate.loginSuccess()
//        self.view.endEditing(true)
    }
    
    @IBAction func onClickCancel(_ sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
//    func updateCookies() {
//        Alamofire.SessionManager.default.session.configuration.httpCookieStorage?.removeCookies(since: Date(timeIntervalSinceReferenceDate: 1))
//    }
    
    
}
