//
//  ExtraParts_Vc.swift
//  ServiceMyCar
//
//  Created by baps on 16/02/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import ZAlertView
import SJSwiftSideMenuController
class ExtraParts_Vc:UIViewController, UITableViewDataSource , UITableViewDelegate ,HttpWrapperDelegate, SignatureViewDelegate{
    
    @IBOutlet weak var viewSignature: UIView!
    @IBOutlet weak var lblTotalCost: UILabel!
    @IBOutlet weak var lblTotalLabourCost: UILabel!
    @IBOutlet weak var bllTotalPartCost: UILabel!
    @IBOutlet weak var btnClear: UIButton!
    @IBOutlet weak var SignatureView: SignatureView!
    
    var objGetExtraParts = HttpWrapper()
    var objExtrapartsApproval = HttpWrapper()
    var objgetOrderDetailById = HttpWrapper()
    
    var imgsignature:UIImage = UIImage()
    var net:NetworkReachabilityManager = NetworkReachabilityManager()!
    var orderId:String = String()
    var arrExtraPartList:NSMutableArray = NSMutableArray()
    var Data:NSDictionary = NSDictionary()
    var arrSelected:[Int] = [Int]()
     let badgeButton : MIBadgeButton = MIBadgeButton(frame:CGRect(x: -10, y: 15, width: 40, height: 20))
    var arrpartsIdValues:[String] = [String]()
    @IBOutlet weak var tblMain: UITableView!
    
    var order = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SJSwiftSideMenuController.enableSwipeGestureWithMenuSide(menuSide: .NONE)
          SignatureView.delegate = self
        SignatureView.backgroundColor = UIColor.white
        self.navigationController?.isNavigationBarHidden = false
        let lbNavTitle = UILabel(frame: CGRect(x: CGFloat(35), y:self.view.bounds.size.width/2-235, width: CGFloat(200), height: CGFloat(40)))
        lbNavTitle.textAlignment = .center
        lbNavTitle.text = "Extra Parts"
        lbNavTitle.textColor = Colorblack
        lbNavTitle.font = UIFont(name: Regular, size: 16)
        
        //        let logo = UIImage(named: "AppTopIcon")
        //        let imageView = UIImageView(image:logo)
        //        imageView.contentMode = .scaleAspectFit
        //        self.navigationItem.titleView = imageView
        self.navigationItem.titleView = lbNavTitle
        self.navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.white
        
        let titleDict: NSDictionary = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [NSAttributedString.Key : Any]
        
        let button = UIButton.init(type: .custom)
        //backarrow , menu
      
        
        if appDelegate.isFromNotification == true
        {
            let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
            
            if languageCode == "en" {
                button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
            }
            else {
                button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
            }
        }
        else
        {
            let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
            
            if languageCode == "en" {
                button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
            }
            else {
                button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
            }
        }
        
        button.addTarget(self, action:#selector(SideMenu), for: UIControl.Event.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        //        self.tblPage1.register(HistoryDatatable_Cell.self, forCellReuseIdentifier: "HistoryDatatable_Cell")
        //        self.tblPage2.register(HistoryDatatable_Cell.self, forCellReuseIdentifier: "HistoryDatatable_Cell")
        
        
       
        badgeButton.edgeInsetTop = 10
        badgeButton.setImage(UIImage(named : "notification"), for: .normal)
        print(appDelegate.NCount)
         badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        badgeButton.badgeString = appDelegate.NCount
        
        if appDelegate.NCount == "0"
        {
            badgeButton.badgeBackgroundColor = UIColor.clear
            badgeButton.badgeTextColor = UIColor.clear
        }
        else
        {
            badgeButton.badgeBackgroundColor = colorGreen
            badgeButton.badgeTextColor = UIColor.white
        }
        badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        let barButton2 : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton2
         viewSignature.isHidden = true
        getUserExtraparts()
        getOrderDetailById()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)
    }
    
    
    // MARK: - Google Api Calling
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            //self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                
                badgeButton.badgeString = appDelegate.NCount
                if appDelegate.NCount == "0"
                {
                    badgeButton.badgeBackgroundColor = UIColor.clear
                    badgeButton.badgeTextColor = UIColor.clear
                }
                else
                {
                    badgeButton.badgeBackgroundColor = colorGreen
                    badgeButton.badgeTextColor = UIColor.white
                }
            }
        }
    }
    
    // MARK:- goBack
    
    @objc func SideMenu()
    {
//        SJSwiftSideMenuController.enableSwipeGestureWithMenuSide(menuSide: .RIGHT)
//        if appDelegate.isFromNotification == true
//        {
//            appDelegate.loginSuccess()
//        }
//        else
//        {
            self.navigationController?.popViewController(animated: true)
       // }
    }
    
    @IBAction func onClickBtnCloseSingatureView(_ sender: Any) {
        
         viewSignature.isHidden = true
        
    }
    @objc func NotificationAlert()
    {
        guard let vcs = self.navigationController?.viewControllers else{return}
        let inStack = vcs.contains(where: { (vc) -> Bool in
            return vc is MyNotificationList_Vc
        })
        if inStack{
            self.performSegue(withIdentifier: unwindToNotificationController, sender: self)
        }else{
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
//        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
//        
//        self.navigationController?.pushViewController(nextViewController, animated: true)

    }
    
     // MARK: - onClickBtnUploadData
    @IBAction func onClickBtnUploadData(_ sender: Any) {
        
        SignatureView.captureSignature()
        
    }
    
     // MARK: - onclickbtnClear
    @IBAction func onclickbtnClear(_ sender: Any) {
        
        SignatureView.clearCanvas()
    }
    
    // MARK: - onClickSignAndSend
    @IBAction func onClickSignAndSend(_ sender:UIButton!) {
        
        if arrSelected.contains(0)
        {
           viewSignature.isHidden = true
            ShowAlert(Title: "Alert", Msg: "You must have to select all parts!")
        }
        else
        {
            var partsCost = Float()
            var labourCost = Float()
            arrpartsIdValues.removeAll()
            for (index, element) in arrSelected.enumerated() {
                
                if element == 1
                {
                    if let name = (arrExtraPartList.object(at: index) as! NSDictionary).value(forKey: "varPrice") as? String
                    {
                        partsCost = partsCost + Float(name)!
                        
                    }
                    var FintLabourHours:Float?
                    var FvarTime:Float?
                    if let intLabourHours = (arrExtraPartList.object(at: index) as! NSDictionary).value(forKey: "intLabourHours") as? NSString
                    {
                        FintLabourHours = Float(intLabourHours as String)
                    }
                    else  if let intLabourHours = (arrExtraPartList.object(at: index) as! NSDictionary).value(forKey: "intLabourHours") as? NSNumber
                    {
                        FintLabourHours = Float(truncating: intLabourHours)
                    }
                    
                    if let paidAmount = (arrExtraPartList.object(at: index) as! NSDictionary).value(forKey: "varTime") as? NSString
                    {
                        FvarTime = Float(paidAmount as String)
                        
                    }
                    else  if let paidAmount = (arrExtraPartList.object(at: index) as! NSDictionary).value(forKey: "varTime") as? NSNumber
                    {
                        FvarTime = Float(truncating: paidAmount)
                        
                        
                    }
                    let value = (Float(FintLabourHours!) * Float(FvarTime!))
                    
                    labourCost = labourCost + value
                   // cell.lblPrice.text  = "Labour Price : \(Float(FintLabourHours!) * Float(FvarTime!)) AED"
                    
                    if let name = (arrExtraPartList.object(at: index) as! NSDictionary).value(forKey: "intId") as? String
                    {
                        arrpartsIdValues.append("\(name)||Y")
                        
                    }
                    else if let name = (arrExtraPartList.object(at: index) as! NSDictionary).value(forKey: "intId") as? NSNumber
                    {
                        arrpartsIdValues.append("\(name)||Y")
                        
                    }
                    
                }
                else
                {
                    if let name = (arrExtraPartList.object(at: index) as! NSDictionary).value(forKey: "intId") as? String
                    {
                        arrpartsIdValues.append("\(name)||R")
                        
                    }
                    else if let name = (arrExtraPartList.object(at: index) as! NSDictionary).value(forKey: "intId") as? NSNumber
                    {
                        arrpartsIdValues.append("\(name)||R")
                        
                    }
                }
                
                bllTotalPartCost.text = "Total Parts Cost : \(partsCost) AED"
                lblTotalLabourCost.text = "Total Labour Cost : \(labourCost) AED"
                lblTotalCost.text = "Total Cost : \(partsCost + labourCost) AED"
                // do something useful
            }
            viewSignature.isHidden = false
        }
        
    }
    
    // MARK: - onClickPayOrPaid
    @IBAction func onClickAccept(_ sender:UIButton!) {
        arrSelected[sender.tag] = 1
        tblMain.reloadData()
    }
    // MARK: - onClickView
    @IBAction func onClickDecline(_ sender:UIButton!) {
        arrSelected[sender.tag] = 2
        tblMain.reloadData()
        // do cool stuff here
        
//        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "webview_Vc") as! webview_Vc
//        if let varCar = (arrExtraPartList.object(at: sender.tag) as! NSDictionary).value(forKey: "varCar") as? String ,let varModel = (arrExtraPartList.object(at: sender.tag) as! NSDictionary).value(forKey: "varModel") as? String
//        {
//            nextViewController.strName  = varCar + " " + varModel
//        }
//
//        if let name = (arrExtraPartList.object(at: sender.tag) as! NSDictionary).value(forKey: "invoicePdf") as? String
//        {
//            if name == ""
//            {
//                AppHelper.showAlert("ServiceMyCar", message: "No PDF")
//            }
//            else
//            {
//                nextViewController.URL  = name
//                self.navigationController?.pushViewController(nextViewController, animated: true)
//            }
//
//        }
        
        
    }
    
    
    // MARK: - tableview Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print(Data)
         print(section)
        if section == 0
        {
           return 1
        }
        else if section == 1
        {
            return arrExtraPartList.count
        }
        else
        {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryDatatable_Cell" , for: indexPath) as! HistoryDatatable_Cell
            
            cell.viewBack.backgroundColor = .clear
            if let name = Data.value(forKey: "varPackageDate") as? String
            {
                cell.lblDate.text  = name
            }

            if let plateCode = Data.value(forKey: "plateCode") as? String
            {
                cell.lblPlatecode.text  = plateCode
            }

            if let name = Data.value(forKey: "plateNumber") as? String
            {
                cell.lblYear.text  = name
            }
            cell.btnView.tag = indexPath.row
            cell.btnView.setTitle("Sign & Send", for: .normal)
            cell.btnView.addTarget(self, action: #selector(self.onClickSignAndSend(_:)), for: .touchUpInside)


            if let varCar = Data.value(forKey: "varCar") as? String ,let varModel = Data.value(forKey: "varModel") as? String
            {
                cell.lblCarName.text  = varCar + " " + varModel
            }
            
            
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Myinvoice_Cell" , for: indexPath) as! Myinvoice_Cell
            
            
            
            if let name = (arrExtraPartList.object(at: indexPath.row) as! NSDictionary).value(forKey: "varName") as? String
            {
                cell.lblDate.text  = "Part : \(name)"
                
            }
            
            if let name = (arrExtraPartList.object(at: indexPath.row) as! NSDictionary).value(forKey: "lineTotal") as? NSNumber
            {
                cell.lblTotalPrice.text  = "Total Price : \(name) AED"
            }
            
            
            var FintLabourHours:Float?
            var FvarTime:Float?
            if let intLabourHours = (arrExtraPartList.object(at: indexPath.row) as! NSDictionary).value(forKey: "intLabourHours") as? NSString
            {
                FintLabourHours = Float(intLabourHours as String)
            }
            else  if let intLabourHours = (arrExtraPartList.object(at: indexPath.row) as! NSDictionary).value(forKey: "intLabourHours") as? NSNumber
            {
                FintLabourHours = Float(truncating: intLabourHours)
            }
            
            if let paidAmount = (arrExtraPartList.object(at: indexPath.row) as! NSDictionary).value(forKey: "varTime") as? NSString
            {
                FvarTime = Float(paidAmount as String)
               
            }
            else  if let paidAmount = (arrExtraPartList.object(at: indexPath.row) as! NSDictionary).value(forKey: "varTime") as? NSNumber
            {
                FvarTime = Float(truncating: paidAmount)
               
                
            }
            
            cell.lblPrice.text  = "Labour Price : \(Float(FintLabourHours!) * Float(FvarTime!)) AED"
            
            //Accept
            if arrSelected[indexPath.row] == 1
            {
                cell.btnPay.backgroundColor = colorGreen
                cell.btnPay.setTitle("Accepted", for: .normal)
                
                cell.btnView.backgroundColor = coloLightPink
                cell.btnView.setTitle("Decline", for: .normal)
            }//Decline
            else if arrSelected[indexPath.row] == 2
            {
                cell.btnPay.backgroundColor = colorGreen
                cell.btnPay.setTitle("Accept", for: .normal)
                
                cell.btnView.backgroundColor = colorRed
                cell.btnView.setTitle("Declined", for: .normal)
            }//Non of selected
            else
            {
                cell.btnPay.backgroundColor = colorGreen
                cell.btnPay.setTitle("Accept", for: .normal)
                
                cell.btnView.backgroundColor = colorRed
                cell.btnView.setTitle("Decline", for: .normal)
            }
            
            cell.btnView.tag = indexPath.row
             cell.btnPay.tag = indexPath.row
            cell.btnView.addTarget(self, action: #selector(self.onClickDecline(_:)), for: .touchUpInside)
            cell.btnPay.addTarget(self, action: #selector(self.onClickAccept(_:)), for: .touchUpInside)
            
            if let varPrice = (arrExtraPartList.object(at: indexPath.row) as! NSDictionary).value(forKey: "varPrice") as? String
            {
                cell.lblCarName.text  = "Part Price : \(varPrice) AED"
            }
            
            
            return cell
        }
        
    }
    
    // MARK: - getOrderDetailById API
    
    func getOrderDetailById()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["orderId" : orderId as AnyObject]
           
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objgetOrderDetailById = HttpWrapper.init()
            self.objgetOrderDetailById.delegate = self
            self.objgetOrderDetailById.requestWithparamdictParamMethodwithHeaderAndDictGet(url:kgetOrderDetailById , dictParams: dictParameters, headers: header)
            //self.objgetOrderDetailById.requestWithparamdictParamPostMethod(url: kgetOrderDetailById, dicsParams: dictParameters)
        }
        
    }
    // MARK: - getUserExtraparts API
    
    func getUserExtraparts()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["orderId" : orderId as AnyObject]
           
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objGetExtraParts = HttpWrapper.init()
            self.objGetExtraParts.delegate = self
            self.objGetExtraParts.requestWithparamdictParamMethodwithHeaderAndDictGet(url: kgetExtraParts, dictParams: dictParameters, headers: header)
            //self.objGetExtraParts.requestWithparamdictParamPostMethod(url: kgetExtraParts, dicsParams: dictParameters)
        }
        
    }
    
    // MARK: - submitExtraPartsApproval API
    
    func submitExtraPartsApproval()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
           
            let PartValue = arrpartsIdValues.joined(separator: ",")
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["orderId" : orderId as AnyObject,
                                                     "type" : "extraParts" as AnyObject,
                                                     "partsIdValues" : PartValue as AnyObject,
                                                     "signature" : imgsignature as AnyObject]
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objExtrapartsApproval = HttpWrapper.init()
            self.objExtrapartsApproval.delegate = self
            self.objExtrapartsApproval.requestWithparamdictParamPostMethodwithHeader(url: ksubmitExtraPartsApproval, dicsParams: dictParameters, headers: header)
            //self.objExtrapartsApproval.requestMultipartFormDataWithImageAndVideo(ksubmitExtraPartsApproval, dicsParams: dictParameters, dictHeader: nil)
        }
        
    }
    
    // MARK: - HttpWrapperfetchDataSuccess
    
    func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary)
    {
        AppHelper.hideLoadingView()
        print(dicsResponse)
        if wrapper == objGetExtraParts {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                arrExtraPartList.removeAllObjects()
                
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray
                {
                    arrExtraPartList = tempNames.mutableCopy() as! NSMutableArray
                }
                print(arrExtraPartList)
                
                for i in arrExtraPartList
                {
                    arrSelected.append(0)
                }
                tblMain.reloadData()
            }
            else
            {
                tblMain.isHidden = true
                
               // AppHelper.showAlert("Alert", message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }
        else if wrapper == objExtrapartsApproval {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                viewSignature.isHidden = true
                SJSwiftSideMenuController.enableSwipeGestureWithMenuSide(menuSide: .RIGHT)
                if appDelegate.isFromNotification == true
                {
                    appDelegate.loginSuccess()
                }
                else
                {
                    self.navigationController?.popViewController(animated: true)
                }
//                arrExtraPartList.removeAllObjects()
//
//                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray
//                {
//                    arrExtraPartList = tempNames.mutableCopy() as! NSMutableArray
//                }
//                print(arrExtraPartList)
//
//                for i in arrExtraPartList
//                {
//                    arrSelected.append(0)
//                }
//                tblMain.reloadData()
            }
            else
            {
                tblMain.isHidden = true
                
               // AppHelper.showAlert("Alert", message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }
        else if wrapper == objgetOrderDetailById {
            if dicsResponse.value(forKey: "status") as? NSString == "success"
            {
                
                               // arrExtraPartList.removeAllObjects()
                
                                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray
                                {
                                    Data = tempNames.object(at: 0) as! NSDictionary
                                }
                              tblMain.reloadData()
                
                //                tblMain.reloadData()
            }
            else
            {
                tblMain.isHidden = true
                
                // AppHelper.showAlert("Alert", message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }
        
        
    }
    // MARK: - HttpWrapperfetchDataFail
    
    func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError)
    {
        AppHelper.hideLoadingView()
        print(error.debugDescription)
    }
    
    // MARK: - Show Toast
    func ShowAlert(Title:String,Msg:String)
    {
        //        ZAlertView.positiveColor            = UIColor.color("#669999")
        //        ZAlertView.negativeColor            = UIColor.color("#CC3333")
        //        ZAlertView.blurredBackground        = true
        ZAlertView.showAnimation            = .bounceBottom
        ZAlertView.hideAnimation            = .bounceRight
        ZAlertView.initialSpringVelocity    = 0.9
        ZAlertView.duration                 = 2
        ZAlertView.titleColor = colorGreen
        
        let dialog = ZAlertView(title: Title, message: Msg, closeButtonText: "OK", closeButtonHandler: { (alertView) -> () in
            alertView.dismissAlertView()
        })
        
        dialog.width = self.view.frame.width - 40
        dialog.show()
        dialog.allowTouchOutsideToDismiss = true
    }
    
    func SignatureViewDidCaptureSignature(view: SignatureView, signature: Signature?) {
        if signature != nil {
            print(signature!)
            imgsignature = (signature?.image)!
            SignatureView.clearCanvas()
            submitExtraPartsApproval()
        } else {
            if SignatureView.signaturePresent == false {
                print("Signature is blank")
            } else {
                print("Failed to Capture Signature")
            }
        }
    }
    
    func SignatureViewDidBeginDrawing(view: SignatureView) {
        print("Began drawing Signature")
    }
    
    func SignatureViewIsDrawing(view: SignatureView) {
        print("Is drawing Signature")
    }
    
    func SignatureViewDidFinishDrawing(view: SignatureView) {
        print("Did finish drawing Signature")
    }
    
    func SignatureViewDidCancelDrawing(view: SignatureView) {
        print("Did cancel drawing signature")
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
