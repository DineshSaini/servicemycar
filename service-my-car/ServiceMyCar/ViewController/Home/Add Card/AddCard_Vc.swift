//
//  AddCard_Vc.swift
//  ServiceMyCar
//
//  Created by baps on 14/02/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class AddCard_Vc: UIViewController,HttpWrapperDelegate ,UITextFieldDelegate , UIPickerViewDelegate , UIPickerViewDataSource{

    @IBOutlet weak var btnSave: UIButtonX!
    @IBOutlet weak var btnDebitCard: UIButtonX!
    @IBOutlet weak var btnCreditCard: UIButtonX!
    @IBOutlet weak var txtCountyRegion: UITextField!
    @IBOutlet weak var txtCardHolderName: UITextField!
    @IBOutlet weak var btnMonth: UIButton!
    @IBOutlet weak var txtYear: UITextField!
    @IBOutlet weak var txtMonth: UITextField!
    @IBOutlet weak var txtCardNumber: UITextField!
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var lblYear: UILabel!
    
    var arrMonth = [1,2,3,4,5,6,7,8,9,10,11,12]
    var objAddCard = HttpWrapper()
    var net:NetworkReachabilityManager = NetworkReachabilityManager()!
    var myPickerView : UIPickerView!
    var strcardtype = ""
    var arrYear:[Int] = [Int]()
    let badgeButton : MIBadgeButton = MIBadgeButton(frame:CGRect(x: -10, y: 15, width: 40, height: 20))
    override func viewDidLoad() {
        txtCardNumber.delegate = self
        txtMonth.delegate = self
        txtYear.delegate = self
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false

        let lbNavTitle = UILabel(frame: CGRect(x: CGFloat(35), y:self.view.bounds.size.width/2-235, width: CGFloat(200), height: CGFloat(40)))
        lbNavTitle.textAlignment = .center
        lbNavTitle.text = "add_card_title".localized()
        lbNavTitle.textColor = Colorblack
        lbNavTitle.font = UIFont(name: Regular, size: 16)
        
        let logo = UIImage(named: "AppTopIcon")
        let imageView = UIImageView(image:logo)
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
        
        //self.navigationItem.titleView = lbNavTitle
        self.navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.white
        
        let titleDict: NSDictionary = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [NSAttributedString.Key : Any]
        
        let button = UIButton.init(type: .custom)
        //backarrow , menu
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        
        if languageCode == "en" {
            button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
        }
        else {
            button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
        }
        button.addTarget(self, action:#selector(SideMenu), for: UIControl.Event.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        //        let buttonLogout = UIButton.init(type: .custom)
        //        buttonLogout.setImage(UIImage.init(named: "notification"), for: UIControl.State.normal)
        //        buttonLogout.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        //        buttonLogout.frame = CGRect.init(x: self.view.frame.size.width-30, y: 0, width: 25, height: 25)
        //        // buttonLogout.titleLabel!.font =  UIFont(name:FontSemibold, size: 14)
        //        let barButtonLog = UIBarButtonItem.init(customView: buttonLogout)
        //        // self.navigationItem.rightBarButtonItem = barButtonLog
        //
        
        
        //        if let value = DataDic.value(forKey: "varSpecification") as? NSArray
        //        {
        //            arrService = value.mutableCopy() as! NSMutableArray
        //        }
        
        btnDebitCard.backgroundColor = UIColor.white
        btnDebitCard.borderColor = UIColor.black
        btnDebitCard.cornerRadius = 5
        btnDebitCard.setTitleColor(UIColor.black, for: .normal)
        btnDebitCard.borderWidth = 1
        
        btnCreditCard.backgroundColor = UIColor.white
        btnCreditCard.borderColor = UIColor.black
        btnCreditCard.cornerRadius = 5
        btnCreditCard.setTitleColor(UIColor.black, for: .normal)
        btnCreditCard.borderWidth = 1
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy"
        let dateStr = formatter.string(from: NSDate() as Date)
        print(dateStr)
        var currentYear = Int(dateStr)
        for i in 0...50
        {
            arrYear.append(currentYear!)
            
            currentYear = currentYear! + 1
        }
        
        print(arrYear)
        badgeButton.edgeInsetTop = 10
        badgeButton.setImage(UIImage(named : "notification"), for: .normal)
        print(appDelegate.NCount)
         badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        badgeButton.badgeString = appDelegate.NCount
        if appDelegate.NCount == "0"
        {
            badgeButton.badgeBackgroundColor = UIColor.clear
            badgeButton.badgeTextColor = UIColor.clear
        }
        else
        {
            badgeButton.badgeBackgroundColor = colorGreen
            badgeButton.badgeTextColor = UIColor.white
        }
        badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        let barButton2 : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton2
//        getPackagesListByCategory()
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)
        
        txtCardNumber.placeholder = "add_card_number_placeholder".localized()
        txtYear.placeholder = "add_card_year_placeholder".localized()
        txtMonth.placeholder = "add_card_month_placeholder".localized()
        txtCardHolderName.placeholder = "add_card_holder_name_placeholder".localized()
        txtCountyRegion.placeholder = "add_card_country_region_placeholder".localized()
        btnCreditCard.setTitle("add_card_debit_card".localized(), for: .normal)
        btnDebitCard.setTitle("add_card_credit_card".localized(), for: .normal)
        btnSave.setTitle("add_card_save".localized(), for: .normal)
        lblYear.text = "add_card_year_placeholder".localized()
        lblMonth.text = "add_card_month_placeholder".localized()
    }
    
    
    
    // Step 3
    
    @objc func NotificationAlert()
    {
        guard let vcs = self.navigationController?.viewControllers else{return}
        let inStack = vcs.contains(where: { (vc) -> Bool in
            return vc is MyNotificationList_Vc
        })
        if inStack{
            self.performSegue(withIdentifier: unwindToNotificationController, sender: self)
        }else{
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
//        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
//        
//        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    // Step 4
    
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            //self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                
                badgeButton.badgeString = appDelegate.NCount
                if appDelegate.NCount == "0"
                {
                    badgeButton.badgeBackgroundColor = UIColor.clear
                    badgeButton.badgeTextColor = UIColor.clear
                }
                else
                {
                    badgeButton.badgeBackgroundColor = colorGreen
                    badgeButton.badgeTextColor = UIColor.white
                }
            }
        }
    }
    
    // MARK:- goBack
    
    @objc func SideMenu()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - onClickBtnCreditCard
    @IBAction func onClickBtnCreditCard(_ sender: UIButton) {
        
        btnDebitCard.backgroundColor = UIColor.white
        btnDebitCard.borderColor = UIColor.black
        btnDebitCard.cornerRadius = 5
        btnDebitCard.setTitleColor(UIColor.black, for: .normal)
        btnDebitCard.borderWidth = 1
        
        btnCreditCard.backgroundColor = colorGreen
        btnCreditCard.borderColor = UIColor.black
        btnCreditCard.cornerRadius = 5
        btnCreditCard.setTitleColor(UIColor.white, for: .normal)
        btnCreditCard.borderWidth = 1
        strcardtype = "credit"
    }
    
    // MARK: - onClickBtnDebitCard
    @IBAction func onClickBtnDebitCard(_ sender: UIButton) {
        btnDebitCard.backgroundColor = colorGreen
        btnDebitCard.borderColor = UIColor.black
        btnDebitCard.cornerRadius = 5
        btnDebitCard.setTitleColor(UIColor.white, for: .normal)
        
        btnCreditCard.backgroundColor = UIColor.white
        btnCreditCard.borderColor = UIColor.black
        btnCreditCard.cornerRadius = 5
        btnCreditCard.setTitleColor(UIColor.black, for: .normal)
        strcardtype = "debit"
    }
    
    // MARK: - onClickBtnSave
    @IBAction func onClickBtnSave(_ sender: UIButton) {
        AddCard()
    }
    
    
    func pickUp(_ textField : UITextField){
        
        // UIPickerView
        self.myPickerView = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.myPickerView.delegate = self
        self.myPickerView.dataSource = self
        self.myPickerView.backgroundColor = UIColor.white
        
        textField.inputView = self.myPickerView
        myPickerView.reloadAllComponents()
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .black
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.white
        toolBar.sizeToFit()
        toolBar.backgroundColor = colorGreen
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "add_card_done".localized(), style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "add_card_cancel".localized(), style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    @objc func doneClick() {
        txtMonth.resignFirstResponder()
       
        txtYear.resignFirstResponder()
       
    }
    @objc func cancelClick() {
        txtMonth.resignFirstResponder()
        
        txtYear.resignFirstResponder()
    }
    
    
    //MARK:- PickerView Delegate & DataSource
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1
        {
            return arrMonth.count
        }
        else if pickerView.tag == 2
        {
            return arrYear.count
        }
        else
        {
            return 0
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label: UILabel
        
        if let view = view {
            label = view as! UILabel
        }
        else {
            label = UILabel(frame: CGRect(x: 0, y: 0, width: pickerView.frame.width, height: 400))
        }
//
        label.lineBreakMode = .byWordWrapping;
        label.textAlignment = .center
        if pickerView.tag == 1
        {
            label.text = "\(arrMonth[row])"
        }
        else if pickerView.tag == 2
        {
            label.text = "\(arrYear[row])"
        }
//        else
//        {
//            label.text = "tet"
//        }
//        label.numberOfLines = 0;
//        //label.text = arr[row]
//        label.sizeToFit()
        return label;
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1
        {
            self.txtMonth.text = "\(arrMonth[row])"
        }
        else  if pickerView.tag == 2
        {
            self.txtYear.text = "\(arrYear[row])"
        }
//        else  if pickerView.tag == 3
//        {
//            self.txtYear.text = "\(arrCarYears.object(at: row) as! NSNumber)"
//        }
//        else  if pickerView.tag == 4
//        {
//            self.txtEngineCC.text = (arrCarEngine.object(at: row) as! NSDictionary).value(forKey: "trimengine") as? String
//        }
//        else  if pickerView.tag == 5
//        {
//            self.txtSelectCar.text = (arrCarlist.object(at: row) as! NSDictionary).value(forKey: "varModel") as? String
//            SelectedCarIndex = row
//        }
//        else
//        {
//            // self.txtCarType.text = arrCarType[row]
//
//        }
//
        
        
        // strCountryCode = (arrStores.object(at: row) as! NSDictionary).value(forKey: "cLocationName") as? String
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtMonth
        {
            self.pickUp(txtMonth)
            
            myPickerView.tag = 1
        }
        else if textField == txtYear
            {
                self.pickUp(txtYear)
                
                myPickerView.tag = 2
            }
       
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let maxLength = 16
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    // MARK: - AddCard
    func AddCard()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else if isLoginDataValid()
        {
            
             let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["intUserId" : nUserId as AnyObject,
                                                     "varCardNumber" : self.txtCardNumber.text as AnyObject,
                                                     "varExpYear" : self.txtYear.text as AnyObject,
                                                     "varExpMonth" : self.txtMonth.text as AnyObject,
                                                     "varCardHolderName" : self.txtCardHolderName.text as AnyObject,
                                                     "varCardType" : strcardtype as AnyObject]
            //            let dictHeaaderPera:[String:AnyObject] = [
            //                "platform" : "IOS" as AnyObject,
            //                "modelNumber" : "ML2000" as AnyObject
            //                ,"serialNumber" :"1234567891" as AnyObject,
            //                 "deviceId" : "12345" as AnyObject]
            //
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objAddCard = HttpWrapper.init()
            self.objAddCard.delegate = self
            self.objAddCard.requestWithparamdictParamPostMethod(url: kaddUserCard, dicsParams: dictParameters)
        }
        
    }
    
    // MARK: - HttpWrapperfetchDataSuccess
    
    func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary)
    {
        AppHelper.hideLoadingView()
        print(dicsResponse)
        if wrapper == objAddCard {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                
                
                //                UserDefaults.standard.setValue("\(json["nUserId"])", forKey: knUserId)
                //                // UserDefaults.standard.setValue((dicsResponse.value(forKey: "data") as! NSDictionary).value(forKey: "apiToken") as! String, forKey: "apiToken")
                //
                //                UserDefaults.standard.setValue("\(json["nLoginId"])", forKey: knLoginId)
                //
                //                UserDefaults.standard.setValue("\(json["cCustomerName"])", forKey: kcCustomerName)
                //
                //                UserDefaults.standard.setValue("\(json["nCustomerId"])", forKey: knCustomerId)
                //
                //                UserDefaults.standard.setValue("\(json["cEmail"])", forKey: kcEmail)
                //
                //                UserDefaults.standard.setValue("\(json["cCustomerImage"])", forKey: kcCustomerImage)
                //
                //                UserDefaults.standard.setValue("\(json["cToken"])", forKey: kcToken)
                //
//                let nextViewController = objLogin_SignUpSB.instantiateViewController(withIdentifier: "VarifyMobile_Vc") as! VarifyMobile_Vc
//                nextViewController.strvarRegisterPhone = txtPhoneNo.text!
//                nextViewController.isFromRegister = true
//                //let nextViewController = objPostJob.instantiateViewController(withIdentifier:"PostYourJob") as! PostYourJob
//                nextViewController.navigationController?.isNavigationBarHidden = false
//                self.navigationController?.pushViewController(nextViewController, animated: true)
//                UserDefaults.standard.synchronize()
                
                self.navigationController?.popViewController(animated: true)
                // appDelegate.loginSuccess()
                
            }
            else
            {
                AppHelper.showAlert("app_alert_title".localized(), message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }
        
    }
    // MARK: - HttpWrapperfetchDataFail
    
    func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError)
    {
        AppHelper.hideLoadingView()
        print(error.debugDescription)
    }
    // MARK: - isLoginDataValid
    func isLoginDataValid() -> Bool
    {
        
        
        if (txtCardNumber.text?.trimmingCharacters(in: CharacterSet.whitespaces).characters.count)! <= 0
        {
            AppHelper.showAlert("app_alert_title".localized(), message: "add_card_card_number_validation".localized())
            return false
        }
        
       
        
        if (txtMonth.text?.trimmingCharacters(in: CharacterSet.whitespaces).characters.count)! <= 0
        {
            AppHelper.showAlert("app_alert_title".localized(), message: "add_card_month_validation".localized())
            return false
        }
        
        
        if (txtYear.text?.trimmingCharacters(in: CharacterSet.whitespaces).characters.count)! <= 0
        {
            AppHelper.showAlert("app_alert_title".localized(), message: "add_card_year_validation".localized())
            return false
        }
        
        if (txtCardHolderName.text?.trimmingCharacters(in: CharacterSet.whitespaces).characters.count)! <= 0
        {
            AppHelper.showAlert("app_alert_title".localized(), message: "add_card_holder_name_validation".localized())
            return false
        }
        
        if (txtCountyRegion.text?.trimmingCharacters(in: CharacterSet.whitespaces).characters.count)! <= 0
        {
            AppHelper.showAlert("app_alert_title".localized(), message: "add_card_country_validation".localized())
            return false
        }
       
        
        
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
