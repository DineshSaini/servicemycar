//
//  AddAddress_Vc.swift
//  ServiceMyCar
//
//  Created by baps on 25/01/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import DropDown
import Alamofire
import SwiftyJSON
import GoogleMaps
import GooglePlaces



protocol AddAddress_VcDelegate {
    func getCollectionPointAddress(_ viewController:UIViewController, collectAddress address:String, newAddress:NSDictionary)
    func getDropPointAddress(_ viewController:UIViewController, dropAddress address:String, newAddress:NSDictionary)
    func getAddAddress(_ viewController:UIViewController, address isAdded:Bool)

}

class AddAddress_Vc: UIViewController,UITextFieldDelegate,HttpWrapperDelegate {

    @IBOutlet weak var btnNext: UIButtonX!
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var txtSelectState: UITextField!
    @IBOutlet weak var txtAdditionaltext: UITextField!
    @IBOutlet weak var txtBuildingNameNO: UITextField!
    @IBOutlet weak var txtPlaceHere: UITextField!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var selectAddressButton:UIButton!
    @IBOutlet weak var placeHereView:UIView!
    @IBOutlet weak var countryView:UIView!
    @IBOutlet weak var selectStateView:UIView!
    @IBOutlet weak var buildingNameNOView:UIView!
    @IBOutlet weak var additionalAddressView:UIView!

    var alertIsShow:Bool = true
    
    let chooseArticleDropDown = DropDown()
    let chooseStateDropDown = DropDown()
    var isDropSelection:Bool = Bool()
    
     var arrStateList:NSMutableArray = NSMutableArray()
    var stateItems = [State]()
     var objGetState = HttpWrapper()
    var objEditAddress = HttpWrapper()
    var objAddUserAddress = HttpWrapper()

     var net:NetworkReachabilityManager = NetworkReachabilityManager()!
    let badgeButton : MIBadgeButton = MIBadgeButton(frame:CGRect(x: -10, y: 15, width: 40, height: 20))
    
    var strPickupaddress:String = String()
    var strDropOffAddress:String = String()
    var isFrom = ""
    
    
    var isEditingAddress:Bool = false
    var isAddAddress:Bool = false
    
    var editAddress = NSDictionary()
    
    
    var addressTypeString:String = ""
    var addressChrType:String = "B"
    var latLongString:String = ""
    
    var fullAddress:String = ""
    
    var getState:String = ""
    
    var delegate:AddAddress_VcDelegate?
    
    var filter = GMSAutocompleteFilter()
   
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.clipsToBounds = false
        self.navigationItem.setHidesBackButton(true, animated:true)
        self.navigationController?.makeTransparent()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.makeTransparent()

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtPlaceHere.addTarget(self, action: #selector(searchPlaceFromGoogle(_:)), for: .editingChanged)
       // txtPlaceHere.estimatedRowHeight = 44.0
      //  txtPlaceHere.dataSource = self
        txtPlaceHere.delegate = self
        txtSelectState.delegate = self
        
        
        txtPlaceHere.text = strPickupaddress
        self.navigationController?.isNavigationBarHidden = false
                let lbNavTitle = UILabel(frame: CGRect(x: CGFloat(35), y:self.view.bounds.size.width/2-235, width: CGFloat(200), height: CGFloat(40)))
                lbNavTitle.textAlignment = .center
            if !isEditingAddress{
                lbNavTitle.text = "location_title".localized()
            }else{
                lbNavTitle.text = "Edit Address".localized()
            }
                lbNavTitle.textColor = Colorblack
                lbNavTitle.font = UIFont(name: FontBold, size: 16)
        

        self.navigationItem.titleView = lbNavTitle
        self.navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.white
        
        let titleDict: NSDictionary = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [NSAttributedString.Key : Any]
        if !isEditingAddress{
            if addressTypeString != ""{
               lblTitle.text = self.addressTypeString.capitalized.localized()
            }else{
                lblTitle.text = "location_select_pickup_location".localized()
            }
        }else{
            lblTitle.text = ""
            //self.selectAddressButton.isHidden = true
        }
        
        let button = UIButton.init(type: .custom)
        //backarrow , menu
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        
        if languageCode == "en" {
            button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
        }
        else {
            button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
        }
        button.addTarget(self, action:#selector(goBack), for: UIControl.Event.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        GetAllState()
       setupChooseStateDropDown()
        badgeButton.edgeInsetTop = 10
        badgeButton.setImage(UIImage(named : "notification"), for: .normal)
         badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        print(appDelegate.NCount)
        badgeButton.badgeString = appDelegate.NCount
        if appDelegate.NCount == "0"{
            badgeButton.badgeBackgroundColor = UIColor.clear
            badgeButton.badgeTextColor = UIColor.clear
        }else{
            badgeButton.badgeBackgroundColor = colorGreen
            badgeButton.badgeTextColor = UIColor.white
        }
        badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        let barButton2 : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton2
        //        getPackagesListByCategory()
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)
        
        
        txtPlaceHere.placeholder = "location_state_placeholder".localized().uppercased()
        txtBuildingNameNO.placeholder = "location_building_name".localized().uppercased()
        txtAdditionaltext.placeholder = "location_additional_instruction".localized().uppercased()
        txtSelectState.placeholder = "location_select_State".localized().uppercased()
        selectAddressButton.setTitle("location_Select_Address_from_a_map".localized(), for: .normal)
        
        if !isEditingAddress{
            //btnNext.setTitle("location_next".localized(), for: .normal)
            btnNext.setTitle("Save to My Address".localized(), for: .normal)
        }else{
            btnNext.setTitle("Update".localized(), for: .normal)
        }
        self.setDrawView()

//        txtSelectState.placeholder = "".localized()
//        txtCountry.placeholder = "".localized()
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            if self.addressTypeString == "Enter Drop Address"{
                let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "SelectAddress_Vc") as! SelectAddress_Vc
                nextViewController.delegate = self
                nextViewController.isFrom = self.isFrom
                self.navigationController?.pushViewController(nextViewController, animated: true)
            }
        }
        
        filter.type = .establishment
        filter.country = "AE"
        filter.accessibilityLanguage = "en"
        
    }
    
    // Step 3
    
    @objc func NotificationAlert(){
        guard let vcs = self.navigationController?.viewControllers else{return}
        let inStack = vcs.contains(where: { (vc) -> Bool in
            return vc is MyNotificationList_Vc
        })
        if inStack{
            self.performSegue(withIdentifier: unwindToNotificationController, sender: self)
        }else{
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
//        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
//        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    // Step 4
    
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            //self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                
                badgeButton.badgeString = appDelegate.NCount
                if appDelegate.NCount == "0"{
                    badgeButton.badgeBackgroundColor = UIColor.clear
                    badgeButton.badgeTextColor = UIColor.clear
                }else{
                    badgeButton.badgeBackgroundColor = colorGreen
                    badgeButton.badgeTextColor = UIColor.white
                }
            }
        }
    }
    
    
    //Mark:- setViewlayout
    
    func setDrawView(){
        AppHelper.makeViewCircularWithRespectToHeight(placeHereView, borderColor: .lightGray, borderWidth: 1.0)
        AppHelper.makeViewCircularWithRespectToHeight(additionalAddressView, borderColor: .lightGray, borderWidth: 1.0)
        AppHelper.makeViewCircularWithRespectToHeight(countryView, borderColor: .lightGray, borderWidth: 1.0)
        AppHelper.makeViewCircularWithRespectToHeight(selectStateView, borderColor: .lightGray, borderWidth: 1.0)
        AppHelper.makeViewCircularWithRespectToHeight(buildingNameNOView, borderColor: .lightGray, borderWidth: 1.0)
        AppHelper.makeViewCircularWithRespectToHeight(btnNext, borderColor: .clear, borderWidth: 1.0)
        
        if isEditingAddress{
            self.txtPlaceHere.text = editAddress.value(forKey: "varAddress") as? String
            self.txtBuildingNameNO.text = editAddress.value(forKey: "varBuildingName") as? String
            self.txtAdditionaltext.text = editAddress.value(forKey: "varAdditionalInstruction") as? String
            self.txtCountry.text = editAddress.value(forKey: "varCountry") as? String
            self.txtSelectState.text = editAddress.value(forKey: "varState") as? String
        }else{
        self.txtCountry.text = "United Arab Emirates".uppercased()
        }
        
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        if languageCode != "en" {
            self.txtPlaceHere.textAlignment = .right
            txtBuildingNameNO.textAlignment = .right
            txtAdditionaltext.textAlignment = .right
            txtCountry.textAlignment = .right
            txtSelectState.textAlignment = .right
            self.selectAddressButton.semanticContentAttribute = .forceLeftToRight

        }else{
        self.selectAddressButton.semanticContentAttribute = .forceRightToLeft
        }

       
    }
    
    // MARK:- goBack
    
    @objc func goBack(){
       self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickBtnNext(_ sender: Any) {
        
        if !isEditingAddress{
        if isLoginDataValid(){
            let address = txtPlaceHere.text ?? ""
            //self.addNewAddress()

            //self.getCordinateFromAddress(address: address) { (state) in
              //  if /*state.capitalized*/"Dubai" == self.txtSelectState.text?.capitalized{
                if self.stateItems.contains(where: {($0.state.lowercased() == self.txtSelectState.text?.lowercased()) && ($0.activeState == "Y")}) {
                self.addNewAddress()
            }else{
                AppHelper.showAlert("SerivceMyCar".localized(), message: "Sorry we are currently not operating in your emirate and will be launching soon. Keep upto date by following us on Instagram @servicemycar.ae thank you".localized())
            }
          //  }
           // GetLatlongFromAddress()
        }
        }else{
            
          //  let address = txtPlaceHere.text ?? ""
         //   self.UpdateAddress()

          //  self.getCordinateFromAddress(address: address) { (state) in
            //    self.addNewAddress()
            //    print("get state detail  \(state)")
              //  if /*state.capitalized*/"Dubai" == self.txtSelectState.text?.capitalized{
            if isLoginDataValid(){
            if self.stateItems.contains(where: {($0.state.lowercased() == self.txtSelectState.text?.lowercased()) && ($0.activeState == "Y")}){

                    self.UpdateAddress()
                }else{
                    AppHelper.showAlert("SerivceMyCar".localized(), message: "Sorry we are currently not operating in your emirate and will be launching soon. Keep upto date by following us on Instagram @servicemycar.ae thank you".localized())
                }
          //  }
            }
      //      self.UpdateAddress()
        }
    }
    
    @IBAction func onClickSelectAddressButton(_ sender:UIButton){
        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "SelectAddress_Vc") as! SelectAddress_Vc
        nextViewController.delegate = self
        nextViewController.isFrom = isFrom
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    
    func MoveToNext()
    {
        //Checking Drop loaction is required or not
        appDelegate.Bookings.DAddress.PickupAddress = txtPlaceHere.text!
        appDelegate.Bookings.DAddress.Pickup.varBuildingPName = txtBuildingNameNO.text!
        appDelegate.Bookings.DAddress.Pickup.varPackagePState = txtSelectState.text!
        appDelegate.Bookings.DAddress.Pickup.varPackagePCountry = txtCountry.text!
        appDelegate.Bookings.DAddress.Pickup.varPackagePAddress = txtPlaceHere.text!
        appDelegate.Bookings.DAddress.Pickup.additionalPInstruction = txtAdditionaltext.text!
        
        
        if isFrom == "3" || isFrom == "4" {
            if isLoginDataValid(){
                let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "OderCar_Vc") as! OderCar_Vc
                nextViewController.isFrom = isFrom
                self.navigationController?.pushViewController(nextViewController, animated: true)
            }
        }
        else {
            if isDropSelection == true{
                if isLoginDataValid(){
                    let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "AddDropOffAddress_Vc") as! AddDropOffAddress_Vc
                    nextViewController.strPickupaddress = txtPlaceHere.text!
                    nextViewController.strDropOffAddress = strDropOffAddress
                    nextViewController.strBuldingNO = txtBuildingNameNO.text!
                    nextViewController.strAdditionalInfo = txtAdditionaltext.text!
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                }
            }else
            {
                if isLoginDataValid(){
                    let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "SelectParts_Vc") as! SelectParts_Vc
                    // nextViewController.isSelectparts = true
                    //let nextViewController = objPostJob.instantiateViewController(withIdentifier:"PostYourJob") as! PostYourJob
                    self.navigationController?.pushViewController(nextViewController, animated: true)
                }
            }
        }
    }
    
    func setupChooseStateDropDown() {
        chooseStateDropDown.anchorView = txtSelectState
        chooseStateDropDown.bottomOffset = CGPoint(x: 0, y: txtSelectState.bounds.height)
        chooseStateDropDown.dataSource = [
            "iPhone SE | Black | 64G",
            "Samsung S7",
            "Huawei P8 Lite Smartphone 4G",
            "Asus Zenfone Max 4G",
            "Apple Watwh | Sport Edition"
        ]
        // Action triggered on selection
        chooseStateDropDown.selectionAction = { [weak self] (index, item) in
            self?.txtSelectState.text = item
            self!.txtSelectState.endEditing(true)
        }
    }
    
    
    // MARK: - GetAllState API
    
    func GetAllState(){
        if net.isReachable == false{
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["userId" : nUserId as AnyObject]
       
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objGetState = HttpWrapper.init()
            self.objGetState.delegate = self
       
            self.objGetState.requestWithparamdictParamPostMethodwithHeaderAndDictGet(url: kstateList, dictParams: dictParameters, headers: header)
        }
        
    }
    
    
    func UpdateAddress(){
        if net.isReachable == false {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }else{
            
            var addressID = ""
            var addressTittle = ""
            var addresstype = ""
            var latLong = ""
            
            if let addressId = editAddress.value(forKey: "intId") as? String{
                addressID = addressId
            }else if let addressId = editAddress.value(forKey: "intId") as? Int{
                addressID = "\(addressId)"
            }
            if let addreTittle = editAddress.value(forKey: "varTitle") as? String{
                addressTittle = addreTittle
            }
            if let addType = editAddress.value(forKey: "chrType") as? String{
                addresstype = addType
            }
            if let latLongtiude = editAddress.value(forKey: "varLatLong") as? String{
                latLong = latLongtiude
            }else if let latLongtiude = editAddress.value(forKey: "varLatLong") as? String{
                latLong = latLongtiude
            }
            if latLong != ""{
                let getlatLong = latLong.split(separator: ",")
                let lat = getlatLong.first
                let long = getlatLong.last
                self.latLongString = "\(lat!),\(long!)"
            }
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["addressId" : addressID as AnyObject,
                                                     "varTitle" : addressTittle as AnyObject,
                                                     "chrType" : addresstype as AnyObject,
                                                     "varAddress" : txtPlaceHere.text as AnyObject,
                                                     "varBuildingName" : txtBuildingNameNO.text as AnyObject,
                                                     "varAdditionalInstruction" : txtAdditionaltext.text as AnyObject,
                                                     "varCountry" : txtCountry.text as AnyObject,
                                                     "varState" : txtSelectState.text as AnyObject,
                                                     "varLatLong":self.latLongString as AnyObject]
            
            self.fullAddress = "\(txtPlaceHere.text ?? "") \(txtBuildingNameNO.text ?? "") \(txtAdditionaltext.text ?? "") \(txtSelectState.text ?? "") \(txtCountry.text ?? "")"

          
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objEditAddress = HttpWrapper.init()
            self.objEditAddress.delegate = self
           
            self.objEditAddress.requestWithparamdictParamPutMethodwithHeader(url: kEditUserAddress, dicsParams: dictParameters, headers: header)
        }
        
    
        
    }
    
    
    
    func addNewAddress(){
        if net.isReachable == false {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }else{
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            
            let dictParameters:[String:AnyObject] = ["userId" : nUserId as AnyObject,
                                                     "varTitle" : "Home Address" as AnyObject,
                                                     "chrType" : addressChrType as AnyObject,
                                                     "varAddress" : txtPlaceHere.text as AnyObject,
                                                     "varBuildingName" : txtBuildingNameNO.text as AnyObject,
                                                     "varAdditionalInstruction" : txtAdditionaltext.text as AnyObject,
                                                     "varCountry" : txtCountry.text as AnyObject,
                                                     "varState" : txtSelectState.text as AnyObject,
                                                     "varLatLong":self.latLongString as AnyObject]
            self.fullAddress = "\(txtPlaceHere.text ?? "") \(txtBuildingNameNO.text ?? "") \(txtAdditionaltext.text ?? "") \(txtSelectState.text ?? "") \(txtCountry.text ?? "")"
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objAddUserAddress = HttpWrapper.init()
            self.objAddUserAddress.delegate = self
            
            self.objAddUserAddress.requestWithparamdictParamPostMethodwithHeader(url: kAddUserAddress, dicsParams: dictParameters, headers: header)
        }
        
        
        
    }

    
    // MARK: - HttpWrapperfetchDataSuccess
    
    func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary)
    {
        AppHelper.hideLoadingView()
        print(dicsResponse)
       if wrapper == objGetState {
            if dicsResponse.value(forKey: "status") as! NSString == "success"{
                arrStateList.removeAllObjects()
                // txtModel.resignFirstResponder()
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray{
                    arrStateList = tempNames.mutableCopy() as! NSMutableArray
                }
                print(arrStateList)
                
                var arrsarch:[String] = [String]()
                for dct in arrStateList {
                    let data = dct as! NSDictionary
                    // self.resultsArray.append(dct)
                    
                    arrsarch.append(data.value(forKey: "varStateName") as! String)
                    let state = State(dict: data as! Dictionary<String, AnyObject>)
                    self.stateItems.append(state)
                    
                }
                chooseStateDropDown.anchorView = txtSelectState
                
                // Will set a custom with instead of anchor view width
                //        dropDown.width = 100
                
                // By default, the dropdown will have its origin on the top left corner of its anchor view
                // So it will come over the anchor view and hide it completely
                // If you want to have the dropdown underneath your anchor view, you can do this:
                chooseStateDropDown.bottomOffset = CGPoint(x: 0, y: txtSelectState.bounds.height)
                
                // You can also use localizationKeysDataSource instead. Check the docs.
                chooseStateDropDown.dataSource = arrsarch
                
                // Action triggered on selection
                chooseStateDropDown.selectionAction = { [weak self] (index, item) in
                    self?.txtSelectState.text = item
                    self!.txtSelectState.endEditing(true)
                }
                // collectionviewselect.reloadData()
            }else{
                AppHelper.showAlert("ServiceMyCar".localized(), message:dicsResponse.value(forKey: "msg") as! String)
            }
       }else if wrapper == objEditAddress{
        if dicsResponse.value(forKey: "status") as! NSString == "success"{
            
//            if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray{
//                arrAddresslist = tempNames.mutableCopy() as! NSMutableArray
//                self.arrayCount = arrAddresslist.count
//            }
            if let editAddress = dicsResponse.value(forKey: "data") as? NSArray{
                if addressTypeString == "Enter Collection Address"{
                    if let address = editAddress.firstObject as? NSDictionary{
                        self.delegate?.getCollectionPointAddress(self, collectAddress: self.fullAddress, newAddress: address)
                    }
                }else if addressTypeString == "Enter Drop Address"{
                    if let address = editAddress.firstObject as? NSDictionary{
                        self.delegate?.getDropPointAddress(self, dropAddress: self.fullAddress, newAddress: address)
                    }
                }
                self.delegate?.getAddAddress(self, address: true)
                self.navigationController?.popViewController(animated: true)
            }

        }else{
            AppHelper.showAlert("app_alert_title".localized(), message:dicsResponse.value(forKey: "msg") as! String)
            }
        
       }else if wrapper == objAddUserAddress{
        if dicsResponse.value(forKey: "status") as! NSString == "success"{
            if let newAddress = dicsResponse.value(forKey: "data") as? NSArray{
                if addressTypeString == "Enter Collection Address"{
                    if let address = newAddress.firstObject as? NSDictionary{
                    self.delegate?.getCollectionPointAddress(self, collectAddress: self.fullAddress, newAddress: address)
                    }
                }else if addressTypeString == "Enter Drop Address"{
                    if let address = newAddress.firstObject as? NSDictionary{
                    self.delegate?.getDropPointAddress(self, dropAddress: self.fullAddress, newAddress: address)
                    }
                }
                self.delegate?.getAddAddress(self, address: true)
                self.navigationController?.popViewController(animated: true)
            }
        }else{
            AppHelper.showAlert("app_alert_title".localized(), message:dicsResponse.value(forKey: "msg") as! String)
        }

        }
        
    }
    // MARK: - HttpWrapperfetchDataFail
    
    func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError)
    {
        AppHelper.hideLoadingView()
        print(error.debugDescription)
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidEndEditing(_ textField: UITextField) {
//        if textField == txtState
//        {
//            
//        }
       
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtSelectState{
            chooseStateDropDown.show()
            txtSelectState.endEditing(true)
          // call()
//            self.pickUp(txtState)
//
//            myPickerView.tag = 1
        }
       
    }
    
    func GetLatlongFromAddress() {
        
        
        // https://maps.googleapis.com/maps/api/place/autocomplete/json?input=a&types=(cities)&key=AIzaSyC4_bhTW7L0-uoV35GxuZy2yEc-XsI-fV0&components=country:ind
        
        var strGoogleApi = "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyAyJi39vvp3M2K3oF6hmRr2iRwjhxPhkkk&address=\(txtPlaceHere.text!)"
        strGoogleApi = strGoogleApi.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        print(strGoogleApi)
        var urlRequest = URLRequest(url: URL(string: strGoogleApi)!)
        urlRequest.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, resopnse, error) in
            if error == nil {
                
                if let responseData = data {
                    let jsonDict = try? JSONSerialization.jsonObject(with: responseData, options: .mutableContainers)
                    
                    if let dict = jsonDict as? Dictionary<String, AnyObject>{
                        
                        if let results = dict["results"] as? NSArray {
                            print("json == \(results)")
                            if let data = results.object(at: 0) as? NSDictionary{
                                if let nextvalue = data.value(forKey: "geometry") as? NSDictionary{
                                    print("json == \(nextvalue)")
                                    if let locaiton = nextvalue.value(forKey: "location") as? NSDictionary{
                                        
                                        let lat = locaiton.value(forKey: "lat") as! NSNumber
                                        let long = locaiton.value(forKey: "lng") as! NSNumber
                                        
                                        appDelegate.Bookings.DAddress.Pickup.Latitude = "\(lat)"
                                        appDelegate.Bookings.DAddress.Pickup.Longitude = "\(long)"
                                        self.latLongString = "\(lat),\(long)"
                                        DispatchQueue.main.async {
                                            
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                }
            } else {
                //we have error connection google api
            }
            DispatchQueue.main.async {
                //self.MoveToNext()
                self.addNewAddress()
            }
        }
        
        task.resume()
        
        
        
    }
    @objc func searchPlaceFromGoogle(_ textField:UITextField) {
        
        if let searchQuery = textField.text {
            let langcode = UserDefaults.standard.value(forKey: kAppLanguage) ?? "en"
            var strGoogleApi = "https://maps.googleapis.com/maps/api/place/autocomplete/json?components=country:ae&key=\(googleApiKey)&language=\(langcode)&hasNextPage=true&nextPage()=true&input=\(searchQuery)"
            strGoogleApi = strGoogleApi.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            print(strGoogleApi)
            var urlRequest = URLRequest(url: URL(string: strGoogleApi)!)
            urlRequest.httpMethod = "GET"
            let task = URLSession.shared.dataTask(with: urlRequest) { (data, resopnse, error) in
                if error == nil {
                    
                    if let responseData = data {
                        let jsonDict = try? JSONSerialization.jsonObject(with: responseData, options: .mutableContainers)
                        
                        if let dict = jsonDict as? Dictionary<String, AnyObject>{

                            if let results = dict["predictions"] as? [Dictionary<String, AnyObject>] {
                                print("json == \(results)")
                                
                                var arrsarch:[String] = [String]()
                                for dct in results {
                                    arrsarch.append(dct["description"] as! String)
                                   
                                }
                                
                                print(arrsarch)
                                DispatchQueue.main.async {
                                    self.chooseArticleDropDown.anchorView = self.txtPlaceHere
                                }
                             
                                DispatchQueue.main.async {
                                    self.chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: self.txtPlaceHere.bounds.height)
                                }
                                
                                
                                // You can also use localizationKeysDataSource instead. Check the docs.
                               // if arrsarch.count != 0{
                                DispatchQueue.main.async {
                                    self.chooseArticleDropDown.dataSource = arrsarch
                                }
                               // }
                                
                                // Action triggered on selection
                                self.chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
                                    self?.getsPlaceDetails(item, placeId: "")
                                     self?.txtPlaceHere.text = item
                                  /*  self?.getCordinateFromAddress(address: item, complition: { (state) in
                                        if (self?.stateItems.contains(where: {($0.state.capitalized == state.capitalized) && ($0.activeState == "Y")}))!{
                                            self?.txtPlaceHere.text = item
                                            self?.txtSelectState.text = state
                                            self!.txtPlaceHere.endEditing(true)

                                        }else{
                                            self?.txtPlaceHere.text = ""
                                            self?.txtSelectState.text = ""
                                            self!.txtPlaceHere.endEditing(true)
                                            AppHelper.showAlert("Serivce My Car", message: "Sorry we are currently not operating in your emirate and will be launching soon. Keep upto date by following us on Instagram @servicemycar.ae thank you")
                                        }
                                    })
                                    self?.getsPlaceDetails(item)*/
//                                    self?.txtPlaceHere.text = item
//                                    self!.txtPlaceHere.endEditing(true)
                                }
                                DispatchQueue.main.async {
                                    self.chooseArticleDropDown.show()
                                }
                                self.chooseArticleDropDown.cancelAction = { [unowned self] in
                                    print("Drop down dismissed")
                                    self.txtPlaceHere.endEditing(true)
                                }
                                self.chooseArticleDropDown.willShowAction = { [unowned self] in
                                    print("Drop down will show")
                                }
                            }
                        }
                        
                    }
                } else {
                    //we have error connection google api
                }
            }
            task.resume()
        }
    }
    
    // MARK: - isLoginDataValid
    func isLoginDataValid() -> Bool{
        
        if (txtPlaceHere.text?.trimmingCharacters(in: CharacterSet.whitespaces).count)! <= 0{
            AppHelper.showAlert("ServiceMyCar".localized(), message: "Please Search Address.".localized())
            return false
        }
        
        if (txtBuildingNameNO.text?.trimmingCharacters(in: CharacterSet.whitespaces).count)! <= 0{
            AppHelper.showAlert("ServiceMyCar".localized(), message: "Please Enter building Name/Number.".localized())
            return false
        }
        
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddAddress_Vc:SelectAddress_VcDelegate{
    func getFullAddress(_ viewController: SelectAddress_Vc, getAddress address: String, getLatitude lat: String, getLongitude long: String, getState state: String) {
     //   self.getCordinateFromAddress(address: address) { (state) in
       //     print("current state \(state)")
        //self.getsPlaceDetails(address)
      //  self.getsPlaceDetails(address)
        GetLatlongFromAddress(address:address)
        
     /*       if self.stateItems.contains(where: {($0.state.capitalized == state.capitalized) && ($0.activeState == "Y")}){
                self.txtPlaceHere.text = address
                self.txtSelectState.text = state.capitalized
                self.latLongString = "\(lat),\(long)"
            } else {
                AppHelper.showAlert("Serivce My Car", message: "Sorry we are currently not operating in your emirate and will be launching soon. Keep upto date by following us on Instagram @servicemycar.ae thank you")
                
                //item could not be found
            }*/
        //}
    }
    
    func getCordinateFromAddress(address:String,complition:@escaping (_ state:String)->Void){
        let geoCoder = CLGeocoder()
        var currentState = ""
        
        geoCoder.geocodeAddressString(address) { (placemarks, error) in
            guard let placemarks = placemarks,let location = placemarks.first?.location else {
                AppHelper.showAlert("ServiceMyCar".localized(), message: "Please select address with proper state!")
                return
            }
            currentState = placemarks.first?.locality ?? ""
            self.latLongString = "\(location.coordinate.latitude) \(location.coordinate.longitude)"
            complition(currentState)
        }

    }
    
    
    func getsPlaceDetails(_ textfield:String,placeId:String){
         let text = textfield
        self.alertIsShow = true
        let placesClient = GMSPlacesClient.shared()
        if placeId == ""{
       placesClient.autocompleteQuery(text, bounds: nil, filter: filter) { (results, error) in
            guard let results = results else{
                return
            }
            for result in results {
                var placeId = ""
                if let tempId = result.placeID as String?{
                    placeId = tempId
                }else{
                    return
                }
                placesClient.lookUpPlaceID(placeId, callback: { (place, error) in
                    if error != nil {
                        return
                    }
                    if let place = place{

                        for plc in place.addressComponents!{
                        //    print(" find address  from  map ===========    \(plc)")
                            if plc.type == "administrative_area_level_1"{

                                let stateName =  (plc.name == "دبي" || plc.name == "Dubai") ? "Dubai" : ((plc.name == "إمارة الشارقةّ" || plc.name == "Sharjah") ? "Sharjah" : plc.name.capitalized)
                                
                                let stexist = self.stateItems.contains(where: { (st) -> Bool in
                                    return (st.state.lowercased() == stateName.lowercased()) && (st.activeState == "Y")
                                })
                                
                                
                                if stexist{
                                    self.txtPlaceHere.text = textfield
                                    self.txtSelectState.text = (plc.name == "دبي" || plc.name == "Dubai") ? "Dubai" : ((plc.name == "إمارة الشارقةّ" || plc.name == "Sharjah") ? "Sharjah" : plc.name.capitalized)
                                    self.latLongString = "\(place.coordinate.latitude),\(place.coordinate.longitude)"
                                    self.alertIsShow = true
                                    self.txtPlaceHere.endEditing(true)
                                    return
                                }else{
                                    self.txtPlaceHere.text = ""
                                    self.txtSelectState.text = ""
                                    self.txtPlaceHere.endEditing(true)
                                    if self.alertIsShow{
                                    AppHelper.showAlert("SerivceMyCar".localized(), message: "Sorry we are currently not operating in your emirate and will be launching soon. Keep upto date by following us on Instagram @servicemycar.ae thank you".localized())
                                        self.alertIsShow = false
                                    }
                                    return
                                }
                            }

                        }
                    }
                })
           }
         }
        }else{
            placesClient.lookUpPlaceID(placeId, callback: { (place, error) in
                if error != nil {
                    return
                }
                if let place = place{
                    
                    for plc in place.addressComponents!{
                      //  print(" find address  from  map ===========    \(plc)")
                        if plc.type == "administrative_area_level_1"{
                            
                            let stateName =  (plc.name == "دبي" || plc.name == "Dubai") ? "Dubai" : ((plc.name == "إمارة الشارقةّ" || plc.name == "Sharjah") ? "Sharjah" : plc.name.capitalized)
                            
                            let stexist = self.stateItems.contains(where: { (st) -> Bool in
                                return (st.state.lowercased() == stateName.lowercased()) && (st.activeState == "Y")
                            })
                            
                            
                            if stexist{
                                self.txtPlaceHere.text = textfield
                                self.txtSelectState.text = (plc.name == "دبي" || plc.name == "Dubai") ? "Dubai" : ((plc.name == "إمارة الشارقةّ" || plc.name == "Sharjah") ? "Sharjah" : plc.name.capitalized)
                                self.latLongString = "\(place.coordinate.latitude),\(place.coordinate.longitude)"
                                self.alertIsShow = true
                                self.txtPlaceHere.endEditing(true)
                                return
                            }else{
                                self.txtPlaceHere.text = ""
                                self.txtSelectState.text = ""
                                self.txtPlaceHere.endEditing(true)
                                if self.alertIsShow{
                                    AppHelper.showAlert("SerivceMyCar".localized(), message: "Sorry we are currently not operating in your emirate and will be launching soon. Keep upto date by following us on Instagram @servicemycar.ae thank you".localized())
                                    self.alertIsShow = false
                                }
                                return
                            }
                        }
                        
                    }
                }
            })
        }
    }

    
}



extension AddAddress_Vc{
    
    
    func GetLatlongFromAddress(address:String) {
        let langcode = UserDefaults.standard.value(forKey: kAppLanguage) ?? "en"
        var strGoogleApi = "https://maps.googleapis.com/maps/api/geocode/json?key=\(googleApiKey)&language=\(langcode)&address=\(address)"
        strGoogleApi = strGoogleApi.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        print(strGoogleApi)
        var urlRequest = URLRequest(url: URL(string: strGoogleApi)!)
        urlRequest.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, resopnse, error) in
            if error == nil {
                
                if let responseData = data {
                    let jsonDict = try? JSONSerialization.jsonObject(with: responseData, options: .mutableContainers)
                    
                    if let dict = jsonDict as? Dictionary<String, AnyObject>{
                        
                        if let results = dict["results"] as? NSArray {
                        //    print("json == \(results)")
                            if let data = results.object(at: 0) as? NSDictionary{
//                                if let nextvalue = data.value(forKey: "geometry") as? NSDictionary{
//                                    //   print("json == \(nextvalue)")
//                                    if let locaiton = nextvalue.value(forKey: "location") as? NSDictionary{
//                                        let lat = locaiton.value(forKey: "lat")
//                                        let long = locaiton.value(forKey: "lng")
//                                        print("latitude =========== \(lat) and longitude ============ \(long)")
//                                    }
//                                }
                                if let nextvalue = data.value(forKey: "place_id") as? String/*,let address = data.value(forKey: "formatted_address") as? String*/{
                                    // print("json formatted_address == \(nextvalue)")
                                    self.getsPlaceDetails(address, placeId: nextvalue)
                                }
                                
                                
                            }
                        }
                    }
                }
            } else {
                //we have error connection google api
            }
          
        }
        task.resume()
    }
    
    
    
    
    
    

  /*  func geocodeAddress(address: String!, withCompletionHandler completionHandler: @escaping ((_ status: String, _ success: Bool) -> Void)) {
        let baseURLGeocode = "https://maps.googleapis.com/maps/api/geocode/json?"
        var lookupAddressResults = Dictionary<String, Any>()
        if let lookupAddress = address {
            
            var geocodeURLString = baseURLGeocode + "address=" + lookupAddress
            //geocodeURLString = geocodeURLString.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
            
            let geocodeURL = NSURL(string: geocodeURLString)
            DispatchQueue.main.async {
          
                let geocodingResultsData = NSData(contentsOf: geocodeURL! as URL)
                
                var error: Error?
                let dictionary: Dictionary<String, AnyObject> = JSONSerialization.jsonObject(with: geocodingResultsData! as Data, options: JSONSerialization.ReadingOptions.mutableContainers) as! Dictionary<String, AnyObject> 
                
                if (error != nil) {
                   // println(error)
                    completionHandler("", false)
                }
                else {
                    // Get the response status.
                    let status = dictionary["status"] as! String
                    
                    if status == "OK" {
                        let allResults = dictionary["results"] as! Array<Dictionary<String, AnyObject>>
                        lookupAddressResults = allResults[0]
                        print("Get Detail from Address \(lookupAddressResults)")
                        // Keep the most important values.
                        let fetchedFormattedAddress = lookupAddressResults["formatted_address"] as! String
                        print("get full address \(fetchedFormattedAddress)")
                        let geometry = lookupAddressResults["geometry"] as! Dictionary<String, AnyObject>
                        let getLongitude = ((geometry["location"] as! Dictionary<String, AnyObject>)["lng"] as! NSNumber).doubleValue
                        let getLatitude = ((geometry["location"] as! Dictionary<String, AnyObject>)["lat"] as! NSNumber).doubleValue
                        self.latLongString = "\(getLatitude),\(getLongitude)"
                        
                        completionHandler(status, true)
                    }
                    else {
                        completionHandler(status, false)
                    }
                }
            }
        }
        else {
            completionHandler("No valid address.", false)
        }
    }*/

}


class State:NSObject{
    
    let kState = "varStateName"
    let kActiveState = "chrAvailable"
    
    var state:String = ""
    var activeState:String = ""

    
    override init() {
        super.init()
    }
    init(json:JSON){
        if let _state = json[kState].string as String?{
            self.state = _state
        }
        if let _activeState = json[kActiveState].string as String?{
            self.activeState = _activeState
        }
        super.init()
    }
    
    init(dict:Dictionary<String,AnyObject>){
        if let _state = dict[kState] as? String{
            self.state = _state
        }
        if let _activeState = dict[kActiveState] as? String{
            self.activeState = _activeState
        }
        super.init()
    }

}
