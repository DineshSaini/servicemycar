//
//  AddDropOffAddress_Vc.swift
//  ServiceMyCar
//
//  Created by baps on 28/01/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import DropDown
import Alamofire
import SwiftyJSON

class AddDropOffAddress_Vc: UIViewController ,UITextFieldDelegate,HttpWrapperDelegate {
    
    @IBOutlet weak var btnsameAs: UIButton!
    @IBOutlet weak var btnNext: UIButtonX!
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var txtSelectState: UITextField!
    @IBOutlet weak var txtAdditionaltext: UITextField!
    @IBOutlet weak var txtBuildingNameNO: UITextField!
    @IBOutlet weak var txtPlaceHere: UITextField!
    @IBOutlet weak var lblTitle: UILabel!
    let chooseArticleDropDown = DropDown()
    let chooseStateDropDown = DropDown()
    var arrStateList:NSMutableArray = NSMutableArray()
    var objGetState = HttpWrapper()
    var net:NetworkReachabilityManager = NetworkReachabilityManager()!
    var isCheck = false
    var strPickupaddress:String = String()
    var strDropOffAddress:String = String()
    
    var strBuldingNO:String = String()
    var strAdditionalInfo:String = String()
    let badgeButton : MIBadgeButton = MIBadgeButton(frame:CGRect(x: -10, y: 15, width: 40, height: 20))
    override func viewDidLoad() {
        super.viewDidLoad()
        txtPlaceHere.addTarget(self, action: #selector(searchPlaceFromGoogle(_:)), for: .editingChanged)
        // txtPlaceHere.estimatedRowHeight = 44.0
        //  txtPlaceHere.dataSource = self
        let origImage = UIImage(named: "Uncheck");
       
        btnsameAs.setImage(origImage, for: .normal)
        btnsameAs.tintColor = UIColor.black
        txtPlaceHere.text = strDropOffAddress
        txtPlaceHere.delegate = self
        txtSelectState.delegate = self
        self.navigationController?.isNavigationBarHidden = false
        let lbNavTitle = UILabel(frame: CGRect(x: CGFloat(35), y:self.view.bounds.size.width/2-235, width: CGFloat(200), height: CGFloat(40)))
        lbNavTitle.textAlignment = .center
        lbNavTitle.text = "location_title".localized()
        lbNavTitle.textColor = Colorblack
        lbNavTitle.font = UIFont(name: Regular, size: 16)
        
        //        let logo = UIImage(named: "AppTopIcon")
        //        let imageView = UIImageView(image:logo)
        //        imageView.contentMode = .scaleAspectFit
        //        self.navigationItem.titleView = imageView
        self.navigationItem.titleView = lbNavTitle
        self.navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.white
        
        let titleDict: NSDictionary = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [NSAttributedString.Key : Any]
        
        let button = UIButton.init(type: .custom)
        //backarrow , menu
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        
        if languageCode == "en" {
            button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
        }
        else {
            button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
        }
        button.addTarget(self, action:#selector(goBack), for: UIControl.Event.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        badgeButton.edgeInsetTop = 10
        badgeButton.setImage(UIImage(named : "notification"), for: .normal)
         badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        print(appDelegate.NCount)
        badgeButton.badgeString = appDelegate.NCount
        if appDelegate.NCount == "0"
        {
            badgeButton.badgeBackgroundColor = UIColor.clear
            badgeButton.badgeTextColor = UIColor.clear
        }
        else
        {
            badgeButton.badgeBackgroundColor = colorGreen
            badgeButton.badgeTextColor = UIColor.white
        }
        
        // badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        let barButton2 : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton2
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)
        
        txtPlaceHere.placeholder = "location_state_placeholder".localized()
        txtBuildingNameNO.placeholder = "location_building_name".localized()
        txtAdditionaltext.placeholder = "location_additional_instruction".localized()
        btnNext.setTitle("location_next".localized(), for: .normal)
        btnsameAs.setTitle("Same_As_pickup_Location".localized(), for: .normal)
        lblTitle.text = "Select_Drop_Off_Location".localized()

        GetAllState()
        setupChooseStateDropDown()
    }
    
    // MARK: - Google Api Calling
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            //self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                
                badgeButton.badgeString = appDelegate.NCount
                if appDelegate.NCount == "0"
                {
                    badgeButton.badgeBackgroundColor = UIColor.clear
                    badgeButton.badgeTextColor = UIColor.clear
                }
                else
                {
                    badgeButton.badgeBackgroundColor = colorGreen
                    badgeButton.badgeTextColor = UIColor.white
                }
                
            }
        }
    }
    
    
    @objc func NotificationAlert()
    {
        guard let vcs = self.navigationController?.viewControllers else{return}
        let inStack = vcs.contains(where: { (vc) -> Bool in
            return vc is MyNotificationList_Vc
        })
        if inStack{
            self.performSegue(withIdentifier: unwindToNotificationController, sender: self)
        }else{
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        //        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
        //
        //        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    // MARK:- goBack
    
    @objc func goBack()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickBtnSameAs(_ sender: UIButton) {
        
        if isCheck == true
        {
            isCheck = false
            let origImage = UIImage.init(named: "Uncheck");
            let tintedImage = origImage?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
            sender.setImage(origImage, for: .normal)
            sender.tintColor = UIColor.black
            txtPlaceHere.text = ""
            txtAdditionaltext.text = ""
            txtBuildingNameNO.text = ""
        }
        else
        {
            isCheck = true
            let origImage = UIImage.init(named: "checked");
            
            txtPlaceHere.text = strPickupaddress
            txtAdditionaltext.text = strAdditionalInfo
            txtBuildingNameNO.text = strBuldingNO
            
            let tintedImage = origImage?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
            sender.setImage(origImage, for: .normal)
            sender.tintColor = UIColor.black
        }
       
        
    }
    @IBAction func onClickBtnNext(_ sender: Any) {
        if isLoginDataValid()
        {
           GetLatlongFromAddress()
        }
       
    }
    
    func MoveToNext()
    {
        appDelegate.Bookings.DAddress.DropOffAddress = txtPlaceHere.text!
        appDelegate.Bookings.DAddress.Dropoff.varBuildingDName = txtBuildingNameNO.text!
        appDelegate.Bookings.DAddress.Dropoff.varPackageDState = txtSelectState.text!
        appDelegate.Bookings.DAddress.Dropoff.varPackageDCountry = txtCountry.text!
        appDelegate.Bookings.DAddress.Dropoff.varPackageDAddress = txtPlaceHere.text!
        appDelegate.Bookings.DAddress.Dropoff.additionalDInstruction = txtAdditionaltext.text!
        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "SelectParts_Vc") as! SelectParts_Vc
        // nextViewController.isSelectparts = true
        //let nextViewController = objPostJob.instantiateViewController(withIdentifier:"PostYourJob") as! PostYourJob
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    func setupChooseStateDropDown() {
        chooseStateDropDown.anchorView = txtSelectState
        
        // Will set a custom with instead of anchor view width
        //        dropDown.width = 100
        
        // By default, the dropdown will have its origin on the top left corner of its anchor view
        // So it will come over the anchor view and hide it completely
        // If you want to have the dropdown underneath your anchor view, you can do this:
        chooseStateDropDown.bottomOffset = CGPoint(x: 0, y: txtSelectState.bounds.height)
        
        // You can also use localizationKeysDataSource instead. Check the docs.
        chooseStateDropDown.dataSource = [
            "iPhone SE | Black | 64G",
            "Samsung S7",
            "Huawei P8 Lite Smartphone 4G",
            "Asus Zenfone Max 4G",
            "Apple Watwh | Sport Edition"
        ]
        
        // Action triggered on selection
        chooseStateDropDown.selectionAction = { [weak self] (index, item) in
            self?.txtSelectState.text = item
            self!.txtSelectState.endEditing(true)
        }
        
        //  chooseArticleDropDown.multiSelectionAction = { [weak self] (indices, items) in
        //            print("Muti selection action called with: \(items)")
        //            if items.isEmpty {
        //                self?.chooseArticleButton.setTitle("", for: .normal)
        //            }
        //        }
        
        // Action triggered on dropdown cancelation (hide)
        //        dropDown.cancelAction = { [unowned self] in
        //            // You could for example deselect the selected item
        //            self.dropDown.deselectRowAtIndexPath(self.dropDown.indexForSelectedRow)
        //            self.actionButton.setTitle("Canceled", forState: .Normal)
        //        }
        
        // You can manually select a row if needed
        //        dropDown.selectRowAtIndex(3)
    }
    
    
    // MARK: - GetAllState API
    
    func GetAllState()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["userId" : nUserId as AnyObject]
            //            let dictHeaaderPera:[String:AnyObject] = [
            //                "platform" : "IOS" as AnyObject,
            //                "modelNumber" : "ML2000" as AnyObject
            //                ,"serialNumber" :"1234567891" as AnyObject,
            //                 "deviceId" : "12345" as AnyObject]
            //
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objGetState = HttpWrapper.init()
            self.objGetState.delegate = self
            // self.objGetAllCar.requestWithparamdictParamPostMethod(url: kgetUserCars, dicsParams: dictParameters)
            self.objGetState.requestWithparamdictParam(kstateList)
        }
        
    }
    
    // MARK: - HttpWrapperfetchDataSuccess
    
    func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary)
    {
        AppHelper.hideLoadingView()
        print(dicsResponse)
        if wrapper == objGetState {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                arrStateList.removeAllObjects()
                // txtModel.resignFirstResponder()
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray
                {
                    arrStateList = tempNames.mutableCopy() as! NSMutableArray
                }
                print(arrStateList)
                
                var arrsarch:[String] = [String]()
                for dct in arrStateList {
                    let data = dct as! NSDictionary
                    // self.resultsArray.append(dct)
                    arrsarch.append(data.value(forKey: "varStateName") as! String)
                    
                }
                chooseStateDropDown.anchorView = txtSelectState
                
                // Will set a custom with instead of anchor view width
                //        dropDown.width = 100
                
                // By default, the dropdown will have its origin on the top left corner of its anchor view
                // So it will come over the anchor view and hide it completely
                // If you want to have the dropdown underneath your anchor view, you can do this:
                chooseStateDropDown.bottomOffset = CGPoint(x: 0, y: txtSelectState.bounds.height)
                
                // You can also use localizationKeysDataSource instead. Check the docs.
                chooseStateDropDown.dataSource = arrsarch
                
                // Action triggered on selection
                chooseStateDropDown.selectionAction = { [weak self] (index, item) in
                    self?.txtSelectState.text = item
                    self!.txtSelectState.endEditing(true)
                }
                // collectionviewselect.reloadData()
            }
            else
            {
                AppHelper.showAlert("Alert", message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }
        
    }
    // MARK: - HttpWrapperfetchDataFail
    
    func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError)
    {
        AppHelper.hideLoadingView()
        print(error.debugDescription)
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //        if textField == txtState
        //        {
        //
        //        }
        
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtSelectState
        {
            chooseStateDropDown.show()
            txtSelectState.endEditing(true)
            // call()
            //            self.pickUp(txtState)
            //
            //            myPickerView.tag = 1
        }
        
    }
    
    func GetLatlongFromAddress() {
        
        
        // https://maps.googleapis.com/maps/api/place/autocomplete/json?input=a&types=(cities)&key=AIzaSyC4_bhTW7L0-uoV35GxuZy2yEc-XsI-fV0&components=country:ind
        
        var strGoogleApi = "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyAyJi39vvp3M2K3oF6hmRr2iRwjhxPhkkk&address=\(txtPlaceHere.text!)"
        strGoogleApi = strGoogleApi.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        print(strGoogleApi)
        var urlRequest = URLRequest(url: URL(string: strGoogleApi)!)
        urlRequest.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, resopnse, error) in
            if error == nil {
                
                if let responseData = data {
                    let jsonDict = try? JSONSerialization.jsonObject(with: responseData, options: .mutableContainers)
                    
                    if let dict = jsonDict as? Dictionary<String, AnyObject>{
                        
                        if let results = dict["results"] as? NSArray {
                            print("json == \(results)")
                            if let data = results.object(at: 0) as? NSDictionary
                            {
                                if let nextvalue = data.value(forKey: "geometry") as? NSDictionary
                                {
                                    print("json == \(nextvalue)")
                                    if let locaiton = nextvalue.value(forKey: "location") as? NSDictionary
                                    {
                                        
                                        let lat = locaiton.value(forKey: "lat") as! NSNumber
                                        let long = locaiton.value(forKey: "lng") as! NSNumber
                                        
                                        appDelegate.Bookings.DAddress.Dropoff.Latitude = "\(lat)"
                                        appDelegate.Bookings.DAddress.Dropoff.Longitude = "\(long)"
                                        DispatchQueue.main.async {
                                            
                                        }
                                    }
                                    
                                }
                                
                                //                                if let nextvalue = data.value(forKey: "formatted_address") as? String
                                //                                {
                                //                                    appDelegate.selectedAddress = nextvalue
                                //                                }
                                
                            }
                            
                            //                                var resultsArray1:[Dictionary<String, AnyObject>] = Array()
                            //                                resultsArray1.append(dict)
                            //                                let place = resultsArray1[0]
                            //                                 if let geometry = dict["geometry"] as? [Dictionary<String, AnyObject>] {
                            //                                    let value = place["lat"] as! String
                            //                                    print(geometry)
                            //                                }
                            
                            
                            //                                self.resultsArray.removeAll()
                            //                                for dct in results {
                            //                                    self.resultsArray.append(dct)
                            //                                }
                            //
                            
                            
                        }
                    }
                    
                }
            } else {
                //we have error connection google api
            }
            
            DispatchQueue.main.async {
                // self.items = downloadedItems
                self.MoveToNext()
            }
            
            //  self.Back()
        }
        
        task.resume()
        
        
        
    }
    @objc func searchPlaceFromGoogle(_ textField:UITextField) {
        
        if let searchQuery = textField.text {
            // https://maps.googleapis.com/maps/api/place/autocomplete/json?input=a&types=(cities)&key=AIzaSyC4_bhTW7L0-uoV35GxuZy2yEc-XsI-fV0&components=country:ind
            
           var strGoogleApi = "https://maps.googleapis.com/maps/api/place/autocomplete/json?components=country:ae&key=AIzaSyAyJi39vvp3M2K3oF6hmRr2iRwjhxPhkkk&hasNextPage=true&nextPage()=true&input=\(searchQuery)"
            strGoogleApi = strGoogleApi.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            print(strGoogleApi)
            var urlRequest = URLRequest(url: URL(string: strGoogleApi)!)
            urlRequest.httpMethod = "GET"
            let task = URLSession.shared.dataTask(with: urlRequest) { (data, resopnse, error) in
                if error == nil {
                    
                    if let responseData = data {
                        let jsonDict = try? JSONSerialization.jsonObject(with: responseData, options: .mutableContainers)
                        
                        if let dict = jsonDict as? Dictionary<String, AnyObject>{
                            
                            if let results = dict["predictions"] as? [Dictionary<String, AnyObject>] {
                                print("json == \(results)")
                                // self.resultsArray.removeAll()
                                
                                var arrsarch:[String] = [String]()
                                for dct in results {
                                    // self.resultsArray.append(dct)
                                    arrsarch.append(dct["description"] as! String)
                                    
                                }
                                
                                print(arrsarch)
                                self.chooseArticleDropDown.anchorView = self.txtPlaceHere
                                
                                // Will set a custom with instead of anchor view width
                                //        dropDown.width = 100
                                
                                // By default, the dropdown will have its origin on the top left corner of its anchor view
                                // So it will come over the anchor view and hide it completely
                                // If you want to have the dropdown underneath your anchor view, you can do this:
                                self.chooseArticleDropDown.bottomOffset = CGPoint(x: 0, y: self.txtPlaceHere.bounds.height)
                                
                                // You can also use localizationKeysDataSource instead. Check the docs.
                                self.chooseArticleDropDown.dataSource = arrsarch
                                
                                // Action triggered on selection
                                self.chooseArticleDropDown.selectionAction = { [weak self] (index, item) in
                                    self?.txtPlaceHere.text = item
                                    self!.txtPlaceHere.endEditing(true)
                                }
                                DispatchQueue.main.async {
                                    self.chooseArticleDropDown.show()
                                }
                                
                                self.chooseArticleDropDown.cancelAction = { [unowned self] in
                                    print("Drop down dismissed")
                                    self.txtPlaceHere.endEditing(true)
                                }
                                
                                self.chooseArticleDropDown.willShowAction = { [unowned self] in
                                    print("Drop down will show")
                                }
                            }
                        }
                        
                    }
                } else {
                    //we have error connection google api
                }
            }
            task.resume()
        }
    }
    
    
    // MARK: - isLoginDataValid
    func isLoginDataValid() -> Bool
    {
        
        if (txtPlaceHere.text?.trimmingCharacters(in: CharacterSet.whitespaces).count)! <= 0
        {
            AppHelper.showAlert("Alert", message: pleaseSearchAdress)
            return false
        }
        
        if (txtBuildingNameNO.text?.trimmingCharacters(in: CharacterSet.whitespaces).count)! <= 0
        {
            AppHelper.showAlert("Alert", message: pleasebuldingNameNo)
            return false
        }
        
        return true
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
