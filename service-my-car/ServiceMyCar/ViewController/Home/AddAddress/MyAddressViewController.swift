//
//  MyAddressViewController.swift
//  ServiceMyCar
//
//  Created by admin on 14/06/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import ZAlertView

class MyAddressViewController: UIViewController,UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout,HttpWrapperDelegate {
    
    @IBOutlet weak var addressCollectionView:UICollectionView!
    @IBOutlet weak var addNewAddressButton:UIButton!

    var objGetUserAddress = HttpWrapper()
    var objdeleteUserAddress = HttpWrapper()

    
    var net:NetworkReachabilityManager = NetworkReachabilityManager()!
    var arrAddresslist:NSMutableArray = NSMutableArray()
    var arrayCount = 0
    

    override func viewDidLoad() {
        super.viewDidLoad()
        AppHelper.makeViewCircularWithRespectToHeight(addNewAddressButton, borderColor: .clear, borderWidth: 0.0)
   //     self.getUserAddress()
        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
//        self.getUserAddress()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getUserAddress()
    }
    
    override func viewDidLayoutSubviews() {
        addNewAddressButton.setTitle("Add New".localized(), for: .normal)
    }
    
    
    // MARK:- onClickBtnAddAddress
    @IBAction func onClickBtnAddAddress(_ sender: Any) {
        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "AddAddress_Vc") as! AddAddress_Vc
            nextViewController.addressChrType = "B"
            nextViewController.delegate = self
        
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    // MARK:- onClickBtnEditAddress
    @IBAction func onClickBtnEditAddress(_ sender: UIButton) {
        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "AddAddress_Vc") as! AddAddress_Vc
        let editAddress = self.arrAddresslist.object(at: sender.tag) as! NSDictionary
        nextViewController.editAddress = editAddress
        nextViewController.isEditingAddress = true
        nextViewController.delegate = self
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }

    
    // MARK:- goBack
    
    @objc func onClickbtnDeleteAddress(sender:UIButton){
       
        ZAlertView.showAnimation            = .bounceBottom
        ZAlertView.hideAnimation            = .bounceRight
        ZAlertView.initialSpringVelocity    = 0.9
        ZAlertView.duration                 = 2
        
        let dialog = ZAlertView(title: "select_car_delete".localized(),
                                message: "are_you_sure_you_want_to_delete_this_car".localized(),
                                isOkButtonLeft: true,
                                okButtonText: "app_yes".localized(),
                                cancelButtonText: "app_alert_cancel".localized(),
                                okButtonHandler: { (alertView) -> () in
                                    alertView.dismissAlertView()
                                    
                                    if let id = (self.arrAddresslist.object(at: sender.tag) as! NSDictionary).value(forKey: "intId") as? String{
                                        self.DeleteUserAddress(addressId: id)
                                    }else if let id = (self.arrAddresslist.object(at: sender.tag) as! NSDictionary).value(forKey: "intId") as? NSNumber{
                                        self.DeleteUserAddress(addressId: "\(id)")
                                    }
        },cancelButtonHandler: { (alertView) -> () in
                                    alertView.dismissAlertView()
        })
        dialog.show()
        dialog.allowTouchOutsideToDismiss = true
    }
    
    
    
    
    
    // MARK: - getUserCars API
    
    func getUserAddress()
    {
        if net.isReachable == false{
            
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }else{
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["userId" : nUserId as AnyObject]
            //let header = ["X-API-KEY" : "smc1989"] as [String:AnyObject]
           
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objGetUserAddress = HttpWrapper.init()
            self.objGetUserAddress.delegate = self
            
            self.objGetUserAddress.requestWithparamdictParamPostMethodwithHeaderAndDictGet(url: kgetUserAddress, dictParams: dictParameters, headers: header)
        }
    }
    
    
    // MARK: - DeleteUserCars API
    
    func DeleteUserAddress(addressId:String)
    {
        if net.isReachable == false{
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["userId" : nUserId as AnyObject,
                                                     "addressId" : addressId as AnyObject]
            //let header = ["X-API-KEY" : "smc1989"] as [String:AnyObject]
          //deleteUserAddress?userId=195&addressId=152
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objdeleteUserAddress = HttpWrapper.init()
            self.objdeleteUserAddress.delegate = self
            //self.objdeleteUserAddress.requestWithparamdictParamPostMethodwithHeader(url: kdeleteUserCar, dicsParams: dictParameters, headers: header)
            self.objdeleteUserAddress.requestWithparamdictParamDeleteMethodwithHeader(url: kdeleteUserAddress, dicsParams: dictParameters, headers: header)
        }
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary){
        AppHelper.hideLoadingView()
        print(dicsResponse)
        if wrapper == objGetUserAddress {
            if dicsResponse.value(forKey: "status") as! NSString == "success"{
                arrAddresslist.removeAllObjects()
                
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray{
                    arrAddresslist = tempNames.mutableCopy() as! NSMutableArray
                    self.arrayCount = arrAddresslist.count
                }
                print(arrAddresslist)
                addressCollectionView.reloadData()
            }else{
                // AppHelper.showAlert("Alert", message:dicsResponse.value(forKey: "msg") as! String)
            }
        }else if wrapper == objdeleteUserAddress {
            if dicsResponse.value(forKey: "status") as! NSString == "success"{
                getUserAddress()
            }else{
                AppHelper.showAlert("app_alert_title".localized(), message:dicsResponse.value(forKey: "msg") as! String)
            }
        }
        
    }
    // MARK: - HttpWrapperfetchDataFail
    
    func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError)
    {
        AppHelper.hideLoadingView()
        print(error.debugDescription)
    }

}


extension MyAddressViewController{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if section == 0
        {
            return arrAddresslist.count
        }
        else
        {
            return 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AllAddressCell", for: indexPath) as! AllAddressCell
        
        
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        if languageCode == "en" {
            if indexPath.row == 0{
                AppHelper.makeCircularWithLeftTopTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)
                
            }else if indexPath.row == 1{
                AppHelper.makeCircularWithRightTopTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)
            }else if (indexPath.row%2 == 0){
                if (indexPath.row == arrayCount-1) || indexPath.row == arrayCount-2{
                    AppHelper.makeCircularWithRightBottmTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)
                }else{
                    AppHelper.makeCircularWithLeftTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)
                }
            }else if (indexPath.row%2 == 1){
                if (indexPath.row == arrayCount-1) || indexPath.row == arrayCount-2{
                    AppHelper.makeCircularWithLeftBottmTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)
                }else{
                    AppHelper.makeCircularWithRightTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)
                }
            }
        }else{
            if indexPath.row == 0{
                AppHelper.makeCircularWithRightTopTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)
                
            }else if indexPath.row == 1{
                AppHelper.makeCircularWithLeftTopTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)

            }else if (indexPath.row%2 == 0){
                if (indexPath.row == arrayCount-1) || indexPath.row == arrayCount-2{
                    AppHelper.makeCircularWithLeftBottmTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)
                }else{
                    AppHelper.makeCircularWithRightTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)
                }
            }else if (indexPath.row%2 == 1){
                
                if (indexPath.row == arrayCount-1) || indexPath.row == arrayCount-2{
                    AppHelper.makeCircularWithRightBottmTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)
                }else{
                    AppHelper.makeCircularWithLeftTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)
                }
            }
            
        }
        
        cell.shadowView.backgroundColor = colorBackGroundColor
        cell.viewBack.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).cgColor
        cell.viewBack.backgroundColor = UIColor.white
        cell.viewBack.layer.shadowOffset = CGSize.zero
        cell.viewBack.layer.shadowOpacity = 1.0
        cell.viewBack.layer.shadowRadius = 3.0
        cell.viewBack.layer.masksToBounds = false
        
        cell.deleteButton.tag = indexPath.row
        cell.editButton.tag = indexPath.row
        
        
        cell.deleteButton.addTarget(self, action:#selector(onClickbtnDeleteAddress), for: UIControl.Event.touchUpInside)
        cell.editButton.addTarget(self, action: #selector(onClickBtnEditAddress(_:)), for: .touchUpInside)
        if let address = (arrAddresslist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varAddress") as? String,let buildingName = (arrAddresslist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varBuildingName") as? String,let addtionalInstrct = (arrAddresslist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varAdditionalInstruction") as? String{
            
           cell.fullAddress.text = "\(address) \(buildingName) \(addtionalInstrct)"
        }
        
        if let state = (arrAddresslist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varState") as? String, let country = (arrAddresslist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varCountry") as? String{
            cell.stateCountry.text = "\(state.capitalized), \(country.capitalized)"
        }
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if (indexPath.row).isEven{
            //even Number
            print(indexPath.row)
        }
        else {
            print(indexPath.row)
            // odd Number
        }
        let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
        let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
        let size:CGFloat = (collectionView.frame.size.width - space) / 2.0
        return CGSize(width: size, height: size+20)
    }

}

extension MyAddressViewController:AddAddress_VcDelegate{
    func getAddAddress(_ viewController: UIViewController, address isAdded: Bool) {
        if isAdded{
        self.getUserAddress()
        }
    }
    
    func getCollectionPointAddress(_ viewController: UIViewController, collectAddress address: String, newAddress: NSDictionary) {
    }
    
    func getDropPointAddress(_ viewController: UIViewController, dropAddress address: String, newAddress: NSDictionary) {
        
    }
    
    
}

















class AllAddressCell: UICollectionViewCell {
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var fullAddress: UILabel!
    @IBOutlet weak var stateCountry: UILabel!
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var shadowView: UIView!
}

