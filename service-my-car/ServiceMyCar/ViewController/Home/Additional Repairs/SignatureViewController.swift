//
//  SignatureViewController.swift
//  ServiceMyCar
//
//  Created by admin on 30/07/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import ZAlertView


protocol SignatureViewControllerDelegate {
    func signImageView(_ viewController: UIViewController, isSign sign:Bool, signImage:UIImage)
}


class SignatureViewController: UIViewController,YPSignatureDelegate {
    
@IBOutlet var signatureImageView: YPDrawSignatureView!
    @IBOutlet weak var signHereLabel:UILabel!
    @IBOutlet weak var totalPartsCount : UILabel!
    @IBOutlet weak var totalPartCost : UILabel!
    @IBOutlet weak var tittleLabel:UILabel!
    @IBOutlet weak var clearButton:UIButton!
    @IBOutlet weak var signLabel:UILabel!
    @IBOutlet weak var signAndSendButton:UIButton!

    
    
    var totalParts:String = ""
    var totalCost:String = ""

    
    
    var delegate : SignatureViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.totalPartCost.text = "Total Parts Cost :".localized()+" \(self.totalCost)"+"app_currency".localized()
        self.totalPartsCount.text = "Total Parts :".localized()+" \(self.totalParts)"
        
        self.signatureImageView.delegate = self

        

        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        tittleLabel.text = "Confirm Accepted parts & Repair".localized()
        signLabel.text = "Signature".localized()
        clearButton.setTitle("Clear".localized(), for: .normal)
        signHereLabel.text = "SIGN HERE".localized()
        signAndSendButton.setTitle("Sign & Send".localized(), for: .normal)
        
    }
    
    @IBAction func tappedOnCloseButton(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func onClickClearSign(_ sender: UIButton) {
        self.signatureImageView.clear()
        self.signHereLabel.isHidden = false
        
    }
    
    @IBAction func onClikSendButton(_ sender:UIButton) {
        if let signatureImage = self.signatureImageView.getSignature(scale: 10) {
            self.delegate?.signImageView(self, isSign: true, signImage: signatureImage)
            self.dismiss(animated: true, completion: nil)
        }else{
            ShowAlert(Title: "ServiceMyCar".localized(), Msg: "Please sign the signature!".localized())
        }
        
    }
    
    
    
    
    func didStart(_ view : YPDrawSignatureView) {
        print("Started Drawing")
        self.signHereLabel.isHidden = true
        
    }
    
    
    func didFinish(_ view : YPDrawSignatureView) {
        
        print("Finished Drawing")
        
    }
    
    
    
    // MARK: - Show Toast
    func ShowAlert(Title:String,Msg:String){
        ZAlertView.showAnimation            = .bounceBottom
        ZAlertView.hideAnimation            = .bounceRight
        ZAlertView.initialSpringVelocity    = 0.9
        ZAlertView.duration                 = 2
        ZAlertView.titleColor = colorGreen
        
        let dialog = ZAlertView(title: Title, message: Msg, closeButtonText: "OK".localized(), closeButtonHandler: { (alertView) -> () in
            alertView.dismissAlertView()
        })
        
        dialog.width = self.view.frame.width - 40
        dialog.show()
        dialog.allowTouchOutsideToDismiss = true
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
