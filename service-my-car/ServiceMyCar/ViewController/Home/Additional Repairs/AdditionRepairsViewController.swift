//
//  AdditionRepairsViewController.swift
//  ServiceMyCar
//
//  Created by Parikshit on 19/06/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import ZAlertView


class AdditionRepairsViewController: UIViewController,HttpWrapperDelegate,YPSignatureDelegate {

    
    @IBOutlet weak var aTableView:UITableView!
    
    @IBOutlet weak var sendButton:UIButton!
    @IBOutlet weak var backView:UIView!
    @IBOutlet weak var mainBackView:UIView!
    
     @IBOutlet weak var dateLabel:UILabel!
     @IBOutlet weak var carNameLabel:UILabel!
     @IBOutlet weak var numberPlateLastDigit:UILabel!
     @IBOutlet weak var numberPlateFirstDigit:UILabel!
    
    @IBOutlet weak var plateImageview:UIImageView!

    


    
    
    
    var net:NetworkReachabilityManager = NetworkReachabilityManager()!
    var objGetAdditionalService = HttpWrapper()
    var objGetExtraParts = HttpWrapper()
    var arrExtraPartList:NSMutableArray = NSMutableArray()
    var arrAdditionalService:NSMutableArray = NSMutableArray()
    
    var arrpartsIdValues:[String] = [String]()
    var arrSelected:[Int] = [Int]()
    
    var objExtrapartsApproval = HttpWrapper()
    var orderId = ""
    var totalAmount:Float = 0.0
    
    var fromNotification = false
    var isFromPushNotification = false
    
    var kCost:Float = 0
    var kLCharge :Float = 0
    
    var totalParts: Int = 0
    
    var selParts:[Bool] = [Bool]()
    var isVarprice:[Bool] = [Bool]()
    var isLineTotal:[Bool] = [Bool]()
    
    
    var checkSelectItem = [Bool]()
    
    var order = NSDictionary()
    var isLoading = false

    
    let badgeButton : MIBadgeButton = MIBadgeButton(frame:CGRect(x: -10, y: 15, width: 40, height: 20))



    override func viewDidLoad() {
        super.viewDidLoad()
        

        self.aTableView.register(UINib(nibName: "NoDataCell", bundle: nil), forCellReuseIdentifier:"NoDataCell")
        self.aTableView.dataSource = self
        self.aTableView.delegate = self
        //self.signatureImageView.delegate = self
        self.getUserAdditionalService()
        self.mainBackView.isHidden = true
        
        badgeButton.edgeInsetTop = 10
        badgeButton.setImage(UIImage(named : "notification"), for: .normal)
        print(appDelegate.NCount)
        badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        badgeButton.badgeString = appDelegate.NCount
        if appDelegate.NCount == "0"{
            badgeButton.badgeBackgroundColor = UIColor.clear
            badgeButton.badgeTextColor = UIColor.clear
        }else{
            badgeButton.badgeBackgroundColor = colorGreen
            badgeButton.badgeTextColor = UIColor.white
        }
        badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        let barButton2 : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton2
        //        getPackagesListByCategory()
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)
        setCarDetailOnView()
        
        let button = UIButton.init(type: .custom)
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        
        if languageCode == "en" {
            button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
        }
        else {
            button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
        }
        button.addTarget(self, action:#selector(onClickBackButton(_ :)), for: UIControl.Event.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton

        
    }
    
    override func viewDidLayoutSubviews() {
        backView.backgroundColor = UIColor.clear
        sendButton.setTitle("Sign & Send".localized(), for: .normal)
        self.title = "Additional Repairs".localized()
    }
    
    
    
    
    @objc func NotificationAlert()
    {
        guard let vcs = self.navigationController?.viewControllers else{return}
        let inStack = vcs.contains(where: { (vc) -> Bool in
            return vc is MyNotificationList_Vc
        })
        if inStack{
            self.performSegue(withIdentifier: unwindToNotificationController, sender: self)
        }else{
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
//        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
//        
//        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    // Step 4
    
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            //self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                
                badgeButton.badgeString = appDelegate.NCount
                if appDelegate.NCount == "0"{
                    badgeButton.badgeBackgroundColor = UIColor.clear
                    badgeButton.badgeTextColor = UIColor.clear
                }else{
                    badgeButton.badgeBackgroundColor = colorGreen
                    badgeButton.badgeTextColor = UIColor.white
                }
            }
        }
    }
    
    
    @IBAction func onClickBackButton(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickClearSign(_ sender: UIButton) {
       // self.signatureImageView.clear()
        //self.signHereLabel.isHidden = false

    }
    
    func setCarDetailOnView(){
        if let date = order.value(forKey: "varPackageDate") as? String{
            self.dateLabel.text = date
        }
        if let carName = order.value(forKey: "varCar") as? String,let carModel = order.value(forKey: "varModel") as? String{
            self.carNameLabel.text = carName + " " + carModel
        }
        
        if let plateCode = order.value(forKey: "plateCode") as? String{
            self.numberPlateFirstDigit.text = plateCode
        }else if let plateCode = order.value(forKey: "plateCode") as? NSNumber{
            self.numberPlateFirstDigit.text = "\(plateCode)"
        }
        if let plateNumber = order.value(forKey: "plateNumber") as? String{
            self.numberPlateLastDigit.text = plateNumber
        }else if let plateNumber = order.value(forKey: "plateNumber") as? NSNumber{
            self.numberPlateLastDigit.text = "\(plateNumber)"
        }
        if let stName = order.value(forKey: "carState") as? String{
        self.plateImageview.image = AppHelper.setNumberPlateImage(stateName: stName)
        }
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        if languageCode != "en" {
            carNameLabel.textAlignment = .right
            self.dateLabel.textAlignment = .right
        }
    }
  
    
    @IBAction func onClikSendButton(_ sender:UIButton) {
        
        
        if self.checkSelectItem.contains(where: {($0 == false)}){
            // it exists, do something
            ShowAlert(Title: "ServiceMyCar".localized(), Msg: "Please make all decision!".localized())
            return
        } else {
            let signVc = objHomeSB.instantiateViewController(withIdentifier: "SignatureViewController") as! SignatureViewController
            signVc.totalParts = "\(self.totalParts)"
            signVc.totalCost = "\(self.totalAmount)"
            signVc.delegate = self
            
            let nav = UINavigationController(rootViewController: signVc)
            nav.navigationBar.isHidden = true
            nav.modalPresentationStyle = .overFullScreen
            self.present(nav, animated: true, completion: nil)

            //item could not be found
        }
 
    }
    
    

    func setAllDateForApi(signImage:UIImage){
            if arrExtraPartList.contains(0){
                ShowAlert(Title: "ServiceMyCar".localized(), Msg: "You must have to select all parts!".localized())
            }else{
                var partsCost = Float()
                var labourCost = Float()
                arrpartsIdValues.removeAll()
                for (index, element) in arrSelected.enumerated() {

                    if element == 1
                    {
                        if let name = (arrExtraPartList.object(at: index) as! NSDictionary).value(forKey: "varPrice") as? String
                        {
                            partsCost = partsCost + Float(name)!

                        }

                        var FintLabourHours:Float?
                        var FvarTime:Float?
                        if let intLabourHours = (arrExtraPartList.object(at: index) as! NSDictionary).value(forKey: "intLabourHours") as? NSString
                        {
                            FintLabourHours = Float(intLabourHours as String)
                        }
                        else  if let intLabourHours = (arrExtraPartList.object(at: index) as! NSDictionary).value(forKey: "intLabourHours") as? NSNumber
                        {
                            FintLabourHours = Float(truncating: intLabourHours)
                        }

                        if let paidAmount = (arrExtraPartList.object(at: index) as! NSDictionary).value(forKey: "varTime") as? NSString
                        {
                            FvarTime = Float(paidAmount as String)

                        }
                        else  if let paidAmount = (arrExtraPartList.object(at: index) as! NSDictionary).value(forKey: "varTime") as? NSNumber
                        {
                            FvarTime = Float(truncating: paidAmount)


                        }
                        let value = (Float(FintLabourHours!) * Float(FvarTime!))

                        labourCost = labourCost + value

                        if let name = (arrExtraPartList.object(at: index) as! NSDictionary).value(forKey: "intId") as? String
                        {
                            arrpartsIdValues.append("\(name)||Y")

                        }
                        else if let name = (arrExtraPartList.object(at: index) as! NSDictionary).value(forKey: "intId") as? NSNumber
                        {
                            arrpartsIdValues.append("\(name)||Y")

                        }

                    }
                    else
                    {
                        if let name = (arrExtraPartList.object(at: index) as! NSDictionary).value(forKey: "intId") as? String
                        {
                            arrpartsIdValues.append("\(name)||R")

                        }
                        else if let name = (arrExtraPartList.object(at: index) as! NSDictionary).value(forKey: "intId") as? NSNumber
                        {
                            arrpartsIdValues.append("\(name)||R")

                        }
                    }
//
//                    bllTotalPartCost.text = "Total Parts Cost : \(partsCost) AED"
//                    lblTotalLabourCost.text = "Total Labour Cost : \(labourCost) AED"
//                    lblTotalCost.text = "Total Cost : \(partsCost + labourCost) AED"
                    // do something useful
                    

                }
                
                self.submitExtraPartsApproval(signImage:signImage)

            }
            
            
            print("rlknynviguthu byhgv\(arrpartsIdValues)")

        }


    

}

extension AdditionRepairsViewController: UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            return arrExtraPartList.count == 0 ? 1 : arrExtraPartList.count
       
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 135

    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if arrExtraPartList.count == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath) as! NoDataCell
            cell.messageLabel.text = isLoading ? "No data found".localized() : "Loading...".localized()
            return cell
            
        }else{
        
            var tempLCharge:Float?
            var tempCost:Float?
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "AdditionalRepairsTableViewCell", for: indexPath) as! AdditionalRepairsTableViewCell
            cell.staticTotalCost.text = "Total : ".localized()
            cell.staticCost.text = "Cost : ".localized()
            cell.staticLabourCharge.text = "L Charge : ".localized()
            
            if let name = (arrExtraPartList.object(at: indexPath.row) as! NSDictionary).value(forKey: "varName") as? String{
                cell.nameLabel.text  = name.uppercased()
            }
            
            
            if let cost = (arrExtraPartList.object(at: indexPath.row) as! NSDictionary).value(forKey: "varPrice") as? String{
                
                tempCost = Float(cost as String)
                cell.costLabel.text = cost + "app_currency".localized()

            }else  if let cost = (arrExtraPartList.object(at: indexPath.row) as! NSDictionary).value(forKey: "varPrice") as? NSNumber{
                let tempCharge = Float(truncating: cost)//Int(Float(truncating: cost))
                cell.costLabel.text = "\(tempCharge)"+"app_currency".localized()
                tempCost = Float(truncating: cost)
            }
            
            if let LCharge = (arrExtraPartList.object(at: indexPath.row) as! NSDictionary).value(forKey: "intLabourHours") as? String,let Ltime = (arrExtraPartList.object(at: indexPath.row) as! NSDictionary).value(forKey: "varTime") as? String{
                let labourCharge = Float(LCharge as String)
                let labourTime =  Float(Ltime as String)
                tempLCharge = (Float(labourCharge!) * Float(labourTime!)) // Float(LCharge as String)
                let totalLabourCharge = (Float(labourCharge!) * Float(labourTime!))
                cell.lChargeLabel.text = "\(totalLabourCharge)" + "app_currency".localized()


            }else  if let LCharge = (arrExtraPartList.object(at: indexPath.row) as! NSDictionary).value(forKey: "intLabourHours") as? NSNumber,let Ltime = (arrExtraPartList.object(at: indexPath.row) as! NSDictionary).value(forKey: "varTime") as? NSNumber{
                let tempCharge = Float(truncating: LCharge)//Int(Float(truncating: LCharge))
                let labourTime = Float(truncating: Ltime)
                let totalLabourCharge = (Float(tempCharge) * Float(labourTime))

                cell.lChargeLabel.text = "\(totalLabourCharge)"+"app_currency".localized()
                tempLCharge = totalLabourCharge//Float(truncating: totalLabourCharge)

            }
            
            let kTotal = (tempLCharge! + tempCost!)//Int(tempLCharge! + tempCost!)
            cell.totalCostLabel.text = "\(kTotal)"+"app_currency".localized()
            
            //Accept
            if arrSelected[indexPath.row] == 1
            {
                cell.acceptButton.backgroundColor = colorGreen
                cell.acceptButton.setTitle("Accepted".localized(), for: .normal)

                cell.declineButton.backgroundColor = colorRed.withAlphaComponent(0.5)
                cell.declineButton.setTitle("Decline".localized(), for: .normal)
                
                cell.declineButton.isUserInteractionEnabled = true
                cell.acceptButton.isUserInteractionEnabled = false

            }//Decline
            else if arrSelected[indexPath.row] == 2
            {
                cell.acceptButton.backgroundColor = colorGreen.withAlphaComponent(0.5)
                cell.acceptButton.setTitle("Accept".localized(), for: .normal)

                cell.declineButton.backgroundColor = colorRed
                cell.declineButton.setTitle("Declined".localized(), for: .normal)
                
                cell.declineButton.isUserInteractionEnabled = false
                cell.acceptButton.isUserInteractionEnabled = true

            }//Non of selected
            else
            {
                cell.acceptButton.backgroundColor = colorGreen
                cell.acceptButton.setTitle("Accept".localized(), for: .normal)

                cell.declineButton.backgroundColor = colorRed
                cell.declineButton.setTitle("Decline".localized(), for: .normal)
                
                cell.declineButton.isUserInteractionEnabled = true
                cell.acceptButton.isUserInteractionEnabled = true
            }

           // cell.acceptButton.tag = indexPath.row
           // cell.declineButton.tag = indexPath.row
            cell.declineButton.addTarget(self, action: #selector(self.onClickDecline(_:)), for: .touchUpInside)
            cell.acceptButton.addTarget(self, action: #selector(self.onClickAccept(_:)), for: .touchUpInside)
            

            cell.setNeedsLayout()
            cell.setNeedsDisplay()
            return cell
        }

    }
    
    
    
    // MARK: - onClickPayOrPaid
    @IBAction func onClickAccept(_ sender:UIButton) {
        if let indexpath = sender.tableViewIndexPath(self.aTableView) as IndexPath?{
            self.selParts[indexpath.row] = true
            self.isVarprice[indexpath.row] = true
            self.isLineTotal[indexpath.row] = true
            
            arrSelected[indexpath.row] = 1
            checkSelectItem[indexpath.row] = true

            self.addAndRemoveTotalAmount(index: indexpath.row, isAdd: true)
            self.aTableView.reloadData()
        }
   
    }
    // MARK: - onClickView
    @IBAction func onClickDecline(_ sender:UIButton) {
        if let indexpath = sender.tableViewIndexPath(self.aTableView) as IndexPath?{
            
            arrSelected[indexpath.row] = 2
            checkSelectItem[indexpath.row] = true
            self.addAndRemoveTotalAmount(index: indexpath.row, isAdd: true)
            self.aTableView.reloadData()

        }
//        self.selParts[sender.tag] = false

        
    }
    
    
    
    func addAndRemoveTotalAmount(index:Int,isAdd:Bool) {

        if let cost = (arrExtraPartList.object(at: index) as! NSDictionary).value(forKey: "varPrice") as? String
        {
          if arrSelected[index] == 1{
            kCost =  kCost + Float(cost as String)!
            self.totalParts = self.totalParts + 1
            }else{
            if kCost >= Float(cost as String)! && self.isVarprice[index] == true{
                kCost =  kCost - Float(cost as String)!
                self.isVarprice[index] = false
            }
            
            if self.totalParts > 0 && self.selParts[index] == true{
                self.totalParts = self.totalParts - 1
                self.selParts[index] = false
                }
            }
            
        }else  if let cost = (arrExtraPartList.object(at: index) as! NSDictionary).value(forKey: "varPrice") as? NSNumber{
            if arrSelected[index] == 1{
                self.totalParts = self.totalParts + 1
                kCost = kCost + Float(truncating: cost)
            }else{
                if kCost >= Float(truncating: cost) && self.isVarprice[index] == true {
                kCost = kCost - Float(truncating: cost)
                    self.isVarprice[index] = false
                }
                if self.totalParts > 0 && self.selParts[index] == true{
                    self.totalParts = self.totalParts - 1
                    self.selParts[index] = false

                }
            }
        }
        
        if let LCharge = (arrExtraPartList.object(at: index) as! NSDictionary).value(forKey: "intLabourHours") as? String,let LTime = (arrExtraPartList.object(at: index) as! NSDictionary).value(forKey: "varTime") as? String
        {
            let labourCharge = Float(LCharge as String)!
            let labourTime = Float(LTime as String)!
            let totalLabourCharge = labourCharge*labourTime
           if arrSelected[index] == 1{
            
                kLCharge =   kLCharge + totalLabourCharge //Float(LCharge as String)!

            }else{
            if kLCharge >= totalLabourCharge/*Float(LCharge as String)!*/  && self.isLineTotal[index] == true{

                kLCharge =   kLCharge - totalLabourCharge//Float(LCharge as String)!
                self.isLineTotal[index] = false

            }
            }
            
            
        }else  if let LCharge = (arrExtraPartList.object(at: index) as! NSDictionary).value(forKey: "lineTotal") as? NSNumber,let LTime = (arrExtraPartList.object(at: index) as! NSDictionary).value(forKey: "varTime") as? NSNumber
        {
            let labourCharge = Float(truncating: LCharge)
            let labourTime = Float(truncating: LTime)
            let totalLabourCharge = labourCharge*labourTime
            if arrSelected[index] == 1{
                kLCharge =  kLCharge  + totalLabourCharge//Float(truncating: LCharge)

            }else{
                if kLCharge >= totalLabourCharge /*Float(truncating: LCharge)*/  && self.isLineTotal[index] == true{

                kLCharge =  kLCharge  - totalLabourCharge //Float(truncating: LCharge)
                    self.isLineTotal[index] = false

                }
            }
            
        }
        
        
        totalAmount = kCost + kLCharge
        print(" select array  =========== total amount  \(totalAmount) totalPats \(totalParts)  arraySelect \(arrSelected[index])")

      //  self.totalCostLabel.text = "\(Int(totalAmount)) AED"
       // self.totalPartsLabel.text = "Total \(self.totalParts) Parts"
        
    }
    
    
    
    
    
    
    
    func addAndRemoveTrmpery(index:Int,isAdd:Bool) {
        
        
        if let cost = (arrExtraPartList.object(at: index) as! NSDictionary).value(forKey: "varPrice") as? String
        {
            if arrSelected[index] == 1{
                kCost =  kCost + Float(cost as String)!
                self.totalParts = self.totalParts + 1
            }else{
                if kCost >= Float(cost as String)!  && self.isVarprice[index] == true{
                    kCost =  kCost - Float(cost as String)!
                    self.isVarprice[index] = false
                }
                
                if self.totalParts > 0 && self.selParts[index] == true{
                    self.totalParts = self.totalParts - 1
                    self.selParts[index] = false
                    
                }
                
            }
            
        }else  if let cost = (arrExtraPartList.object(at: index) as! NSDictionary).value(forKey: "varPrice") as? NSNumber
        {
            if arrSelected[index] == 1{
                
                self.totalParts = self.totalParts + 1
                
                kCost = kCost + Float(truncating: cost)
            }else{
                
                if kCost >= Float(truncating: cost) && self.isVarprice[index] == true {
                    
                    kCost = kCost - Float(truncating: cost)
                    self.isVarprice[index] = false
                    
                    
                }
                
                if self.totalParts > 0 && self.selParts[index] == true{
                    
                    
                    self.totalParts = self.totalParts - 1
                    self.selParts[index] = false
                    
                }
            }
        }
        
        if let LCharge = (arrExtraPartList.object(at: index) as! NSDictionary).value(forKey: "lineTotal") as? String
        {
            if arrSelected[index] == 1{
                kLCharge =   kLCharge + Float(LCharge as String)!
                
            }else{
                if kLCharge >= Float(LCharge as String)!  && self.isLineTotal[index] == true{
                    
                    kLCharge =   kLCharge - Float(LCharge as String)!
                    self.isLineTotal[index] = false
                    
                }
            }
            
            
        }else  if let LCharge = (arrExtraPartList.object(at: index) as! NSDictionary).value(forKey: "lineTotal") as? NSNumber
        {
            if arrSelected[index] == 1{
                kLCharge =  kLCharge  + Float(truncating: LCharge)
                
            }else{
                if kLCharge >= Float(truncating: LCharge)  && self.isLineTotal[index] == true{
                    
                    kLCharge =  kLCharge  - Float(truncating: LCharge)
                    self.isLineTotal[index] = false
                    
                }
            }
            
        }
        
        
        totalAmount = kCost + kLCharge
    //    self.totalCostLabel.text = "\(Int(totalAmount)) AED"
       // self.totalPartsLabel.text = "Total \(self.totalParts) Parts"
        
    }
    
    
    
    
    
    
    
    
    func getUserAdditionalService(){
        if net.isReachable == false{
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            var odrID = ""
            if fromNotification{
                if let orderID = order.value(forKey: "intOrderId") as? String{
                    odrID = orderID
                }else if let orderID = order.value(forKey: "intOrderId") as? NSNumber{
                    odrID = "\(orderID)"
                }
            }else if isFromPushNotification {
               
                    odrID = self.orderId

            }else{
                if let orderID = order.value(forKey: "intId") as? String{
                    odrID = orderID
                }else if let orderID = order.value(forKey: "intId") as? NSNumber{
                    odrID = "\(orderID)"
                }
            }
            let dictParameters:[String:AnyObject] = ["orderId" : odrID as AnyObject]
            //
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objGetAdditionalService = HttpWrapper.init()
            self.objGetAdditionalService.delegate = self
            self.objGetAdditionalService.requestWithparamdictParamPostMethodwithHeaderAndDictGet(url: kadditionalService, dictParams: dictParameters, headers: header)
            
        }
        
    }
    
    
    // MARK: - getUserExtraparts API
    
    func getUserExtraparts(orderID:String)
    {
        if net.isReachable == false{
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            let dictParameters:[String:AnyObject] = ["orderId" : orderID as AnyObject]
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objGetExtraParts = HttpWrapper.init()
            self.objGetExtraParts.delegate = self
            self.objGetExtraParts.requestWithparamdictParamPostMethodwithHeaderAndDictGet(url: kgetExtraParts, dictParams: dictParameters, headers: header)
        }
        
    }
    
    func submitExtraPartsApproval(signImage:UIImage){
            if net.isReachable == false{
                let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }else{
                let PartValue = arrpartsIdValues.joined(separator: ",")
                var base64Image = ""
                if let imageString = signImage.base64(format: .png){
                let baseImage = "data:image/png;base64,"+imageString
                    base64Image = baseImage
                }
                let dictParameters:[String:AnyObject] = ["orderId" : orderId as AnyObject,
                                                         "type" : "extraParts" as AnyObject,
                                                         "partsIdValues" : PartValue as AnyObject,
                                                         "signature" : base64Image as AnyObject]
                
                //
                print("URL :--\(dictParameters)")
//                let dataDecoded : Data = Data(base64Encoded: strBase64, options: .ignoreUnknownCharacters)!
                AppHelper.showLoadingView()
                self.objExtrapartsApproval = HttpWrapper.init()
                self.objExtrapartsApproval.delegate = self
                self.objExtrapartsApproval.requestMultipartFormDataWithImageAndVideo(ksubmitExtraPartsApproval, dicsParams: dictParameters, dictHeader: header)
            }
            
        }
    
    
        
    func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary) {
        AppHelper.hideLoadingView()
        print(dicsResponse)
        //self.isLoading = true
        if wrapper == objGetExtraParts {
            if dicsResponse.value(forKey: "status") as! NSString == "success"{
                arrExtraPartList.removeAllObjects()
                
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray{
                    arrExtraPartList = tempNames.mutableCopy() as! NSMutableArray
                }
                print(arrExtraPartList)
                if arrExtraPartList.count != 0 {
                    self.mainBackView.isHidden = false
                }
                for i in arrExtraPartList{
                    arrSelected.append(3)
                    checkSelectItem.append(false)
                    self.selParts.append(false)
                    self.isVarprice.append(false)
                    self.isLineTotal.append(false)
                }
                aTableView.reloadData()
                aTableView.scrollsToTop = true

            }else{
                self.aTableView.reloadData()
            }

            
            
        }else if  wrapper == objGetAdditionalService{
            self.isLoading = true
            if dicsResponse.value(forKey: "status") as! NSString == "success"{
                arrAdditionalService.removeAllObjects()
                
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray{
                    arrAdditionalService = tempNames.mutableCopy() as! NSMutableArray
                    
                    if let ID = (arrAdditionalService.object(at:0) as! NSDictionary).value(forKey: "intId") as? String{
                        self.getUserExtraparts(orderID: ID)
                        self.orderId = ID
                    }
                }
                
                self.order = (arrAdditionalService.object(at:0) as! NSDictionary)
                setCarDetailOnView()
                print(arrExtraPartList)
                aTableView.reloadData()
                aTableView.scrollsToTop = true
                
            }else{
                aTableView.reloadData()
            }
            
            
        }   else if wrapper == objExtrapartsApproval {
            if dicsResponse.value(forKey: "status") as! NSString == "success"{
                let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "Thankyou_Vc") as! Thankyou_Vc
                nextViewController.isFrom = "2"
                self.navigationController?.pushViewController(nextViewController, animated: true)
            }else{
                AppHelper.showAlert("ServiceMyCar".localized(), message:dicsResponse.value(forKey: "msg") as! String)
                self.aTableView.reloadData()
            }
        }
        
            
    
        
        
    }
    
    func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError) {
        AppHelper.hideLoadingView()
        print(error.debugDescription)
    }
    
    
    func didStart(_ view : YPDrawSignatureView) {
        print("Started Drawing")
        self.aTableView.isScrollEnabled = false
       // self.signHereLabel.isHidden = true
        
    }
    
    
    func didFinish(_ view : YPDrawSignatureView) {
        
        print("Finished Drawing")
        self.aTableView.isScrollEnabled = true

    }
    
    
    
    // MARK: - Show Toast
    func ShowAlert(Title:String,Msg:String){

        ZAlertView.showAnimation            = .bounceBottom
        ZAlertView.hideAnimation            = .bounceRight
        ZAlertView.initialSpringVelocity    = 0.9
        ZAlertView.duration                 = 2
        ZAlertView.titleColor = colorGreen
        
        let dialog = ZAlertView(title: Title, message: Msg, closeButtonText: "OK".localized(), closeButtonHandler: { (alertView) -> () in
            alertView.dismissAlertView()
        })
        
        dialog.width = self.view.frame.width - 40
        dialog.show()
        dialog.allowTouchOutsideToDismiss = true
    }
    
}


extension AdditionRepairsViewController:SignatureViewControllerDelegate{
    func signImageView(_ viewController: UIViewController, isSign sign: Bool, signImage: UIImage) {
        if sign{
           setAllDateForApi(signImage:signImage)
            self.aTableView.reloadData()
        }
    }
    
    
}


public enum ImageFormat {
    case png
    case jpeg(CGFloat)
}

extension UIImage {
    public func base64(format: ImageFormat) -> String? {
        var imageData: Data?
        switch format {
        case .png: imageData = self.pngData()
        case .jpeg(let compression): imageData = self.jpegData(compressionQuality: compression)
        }
        return imageData?.base64EncodedString()
    }
}



