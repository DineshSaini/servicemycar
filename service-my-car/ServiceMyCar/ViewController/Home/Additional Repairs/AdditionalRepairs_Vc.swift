//
//  AdditionalRepairs_Vc.swift
//  ServiceMyCar
//
//  Created by baps on 15/02/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import SJSwiftSideMenuController
import Alamofire
import SwiftyJSON

class AdditionalRepairs_Vc:UIViewController ,UIScrollViewDelegate , UITableViewDelegate , UITableViewDataSource ,HttpWrapperDelegate {
    
    
    @IBOutlet weak var btnBookServicePage2: UIButtonX!
    @IBOutlet weak var btnBookServicePage1: UIButtonX!
    
    @IBOutlet weak var tblPage2: UITableView!
    @IBOutlet weak var tblPage1: UITableView!
    @IBOutlet weak var btnPage2: UIButton!
    @IBOutlet weak var btnPage1: UIButton!
    @IBOutlet var viewPage2: UIView!
    @IBOutlet var viewPage1: UIView!
    @IBOutlet weak var ViewPager: UIScrollView!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var lblSwipeWhiteViewConstant: NSLayoutConstraint!
    @IBOutlet weak var lblYouDontHaveAnyAdditionalHistory1: UILabel!
    @IBOutlet weak var lblLetsDoSomething1: UILabel!
    @IBOutlet weak var lblYouDontHaveAnyAdditionalHistory2: UILabel!
    @IBOutlet weak var lblLetsDoSomething2: UILabel!
    
    var scrollPage:UIScrollView = UIScrollView()
    var objGetDelivery = HttpWrapper()
    var objGetCollection = HttpWrapper()
     let badgeButton : MIBadgeButton = MIBadgeButton(frame:CGRect(x: -10, y: 15, width: 40, height: 20))
    var net:NetworkReachabilityManager = NetworkReachabilityManager()!
    var arrHistory:NSMutableArray = NSMutableArray()
    var arrScheduled:NSMutableArray = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        let lbNavTitle = UILabel(frame: CGRect(x: CGFloat(35), y:self.view.bounds.size.width/2-235, width: CGFloat(200), height: CGFloat(40)))
        lbNavTitle.textAlignment = .center
        lbNavTitle.text = "additional_repairs_title".localized()
        lbNavTitle.textColor = Colorblack
        lbNavTitle.font = UIFont(name: Regular, size: 16)
        
        //        let logo = UIImage(named: "AppTopIcon")
        //        let imageView = UIImageView(image:logo)
        //        imageView.contentMode = .scaleAspectFit
        //        self.navigationItem.titleView = imageView
        self.navigationItem.titleView = lbNavTitle
        self.navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.white
        
        let titleDict: NSDictionary = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [NSAttributedString.Key : Any]
        
        let button = UIButton.init(type: .custom)
        //backarrow , menu
        if appDelegate.isFromNotification == true
        {
            button.setImage(UIImage.init(named: "menu"), for: UIControl.State.normal)
        }
        else
        {
            let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
            
            if languageCode == "en" {
                button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
            }
            else {
                button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
            }
        }
        button.addTarget(self, action:#selector(SideMenu), for: UIControl.Event.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        //        self.tblPage1.register(HistoryDatatable_Cell.self, forCellReuseIdentifier: "HistoryDatatable_Cell")
        //        self.tblPage2.register(HistoryDatatable_Cell.self, forCellReuseIdentifier: "HistoryDatatable_Cell")
        
       
        badgeButton.edgeInsetTop = 10
        badgeButton.setImage(UIImage(named : "notification"), for: .normal)
         badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        print(appDelegate.NCount)
        badgeButton.badgeString = appDelegate.NCount

        if appDelegate.NCount == "0"
        {
            badgeButton.badgeBackgroundColor = UIColor.clear
            badgeButton.badgeTextColor = UIColor.clear
        }
        else
        {
            badgeButton.badgeBackgroundColor = colorGreen
            badgeButton.badgeTextColor = UIColor.white
        }
        
        badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        let barButton2 : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton2
        
       
        btnBookServicePage1.addTarget(self, action:#selector(onclickBtnBookServiceNow), for: UIControl.Event.touchUpInside)
        btnBookServicePage2.addTarget(self, action:#selector(onclickBtnBookServiceNow), for: UIControl.Event.touchUpInside)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)
        btnPage1.setTitle("additional_repairs_history".localized(), for: .normal)
        btnPage2.setTitle("additional_repairs_schedule".localized(), for: .normal)
        btnBookServicePage1.setTitle("additional_repairs_book_a_service_now".localized(), for: .normal)
        btnBookServicePage2.setTitle("additional_repairs_book_a_service_now".localized(), for: .normal)
        lblYouDontHaveAnyAdditionalHistory1.text = "additional_repairs_you_dont_have_any_additional_scheduled".localized()
        lblYouDontHaveAnyAdditionalHistory2.text = "additional_repairs_you_dont_have_any_additional_scheduled".localized()
        lblLetsDoSomething1.text = "additional_repairs_lets_do_something".localized()
        lblLetsDoSomething2.text = "additional_repairs_lets_do_something".localized()
    }
    
    
    // MARK: - Google Api Calling
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            //self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                
               
                badgeButton.badgeString = appDelegate.NCount
                if appDelegate.NCount == "0"
                {
                    badgeButton.badgeBackgroundColor = UIColor.clear
                    badgeButton.badgeTextColor = UIColor.clear
                }
                else
                {
                    badgeButton.badgeBackgroundColor = colorGreen
                    badgeButton.badgeTextColor = UIColor.white
                }
            }
        }
    }
    
    // MARK:- goBack
    
    @objc func onclickBtnBookServiceNow()
    {
       appDelegate.loginSuccess()
    }
    
    // MARK:- goBack
    
    @objc func SideMenu()
    {
        
        if appDelegate.isFromNotification == true
        {
            SJSwiftSideMenuController.toggleLeftSideMenu()
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func NotificationAlert()
    {
        guard let vcs = self.navigationController?.viewControllers else{return}
        let inStack = vcs.contains(where: { (vc) -> Bool in
            return vc is MyNotificationList_Vc
        })
        if inStack{
            self.performSegue(withIdentifier: unwindToNotificationController, sender: self)
        }else{
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
//        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
//      
//        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.scrollPage = ViewPager
        var arrview = [viewPage1 , viewPage2]
        // self.view.addSubview(ViewPager)
        for index in 0..<2 {
            let imageView = UIView()
            
            let x = self.view.frame.size.width * CGFloat(index)
            imageView.frame = CGRect(x: x, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            imageView.contentMode = .scaleAspectFit
            imageView.clipsToBounds = true
            let view = arrview[index]
            view?.frame = self.view.frame
            view?.frame.origin.y = 0
            //view?.frame = CGRect(x: 0, y: 0, width: self.viewMain.frame.width, height: self.viewMain.frame.height)
            imageView.addSubview(view!)
            
            ViewPager.contentSize.width = self.view.frame.size.width * CGFloat(index + 1)
            ViewPager.contentSize.height = self.viewMain.frame.height
            //viewPage1.bringSubviewToFront(tblPage1)
            ViewPager.addSubview(imageView)
            //            print(index)
            //            frame.origin.x = self.ViewPager.frame.size.width * CGFloat(index)
            //            frame.size = self.ViewPager.frame.size
            //
            //            let subView = UIView(frame: frame)
            //            subView.backgroundColor = colors[index]
            //            let view = arrview[index]
            //            let x = self.one.frame.size.width * CGFloat(index)
            //            view?.frame = CGRect(x: x, y: 0, width: self.one.frame.width, height: self.one.frame.height)
            //            view?.frame = one.frame
            //            ViewPager.contentSize.width = ViewPager.frame.size.width * CGFloat(index + 1)
            //            //ViewPager.addSubview(view)
            //            print(view)
            //            self.ViewPager.addSubview(view!)
            
            
        }
        tblPage1.dataSource = self
        tblPage1.delegate = self
        tblPage1.reloadData()
        getUserCollectionReports()
        getUserDeliveryReports()
    }
    
    // MARK: - onClickBtnPage2
    
    @IBAction func onClickBtnPage2(_ sender: UIButton) {
        let frame = scrollPage.frame
        let offset:CGPoint = CGPoint(x: CGFloat(1) * frame.size.width, y: 0)
        self.scrollPage.setContentOffset(offset, animated: true)
        lblSwipeWhiteViewConstant.constant = btnPage1.frame.width
        btnPage2.setTitleColor(colorGreen, for: .normal)
        btnPage1.setTitleColor(UIColor.darkGray, for: .normal)
    }
    
    // MARK: - onClickBtnPage1
    @IBAction func onClickBtnPage1(_ sender: UIButton) {
        
        
        let frame = scrollPage.frame
        let offset:CGPoint = CGPoint(x: CGFloat(0) * frame.size.width, y: 0)
        self.scrollPage.setContentOffset(offset, animated: true)
        lblSwipeWhiteViewConstant.constant = 0
        btnPage1.setTitleColor(colorGreen, for: .normal)
        btnPage2.setTitleColor(UIColor.darkGray, for: .normal)
    }
    
    // MARK: - onClickCollectionReport
    @IBAction func onClickCollectionReport(_ sender:UIButton!) {
        // do cool stuff here
        
        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "webview_Vc") as! webview_Vc
        if let varCar = (arrHistory.object(at: sender.tag) as! NSDictionary).value(forKey: "varCar") as? String ,let varModel = (arrHistory.object(at: sender.tag) as! NSDictionary).value(forKey: "varModel") as? String
        {
            nextViewController.strName  = varCar + " " + varModel
        }
        
        if let name = (arrHistory.object(at: sender.tag) as! NSDictionary).value(forKey: "extraPartsPdf") as? String
        {
            nextViewController.URL  = name
        }
        
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    // MARK: - onClickDeliveryReport
    @IBAction func onClickDeliveryReport(_ sender:UIButton!) {
        // do cool stuff here
        
        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "ExtraParts_Vc") as! ExtraParts_Vc
        
        
       
//        if let DicData = arrScheduled.object(at: sender.tag) as? NSDictionary
//        {
//            nextViewController.Data  = DicData
//        }
//
        if let name = (arrScheduled.object(at: sender.tag) as! NSDictionary).value(forKey: "intId") as? String
        {
            nextViewController.orderId  = name
            print(name)
        }
        
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    // MARK: - Tableview Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblPage1
        {
            return arrHistory.count
        }
        else
        {
            return arrScheduled.count
        }
    }
    
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblPage1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryDatatable_Cell" , for: indexPath) as! HistoryDatatable_Cell
            
            if let name = (arrHistory.object(at: indexPath.row) as! NSDictionary).value(forKey: "varPackageDate") as? String
            {
                cell.lblDate.text  = name
            }
            
            if let plateCode = (arrHistory.object(at: indexPath.row) as! NSDictionary).value(forKey: "plateCode") as? String
            {
                cell.lblPlatecode.text  = plateCode
            }
            
            if let name = (arrHistory.object(at: indexPath.row) as! NSDictionary).value(forKey: "varYear") as? String
            {
                cell.lblYear.text  = name
            }
            cell.btnView.tag = indexPath.row
            cell.btnView.addTarget(self, action: #selector(self.onClickCollectionReport(_:)), for: .touchUpInside)
            
            
            if let varCar = (arrHistory.object(at: indexPath.row) as! NSDictionary).value(forKey: "varCar") as? String ,let varModel = (arrHistory.object(at: indexPath.row) as! NSDictionary).value(forKey: "varModel") as? String
            {
                cell.lblCarName.text  = varCar + " " + varModel
            }
            
            
            cell.btnView.setTitle("additional_repairs_view".localized(), for: .normal)
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryDatatable_Cell" , for: indexPath) as! HistoryDatatable_Cell
            
            if let name = (arrScheduled.object(at: indexPath.row) as! NSDictionary).value(forKey: "varPackageDate") as? String
            {
                cell.lblDate.text  = name
            }
            
            if let plateCode = (arrScheduled.object(at: indexPath.row) as! NSDictionary).value(forKey: "plateCode") as? String
            {
                cell.lblPlatecode.text  = plateCode
            }
            
            if let name = (arrScheduled.object(at: indexPath.row) as! NSDictionary).value(forKey: "plateNumber") as? String
            {
                cell.lblYear.text  = name
            }
            
            cell.btnView.tag = indexPath.row
            cell.btnView.addTarget(self, action: #selector(self.onClickDeliveryReport(_:)), for: .touchUpInside)
            
            if let varCar = (arrScheduled.object(at: indexPath.row) as! NSDictionary).value(forKey: "varCar") as? String ,let varModel = (arrScheduled.object(at: indexPath.row) as! NSDictionary).value(forKey: "varModel") as? String
            {
                cell.lblCarName.text  = varCar + " " + varModel
            }
            
            cell.btnView.setTitle("additional_repairs_view".localized(), for: .normal)
            
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblPage1
        {
            
        }
        else
        {
            
        }
    }
    
    // MARK: - scrollViewDidScroll
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == ViewPager
        {
            self.scrollPage = scrollView
            //                let verticalIndicator: UIImageView = (scrollView.subviews[(scrollView.subviews.count - 1)] as! UIImageView)
            //                verticalIndicator.backgroundColor = UIColor(red: 211/255.0, green: 138/255.0, blue: 252/255.0, alpha: 1)
            let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
            
            if pageNumber > 0
            {
                lblSwipeWhiteViewConstant.constant = scrollView.contentOffset.x/2
                btnPage2.setTitleColor(colorGreen, for: .normal)
                btnPage1.setTitleColor(UIColor.darkGray, for: .normal)
            }
            else
            {
                lblSwipeWhiteViewConstant.constant = scrollView.contentOffset.x/2
                btnPage1.setTitleColor(colorGreen, for: .normal)
                btnPage2.setTitleColor(UIColor.darkGray, for: .normal)
            }
            print(pageNumber)
            
            //        verticalIndicator.backgroundColor = UIColor.blue
        }
        
        
    }
    
    
    // MARK: - getUserCollectionReports API
    
    func getUserCollectionReports()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["userId" : nUserId as AnyObject]
            //            let dictHeaaderPera:[String:AnyObject] = [
            //                "platform" : "IOS" as AnyObject,
            //                "modelNumber" : "ML2000" as AnyObject
            //                ,"serialNumber" :"1234567891" as AnyObject,
            //                 "deviceId" : "12345" as AnyObject]
            //
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objGetCollection = HttpWrapper.init()
            self.objGetCollection.delegate = self
            self.objGetCollection.requestWithparamdictParamPostMethod(url: kgetPreviousExtraPartsReport, dicsParams: dictParameters)
        }
        
    }
    
    // MARK: - getUserDeliveryReports API
    
    func getUserDeliveryReports()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["userId" : nUserId as AnyObject]
            //            let dictHeaaderPera:[String:AnyObject] = [
            //                "platform" : "IOS" as AnyObject,
            //                "modelNumber" : "ML2000" as AnyObject
            //                ,"serialNumber" :"1234567891" as AnyObject,
            //                 "deviceId" : "12345" as AnyObject]
            //
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objGetDelivery = HttpWrapper.init()
            self.objGetDelivery.delegate = self
            self.objGetDelivery.requestWithparamdictParamPostMethod(url: kadditionalService, dicsParams: dictParameters)
        }
        
    }
    
    
    // MARK: - HttpWrapperfetchDataSuccess
    
    func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary)
    {
        AppHelper.hideLoadingView()
        print(dicsResponse)
        if wrapper == objGetCollection {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                arrHistory.removeAllObjects()
                
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray
                {
                    arrHistory = tempNames.mutableCopy() as! NSMutableArray
                }
                print(arrHistory)
                tblPage1.isHidden = false
                btnBookServicePage1.isHidden = true
                tblPage1.reloadData()
            }
            else
            {
                tblPage1.isHidden = true
                btnBookServicePage1.isHidden = false
                //AppHelper.showAlert("Alert", message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }
        else if wrapper == objGetDelivery {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                arrScheduled.removeAllObjects()
                
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray
                {
                    arrScheduled = tempNames.mutableCopy() as! NSMutableArray
                }
                print(arrScheduled)
                tblPage2.isHidden = false
                btnBookServicePage2.isHidden = true
                tblPage2.reloadData()
            }
            else
            {
                tblPage2.isHidden = true
                btnBookServicePage2.isHidden = false
               // AppHelper.showAlert("Alert", message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }
        
        
    }
    // MARK: - HttpWrapperfetchDataFail
    
    func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError)
    {
        AppHelper.hideLoadingView()
        print(error.debugDescription)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
