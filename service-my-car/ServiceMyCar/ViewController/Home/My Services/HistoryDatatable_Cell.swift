//
//  HistoryDatatable_ swift
//  ServiceMyCar
//
//  Created by baps on 12/02/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit

class HistoryDatatable_Cell: UITableViewCell {

    @IBOutlet weak var lblPlatecode: UILabel!
    @IBOutlet weak var viewPlateWidthConstant: NSLayoutConstraint!
    @IBOutlet weak var PlateNoWidthContant: NSLayoutConstraint!
    @IBOutlet weak var btnView: UIButtonX!
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var lblServiceName: UILabel!
    @IBOutlet weak var lblYear: UILabel!
    @IBOutlet weak var lblCarName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
         viewBack.layer.cornerRadius = 4.0
         viewBack.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
         viewBack.backgroundColor = UIColor.white
         viewBack.layer.shadowOffset = CGSize.zero
         viewBack.layer.shadowOpacity = 2.0
         viewBack.layer.shadowRadius = 3.0
         viewBack.layer.masksToBounds = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
