//
//  CategoryDocViewController.swift
//  ServiceMyCar
//
//  Created by Parikshit on 19/06/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import TelrSDK

class CategoryDocViewController: UIViewController {
    @IBOutlet weak var aCollectionView:UICollectionView!
    @IBOutlet weak var carModelLadel:UILabel!

    var firstArray = ["Collection","Delivery","Service","Additional","Health","INOICE"]
    
    var carName = ""
    var carModel = ""
    var order = NSDictionary()
    var fromNotification = false
    var isOpenFromPushNotification = false
    
    
    var objGetCategoryDoc = HttpWrapper()
    var arrHistory:NSMutableArray = NSMutableArray()
    
    var orderId:String = ""
    var servicetype:String = ""
    
    //*** Payment Section Variable ***//
    
    
    let KEY:String = "9Std-fqjHP#Q6dW2"  // TODO fill key
    let STOREID:String = "20796"         // TODO fill store id
    var EMAIL:String = ""//isLive ? "accounts@servicemycar.ae" : "Accounts@servicemycar.ae"  // TODO fill email
    var paymentRequest:PaymentRequest?
    let UserLName = UserDefaults.standard.value(forKey: kUserName) as! String
    let UserEmail = UserDefaults.standard.value(forKey: KUserLoginEmail) as! String
    let UserPhoneNo = UserDefaults.standard.value(forKey: kUserLoginPhone) as! String
    var strPaidAmaunt:String = ""

    //*** Payment Section Variable ***//

    var net:NetworkReachabilityManager = NetworkReachabilityManager()!

    let badgeButton : MIBadgeButton = MIBadgeButton(frame:CGRect(x: -10, y: 15, width: 40, height: 20))


    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.title = carName.lowercased()
        if let UserEmail = UserDefaults.standard.value(forKey: KUserLoginEmail) as? String{
            self.EMAIL = UserEmail
        }
        self.carModelLadel.text = carModel
        self.aCollectionView.register(UINib(nibName: "CategoryDocCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CategoryDocCollectionViewCell")
        self.aCollectionView.register(UINib(nibName: "DocumentCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DocumentCollectionViewCell")

        self.aCollectionView.register(UINib(nibName: "EmptyCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "EmptyCollectionViewCell")
        self.aCollectionView.dataSource = self
        self.aCollectionView.delegate = self
        
        badgeButton.edgeInsetTop = 10
        badgeButton.setImage(UIImage(named : "notification"), for: .normal)
        print(appDelegate.NCount)
        badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        badgeButton.badgeString = appDelegate.NCount
        if appDelegate.NCount == "0"{
            badgeButton.badgeBackgroundColor = UIColor.clear
            badgeButton.badgeTextColor = UIColor.clear
        }else{
            badgeButton.badgeBackgroundColor = colorGreen
            badgeButton.badgeTextColor = UIColor.white
        }
        badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        let barButton2 : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton2
        //        getPackagesListByCategory()
        
        self.getAllDocumentFromServer()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)
        
        let button = UIButton.init(type: .custom)
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        
        if languageCode == "en" {
            button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
        }
        else {
            button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
        }
        button.addTarget(self, action:#selector(onClickBackButton(_ :)), for: UIControl.Event.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton


    }
    
    
    @IBAction func onClickBackButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
 
    @objc func NotificationAlert()
    {
        guard let vcs = self.navigationController?.viewControllers else{return}
        let inStack = vcs.contains(where: { (vc) -> Bool in
            return vc is MyNotificationList_Vc
        })
        if inStack{
            self.performSegue(withIdentifier: unwindToNotificationController, sender: self)
        }else{
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        //        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
        //
        //        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    // Step 4
    
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            //self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                
                badgeButton.badgeString = appDelegate.NCount
                if appDelegate.NCount == "0"
                {
                    badgeButton.badgeBackgroundColor = UIColor.clear
                    badgeButton.badgeTextColor = UIColor.clear
                }
                else
                {
                    badgeButton.badgeBackgroundColor = colorGreen
                    badgeButton.badgeTextColor = UIColor.white
                }
            }
        }
    }
    
    
    private func preparePaymentRequest() -> PaymentRequest{
        
        if let UserEmail = UserDefaults.standard.value(forKey: KUserLoginEmail) as? String{
            self.EMAIL = UserEmail
        }

        
        let paymentReq = PaymentRequest()
        paymentReq.key = KEY
        paymentReq.store = STOREID
        paymentReq.appId = "123456789"
        paymentReq.appName = "ServiceMyCar"
        paymentReq.appUser = "123456"
        paymentReq.appVersion = "0.0.1"
        paymentReq.transTest = Istesting
        paymentReq.transType = "sale"
        paymentReq.transClass = "paypage"
        paymentReq.transCartid = String(arc4random())
        paymentReq.transDesc = isLive ? "Booking on Servicemycar.ae" : "service payment"
        paymentReq.transCurrency = "AED"
        
        appDelegate.Bookings.telr_value = String(self.strPaidAmaunt)
        paymentReq.transAmount = String(self.strPaidAmaunt)
        paymentReq.transLanguage = "en"
        paymentReq.billingEmail = EMAIL
        
        let name = UserLName.split(separator: " ")
        
        
        paymentReq.billingFName = "\(name[0])"
        paymentReq.billingLName = "\(name[1])"
        paymentReq.billingTitle = "Mr"
        paymentReq.city = "Dubai"
        paymentReq.country = "AE"
        paymentReq.region = "Dubai"
        paymentReq.address = "line 1"
        paymentReq.billingPhone = UserPhoneNo
        
        return paymentReq
    }
    
    func getAllDocumentFromServer(){
        if net.isReachable == false{
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            var orderID = ""
            var service = "S"
            if fromNotification{
                if let orderId = order.value(forKey: "intOrderId") as? String{
                    orderID = orderId
                }
                if let servicetype = order.value(forKey: "serviceType") as? String{
                   service = servicetype
                }
            }else if isOpenFromPushNotification{
                orderID = self.orderId
                service = self.servicetype
                self.title = "OrderID : ".localized() + "\(self.orderId)"
            }else{
                if let orderId = order.value(forKey: "intId") as? String{
                    orderID = orderId
                    self.title = "OrderID : ".localized() + "\(orderId)"
                }
                if let servicetype = order.value(forKey: "serviceType") as? String{
                    service = servicetype
                }

            }
            if service.isEmpty{
                service = "S"
            }
            
            var languageCode = ""
            if UserDefaults.standard.value(forKey: kAppLanguage) != nil {
                languageCode = UserDefaults.standard.value(forKey: kAppLanguage) as! String
            }
            
            
           // let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["orderId" : orderID as AnyObject,"lang" : languageCode as AnyObject,"serviceType" : service as AnyObject]
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objGetCategoryDoc = HttpWrapper.init()
            self.objGetCategoryDoc.delegate = self
            self.objGetCategoryDoc.requestWithparamdictParamMethodwithHeaderAndDictGet(url: kgetAllDocument, dictParams: dictParameters, headers: header)
            
        }
        
    }


}


extension CategoryDocViewController: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    // MARK:- CollectionView Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrHistory.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if arrHistory.count == 0{
             let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EmptyCollectionViewCell", for: indexPath) as! EmptyCollectionViewCell
            return cell
        }else{
            
            var documentName = ""
            if let docName = (arrHistory.object(at: indexPath.item) as! NSDictionary).value(forKey: "title") as? String{
               documentName = docName
            }
            print("documentName is ====  \(documentName)")
            if documentName == "INVOICE" || documentName == "فاتورة"{
                var paidAmountRemain = 0.0
                if let paidAmount = (arrHistory.object(at: indexPath.item) as! NSDictionary).value(forKey: "totaldue") as? String{
                    if paidAmount > "0"{
                        paidAmountRemain = Double(paidAmount) ?? 0.0
                    }
                    
                }else if let paidAmount = (arrHistory.object(at: indexPath.item) as! NSDictionary).value(forKey: "totaldue") as? NSNumber{
                    if Float(truncating: paidAmount) > 0.0{
                        paidAmountRemain = Double(truncating: paidAmount)
                    }
                }
                
                if paidAmountRemain != 0.0{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryDocCollectionViewCell", for: indexPath) as! CategoryDocCollectionViewCell
                    let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
                    if languageCode == "en" {
                    AppHelper.makeCircularWithLeftBottmTwoCornerRadius(cell.backView, cornerRadius: 20.0)
                    }else{
                        AppHelper.makeCircularWithRightBottmTwoCornerRadius(cell.backView, cornerRadius: 20.0)
                    }
                    if let docName = (arrHistory.object(at: indexPath.item) as! NSDictionary).value(forKey: "title") as? String{
                        cell.firstLbl.text = docName
                        if docName == "INVOICE" || docName == "فاتورة"{
                            if let paidAmount = (arrHistory.object(at: indexPath.item) as! NSDictionary).value(forKey: "totaldue") as? String{
                                if paidAmount > "0"{
                                    cell.firstBtn.isHidden = false
                                    cell.firstBtn.backgroundColor = colorRed
                                    cell.firstBtn.setTitle("PAY".localized(), for: .normal)
                                    
                                }else{
                                    cell.firstBtn.isHidden = true
                                }
                                
                            }else if let paidAmount = (arrHistory.object(at: indexPath.item) as! NSDictionary).value(forKey: "totaldue") as? NSNumber{
                                if Float(truncating: paidAmount) > 0.0{
                                    cell.firstBtn.isHidden = false
                                    cell.firstBtn.backgroundColor = colorRed
                                    cell.firstBtn.setTitle("PAY".localized(), for: .normal)
                                    
                                }else{
                                    cell.firstBtn.isHidden = true
                                }
                                
                            }
                        }
                    }
                    
                    cell.secondBtn.setTitle("View".localized(), for: .normal)
                    cell.firstBtn.addTarget(self, action: #selector(onClickPayButton(_:)), for: .touchUpInside)
                    cell.secondBtn.addTarget(self, action: #selector(onClicksecondButton(_:)), for: .touchUpInside)
                    cell.contentView.backgroundColor = colorBackGroundColor
                    cell.shadowView.backgroundColor = colorBackGroundColor
                    cell.backView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).cgColor
                    cell.backView.backgroundColor = UIColor.white
                    cell.backView.layer.shadowOffset = CGSize.zero
                    cell.backView.layer.shadowOpacity = 1.0
                    cell.backView.layer.shadowRadius = 3.0
                    cell.backView.layer.masksToBounds = false

                    return cell
                }else{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DocumentCollectionViewCell", for: indexPath) as! DocumentCollectionViewCell
                    let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
                    if languageCode == "en" {
                    AppHelper.makeCircularWithLeftBottmTwoCornerRadius(cell.backView, cornerRadius: 20.0)
                    }else{
                        AppHelper.makeCircularWithRightBottmTwoCornerRadius(cell.backView, cornerRadius: 20.0)
                    }
                    if let docName = (arrHistory.object(at: indexPath.item) as! NSDictionary).value(forKey: "title") as? String{
                        cell.firstLbl.text = docName
                    }
                    cell.secondBtn.setTitle("View".localized(), for: .normal)
                    cell.secondBtn.addTarget(self, action: #selector(onClicksecondButton(_:)), for: .touchUpInside)
                    cell.contentView.backgroundColor = colorBackGroundColor
                    cell.shadowView.backgroundColor = colorBackGroundColor
                    cell.backView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).cgColor
                    cell.backView.backgroundColor = UIColor.white
                    cell.backView.layer.shadowOffset = CGSize.zero
                    cell.backView.layer.shadowOpacity = 1.0
                    cell.backView.layer.shadowRadius = 3.0
                    cell.backView.layer.masksToBounds = false

                    return cell
                }
            }else{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DocumentCollectionViewCell", for: indexPath) as! DocumentCollectionViewCell
                cell.secondBtn.setTitle("View".localized(), for: .normal)
                
                let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
                if languageCode == "en" {
                
                switch indexPath.row{
                case 0:
                    AppHelper.makeCircularWithLeftTopTwoCornerRadius(cell.backView, cornerRadius: 20.0)
                    break;
                case 1:
                    AppHelper.makeCircularWithRightTopTwoCornerRadius(cell.backView, cornerRadius: 20.0)
                    break;
                case 2:
                    AppHelper.makeCircularWithLeftTwoCornerRadius(cell.backView, cornerRadius: 20.0)
                    break;
                case 3:
                    AppHelper.makeCircularWithRightTwoCornerRadius(cell.backView, cornerRadius: 20.0)
                    break;
                case 4:
                    AppHelper.makeCircularWithRightBottmTwoCornerRadius(cell.backView, cornerRadius: 20.0)
                    break;
                case 5:
                    AppHelper.makeCircularWithLeftBottmTwoCornerRadius(cell.backView, cornerRadius: 20.0)
                    break;
                default:
                    break;
                }
                }else{
                    switch indexPath.row{
                    case 0:
                        AppHelper.makeCircularWithRightTopTwoCornerRadius(cell.backView, cornerRadius: 20.0)

                        break;
                    case 1:
                        AppHelper.makeCircularWithLeftTopTwoCornerRadius(cell.backView, cornerRadius: 20.0)

                        break;
                    case 2:
                        AppHelper.makeCircularWithRightTwoCornerRadius(cell.backView, cornerRadius: 20.0)

                        break;
                    case 3:
                        AppHelper.makeCircularWithLeftTwoCornerRadius(cell.backView, cornerRadius: 20.0)

                        break;
                    case 4:
                        AppHelper.makeCircularWithLeftBottmTwoCornerRadius(cell.backView, cornerRadius: 20.0)

                        break;
                    case 5:
                        
                        AppHelper.makeCircularWithRightBottmTwoCornerRadius(cell.backView, cornerRadius: 20.0)

                        break;
                    default:
                        break;
                    }

                }
                
                if let docName = (arrHistory.object(at: indexPath.item) as! NSDictionary).value(forKey: "title") as? String{
                    cell.firstLbl.text = docName
                }
                

                
                cell.contentView.backgroundColor = colorBackGroundColor
                cell.shadowView.backgroundColor = colorBackGroundColor
                cell.backView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).cgColor
                cell.backView.backgroundColor = UIColor.white
                cell.backView.layer.shadowOffset = CGSize.zero
                cell.backView.layer.shadowOpacity = 1.0
                cell.backView.layer.shadowRadius = 3.0
                cell.backView.layer.masksToBounds = false
                cell.secondBtn.addTarget(self, action: #selector(onClicksecondButton(_:)), for: .touchUpInside)
                
                return cell
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if arrHistory.count == 0{
            return CGSize(width: collectionView.frame.size.width, height: 100)
        }else{
        
        //        if indexPath.row < 2
        //        {
        let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
        let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
        let size:CGFloat = (collectionView.frame.size.width - space) / 2.0
        return CGSize(width: size, height: collectionView.frame.size.height/3 - 15)
        }

        
    }
    
    
    @IBAction func onClickfirstButton(_ sender:UIButton) {
        
    }
    
    @IBAction func onClicksecondButton(_ sender:UIButton) {
        if let indexPath = sender.collectionViewIndexPath(self.aCollectionView) as IndexPath?{
            
            if let docName = (arrHistory.object(at: indexPath.item) as! NSDictionary).value(forKey: "title") as? String{
                if docName != "INVOICE" || docName != "فاتورة"{
                  //  if docName.lowercased() == "ADDITIONAL REPAIRS".lowercased(){
                        if let isSubmit = (arrHistory.object(at: indexPath.item) as! NSDictionary).value(forKey: "isSubmitted") as? String{
                            if isSubmit == "0"{
                                self.navigatetoAdditionalRepairsScreen()
                            }else{
                                self.navigateToWebView(indexPath: indexPath.item)
                            }
                        }else if let isSubmit = (arrHistory.object(at: indexPath.item) as! NSDictionary).value(forKey: "isSubmitted") as? NSNumber{
                            if isSubmit == 0{
                                self.navigatetoAdditionalRepairsScreen()
                            }else{
                            self.navigateToWebView(indexPath: indexPath.item)
                            }
                        }else{
                            self.navigateToWebView(indexPath: indexPath.item)
                        }
//                    }else{
//                        self.navigateToWebView(indexPath: indexPath.item)
//                    }
                }else if docName == "INVOICE" || docName == "فاتورة"{
                    self.navigateToWebView(indexPath: indexPath.item)
                    //self.navigatetoAdditionalRepairsScreen()
                }
            }
        }

    }
    
    
    
    @IBAction func onClickPayButton(_ sender:UIButton){
        if let indexPath = sender.collectionViewIndexPath(self.aCollectionView) as IndexPath?{
            if let docName = (arrHistory.object(at: indexPath.item) as! NSDictionary).value(forKey: "title") as? String{
                if docName == "INVOICE" || docName == "فاتورة"{
                    if let paidAmount = (arrHistory.object(at: indexPath.item) as! NSDictionary).value(forKey: "totaldue") as? String{
                        if paidAmount > "0"{
                           self.navigateToPaymentViewController(indexPath:indexPath.item)
                        }else{
                        }
                    }else if let paidAmount = (arrHistory.object(at: indexPath.item) as! NSDictionary).value(forKey: "totaldue") as? NSNumber{
                        if Float(truncating: paidAmount) > 0.0{
                            
                            self.navigateToPaymentViewController(indexPath:indexPath.item)
                        }else{
                        }
                        
                    }
                    
                }
            }
        }
        
    }
    
    func navigateToPaymentViewController(indexPath:Int){
        
        if let paidAmount = (arrHistory.object(at: indexPath) as! NSDictionary).value(forKey: "totaldue") as? NSNumber{
            strPaidAmaunt = "\(paidAmount)"
        }
        
        if fromNotification{
            if let orderId = order.value(forKey: "intOrderId") as? String{
            appDelegate.ExtraInvoiceOderId = orderId
            }
        }else if isOpenFromPushNotification{
            appDelegate.ExtraInvoiceOderId = self.orderId
        }else{
            if let orderId = order.value(forKey: "intId") as? String{
                appDelegate.ExtraInvoiceOderId = orderId
            }
        }
        
        if let paidAmount = order.value(forKey: "intId") as? NSNumber{
            appDelegate.ExtraInvoiceOderId = "\(paidAmount)"
        }else if let paidAmount = order.value(forKey: "intId") as? String{
            appDelegate.ExtraInvoiceOderId = paidAmount
        }
        let nextViewController = objMain.instantiateViewController(withIdentifier: "TelrController") as! TelrController
        appDelegate.Bookings.Price.total = String(self.strPaidAmaunt)
        paymentRequest = preparePaymentRequest()
        appDelegate.isPayExtraInvoice = "2"
        nextViewController.paymentRequest = paymentRequest!
        nextViewController.navigationItem.leftBarButtonItem?.isEnabled = true
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }

    
    func navigateToWebView(indexPath:Int) {
        let viewOrder = (arrHistory.object(at: indexPath) as! NSDictionary)
        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "webview_Vc") as! webview_Vc
            nextViewController.indexPath = indexPath
        if fromNotification{
            nextViewController.fromInvoiceReceived = self.fromNotification
        }else{
            nextViewController.order = order
        }
        nextViewController.viewOrder = viewOrder
        self.navigationController?.pushViewController(nextViewController, animated: true)

    }
    
    
    func navigatetoAdditionalRepairsScreen() {
    let nextVC = objHomeSB.instantiateViewController(withIdentifier: "AdditionRepairsViewController") as! AdditionRepairsViewController
       // let nextVC = objHomeSB.instantiateViewController(withIdentifier: "ExtraParts_Vc") as! ExtraParts_Vc

        nextVC.order = self.order
        self.navigationController?.pushViewController(nextVC, animated: true)

    }
}


extension CategoryDocViewController:HttpWrapperDelegate{
    func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary) {
        AppHelper.hideLoadingView()
        print(dicsResponse)
        if wrapper == objGetCategoryDoc {
            if dicsResponse.value(forKey: "status") as? NSString == "success"{
                arrHistory.removeAllObjects()
                
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray{
                    arrHistory = tempNames.mutableCopy() as! NSMutableArray
                    
                }
                print(arrHistory)
                self.aCollectionView.reloadData()
                
            }
            else{
            }
        }
        
    }
    
    func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError) {
        AppHelper.hideLoadingView()
        print(error.debugDescription)
    }

    
}




extension UIView {
    
    func dropShadow(shadowColor: UIColor = .gray, shadowRadius: CGFloat = 2.0, shadowOpacity:Float = 0.4, shadowOffset:CGSize = CGSize(width: 2, height: 2)){
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.layer.cornerRadius).cgPath
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.shadowOpacity = shadowOpacity
        self.layer.shadowOffset = shadowOffset
        self.layer.shadowRadius = shadowRadius
        self.layer.masksToBounds = false
    }
}

