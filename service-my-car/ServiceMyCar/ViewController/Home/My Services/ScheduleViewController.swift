//
//  ScheduleViewController.swift
//  ServiceMyCar
//
//  Created by Parikshit on 23/06/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class ScheduleViewController:  UIViewController,UITableViewDataSource,UITableViewDelegate,HttpWrapperDelegate {


@IBOutlet weak var aTableView:UITableView!

var objGetHistory = HttpWrapper()
var arrHistory:NSMutableArray = NSMutableArray()
var net:NetworkReachabilityManager = NetworkReachabilityManager()!
    var isLoading = false

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor(red: 249.0/255.0, green: 249.0/255.0, blue: 249.0/255.0, alpha: 1.0)
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: UIControl.Event.valueChanged)
        return refreshControl
    }()
    

override func viewDidLoad() {
    super.viewDidLoad()
    self.aTableView.addSubview(refreshControl)

    self.aTableView.register(UINib(nibName: "MyServicePreviousTableViewCell", bundle: nil), forCellReuseIdentifier:"MyServicePreviousTableViewCell")
    self.aTableView.register(UINib(nibName: "NoDataCell", bundle: nil), forCellReuseIdentifier:"NoDataCell")
    self.aTableView.dataSource = self
    self.aTableView.delegate = self
    self.getUserCurrentHistory()

    // Do any additional setup after loading the view.
}
    
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.getUserCurrentHistory()
        
    }


func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    return arrHistory.count == 0 ? 1 : arrHistory.count
    
    
}

func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 160
}

func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    
    if arrHistory.count == 0 {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath) as! NoDataCell
        cell.messageLabel.text = isLoading ? "No data found".localized() : "Loading...".localized()
        return cell
        
    }else{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyServicePreviousTableViewCell", for: indexPath) as! MyServicePreviousTableViewCell
        AppHelper.makeCircularWithRightTopTwoCornerRadius(cell.backView, cornerRadius: 25.0)
        cell.backView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).cgColor
        cell.backView.backgroundColor = UIColor.white
        cell.backView.layer.shadowOffset = CGSize.zero
        cell.backView.layer.shadowOpacity = 1.0
        cell.backView.layer.shadowRadius = 3.0
        cell.backView.layer.masksToBounds = false
        cell.documentsButton.layer.cornerRadius = 20
        cell.documentsButton.layer.masksToBounds = false
        cell.documentsButton.setTitle("View Documents".localized(), for: .normal)
        cell.totalLabel.text = "Total : ".localized()
        
        if let name = (arrHistory.object(at: indexPath.row) as! NSDictionary).value(forKey: "date") as? String{
            cell.dateLbl.text  = name
        }
        
        if let orderID = (arrHistory.object(at: indexPath.row) as! NSDictionary).value(forKey: "intId") as? String{
            cell.trackDricer.text  = "Reference No. : ".localized() + "\(orderID)"
            cell.trackDricer.textColor = UIColor.black

        }
        
        if let name = (arrHistory.object(at: indexPath.row) as! NSDictionary).value(forKey: "netAmount") as? String{
            cell.netAmountLbl.text  = name
        }
        
        if let name = (arrHistory.object(at: indexPath.row) as! NSDictionary).value(forKey: "intPackage") as? String{
            cell.packageLbl.text  = name
        }
        
        
        if let varCar = (arrHistory.object(at: indexPath.row) as! NSDictionary).value(forKey: "varCar") as? String{
            cell.carNameLbl.text  = varCar
        }
        
        
        if let varModel = (arrHistory.object(at: indexPath.row) as! NSDictionary).value(forKey: "varModel") as? String{
            cell.carModelLbl.text  = varModel
        }
        
        
        
     /*   if let collectionTrack = (arrHistory.object(at: indexPath.row) as! NSDictionary).value(forKey: "c_track_assign") as? String, let collectionStatus = (arrHistory.object(at: indexPath.row) as! NSDictionary).value(forKey: "c_track_status") as? String{
            if collectionTrack != "Y" && collectionStatus == ""{
                cell.trackCollectionButton.isHidden = true
                cell.trackCollectionButton.backgroundColor = UIColor.darkGray
                
            }else if collectionTrack == "Y" && collectionStatus == "P"{
                cell.trackCollectionButton.isHidden = false
                cell.trackCollectionButton.backgroundColor = colorGreen
                
            }else if collectionTrack == "Y" && collectionStatus == "S"{
                cell.trackCollectionButton.isHidden = false
                cell.trackCollectionButton.backgroundColor = colorGreen
                
            }else if collectionTrack == "Y" && collectionStatus == "C"{
                cell.trackCollectionButton.isHidden = true
                cell.trackCollectionButton.backgroundColor = colorGreen
            }
            
        }
        
        if let deliveryTrack = (arrHistory.object(at: indexPath.row) as! NSDictionary).value(forKey: "d_track_assign") as? String,let deliveryStatus = (arrHistory.object(at: indexPath.row) as! NSDictionary).value(forKey: "d_track_status") as? String{
            if deliveryTrack != "Y" && deliveryStatus == ""{
                cell.trackOrderButton.isHidden = true
                cell.trackOrderButton.backgroundColor = UIColor.darkGray
                
            }else if deliveryTrack == "Y" && deliveryStatus == "P"{
                cell.trackOrderButton.isHidden = false
                cell.trackOrderButton.backgroundColor = colorGreen
                
            }else if deliveryTrack == "Y" && deliveryStatus == "S"{
                cell.trackOrderButton.isHidden = false
                cell.trackOrderButton.backgroundColor = colorGreen
                
            }else if deliveryTrack == "Y" && deliveryStatus == "C"{
                cell.trackOrderButton.isHidden = true
                cell.trackOrderButton.backgroundColor = UIColor.darkGray
            }
        }*/
        
//        if let assignTrack = (arrHistory.object(at: indexPath.row) as! NSDictionary).value(forKey: "track_assign") as? String{
//            if assignTrack == "N"{
//                cell.trackOrderButton.isHidden = true
//                cell.trackCollectionButton.isHidden = true
//            }
//        }
        
    /*    if let trackStatus = (arrHistory.object(at: indexPath.row) as! NSDictionary).value(forKey: "track_status") as? String, let serviceType = (arrHistory.object(at: indexPath.row) as! NSDictionary).value(forKey: "serviceType") as? String,let trackAssign = (arrHistory.object(at: indexPath.row) as! NSDictionary).value(forKey: "track_assign") as? String{
            if trackStatus == "" && trackAssign == "N" && serviceType == "O"{
                cell.trackOrderButton.isHidden = true
                cell.trackCollectionButton.isHidden = true
                cell.trackCollectionButton.backgroundColor = UIColor.darkGray
                
            }else if trackStatus == "P" && trackAssign == "Y" && serviceType == "O"{
                cell.trackOrderButton.isHidden = true
                cell.trackCollectionButton.isHidden = false
                cell.trackCollectionButton.backgroundColor = colorGreen

            }else if trackStatus == "S" && trackAssign == "Y" && serviceType == "O"{
                cell.trackOrderButton.isHidden = true
                cell.trackCollectionButton.isHidden = false
                cell.trackCollectionButton.backgroundColor = colorGreen

            }else if trackStatus == "C" && trackAssign == "Y" && serviceType == "O"{
                cell.trackOrderButton.isHidden = true
                cell.trackCollectionButton.isHidden = true
                cell.trackCollectionButton.backgroundColor = colorGreen

            }
        }*/
        
        
        cell.documentsButton.addTarget(self, action: #selector(onClickDocumentButton(_:)), for: .touchUpInside)
//        cell.trackOrderButton.addTarget(self, action: #selector(onClickTrackDriverButton(_:)), for: .touchUpInside)
//        cell.trackCollectionButton.addTarget(self, action: #selector(onClickTrackDriverButton(_:)), for: .touchUpInside)
//        cell.trackCollectionButton.isHidden = true
//        cell.trackOrderButton.isHidden = true
//        cell.buttonStackView.isHidden = true

        
        return cell
        
    }
}


    
    @IBAction func onClickTrackDriverButton(_ sender:UIButton) {
        if arrHistory.count == 0 {return}
        if let indexPath = sender.tableViewIndexPath(self.aTableView) as IndexPath? {
            let cell = self.aTableView.cellForRow(at: indexPath) as! MyServiceTableViewCell
            let order = (arrHistory.object(at: indexPath.row) as! NSDictionary)
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "SetLocationOnMapViewController") as! SetLocationOnMapViewController
            if let orderId = order.value(forKey: "IntId") as? String{
                nextViewController.orderID = orderId
            }
            
            if let orderId = order.value(forKey: "IntId") as? NSNumber{
                nextViewController.orderID = "\(orderId)"
            }

            nextViewController.order = order
            nextViewController.navTittle = "Order Track"
            
            if cell.trackOrderButton == sender {
                nextViewController.addressType = "d"
                if let delieryTrack = order.value(forKey: "d_track_assign") as? String, let deliveryStatus = order.value(forKey: "d_track_status") as? String{
                    if delieryTrack == "Y" &&  deliveryStatus == "S" {
                        if let location = order.value(forKey: "pLocation") as? NSDictionary{
                            if let latitude = location.value(forKey: "lat") as? String,let longitude = location.value(forKey: "long") as? String{
                                if latitude == "" && longitude == ""{
                                    AppHelper.showAlert("app_alert_title".localized(), message: "No User Location Found".localized())
                                    return
                                }
                            }
                        }
                        self.navigationController?.pushViewController(nextViewController, animated: true)
                    }else if delieryTrack == "Y" &&  deliveryStatus == "P" {
                        AppHelper.showAlert("app_alert_title".localized(), message: "Order is Pending".localized())
                    } else if delieryTrack == "Y" &&  deliveryStatus == "C" {
                        AppHelper.showAlert("app_alert_title".localized(), message: "Order is Complete".localized())
                    }else if delieryTrack != "Y" &&  deliveryStatus == "C" {
                        AppHelper.showAlert("app_alert_title".localized(), message: "Order is Not Assing".localized())
                    }
                }
            }else{
                nextViewController.addressType = "c"
                if let serviceType = order.value(forKey: "serviceType") as? String{
                    if serviceType == "O"{
                        if let trackStatus = order.value(forKey: "track_status") as? String, let serviceType = order.value(forKey: "serviceType") as? String,let trackAssign = order.value(forKey: "track_assign") as? String{
                            if trackStatus == "" && trackAssign == "N" && serviceType == "O"{
                                AppHelper.showAlert("app_alert_title".localized(), message: "Order is Not Assing".localized())

                            }else if trackStatus == "P" && trackAssign == "Y" && serviceType == "O"{
                                AppHelper.showAlert("app_alert_title".localized(), message: "Order is Pending".localized())
                                
                            }else if trackStatus == "S" && trackAssign == "Y" && serviceType == "O"{
                                if let location = order.value(forKey: "pLocation") as? NSDictionary{
                                    if let latitude = location.value(forKey: "lat") as? String,let longitude = location.value(forKey: "long") as? String{
                                        if latitude == "" && longitude == ""{
                                            AppHelper.showAlert("app_alert_title".localized(), message: "No User Location Found".localized())
                                            return
                                        }
                                    }
                                }
                                self.navigationController?.pushViewController(nextViewController, animated: true)
                                
                            }else if trackStatus == "C" && trackAssign == "Y" && serviceType == "O"{
                                AppHelper.showAlert("app_alert_title".localized(), message: "Order is Completed".localized())
                                
                            }
                        }
                    }else{
                        
                        if let collectionTrack = order.value(forKey: "c_track_assign") as? String, let collectionStatus = order.value(forKey: "c_track_status") as? String{
                            if collectionTrack == "Y" &&  collectionStatus == "S" {
                                if let location = order.value(forKey: "pLocation") as? NSDictionary{
                                    if let latitude = location.value(forKey: "lat") as? String,let longitude = location.value(forKey: "long") as? String{
                                        if latitude == "" && longitude == ""{
                                            AppHelper.showAlert("app_alert_title".localized(), message: "No User Location Found".localized())
                                            return
                                        }
                                    }
                                }
                                self.navigationController?.pushViewController(nextViewController, animated: true)
                            }else if collectionTrack == "Y" &&  collectionStatus == "P" {
                                AppHelper.showAlert("app_alert_title".localized(), message: "Order is Pending".localized())
                            } else if collectionTrack == "Y" &&  collectionStatus == "C" {
                                AppHelper.showAlert("app_alert_title".localized(), message: "Order is Completed".localized())
                            }else if collectionTrack != "Y" &&  collectionStatus == "" {
                                AppHelper.showAlert("app_alert_title".localized(), message: "Order is Not Assing".localized())
                            }
                        }
                    }
                }
            }
        }
    }
    

@IBAction func onClickDocumentButton(_ sender:UIButton) {
    
    var carName = ""
    var carModel = ""
    
    if let indexpath = sender.tableViewIndexPath(self.aTableView){
        if let car = (arrHistory.object(at: indexpath.row) as! NSDictionary).value(forKey: "varCar") as? String
        {
            carName = car
        }
        
        if let model = (arrHistory.object(at: indexpath.row) as! NSDictionary).value(forKey: "varModel") as? String
        {
            carModel  = model
        }
        self.navigateTocategory(carName: carName, model: carModel,indexPath:indexpath)
        
    }
}


    func navigateTocategory(carName:String,model:String,indexPath:IndexPath){
        if arrHistory.count == 0 {return}
        let order = (arrHistory.object(at: indexPath.row) as! NSDictionary)
    let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "CategoryDocViewController") as! CategoryDocViewController
    nextViewController.carName = carName
    nextViewController.carModel = model
    nextViewController.order = order
    self.navigationController?.pushViewController(nextViewController, animated: true)
}


// MARK: - getUserHistory API

func getUserCurrentHistory()
{
    if net.isReachable == false{
        let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }else{
        
        refreshControl.endRefreshing()
        let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
        let dictParameters:[String:AnyObject] = ["userId" : nUserId as AnyObject]
        print("URL :--\(dictParameters)")
        AppHelper.showLoadingView()
        self.objGetHistory = HttpWrapper.init()
        self.objGetHistory.delegate = self
        self.objGetHistory.requestWithparamdictParamMethodwithHeaderAndDictGet(url: kgetPastBookings, dictParams: dictParameters, headers: header)
        
        
    }
    
}



func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary) {
    AppHelper.hideLoadingView()
    print(dicsResponse)
    self.isLoading = true
    if wrapper == objGetHistory {
        if dicsResponse.value(forKey: "status") as! NSString == "success"
        {
            arrHistory.removeAllObjects()
            
            if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray
            {
                arrHistory = tempNames.mutableCopy() as! NSMutableArray
                
            }
            print(arrHistory)
            self.aTableView.reloadData()
            
        }
        else
        {
        arrHistory.removeAllObjects()
        self.aTableView.reloadData()
            //                 tblPage1.isHidden = true
            //                 btnBookServicePage1.isHidden = false
            //AppHelper.showAlert("Alert", message:dicsResponse.value(forKey: "msg") as! String)
        }
        
        
    }
    
}

func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError) {
    AppHelper.hideLoadingView()
    print(error.debugDescription)
}

}
