//
//  SetLocationOnMapViewController.swift
//  ServiceMyCar
//
//  Created by Parikshit on 30/06/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit

import GoogleMaps
import Firebase

class SetLocationOnMapViewController: UIViewController, ARCarMovementDelegate{
    
    @IBOutlet weak var mapview:GMSMapView!
    
    
    var currentLocation : CLLocation!
    
    var navTittle:String = ""
    
    var bikeMarker = GMSMarker()
    var dropOffMarker = GMSMarker()
    var previousLocation: CLLocation?
    var previousLatitude = 0.0
    var previousLongitude = 0.0
    var moveMent: ARCarMovement!
    var isMarkerInitialized = false
    var _refHandle: DatabaseHandle!
    var ref:DatabaseReference!
    var updateTimer:Timer!
    var addressType:String = ""
    
    
    let locationManager = CLLocationManager()
    var didFindMyLocation = false
    
    var markerHeading:CLHeading?
    
    var order = NSDictionary()
    let badgeButton : MIBadgeButton = MIBadgeButton(frame:CGRect(x: -10, y: 15, width: 40, height: 20))
    var orderID:String = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let orderId = order.value(forKey: "intId") as? String{
            orderID = orderId
        }
        
        if let orderId = order.value(forKey: "intId") as? NSNumber{
            orderID = "\(orderId)"
        }

        
        moveMent = ARCarMovement()
        moveMent.delegate = self
        mapview.delegate = self
        
        self.ref = DatabaseReference()
        self._refHandle = DatabaseHandle()
        self.title = "TRACK DRIVER"
        self.mapview.mapStyle(withFilename: "mapStyle", andType: "json")

        self.view.sendSubviewToBack(mapview)
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startMonitoringSignificantLocationChanges()
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.startUpdatingHeading()
        locationManager.distanceFilter = 2
        self.setupNotificationView()

        if navTittle == "Order Track"{
            //self.setupNoGroupData()
            self.upDateMarker()

            // self.locationUpdate()
            //self.setUpMap()

       }else{
            self.setupNoGroupData()
        
       }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.referenceHandle()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.setupNotificationView()
    }
    
    
    func setupNotificationView(){
        badgeButton.edgeInsetTop = 10
        badgeButton.setImage(UIImage(named : "notification"), for: .normal)
        print(appDelegate.NCount)
        badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        badgeButton.badgeString = appDelegate.NCount
        if appDelegate.NCount == "0"
        {
            badgeButton.badgeBackgroundColor = UIColor.clear
            badgeButton.badgeTextColor = UIColor.clear
        }
        else
        {
            badgeButton.badgeBackgroundColor = colorGreen
            badgeButton.badgeTextColor = UIColor.white
        }
        badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        let barButton2 : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton2
        //        getPackagesListByCategory()
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)

    }
    
    
    
    // Step 3
    
    @objc func NotificationAlert()
    {
        guard let vcs = self.navigationController?.viewControllers else{return}
        let inStack = vcs.contains(where: { (vc) -> Bool in
            return vc is MyNotificationList_Vc
        })
        if inStack{
            self.performSegue(withIdentifier: unwindToNotificationController, sender: self)
        }else{
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        //        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
        //
        //        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    // Step 4
    
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            //self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                
                badgeButton.badgeString = appDelegate.NCount
                if appDelegate.NCount == "0"
                {
                    badgeButton.badgeBackgroundColor = UIColor.clear
                    badgeButton.badgeTextColor = UIColor.clear
                }
                else
                {
                    badgeButton.badgeBackgroundColor = colorGreen
                    badgeButton.badgeTextColor = UIColor.white
                }
            }
        }
    }
    
    
    func setupNoGroupData(){
        var lat:Double = 0.0
        var long:Double = 0.0
        
        if let location = order.value(forKey: "pLocation") as? NSDictionary{
            if let latitude = location.value(forKey: "lat") as? String{
                lat = Double(latitude) ?? 0.0
            }
            if let longitude = location.value(forKey: "long") as? String{
                long = Double(longitude) ?? 0.0
            }
        }
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 14.0)
        mapview.camera = camera
        self.bikeMarker = GMSMarker()
        bikeMarker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
        bikeMarker.icon = #imageLiteral(resourceName: "marker_icon")
        bikeMarker.map = mapview
        
        
        //self.mapview.isUserInteractionEnabled = false
    }
    
    func referenceHandle(){
        if let refHandle = _refHandle {
            self.ref?.removeObserver(withHandle: refHandle)
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickLocationButton(_ sender: UIButton){
        if navTittle != "Order Track"{
            var lat:Double = 0.0
            var long:Double = 0.0
            
            if let location = order.value(forKey: "pLocation") as? NSDictionary{
                if let latitude = location.value(forKey: "lat") as? String{
                    lat = Double(latitude) ?? 0.0
                }
                if let longitude = location.value(forKey: "long") as? String{
                    long = Double(longitude) ?? 0.0
                }
            }

            self.currentLocation = CLLocation(latitude: lat, longitude: long)
        }else{
            guard let location = self.currentLocation else {return}
            self.mapview.animate(toLocation: location.coordinate)
            mapview.camera = GMSCameraPosition(target: location.coordinate, zoom: 14.0, bearing: 0, viewingAngle: 0)
        }
        //currentLocation = locationMapView.myLocation
    }
    
    
    func locationUpdate(){
        if self.currentLocation != nil{
            if self.previousLocation != self.currentLocation{
                if self.isLocationWithinScreen(position: self.currentLocation.coordinate){
                    self.moveCarMarker(previouslat: self.previousLatitude, previousLong: self.previousLongitude, currentLocation: self.currentLocation)
                }else{
                    self.animateMapToCenter(position: self.currentLocation.coordinate)
                }
                
            }
        }
    }
    
    @IBAction func onClickBackButton(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension SetLocationOnMapViewController:CLLocationManagerDelegate,GMSMapViewDelegate{
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            manager.startUpdatingLocation()
            manager.startUpdatingHeading()
            
            break
        case .authorizedAlways:
            manager.startUpdatingLocation()
            manager.startUpdatingHeading()
            
            break
        case .restricted:

            break
        case .denied:

            break
        }
    }
    
    
    
    
    
    
    func initializeMarker(center:CLLocationCoordinate2D){
        self.isMarkerInitialized=true
        mapview.camera = GMSCameraPosition(target: center, zoom: 14.0, bearing: 0, viewingAngle: 0)
        self.setUpMap()
        
    }
    
    func animateMapToCenter(position:CLLocationCoordinate2D){
        let heading = markerHeading?.trueHeading
        bikeMarker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        bikeMarker.rotation = heading ?? 0
        let camera = GMSCameraPosition.camera(withTarget: position, zoom: 14.0)
        mapview.camera = camera
        mapview.animate(to: camera)
    }
    
    func isLocationWithinScreen(position: CLLocationCoordinate2D) -> Bool {
        let region = self.mapview.projection.visibleRegion()
        let bounds = GMSCoordinateBounds(region: region)
        return bounds.contains(position)
    }
    
    
    func moveCarMarker(previouslat:Double,previousLong:Double, currentLocation: CLLocation){
        self.moveMent.arCarMovement(self.bikeMarker, withOldCoordinate: CLLocationCoordinate2D.init(latitude: previouslat, longitude: previousLong), andNewCoordinate: CLLocationCoordinate2D.init(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude), inMapview: self.mapview, withBearing: 0.0)
        self.previousLocation = currentLocation
        self.previousLatitude = currentLocation.coordinate.latitude
        self.previousLongitude = currentLocation.coordinate.longitude
    }
    
    func arCarMovement(_ movedMarker: GMSMarker) {
        bikeMarker = movedMarker
        bikeMarker.map = mapview
        
        //animation to make car icon in center of the mapview
        
    }
    
    func setUpMap(){
        if currentLocation != nil{
            self.isMarkerInitialized=true
            var lat:Double = 0.0
            var long:Double = 0.0
            
            if let location = order.value(forKey: "pLocation") as? NSDictionary{
                if let latitude = location.value(forKey: "lat") as? String{
                    lat = Double(latitude) ?? 0.0
                }
                if let longitude = location.value(forKey: "long") as? String{
                    long = Double(longitude) ?? 0.0
                }
            }
            let pickCoordinate = CLLocationCoordinate2D(latitude: self.currentLocation.coordinate.latitude, longitude: self.currentLocation.coordinate.longitude)
            let dropCoordinate = CLLocationCoordinate2D(latitude: lat, longitude:long)

            addMarkerForPickUpAndDropOffAddress(pickupCoordinate: pickCoordinate, dropOffCoordinate: dropCoordinate)
            resetBoundOfMap(pickCoordinate: pickCoordinate, dropCoordinate: dropCoordinate)
        }
    }
    func resetBoundOfMap(pickCoordinate:CLLocationCoordinate2D,dropCoordinate:CLLocationCoordinate2D) -> Void {
        let path = GMSMutablePath()
        path.add(pickCoordinate)
        path.add(dropCoordinate)
        let bounds = GMSCoordinateBounds(path: path)
        
        let camera = mapview.camera(for: bounds, insets:UIEdgeInsets(top: 30, left: 30, bottom: 50, right: 30))
        mapview.camera = camera!;
    }
    
    func addMarkerForPickUpAndDropOffAddress(pickupCoordinate:CLLocationCoordinate2D, dropOffCoordinate:CLLocationCoordinate2D) -> Void {
        isMarkerInitialized = true
        self.bikeMarker = GMSMarker(position: pickupCoordinate)
        bikeMarker.icon = #imageLiteral(resourceName: "car_map")
        bikeMarker.map = self.mapview
        
        self.dropOffMarker = GMSMarker(position: dropOffCoordinate)
        dropOffMarker.icon = #imageLiteral(resourceName: "marker_icon")
        dropOffMarker.map = self.mapview
        
    }
    
    func upDateMarker() {
        
        self.ref = Database.database().reference(withPath: "driverjob").child("order_\(addressType)_\(orderID)")
        guard let fireRef = self.ref else {
            return
        }
        
        self._refHandle = fireRef.observe(.value, with: { (snapshot) -> Void in
            let transporterLocation = snapshot.childSnapshot(forPath: "location")
            let lat = transporterLocation.childSnapshot(forPath: "lat")
            let long = transporterLocation.childSnapshot(forPath: "long")
            
            var driverLat:Double = 0.0
            var driverLong:Double = 0.0
            if let _driverLat = lat.value as? Double{
                driverLat = _driverLat
                print("\(driverLat)")
                
            }else if let _driverLat = lat.value as? String{
                driverLat = Double(_driverLat) ?? 0.0
                print("\(driverLat)")

            }
            if let _driverLong = long.value as? Double{
                driverLong = _driverLong
                print("\(driverLong)")

            }else if let _driverLong = long.value as? String{
                driverLong = Double(_driverLong) ?? 0.0
                print("\(driverLong)")

            }
            if driverLat != 0.0 && driverLong != 0.0{
                self.currentLocation = CLLocation.init(latitude: driverLat, longitude: driverLong)
                
                if self.isMarkerInitialized{
                    self.mapview.animate(toLocation:self.currentLocation.coordinate)
                    self.locationUpdate()
                }else{
                    self.initializeMarker(center:self.self.currentLocation.coordinate)
                }
                
                self.previousLatitude = driverLat
                self.previousLongitude = driverLong
                
            }
            
        })
    }
    
}
