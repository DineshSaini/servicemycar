//
//  SelectYourCarBookingViewController.swift
//  ServiceMyCar
//
//  Created by admin on 20/06/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import TelrSDK

class SelectYourCarBookingViewController: UIViewController,HttpWrapperDelegate {
    
    
    @IBOutlet weak var aCollectionView:UICollectionView!
    @IBOutlet weak var payButton:UIButton!
    @IBOutlet weak var applyCodeButton:UIButton!
    @IBOutlet weak var discountlabel:UILabel!

    
    

    var includeOrNotInclude = ["Date And Time","Would You Like Any Additional Service ?","","",""]
    
    
    var dataDictionay = NSDictionary()
    var carArrayList = NSMutableArray()
    var partArrayList = NSMutableArray()
    
    var extraPrice = NSDictionary()
    
    var objgetServiceYourCar = HttpWrapper()
    var objGetPart = HttpWrapper()
    var objGetExtraPrice = HttpWrapper()
    var objPutCheckBlockDate = HttpWrapper()

    var net:NetworkReachabilityManager = NetworkReachabilityManager()!
    
    
    var selectCar = NSDictionary()
    var selectPart = NSDictionary()
    
    var collectionAddress:String = ""
    var dropAddress:String = ""
    
    var selectDropAddress = NSDictionary()
    var selectColectionAddress = NSDictionary()
    
    
    var isCarWash:Bool = false
    var isCarFuel:Bool = false
    
    let myDatePicker = UIDatePicker()
    var txtSelectDateTime = UITextField()
    var selectedDate:String = "0"

    var badgeButton : MIBadgeButton = MIBadgeButton(frame:CGRect(x:-10, y: 30, width: 40, height: 20)) // Step 1

    var srtPrice = ""
    
    let KEY:String = "9Std-fqjHP#Q6dW2"  // TODO fill key
    let STOREID:String = "20796"         // TODO fill store id
    var EMAIL:String = ""//isLive ? "accounts@servicemycar.ae" : "Accounts@servicemycar.ae"  // TODO fill email
    var paymentRequest:PaymentRequest?
    let UserLName = UserDefaults.standard.value(forKey: kUserName) as! String
    let UserEmail = UserDefaults.standard.value(forKey: KUserLoginEmail) as! String
    let UserPhoneNo = UserDefaults.standard.value(forKey: kUserLoginPhone) as! String
    
    var objGetExtraVat = HttpWrapper()
    var vatPercent = ""
    var excludeVat:Float = 0
    var includeVat:Float = 0
    var vatPrice:Float = 0
    var discount:Float = 0
    
    var isCoupanApply:Bool = false
    var coupanCode: String = ""
    var couponId = ""
    
    
    var dataLoading = false
    
    
    var itemInSection:Int = 2

    let columnLayout = FlowLayout(
        itemSize: CGSize(width: 200, height: 200),
        minimumInteritemSpacing: 0,
        minimumLineSpacing: 0,
        sectionInset: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    )

    override func viewDidLoad() {
        super.viewDidLoad()
        aCollectionView?.collectionViewLayout = columnLayout
        aCollectionView?.contentInsetAdjustmentBehavior = .automatic
        
        aCollectionView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        self.aCollectionView.dataSource = self
        self.aCollectionView.delegate = self
        if let UserEmail = UserDefaults.standard.value(forKey: KUserLoginEmail) as? String{
            self.EMAIL = UserEmail
        }
        self.title = "Confirmation".localized()
        self.payButton.setTitle("Pay".localized(), for: .normal)
        self.applyCodeButton.setTitle("Apply Here".localized(), for: .normal)
        self.discountlabel.text = "Have a discount code? ".localized()
        
        
        self.aCollectionView.register(UINib(nibName: "ServiceCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ServiceCollectionViewCell")
        self.aCollectionView.register(UINib(nibName: "EmptyCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "EmptyCollectionViewCell")
        
        self.aCollectionView.register(UINib(nibName: "ServiceHeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: JEKCollectionElementKindSectionBackground, withReuseIdentifier: "ServiceHeaderCollectionReusableView")
        self.aCollectionView.register(UINib(nibName: "HeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HeaderCollectionReusableView")
        
        self.aCollectionView.register(UINib(nibName: "HeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "HeaderCollectionReusableView")
        
        self.aCollectionView.register(UINib(nibName: "PaymentInfoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PaymentInfoCollectionViewCell")
        
        AppHelper.makeViewCircularWithRespectToHeight(payButton, borderColor: .clear, borderWidth: 0.0)
        if let packagePrice = dataDictionay.value(forKey: "varPrice") as? String{
            self.srtPrice = packagePrice
            self.excludeVat = Float(packagePrice)!
        }else if let packagePrice = dataDictionay.value(forKey: "varPrice") as? NSNumber{
            self.srtPrice = "\(packagePrice)"
            self.excludeVat = Float(truncating: packagePrice)

        }
        
        if self.selectCar.count != 0{
            self.partArrayList.insert(self.selectCar, at: 0)
            if selectPart.count != 0{
            self.partArrayList.insert(self.selectPart, at: 1)
            }
        }
        setUpdateBookingFields()
        self.setupNavigationButton()
        
        
        
        let button = UIButton.init(type: .custom)
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        
        if languageCode == "en" {
            button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
        }
        else {
            button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
        }
        button.addTarget(self, action:#selector(onClickBackBarButton(_ :)), for: UIControl.Event.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton

        
        self.aCollectionView.reloadData()
        
        
        self.GetVat()
        // Do any additional setup after loading the view.
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.setupNavigation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        self.getPartsListItems()
//        self.getServiceCarItems()
        getExtraPriceData()
    }
    
    
    @IBAction func onClickBackBarButton(_ sender:UIBarButtonItem){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    @IBAction func onClickCollectionPointAddressButton(_ sender:UIBarButtonItem){
//        AppHelper.showAlert("Important Message", message: "Functionality Pending!")
//        return
        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "AddAddress_Vc") as! AddAddress_Vc
        nextViewController.addressTypeString = "Enter Collection Address"
//        nextViewController.addressChrType = "C"
        nextViewController.delegate = self
        nextViewController.editAddress = self.selectColectionAddress
        nextViewController.isEditingAddress = true

        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    
    @IBAction func onClickDropPointAddressButton(_ sender:UIBarButtonItem){
        
        
//        AppHelper.showAlert("Important Message", message: "Functionality Pending!")
//        return
        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "AddAddress_Vc") as! AddAddress_Vc
        nextViewController.addressTypeString = "Enter Drop Address"
 //       nextViewController.addressChrType = "D"
        nextViewController.delegate = self
        nextViewController.editAddress = selectDropAddress
        nextViewController.isEditingAddress = true

        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    
    @IBAction func onClickConfrimButton(_ sender:UIButton){
    
        guard let dateAndTime = txtSelectDateTime.text else {
            AppHelper.showAlert("Important Message".localized(), message: "Please Select your Date and Time!".localized())
            return
        }
        if dateAndTime.isEmpty{
            AppHelper.showAlert("Important Message".localized(), message: "Please Select your Date and Time!".localized())
            return
        }
        
        self.checkBlockDateFromServer(selectDate: self.selectedDate)
        
    }
    
    
    func gotoPaymentViewController(){
        
        let nextViewController = objMain.instantiateViewController(withIdentifier: "TelrController") as! TelrController
        appDelegate.Bookings.Price.total = String(self.includeVat)
        paymentRequest = preparePaymentRequest()
        nextViewController.paymentRequest = paymentRequest!
        nextViewController.navigationItem.leftBarButtonItem?.isEnabled = true
        self.navigationController?.pushViewController(nextViewController, animated: true)

    }
    
 
    
    func setupNavigationButton(){
        
        // Step 2
        badgeButton.edgeInsetTop = 10
        badgeButton.setImage(UIImage(named : "notification"), for: .normal)
        badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        print(appDelegate.NCount)
        badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        badgeButton.badgeString = appDelegate.NCount
        if appDelegate.NCount == "0"
        {
            badgeButton.badgeBackgroundColor = UIColor.clear
            badgeButton.badgeTextColor = UIColor.clear
        }
        else
        {
            badgeButton.badgeBackgroundColor = colorGreen
            badgeButton.badgeTextColor = UIColor.white
        }
        badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        let barButton2 : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton2
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)
        
    }
    
    
    
    
    // MARK: - Google Api Calling
    
    // Step 4
    
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            //self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                
                badgeButton.badgeString = appDelegate.NCount
                if appDelegate.NCount == "0"
                {
                    badgeButton.badgeBackgroundColor = UIColor.clear
                    badgeButton.badgeTextColor = UIColor.clear
                }
                else
                {
                    badgeButton.badgeBackgroundColor = colorGreen
                    badgeButton.badgeTextColor = UIColor.white
                }
            }
        }
    }
    
    // step 3
    
    @objc func NotificationAlert()
    {
        guard let vcs = self.navigationController?.viewControllers else{return}
        let inStack = vcs.contains(where: { (vc) -> Bool in
            return vc is MyNotificationList_Vc
        })
        if inStack{
            self.performSegue(withIdentifier: unwindToNotificationController, sender: self)
        }else{
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        //        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
        //
        //        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    
    
    
    @IBAction func onClickAddNavigation(_ sender: UIButton){
        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "AddCar_Vc") as! AddCar_Vc
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    
    
    private func preparePaymentRequest() -> PaymentRequest{
        if let UserEmail = UserDefaults.standard.value(forKey: KUserLoginEmail) as? String{
            self.EMAIL = UserEmail
        }
        let paymentReq = PaymentRequest()
        paymentReq.key = KEY
        paymentReq.store = STOREID
        paymentReq.appId = "123456789"
        paymentReq.appName = "ServiceMyCar"
        paymentReq.appUser = "123456"
        paymentReq.appVersion = "0.0.1"
        paymentReq.transTest = Istesting
        paymentReq.transType = "sale"
        paymentReq.transClass = "paypage"
        paymentReq.transCartid = String(arc4random())
        paymentReq.transDesc = isLive ? "Booking on Servicemycar.ae" : "service payment"
        paymentReq.transCurrency = "AED"
        
        appDelegate.Bookings.telr_value = String(self.includeVat)
        paymentReq.transAmount = String(self.includeVat)
        paymentReq.transLanguage = "en"
        paymentReq.billingEmail = EMAIL
        
        let name = UserLName.split(separator: " ")
        
        
        paymentReq.billingFName = "\(name[0])"
        paymentReq.billingLName = "\(name[1])"
        paymentReq.billingTitle = "Mr"
        paymentReq.city = "Dubai"
        paymentReq.country = "AE"
        paymentReq.region = "Dubai"
        paymentReq.address = "line 1"
        paymentReq.billingPhone = UserPhoneNo
        
        
        return paymentReq
    }
    

  /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if let destination = segue.destination as? TelrController{
            destination.paymentRequest = paymentRequest!
        }
    }*/
    

}
extension SelectYourCarBookingViewController:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else if section == 1{
            return 2
        }else if section == 2{
            return 1
        }else if section == 4{
            return 1

        }
        else{
            if !isCarFuel && !isCarWash && selectPart.count != 0{
                self.itemInSection = 2
                return 2
            }else if !isCarFuel && isCarWash && selectPart.count != 0 {
                self.itemInSection = 3
                return 3
            }else if isCarFuel && !isCarWash && selectPart.count != 0 {
                self.itemInSection = 3
                return 3
            }else if isCarFuel && isCarWash && selectPart.count != 0 {
                self.itemInSection = 4
                return 4
            }else if isCarFuel && isCarWash && selectPart.count == 0 {
                self.itemInSection = 3
                return 3
            }else if isCarFuel && !isCarWash && selectPart.count == 0 {
                self.itemInSection = 2
                return 2
            }else if !isCarFuel && isCarWash && selectPart.count == 0 {
                self.itemInSection = 2
                return 2
            }else {
            return 1
            }
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let edgeInsets = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)

        if section == 1{
            let edgeInsets = UIEdgeInsets.init(top: 0, left: self.view.frame.size.width/6.0, bottom: 0, right: 0)
            return edgeInsets
        }else if section == 3{
                let edgeInsets = UIEdgeInsets.init(top: 0, left: self.view.frame.size.width/6.0, bottom: 0, right: 0)
                return edgeInsets
        }else{
            return edgeInsets
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if section == 0{
            return CGSize(width: self.view.frame.size.width, height: 30.0)

        }else if section == 1{
            return CGSize(width: self.view.frame.size.width, height: 30.0)

        }else{
            return CGSize(width: self.view.frame.size.width, height: 0.0)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: self.view.frame.size.width, height: 0.0)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if (kind == UICollectionView.elementKindSectionHeader) {
            
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderCollectionReusableView", for: indexPath) as! HeaderCollectionReusableView
            let index = indexPath.first ?? 0
            headerView.headerTittle.text = self.includeOrNotInclude[index].localized()
            headerView.infoImage.isHidden = true
            return headerView
        }else{
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderCollectionReusableView", for: indexPath) as! HeaderCollectionReusableView
            headerView.infoImage.isHidden = true
            return headerView
            
        }
    }

    
    
    
    @IBAction func onClickInfoButton(_ sender:UIButton){
        if let indexPath = sender.collectionViewIndexPath(self.aCollectionView)  as IndexPath? {
            if indexPath.item == 1 {
                AppHelper.showAlert("Fuel".localized(), message: "Our driver will stop at the filling station and fill your cars fuel to full.All fuel will be charges extra and a receipt will be provided.(The 10aed is our service charge)".localized())
                return
            }else{
                
                AppHelper.showAlert("Car wash & Car polish".localized(), message: "Treat your car to spa by our team of proffesionals before we hand your car back to after the service.We will amke your car clean and shiny on the inside and outside leaving both you and your car happy.".localized())
                return
            }
            
        }
        

        
    }

    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DateAndTimeCollectionViewCell", for: indexPath) as! DateAndTimeCollectionViewCell
            cell.dateAndTimeTextfield.placeholder = "Select Date And Time".localized()
            let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
            if languageCode != "en" {
                cell.dateAndTimeTextfield.textAlignment = .right
            }
            cell.setLeftIconForTextField(cell.dateAndTimeTextfield, leftIcon: UIImage())
            txtSelectDateTime = cell.dateAndTimeTextfield
            self.showDatePicker()
            cell.setNeedsLayout()
            return cell
        }else if indexPath.section == 1{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ServiceCollectionViewCell", for: indexPath) as! ServiceCollectionViewCell
            if indexPath.item == 0{
                cell.serviceImageview.image = UIImage(named: "car_wash")
                
                if let price = extraPrice.value(forKey: "carValet") as? String{
                    cell.serviceNameLabel.text = "\(price)" + "app_currency".localized()
                }
                cell.selectButton.isSelected = isCarWash ? true : false

            }else{
               cell.serviceImageview.image = UIImage(named: "petrol")
                if let price = extraPrice.value(forKey: "carFuel") as? String{
                    cell.serviceNameLabel.text = "\(price)" + "app_currency".localized()
                }
                cell.selectButton.isSelected = isCarFuel ? true : false
            }
                cell.viewContainer.layer.borderWidth = 1.0
                cell.viewContainer.layer.borderColor = colorGreen.cgColor
                cell.infoButton.isHidden = false
                cell.viewContainer.layer.cornerRadius = 5.0
                cell.viewContainer.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2).cgColor
                cell.viewContainer.layer.shadowOffset = CGSize.zero
                cell.viewContainer.layer.shadowOpacity = 1.0
                cell.viewContainer.layer.shadowRadius = 0.5
                cell.viewContainer.layer.masksToBounds = false
                cell.contentView.backgroundColor = UIColor.init(red: 244.0/255.0, green: 245.0/255.0, blue: 248.0/255.0, alpha: 1.0)
            cell.infoButton.addTarget(self, action: #selector(onClickInfoButton(_:)), for: .touchUpInside)
                return cell
        }else if indexPath.section == 2{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddressCollectionViewCell", for: indexPath) as! AddressCollectionViewCell
            cell.collectAddressLabel.text = "COLLECTION :".localized()+" \(self.collectionAddress)"
            cell.dropAddressLabel.text = "DROP :".localized()+" \(self.dropAddress)"
            cell.collectionAddAdressButton.addTarget(self, action: #selector(onClickCollectionPointAddressButton(_:)), for: .touchUpInside)
            cell.dropAddAdressButton.addTarget(self, action: #selector(onClickDropPointAddressButton(_:)), for: .touchUpInside)
            return cell
        }else if indexPath.section == 3{
            if indexPath.item == 0{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectedCarCollectionViewCell", for: indexPath) as! SelectedCarCollectionViewCell
                cell.backView.layer.cornerRadius = 5.0
                cell.backView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2).cgColor
                cell.backView.layer.shadowOffset = CGSize.zero
                cell.backView.layer.shadowOpacity = 1.0
                cell.backView.layer.shadowRadius = 1.0
                cell.backView.layer.masksToBounds = false
                cell.backView.layer.borderColor = colorGreen.cgColor
                cell.backView.layer.borderWidth = 1.0

                cell.contentView.backgroundColor = UIColor.init(red: 244.0/255.0, green: 245.0/255.0, blue: 248.0/255.0, alpha: 1.0)
                if let name = self.selectCar.value(forKey: "varCar") as? String, let model = self.selectCar.value(forKey: "varModel") as? String{
                    cell.carName.text = "\(name.uppercased()) \(model.uppercased())"
                }

                if let year = self.selectCar.value(forKey: "varYear") as? String{
                   cell.yearLabel.text  = "\(year.uppercased())"
                }
                 if let varPlateCode = self.selectCar.value(forKey: "varPlateCode") as? String, let varPlateNumber = self.selectCar.value(forKey: "varPlateNumber") as? String{
                    cell.carNumber.text = varPlateNumber.uppercased()
                    cell.categoryName.text = varPlateCode.uppercased()
                }
                if let stName = self.selectCar.value(forKey: "varCarState") as? String{
                    cell.plateImageView.image = AppHelper.setNumberPlateImage(stateName: stName)
                }
                return cell
            }
            else{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ServiceCollectionViewCell", for: indexPath) as! ServiceCollectionViewCell
                cell.selectButton.isSelected = false
                cell.infoButton.isHidden = true
                cell.viewContainer.layer.cornerRadius = 5.0
                cell.viewContainer.layer.borderWidth = 1.0
                cell.viewContainer.layer.borderColor = colorGreen.cgColor
                cell.viewContainer.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2).cgColor
                cell.viewContainer.backgroundColor = UIColor.white
                cell.viewContainer.layer.shadowOffset = CGSize.zero
                cell.viewContainer.layer.shadowOpacity = 1.0
                cell.viewContainer.layer.shadowRadius = 1.0
                cell.viewContainer.layer.masksToBounds = false
                cell.contentView.backgroundColor = UIColor.init(red: 244.0/255.0, green: 245.0/255.0, blue: 248.0/255.0, alpha: 1.0)

        
                if !isCarFuel && !isCarWash && selectPart.count != 0{
                    if indexPath.item == 1{
                        if let userPic = selectPart.value(forKey: "icon") as? String{
                            let url = "\(userPic)"
                            print(url)
                            if let url2 = URL(string: url) {
                                cell.serviceImageview.sd_setImage(with: url2, placeholderImage: UIImage(named: "img_plan1"))
                            }
                        }
                        if let name = selectPart.value(forKey: "title") as? String{
                            cell.serviceNameLabel.text  = "\(name.uppercased())"
                        }
                    }
                }else if !isCarFuel && isCarWash && selectPart.count != 0 {
                    
                    if indexPath.item == 1{
                        if let userPic = selectPart.value(forKey: "icon") as? String{
                            let url = "\(userPic)"
                            print(url)
                            if let url2 = URL(string: url) {
                                cell.serviceImageview.sd_setImage(with: url2, placeholderImage: UIImage(named: "img_plan1"))
                            }
                        }
                        if let name = selectPart.value(forKey: "title") as? String{
                            cell.serviceNameLabel.text  = "\(name)"
                        }
                    }else {
                        cell.serviceImageview.image = UIImage(named: "car_wash")
                        cell.serviceNameLabel.text = "CAR WASH".localized()
                    }
                    
                }else if isCarFuel && !isCarWash && selectPart.count != 0 {
                    
                    if indexPath.item == 1{
                        if let userPic = selectPart.value(forKey: "icon") as? String{
                            let url = "\(userPic)"
                            print(url)
                            if let url2 = URL(string: url) {
                                cell.serviceImageview.sd_setImage(with: url2, placeholderImage: UIImage(named: "img_plan1"))
                            }
                        }
                        if let name = selectPart.value(forKey: "title") as? String{
                            cell.serviceNameLabel.text  = "\(name.uppercased())"
                        }
                    }else {
                        cell.serviceImageview.image = UIImage(named: "petrol")
                        cell.serviceNameLabel.text = "FUEL".localized()
                    }
                }else if isCarFuel == true && isCarWash && selectPart.count != 0 {
                    
                    if indexPath.item == 1{
                        if let userPic = selectPart.value(forKey: "icon") as? String{
                            let url = "\(userPic)"
                            print(url)
                            if let url2 = URL(string: url) {
                                cell.serviceImageview.sd_setImage(with: url2, placeholderImage: UIImage(named: "img_plan1"))
                            }
                        }
                        if let name = selectPart.value(forKey: "title") as? String{
                            cell.serviceNameLabel.text  = "\(name.uppercased())"
                        }
                    }else if indexPath.item == 2{
                        
                        cell.serviceImageview.image = UIImage(named: "petrol")
                        cell.serviceNameLabel.text = "FUEL".localized()
                    }else{
                        cell.serviceImageview.image = UIImage(named: "car_wash")
                        cell.serviceNameLabel.text = "CAR WASH".localized()
                    }
                }else if isCarFuel && isCarWash && selectPart.count == 0 {
                    
                    if indexPath.item == 1{
                        cell.serviceImageview.image = UIImage(named: "petrol")
                        cell.serviceNameLabel.text = "FUEL".localized()
                    }else{
                        cell.serviceImageview.image = UIImage(named: "car_wash")
                        cell.serviceNameLabel.text = "CAR WASH".localized()
                    }
                }else if isCarFuel && !isCarWash && selectPart.count == 0 {
                    
                    cell.serviceImageview.image = UIImage(named: "petrol")
                    cell.serviceNameLabel.text = "FUEL".localized()
                }else if !isCarFuel && isCarWash && selectPart.count == 0 {
                    
                    cell.serviceImageview.image = UIImage(named: "car_wash")
                    cell.serviceNameLabel.text = "CAR WASH".localized()
                }
                return cell
            }
        }else if indexPath.section == 4{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PaymentInfoCollectionViewCell", for: indexPath) as! PaymentInfoCollectionViewCell
            cell.totalIncludingVatLabel.text = "Total Including Vat :".localized()
            cell.discountAmount.text = String(format: "%.2f",self.discount ) +  " " + "app_currency".localized()
            cell.totalExcludingVatAmount.text = String(format: "%.2f",self.excludeVat) +  " " + "app_currency".localized()
            cell.vatAmount.text = String(format: "%.2f",self.vatPrice) +  " " + "app_currency".localized()
            cell.totalIncludingVatAmount.text =  String(format: "%.2f",self.includeVat ) +  " " + "app_currency".localized()
            
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0{
            let size:CGFloat = (self.view.frame.size.width - 5) / 1.0
            return CGSize(width: size, height: 40)
        }else if indexPath.section == 1{
                let size:CGFloat = (self.view.frame.size.width - 40) / 3.0
                return CGSize(width: size, height: size - 10)
        }else if indexPath.section == 2{
                let size:CGFloat = (self.view.frame.size.width - 5) / 1.0
                return CGSize(width: size, height: 60)
        }else if indexPath.section == 3{
            let size:CGFloat = (self.view.frame.size.width - 40) / 3.0
            return CGSize(width: size, height: size - 10)
        }else if indexPath.section == 4{
            let size:CGFloat = (self.view.frame.size.width - 5) / 1.0
            return CGSize(width: size, height: 30)
        }
        else{
            let size:CGFloat = (self.view.frame.size.width - 1) / 1
            return CGSize(width: size, height: 0)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 1{
            if isCoupanApply {
                AppHelper.showAlert("Important Message".localized(), message: "Please remove your coupon".localized())
                 return
            }
            if indexPath.item == 0{
                
                self.isCarWash = !self.isCarWash
                self.isFuelAndIsCarWash()
                aCollectionView.reloadData()

            }else if indexPath.item == 1{
                self.isCarFuel = !self.isCarFuel
                self.isFuelAndIsCarWash()
                aCollectionView.reloadData()

            }
        }
    }
    
    
    func isFuelAndIsCarWash(){
        var carWashAmount:Float = 0.0
        var carFuelAmount:Float = 0.0
        var serviceAmount:Float = 0.0
        if let getPrice = extraPrice.value(forKey: "carFuel") as? String{
            carFuelAmount = Float(getPrice) ?? 0.0
            appDelegate.Bookings.carFuel = isCarFuel ? "1" : "0"
        }
        if let getPrice = extraPrice.value(forKey: "carValet") as? String{
            carWashAmount = Float(getPrice) ?? 0.0
            appDelegate.Bookings.carValet = isCarWash ? "1" : "0"

        }
        if let packagePrice = dataDictionay.value(forKey: "varPrice") as? String{
            serviceAmount = Float(packagePrice) ?? 0.0
        }else if let packagePrice = dataDictionay.value(forKey: "varPrice") as? NSNumber{
             serviceAmount = Float(truncating: packagePrice)
        }
        if isCarWash && isCarFuel{
            let totalAmount = serviceAmount+carFuelAmount+carWashAmount
            self.srtPrice = "\(totalAmount)"
            self.excludeVat = totalAmount
            self.addVatAndPrice()

        }else if isCarWash{
            let totalAmount = serviceAmount+carWashAmount
            self.srtPrice = "\(totalAmount)"
            self.excludeVat = totalAmount

            self.addVatAndPrice()

        }else if isCarFuel{
            let totalAmount = serviceAmount+carFuelAmount
            self.srtPrice = "\(totalAmount)"
            self.excludeVat = totalAmount

            self.addVatAndPrice()

        }else{
            let totalAmount = serviceAmount
            self.srtPrice = "\(totalAmount)"
            self.excludeVat = totalAmount
            self.addVatAndPrice()

        }
    }
    
    
    func showDatePicker(){
        //Formate Date
        
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        let loc = Locale(identifier: "\(languageCode)")
        myDatePicker.locale = loc
        var calendar = Calendar.current
        calendar.locale = loc
        self.myDatePicker.calendar = calendar

        
        myDatePicker.datePickerMode = .dateAndTime
        myDatePicker.minimumDate = Date.tomorrow
        let dateFormatter = DateFormatter()
        dateFormatter.locale = loc
        dateFormatter.dateFormat = "YYYY-MM-dd"
        
        
        
        //ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .black
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.white
        toolBar.sizeToFit()
        toolBar.backgroundColor = colorGreen
        
        let doneButton = UIBarButtonItem(title: "date_time_done".localized(), style: .plain, target: self, action: #selector(self.donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "date_time_cancel".localized(), style: .plain, target: self, action: #selector(self.cancelDatePicker))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.txtSelectDateTime.inputAccessoryView = toolBar
        self.txtSelectDateTime.inputView = myDatePicker
    }
    
    @objc func donedatePicker(){
        
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        let loc = Locale(identifier: "\(languageCode)")
        myDatePicker.locale = loc
        var calendar = Calendar.current
        calendar.locale = loc
        self.myDatePicker.calendar = calendar

        myDatePicker.datePickerMode = .dateAndTime
        myDatePicker.minimumDate = Date.tomorrow
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = loc
        dateFormatter.dateFormat = "EEEE d MMMM HH:mm a"
        
        let mydateFormatter = DateFormatter()
        mydateFormatter.locale = Locale(identifier: "en")
        mydateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
       
        self.txtSelectDateTime.text = dateFormatter.string(from: myDatePicker.date)
        self.selectedDate = mydateFormatter.string(from: myDatePicker.date)

        self.setUpdateBookingFields()

        self.aCollectionView.reloadData()
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.aCollectionView.reloadData()
        self.view.endEditing(true)
    }

    
}


extension SelectYourCarBookingViewController{
    func getServiceCarItems(){
        if net.isReachable == false{
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["userId" : nUserId as AnyObject]
            //let header = ["X-API-KEY" : "smc1989"] as [String:AnyObject]
            
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objgetServiceYourCar = HttpWrapper.init()
            self.objgetServiceYourCar.delegate = self
            
            self.objgetServiceYourCar.requestWithparamdictParamPostMethodwithHeaderAndDictGet(url: kgetUserCars, dictParams: dictParameters, headers: header)
        }
        
    }
    
    
    func getPartsListItems(){
        if net.isReachable == false{
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            var languageCode = ""
            if UserDefaults.standard.value(forKey: kAppLanguage) != nil {
                languageCode = UserDefaults.standard.value(forKey: kAppLanguage) as! String
            }
            
            let dictParameters:[String:AnyObject] = ["lang" : languageCode as AnyObject]
            
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objGetPart = HttpWrapper.init()
            self.objGetPart.delegate = self
            self.objGetPart.requestWithparamdictParamPostMethodwithHeaderAndDictGet(url: kgetExtraPartImages, dictParams: dictParameters, headers: header)
        }
        
    }
    
    
    func getExtraPriceData(){
        if net.isReachable == false{
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            var languageCode = ""
            if UserDefaults.standard.value(forKey: kAppLanguage) != nil {
                languageCode = UserDefaults.standard.value(forKey: kAppLanguage) as! String
            }
            
            let dictParameters:[String:AnyObject] = ["lang" : languageCode as AnyObject]
            
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objGetExtraPrice = HttpWrapper.init()
            self.objGetExtraPrice.delegate = self
            self.objGetExtraPrice.requestWithparamdictParamPostMethodwithHeaderGet(url: kGetExtraPrice, headers: header)
        }
        
    }
    
    
    
    func checkBlockDateFromServer(selectDate:String){
        if net.isReachable == false{
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            
            var languageCode = ""
            if UserDefaults.standard.value(forKey: kAppLanguage) != nil {
                languageCode = UserDefaults.standard.value(forKey: kAppLanguage) as! String
            }
            
            
            let dictParameters:[String:AnyObject] = ["selectedDate" : selectDate as AnyObject, "lang" : languageCode as AnyObject]
            
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objPutCheckBlockDate = HttpWrapper.init()
            self.objPutCheckBlockDate.delegate = self
            self.objPutCheckBlockDate.requestWithparamdictParamPutMethodwithHeader(url: kputCheckBlockDays, dicsParams: dictParameters, headers: header)
           // self.objGetExtraPrice.requestWithparamdictParamPostMethodwithHeaderGet(url: kGetExtraPrice, headers: header)
        }
    }

    
    
    // MARK: - HttpWrapperfetchDataSuccess
    
    func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary)
    {
        AppHelper.hideLoadingView()
        print(dicsResponse)
        if wrapper == objgetServiceYourCar {
            if dicsResponse.value(forKey: "status") as? NSString == "success"{
                carArrayList.removeAllObjects()
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray{
                    carArrayList = tempNames.mutableCopy() as! NSMutableArray
                }
                print(carArrayList)
                self.aCollectionView.reloadData()
            }else{
                // AppHelper.showAlert("Alert", message:dicsResponse.value(forKey: "msg") as! String)
            }
            
        }else if wrapper == objGetPart{
            if dicsResponse.value(forKey: "status") as? NSString == "success"{
                partArrayList.removeAllObjects()
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray{
                    partArrayList = tempNames.mutableCopy() as! NSMutableArray
                }
                print(partArrayList)
                self.aCollectionView.reloadData()
            }else{
                
            }
        }else if wrapper == objGetExtraPrice{
            if dicsResponse.value(forKey: "status") as? NSString == "success"{
                if let tempNames = dicsResponse.value(forKey: "data") as? NSDictionary{
                    extraPrice = tempNames.mutableCopy() as! NSDictionary
                }
                self.aCollectionView.reloadData()
            }else{
                
            }
        }else if wrapper == objGetExtraVat{
            if dicsResponse.value(forKey: "status") as? NSString == "success"{
                if let tempNames: NSDictionary = dicsResponse.value(forKey: "data") as? NSDictionary{
                    if let vat = tempNames.value(forKey: "vat") as? String{
                        self.vatPercent = vat
                        if vat != "" {
                            self.addVatAndPrice()
                        }
                        self.aCollectionView.reloadData()
                    }
                    else  if let vat = tempNames.value(forKey: "vat")  as? NSNumber{
                        let price = Float(truncating: vat)
                        self.vatPercent = "\(price)"
                        if price != 0 {
                            self.addVatAndPrice()
                        }
                        self.aCollectionView.reloadData()
                    }
                }
            }
        }else if wrapper == objPutCheckBlockDate{
            if dicsResponse.value(forKey: "status") as? NSString == "success"{
                if let tempDic = dicsResponse.value(forKey: "data") as? NSDictionary{
                    if let tempStatus = tempDic.value(forKey: "status") as? Bool{
                        if !tempStatus{
                           self.gotoPaymentViewController()
                        }else{
                            if let tempMessage = tempDic.value(forKey: "message") as? String{
                                AppHelper.showAlert("ServiceMyCar".localized(), message: tempMessage.capitalized)
                            }
                        }
                    }
                }
            }else{
                self.gotoPaymentViewController()
            }
        }

    }
    // MARK: - HttpWrapperfetchDataFail
    
    func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError){
        AppHelper.hideLoadingView()
        print(error.debugDescription)
    }
}



extension SelectYourCarBookingViewController:AddAddress_VcDelegate{
    func getAddAddress(_ viewController: UIViewController, address isAdded: Bool) {
    
    }
    
    func getCollectionPointAddress(_ viewController: UIViewController, collectAddress address: String, newAddress: NSDictionary) {
        self.collectionAddress = address
        self.selectColectionAddress = newAddress
        self.setUpdateBookingFields()
        self.aCollectionView.reloadData()
    }
    
    func getDropPointAddress(_ viewController: UIViewController, dropAddress address: String, newAddress: NSDictionary) {
        self.dropAddress = address
        self.selectDropAddress = newAddress
        self.setUpdateBookingFields()

        self.aCollectionView.reloadData()

    }
    
}


extension SelectYourCarBookingViewController {
    
    func GetVat()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            
            AppHelper.showLoadingView()
            self.objGetExtraVat = HttpWrapper.init()
            self.objGetExtraVat.delegate = self
            //self.objGetExtraVat.requestWithparamdictParam(kgetVat)
            self.objGetExtraVat.requestWithparamdictParamPostMethodwithHeaderGet(url: kgetVat, headers: header)
        }
        
    }
    
        func addVatAndPrice()  {
            self.vatPrice =  excludeVat * Float(vatPercent)!/100
            self.includeVat =  vatPrice + excludeVat
            
            
        }
    
        
    
    
    
    
    
    @IBAction func onClickApplyCouponButton(_ sender:UIButton) {
        let nextVC = objMain.instantiateViewController(withIdentifier: "ApplyCouponViewController") as! ApplyCouponViewController
        if isCoupanApply {
            nextVC.srtPrice = srtPrice
            nextVC.couponId = self.couponId
            nextVC.coupanCode = self.coupanCode
            nextVC.excludeVatPrice = self.excludeVat
            nextVC.vatPercnt = Float(self.vatPercent) ?? 0
            nextVC.discountAmnt = self.discount
            nextVC.isApply = self.isCoupanApply
            nextVC.includeVatPrice = self.includeVat
            nextVC.srtPrice = self.srtPrice
        }else{
            nextVC.srtPrice = srtPrice
            nextVC.vatPercnt = Float(self.vatPercent) ?? 0
        }

        nextVC.delegate = self
        nextVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        let nav = UINavigationController(rootViewController: nextVC)
        nav.navigationBar.isHidden = true
        nav.modalPresentationStyle = .overCurrentContext
        nav.modalTransitionStyle = .crossDissolve
        self.present(nav, animated: true, completion: nil)
    }
}

extension SelectYourCarBookingViewController:ApplyCouponViewControllerDeledate {
    func applyCouponViewController(viewController: UIViewController, didIncludingVatAmount includingVatAmount: Float, didApplyCoupon isApplyCoupon: Bool, didDiscountAmount discountAmount: Float, didCoupanCode coupanCode: String, didSuccess success: Bool, cuopinId: String, didExcludingVatAmount excludingVatPrice: Float) {
        viewController.dismiss(animated: true, completion: nil)
        if success {
            self.includeVat = includingVatAmount
            self.excludeVat = excludingVatPrice
            self.coupanCode = coupanCode
            self.couponId = cuopinId
            self.isCoupanApply = isApplyCoupon
            self.discount  = discountAmount
            
            appDelegate.Bookings.discountAmount = isCoupanApply ? String(discountAmount) : "0"
            appDelegate.Bookings.couponId = isCoupanApply ? couponId : "0"
            if !isCoupanApply {
                self.excludeVat = Float(srtPrice) ?? 0
            }else{
                
            }
            self.addVatAndPrice()
            self.aCollectionView.reloadData()
            
        }else{
            
        }
    }
    

    

    
}


extension SelectYourCarBookingViewController {
    func setUpdateBookingFields(){

        // PacakgeData
        if let name = dataDictionay.value(forKey: "varName") as? String,let Price = dataDictionay.value(forKey: "varPrice") as? String,let PackageImageUrl = dataDictionay.value(forKey: "icon") as? String,let Id = dataDictionay.value(forKey: "intId") as? String {
            appDelegate.selectedPackagePrice = Float(Price)!
            appDelegate.Bookings.Package.PackageName = name
            appDelegate.Bookings.Package.PackegePrice = Price
            appDelegate.Bookings.Package.PackageImageUrl = PackageImageUrl
            appDelegate.Bookings.Package.Id = Id
        }
        
        // carData
        if let name = self.selectCar.value(forKey: "varCar") as? String, let model = self.selectCar.value(forKey: "varModel") as? String, let year = self.selectCar.value(forKey: "varYear") as? String, let varPlateCode = self.selectCar.value(forKey: "varPlateCode") as? String, let varPlateNumber = self.selectCar.value(forKey: "varPlateNumber") as? String, let trimEngine = self.selectCar.value(forKey: "varTrimEngine") as? String,let carState = self.selectCar.value(forKey: "varCarState") as? String{
            appDelegate.Bookings.Car.CarName = name
            appDelegate.Bookings.Car.carModel = model
            appDelegate.Bookings.Car.varPlateNumber = varPlateNumber
            appDelegate.Bookings.Car.varPlateCode  = varPlateCode
            appDelegate.Bookings.Car.carYear = year
            appDelegate.Bookings.Car.varTrimEngine = trimEngine
            appDelegate.Bookings.Car.varCarState = carState
            
        }
        appDelegate.Bookings.couponId = isCoupanApply ? couponId : "0"
        appDelegate.Bookings.discountAmount = isCoupanApply ? String(self.discount) : "0"
        appDelegate.Bookings.Package.varPackageDate = self.selectedDate
        appDelegate.Bookings.Package.dateType = "1"
        appDelegate.Bookings.carFuel = isCarFuel ? "1" : "0"
        appDelegate.Bookings.carValet = isCarWash ? "1" : "0"
        
        // Collection Address
        if let address = selectColectionAddress.value(forKey: "varAddress") as? String, let buildName = selectColectionAddress.value(forKey: "varBuildingName") as? String, let additionalText = selectColectionAddress.value(forKey: "varAdditionalInstruction") as? String, let state = selectColectionAddress.value(forKey: "varState") as? String, let country = selectColectionAddress.value(forKey: "varCountry") as? String, let latLongtiude = self.selectColectionAddress.value(forKey: "varLatLong") as? String{
            appDelegate.Bookings.DAddress.Pickup.varPackagePAddress = address
            appDelegate.Bookings.DAddress.Pickup.varBuildingPName = buildName
            appDelegate.Bookings.DAddress.Pickup.additionalPInstruction = additionalText
            appDelegate.Bookings.DAddress.Pickup.varPackagePCountry = country
            appDelegate.Bookings.DAddress.Pickup.varPackagePState = state
            
            if !latLongtiude.isEmpty{
                let getLatLong = latLongtiude.split(separator: ",")
                if getLatLong.count == 2 {
                    appDelegate.Bookings.DAddress.Pickup.Latitude = "\(getLatLong[0])"
                    appDelegate.Bookings.DAddress.Pickup.Longitude = "\(getLatLong[1])"
                }else{
                   let getLatLong = latLongtiude.split(separator: " ")
                    appDelegate.Bookings.DAddress.Pickup.Latitude = "\(getLatLong[0])"
                    appDelegate.Bookings.DAddress.Pickup.Longitude = "\(getLatLong[1])"
                }
            }else{
                appDelegate.Bookings.DAddress.Pickup.Latitude = "0"
                appDelegate.Bookings.DAddress.Pickup.Longitude = "0.0"
            }
        }
        
        
        
        // Drop Address
        if let address = selectDropAddress.value(forKey: "varAddress") as? String, let buildName = selectDropAddress.value(forKey: "varBuildingName") as? String, let additionalText = selectDropAddress.value(forKey: "varAdditionalInstruction") as? String, let state = selectDropAddress.value(forKey: "varState") as? String, let country = selectDropAddress.value(forKey: "varCountry") as? String, let latLongtiude = self.selectDropAddress.value(forKey: "varLatLong") as? String{
            appDelegate.Bookings.DAddress.Dropoff.varPackageDAddress = address
            appDelegate.Bookings.DAddress.Dropoff.varBuildingDName = buildName
            appDelegate.Bookings.DAddress.Dropoff.additionalDInstruction = additionalText
            appDelegate.Bookings.DAddress.Dropoff.varPackageDCountry = country
            appDelegate.Bookings.DAddress.Dropoff.varPackageDState = state
            if !latLongtiude.isEmpty{
                let getLatLong = latLongtiude.split(separator: ",")
                if getLatLong.count == 2{
                appDelegate.Bookings.DAddress.Dropoff.Latitude = "\(getLatLong[0])"
                appDelegate.Bookings.DAddress.Dropoff.Longitude = "\(getLatLong[1])"
                }else{
                    let getLatLong = latLongtiude.split(separator: " ")
                    appDelegate.Bookings.DAddress.Dropoff.Latitude = "\(getLatLong[0])"
                    appDelegate.Bookings.DAddress.Dropoff.Longitude = "\(getLatLong[1])"
                }
            }else{
                appDelegate.Bookings.DAddress.Dropoff.Latitude = "0"
                appDelegate.Bookings.DAddress.Dropoff.Longitude = "0"
            }
        }
        
        //SelectPart
        if let partName = selectPart.value(forKey: "title") as? String{
            let getPartName = (partName.lowercased() == "After Market Parts".lowercased() || partName == "سعر بيع قطع الغيار") ?  "aftermarketparts" : (partName.lowercased() == "Genuine Parts".lowercased() || partName == "قطع غيار أصلية المنشأ") ?  "genuineparts" : "customer_parts"
            appDelegate.Bookings.SelectedPart.varParts = getPartName
        }
        
    }
}
