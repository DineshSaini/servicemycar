//
//  MyServiceViewController.swift
//  ServiceMyCar
//
//  Created by Parikshit on 23/06/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit

class MyServiceViewController: UIViewController {
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var scrolling: UIScrollView!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var instantButton: UIButton!
    @IBOutlet weak var scheduleButton: UIButton!
    @IBOutlet weak var moovingView :UIView!
    
    let badgeButton : MIBadgeButton = MIBadgeButton(frame:CGRect(x: -10, y: 15, width: 40, height: 20))

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)
        self.title = "MY SERVICES".localized()
        self.instantButton.setTitle("Current".localized(), for: .normal)
        self.scheduleButton.setTitle("Previous".localized(), for: .normal)
        
        badgeButton.edgeInsetTop = 10
        badgeButton.setImage(UIImage(named : "notification"), for: .normal)
        print(appDelegate.NCount)
        //badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        badgeButton.badgeString = appDelegate.NCount
        if appDelegate.NCount == "0"{
            badgeButton.badgeBackgroundColor = UIColor.clear
            badgeButton.badgeTextColor = UIColor.clear
        }else{
            badgeButton.badgeBackgroundColor = colorGreen
            badgeButton.badgeTextColor = UIColor.white
        }
        badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        let barButton2 : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton2
        
        
        
        let button = UIButton.init(type: .custom)
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        
        if languageCode == "en" {
            button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
        }
        else {
            button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
        }
        button.addTarget(self, action:#selector(SideMenu), for: UIControl.Event.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        
        self.scrolling.isPagingEnabled = true
        self.scrolling.bounces = false
        self.toggleRateButton(button: instantButton)
        
        let allVc = objHomeSB.instantiateViewController(withIdentifier: "CurrentViewController") as! CurrentViewController
        self.addChild(allVc)
        let receivedVc = objHomeSB.instantiateViewController(withIdentifier: "ScheduleViewController") as! ScheduleViewController
        self.addChild(receivedVc)
        
        self.perform(#selector(LoadScollView), with: nil, afterDelay: 0.5)
        
        
        // Do any additional setup after loading the view.
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func SideMenu(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            //self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                
                badgeButton.badgeString = appDelegate.NCount
                if appDelegate.NCount == "0"
                {
                    badgeButton.badgeBackgroundColor = UIColor.clear
                    badgeButton.badgeTextColor = UIColor.clear
                }
                else
                {
                    badgeButton.badgeBackgroundColor = colorGreen
                    badgeButton.badgeTextColor = UIColor.white
                }
            }
        }
    }
    
    

    @objc func NotificationAlert()
    {
        
        guard let vcs = self.navigationController?.viewControllers else{return}
        let inStack = vcs.contains(where: { (vc) -> Bool in
            return vc is MyNotificationList_Vc
        })
        if inStack{
            self.performSegue(withIdentifier: unwindToNotificationController, sender: self)
        }else{
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
//        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
//        
//        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    

    
    
    
 
    
    func toggleRateButton(button:UIButton){
        if button == instantButton{
            self.selectButton(button: instantButton)
            self.unSelectButton(button: scheduleButton)
            
            
        }else{
            self.unSelectButton(button: instantButton)
            self.selectButton(button: scheduleButton)
            
        }
        
    }
    
    func selectButton(button:UIButton) {
        button.isSelected = true
        button.setTitleColor(colorGreen, for: .normal)
    }
    
    
    func unSelectButton(button:UIButton) {
        button.isSelected = false
        button.setTitleColor(UIColor.darkGray, for: .normal)
        
    }
    
}


let kScreenWidth = UIScreen.main.bounds.size.width

extension MyServiceViewController: UIScrollViewDelegate {
    
    @IBAction func onClickInstantButton(_ sender: UIButton){
        self.toggleRateButton(button: instantButton)
        self.raceScrollTo(CGPoint(x:0,y:0), withSnapBack: false, delegate: nil, callback: nil)
        self.raceTo(CGPoint(x:self.instantButton.frame.origin.x,y: 48), withSnapBack: false, delegate: nil, callbackmethod: nil)
    }
    
    
    @IBAction func onClickScheduleButton(_ sender: UIButton){
        self.toggleRateButton(button: scheduleButton)
        self.raceScrollTo(CGPoint(x:1*self.view.frame.size.width,y: 0), withSnapBack: false, delegate: nil, callback: nil)
        self.raceTo(CGPoint(x:self.scheduleButton.frame.origin.x,y: 48), withSnapBack: false, delegate: nil, callbackmethod: nil)
    }
    
    
    
    
    @objc func LoadScollView() {
        scrolling.delegate = nil
        scrolling.contentSize = CGSize(width:kScreenWidth * 2, height:scrolling.frame.size.height)
        for i in 0 ..< self.children.count {
            self.loadScrollViewWithPage(i)
        }
        scrolling.delegate = self
    }
    
    
    func loadScrollViewWithPage(_ page: Int) {
        if page < 0 {
            return
        }
        if page >= self.children.count {
            return
        }
        var allVC : CurrentViewController
        var addedVC : ScheduleViewController
        
        var frame: CGRect = scrolling.frame
        switch page {
        case 0:
            allVC = self.children[page] as! CurrentViewController
            allVC.viewWillAppear(true)
            frame.origin.x = frame.size.width * CGFloat(page)
            frame.origin.y = 0
            allVC.view.frame = frame
            scrolling.addSubview(allVC.view!)
            allVC.view.setNeedsLayout()
            allVC.view.layoutIfNeeded()
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
            allVC.view.setNeedsLayout()
            allVC.view.layoutIfNeeded()
            
        case 1:
            addedVC = self.children[page] as! ScheduleViewController
            addedVC.viewWillAppear(true)
            frame.origin.x = frame.size.width * CGFloat(page)
            frame.origin.y = 0
            addedVC.view.frame = frame
            scrolling.addSubview(addedVC.view!)
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
            addedVC.view.setNeedsLayout()
            addedVC.view.layoutIfNeeded()
            
            
            
            
        default:
            break
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageCurrent = Int(floor(scrolling.contentOffset.x / self.view.frame.size.width))
        switch pageCurrent {
        case 0:
            self.toggleRateButton(button: instantButton)
            self.raceScrollTo(CGPoint(x:0,y:0), withSnapBack: false, delegate: nil, callback: nil)
            self.raceTo(CGPoint(x:self.instantButton.frame.origin.x,y: 48), withSnapBack: false, delegate: nil, callbackmethod: nil)
            
        case 1:
            self.toggleRateButton(button: scheduleButton)
            self.raceScrollTo(CGPoint(x:1*self.view.frame.size.width,y: 0), withSnapBack: false, delegate: nil, callback: nil)
            self.raceTo(CGPoint(x:self.scheduleButton.frame.origin.x,y: 48), withSnapBack: false, delegate: nil, callbackmethod: nil)
            
        default:
            break
        }
    }
    
    func raceTo(_ destination: CGPoint, withSnapBack: Bool, delegate: AnyObject?, callbackmethod : (()->Void)?) {
        var stopPoint: CGPoint = destination
        if withSnapBack {
            let diffx = destination.x - moovingView.frame.origin.x
            let diffy = destination.y - moovingView.frame.origin.y
            if diffx < 0 {
                stopPoint.x -= 10.0
            }
            else if diffx > 0 {
                stopPoint.x += 10.0
            }
            
            if diffy < 0 {
                stopPoint.y -= 10.0
            }
            else if diffy > 0 {
                stopPoint.y += 10.0
            }
        }
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.2)
        UIView.setAnimationCurve(UIView.AnimationCurve.easeIn)
        moovingView.frame = CGRect(x:stopPoint.x, y:stopPoint.y, width: moovingView.frame.size.width,height: moovingView.frame.size.height)
        UIView.commitAnimations()
        let firstDelay = 0.1
        let startTime = firstDelay * Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: .now() + startTime) {
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationDuration(0.1)
            UIView.setAnimationCurve(UIView.AnimationCurve.linear)
            self.moovingView.frame = CGRect(x:destination.x, y:destination.y,width: self.moovingView.frame.size.width, height: self.moovingView.frame.size.height)
            UIView.commitAnimations()
        }
    }
    
    
    func raceScrollTo(_ destination: CGPoint, withSnapBack: Bool, delegate: AnyObject?, callback method:(()->Void)?) {
        var stopPoint = destination
        var isleft: Bool = false
        if withSnapBack {
            let diffx = destination.x - scrolling.contentOffset.x
            if diffx < 0 {
                isleft = true
                stopPoint.x -= 10
            }
            else if diffx > 0 {
                isleft = false
                stopPoint.x += 10
            }
        }
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        UIView.setAnimationCurve(UIView.AnimationCurve.easeIn)
        if isleft {
            scrolling.contentOffset = CGPoint(x:destination.x - 5, y:destination.y)
        }
        else {
            scrolling.contentOffset = CGPoint(x:destination.x + 5, y:destination.y)
        }
        
        UIView.commitAnimations()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0) {() -> Void in
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationDuration(0.1)
            UIView.setAnimationCurve(UIView.AnimationCurve.linear)
            if isleft {
                self.scrolling.contentOffset = CGPoint(x:destination.x + 5, y:destination.y)
            }
            else {
                self.scrolling.contentOffset = CGPoint(x:destination.x - 5,y: destination.y)
            }
            UIView.commitAnimations()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0){() -> Void in
                UIView.beginAnimations(nil, context: nil)
                UIView.setAnimationDuration(0.1)
                UIView.setAnimationCurve(.easeInOut)
                self.scrolling.contentOffset = CGPoint(x:destination.x, y:destination.y)
                UIView.commitAnimations()
            }
        }
    }
}

