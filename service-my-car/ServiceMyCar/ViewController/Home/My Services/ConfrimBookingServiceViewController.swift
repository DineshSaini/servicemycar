//
//  ConfrimBookingServiceViewController.swift
//  ServiceMyCar
//
//  Created by admin on 21/06/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import TelrSDK

class ConfrimBookingServiceViewController: UIViewController, HttpWrapperDelegate {
    
    
    @IBOutlet weak var aCollectionView:UICollectionView!
    @IBOutlet weak var payButton:UIButton!
    @IBOutlet weak var haveDiscountLabel:UILabel!
    @IBOutlet weak var couponApplyButton:UIButton!

    
    var additionalImage = UIImage()
    var dateAndTime = "0"
     var selectDate = ""
    

    
    
    
    
    var includeOrNotInclude = ["","","",""]
    
    var srtTittle = ""
    var srtPrice = ""
    
    
    
    var objgetServiceYourCar = HttpWrapper()
    var objGetPart = HttpWrapper()
    var objGetExtraPrice = HttpWrapper()
    var objGetUserAddress = HttpWrapper()
    var net:NetworkReachabilityManager = NetworkReachabilityManager()!
    
    
    var selectCar = NSDictionary()
    
    var collectionAddress:String = ""
    var dropAddress:String = ""
    
    var selectCollectionAddress = NSDictionary()
   
    let myDatePicker = UIDatePicker()
    
    
    var txtSelectDateTime = UITextField()
    
    var collectionAddressTextfield:UITextField!
    
    
    var collectionPicker = UIPickerView()
    var arrAddresslist = NSMutableArray()
    
    let KEY:String = "9Std-fqjHP#Q6dW2"  // TODO fill key
    let STOREID:String = "20796"         // TODO fill store id
    var EMAIL:String = ""//isLive ? "accounts@servicemycar.ae" : "Accounts@servicemycar.ae"  // TODO fill email
    var paymentRequest:PaymentRequest?
    let UserLName = UserDefaults.standard.value(forKey: kUserName) as! String
    let UserEmail = UserDefaults.standard.value(forKey: KUserLoginEmail) as! String
    let UserPhoneNo = UserDefaults.standard.value(forKey: kUserLoginPhone) as! String
    

    var objGetExtraVat = HttpWrapper()
    
    var vatPercnt :Float = 0
    var isCoupanApply:Bool = false
    var coupanCode: String = ""
    var couponId = ""
    
    var excludeVat:Float = 0
    var includeVat:Float = 0
    var vatPrice:Float = 0
    var discountAmount:Float = 0


    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.aCollectionView.dataSource = self
        self.aCollectionView.delegate = self
        self.title = "Confirm Service".localized()
        self.payButton.setTitle("Pay".localized(), for: .normal)
        self.couponApplyButton.setTitle("Apply Here".localized(), for: .normal)
        self.haveDiscountLabel.text = "have a discount  code: ".localized()
        if let UserEmail = UserDefaults.standard.value(forKey: KUserLoginEmail) as? String{
            self.EMAIL = UserEmail
        }
        let layout = JEKScrollableSectionCollectionViewLayout()
        layout.defaultScrollViewConfiguration.showsHorizontalScrollIndicator = false
        layout.itemSize = CGSize(width: 50, height: 50);
        layout.headerReferenceSize = CGSize(width: 0, height: 22)
        layout.minimumInteritemSpacing = 5
        aCollectionView.collectionViewLayout = layout
        aCollectionView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)

        self.aCollectionView.register(UINib(nibName: "ServiceHeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: JEKCollectionElementKindSectionBackground, withReuseIdentifier: "ServiceHeaderCollectionReusableView")
        self.aCollectionView.register(UINib(nibName: "HeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HeaderCollectionReusableView")
        
        self.aCollectionView.register(UINib(nibName: "HeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "HeaderCollectionReusableView")

        
        
        self.aCollectionView.register(UINib(nibName: "ServiceCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ServiceCollectionViewCell")
        self.aCollectionView.register(UINib(nibName: "EmptyCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "EmptyCollectionViewCell")
        
            self.aCollectionView.register(UINib(nibName: "PaymentInfoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PaymentInfoCollectionViewCell")
        AppHelper.makeViewCircularWithRespectToHeight(payButton, borderColor: .clear, borderWidth: 0.0)
        self.excludeVat = Float(self.srtPrice) ?? 0
        self.setUpdateBookingFields()
        
        let button = UIButton.init(type: .custom)
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        
        if languageCode == "en" {
            button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
        }
        else {
            button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
        }
        button.addTarget(self, action:#selector(onClickBackBarButton(_ :)), for: UIControl.Event.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton

        
        self.aCollectionView.reloadData()
        self.GetVat()

        // Do any additional setup after loading the view.
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.setupNavigation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //self.setupNavigationButton()
        self.getUserAddress()
    }
    
    
    @IBAction func onClickBackBarButton(_ sender:UIBarButtonItem){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    func setUpdateBookingFields(){
        
        if let name = self.selectCar.value(forKey: "varCar") as? String, let model = self.selectCar.value(forKey: "varModel") as? String, let year = self.selectCar.value(forKey: "varYear") as? String, let varPlateCode = self.selectCar.value(forKey: "varPlateCode") as? String, let varPlateNumber = self.selectCar.value(forKey: "varPlateNumber") as? String, let trimEngine = self.selectCar.value(forKey: "varTrimEngine") as? String{
            appDelegate.Bookings.Car.CarName = name
            appDelegate.Bookings.Car.carModel = model
            appDelegate.Bookings.Car.varPlateNumber = varPlateNumber
            appDelegate.Bookings.Car.varPlateCode  = varPlateCode
            appDelegate.Bookings.Car.carYear = year
            appDelegate.Bookings.Car.varTrimEngine = trimEngine
            
        }
        
        appDelegate.Bookings.Package.varPackageDate = self.dateAndTime
        appDelegate.Bookings.Package.dateType = "1"
        
        
        // Collection Address
        if let address = selectCollectionAddress.value(forKey: "varAddress") as? String, let buildName = selectCollectionAddress.value(forKey: "varBuildingName") as? String, let additionalText = selectCollectionAddress.value(forKey: "varAdditionalInstruction") as? String, let state = selectCollectionAddress.value(forKey: "varState") as? String, let country = selectCollectionAddress.value(forKey: "varCountry") as? String, let latLongtiude = self.selectCollectionAddress.value(forKey: "varLatLong") as? String{
            appDelegate.Bookings.DAddress.Pickup.varPackagePAddress = address
            appDelegate.Bookings.DAddress.Pickup.varBuildingPName = buildName
            appDelegate.Bookings.DAddress.Pickup.additionalPInstruction = additionalText
            appDelegate.Bookings.DAddress.Pickup.varPackagePCountry = country
            appDelegate.Bookings.DAddress.Pickup.varPackagePState = state
            
            if !latLongtiude.isEmpty{
                let getLatLong = latLongtiude.split(separator: ",")
                if getLatLong.count == 2{
                appDelegate.Bookings.DAddress.Pickup.Latitude = "\(getLatLong[0])"
                appDelegate.Bookings.DAddress.Pickup.Longitude = "\(getLatLong[1])"
                }else{
                    let getLatLong = latLongtiude.split(separator: " ")
                        appDelegate.Bookings.DAddress.Pickup.Latitude = "\(getLatLong[0])"
                        appDelegate.Bookings.DAddress.Pickup.Longitude = "\(getLatLong[1])"
                }
            }else {
                appDelegate.Bookings.DAddress.Pickup.Latitude = "0"
                appDelegate.Bookings.DAddress.Pickup.Longitude = "0"
            }
        }
    }
    
    
    
    @IBAction func onClickCollectionPointAddressButton(_ sender:UIBarButtonItem){
        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "AddAddress_Vc") as! AddAddress_Vc
        nextViewController.addressTypeString = "Enter Collection Address"
        nextViewController.editAddress = self.selectCollectionAddress
        nextViewController.isEditingAddress = true

        nextViewController.delegate = self
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    
    @IBAction func onClickConfrimButton(_ sender:UIButton){
        
        if selectCar.count == 0{
            AppHelper.showAlert("ServiceMyCar".localized(), message: "Please Select your Car!".localized())
            return
        }
        if collectionAddress.isEmpty{
            AppHelper.showAlert("ServiceMyCar".localized(), message: "Please Select your Collection Address !".localized())
            return
        }
        
        guard let dateAndTime = txtSelectDateTime.text else {
            AppHelper.showAlert("ServiceMyCar".localized(), message: "Please Select your Date and Time!".localized())
            return
        }
        let nextViewController = objMain.instantiateViewController(withIdentifier: "TelrController") as! TelrController
        appDelegate.Bookings.Price.total = self.srtPrice
        paymentRequest = preparePaymentRequest()
        nextViewController.paymentRequest = paymentRequest!
//        nextViewController.navigationItem.leftBarButtonItem?.isEnabled = true
//        nextViewController.navigationController?.navigationBar.clipsToBounds = false
//        nextViewController.navigationController?.navigationBar.isTranslucent = false
//        nextViewController.navigationController?.navigationBar.barTintColor = backGroundColor
//        nextViewController.navigationController?.navigationBar.backgroundColor = backGroundColor
//        nextViewController.navigationController?.navigationBar.isOpaque = false
        self.navigationController?.pushViewController(nextViewController, animated: true)


    }
    
    
    @IBAction func tappedOnNeedHelp(_ sender:UIButton){
        sender.isSelected = !sender.isSelected
    }
    
    
    
    
    func setupNavigationButton(){
        let button = UIButton.init(type: .custom)
        button.frame = CGRect.init(x: 0, y: 0, width:35, height: 35)
        button.setImage(UIImage(named: "plus_green"), for: .normal)
        button.addTarget(self, action: #selector(onClickAddNavigation(_:)), for: .touchUpInside)
        let barButton : UIBarButtonItem = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
        
    }
    
    @IBAction func onClickAddNavigation(_ sender: UIButton){
        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "AddCar_Vc") as! AddCar_Vc
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    
    private func preparePaymentRequest() -> PaymentRequest{
        if let UserEmail = UserDefaults.standard.value(forKey: KUserLoginEmail) as? String{
            self.EMAIL = UserEmail
        }
        let paymentReq = PaymentRequest()
        paymentReq.key = KEY
        paymentReq.store = STOREID
        paymentReq.appId = "123456789"
        paymentReq.appName = "ServiceMyCar"
        paymentReq.appUser = "123456"
        paymentReq.appVersion = "0.0.1"
        paymentReq.transTest = Istesting
        paymentReq.transType = "sale"
        paymentReq.transClass = "paypage"
        paymentReq.transCartid = String(arc4random())
        paymentReq.transDesc = isLive ? "Booking on Servicemycar.ae" : "service payment"
        paymentReq.transCurrency = "AED"
        
        appDelegate.Bookings.telr_value = String(includeVat)
        paymentReq.transAmount = String(includeVat)
        paymentReq.transLanguage = "en"
        paymentReq.billingEmail = EMAIL
        
        let name = UserLName.split(separator: " ")
        
        
        paymentReq.billingFName = "\(name[0])"
        paymentReq.billingLName = "\(name[1])"
        paymentReq.billingTitle = "Mr"
        paymentReq.city = "Dubai"
        paymentReq.country = "AE"
        paymentReq.region = "Dubai"
        paymentReq.address = "line 1"
        paymentReq.billingPhone = UserPhoneNo
        
        
        return paymentReq
    }
    
    
  /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if let destination = segue.destination as? TelrController{
            destination.paymentRequest = paymentRequest!
        }
    }*/
    
}
extension ConfrimBookingServiceViewController:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return 2
        }else{
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let edgeInsets = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        if section == 0{
                let edgeInsets = UIEdgeInsets.init(top: 0, left: self.view.frame.size.width/6.0, bottom: 0, right: 0)
                return edgeInsets
        }else{
            return edgeInsets
        }
    }

    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if section == 0{
            return CGSize(width: self.view.frame.size.width, height: 20.0)
            
        }else{
            return CGSize(width: self.view.frame.size.width, height: 20.0)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: self.view.frame.size.width, height: 0.0)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if (kind == UICollectionView.elementKindSectionHeader) {
            
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderCollectionReusableView", for: indexPath) as! HeaderCollectionReusableView
            let index = indexPath.first ?? 0
            headerView.headerTittle.text = self.includeOrNotInclude[index].localized()
            headerView.infoImage.isHidden = true
           
            return headerView
        }else{
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderCollectionReusableView", for: indexPath) as! HeaderCollectionReusableView
            return headerView
            
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0{
            if indexPath.item == 0{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ConfrimBookingCarServiceCollectionViewCell", for: indexPath) as! ConfrimBookingCarServiceCollectionViewCell
                cell.backView.layer.borderColor = colorGreen.cgColor
                cell.backView.layer.borderWidth = 1.0
                cell.backView.layer.cornerRadius = 5.0
                cell.backView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2).cgColor
                cell.backView.layer.shadowOffset = CGSize.zero
                cell.backView.layer.shadowOpacity = 1.0
                cell.backView.layer.shadowRadius = 1.0
                cell.backView.layer.masksToBounds = false
                cell.contentView.backgroundColor = UIColor.init(red: 244.0/255.0, green: 245.0/255.0, blue: 248.0/255.0, alpha: 1.0)
                if let name = self.selectCar.value(forKey: "varCar") as? String, let model = self.selectCar.value(forKey: "varModel") as? String{
                    cell.carName.text = "\(name.uppercased()) \(model.uppercased())"
                }
                
                if let year = self.selectCar.value(forKey: "varYear") as? String{
                    cell.yearLabel.text  = "\(year.uppercased())"
                }
                if let varPlateCode = self.selectCar.value(forKey: "varPlateCode") as? String, let varPlateNumber = self.selectCar.value(forKey: "varPlateNumber") as? String{
                    cell.carNumber.text = varPlateNumber.uppercased()
                    cell.categoryName.text = varPlateCode.uppercased()
                }
                if let stName = self.selectCar.value(forKey: "varCarState") as? String{
                    cell.plateImageView.image = AppHelper.setNumberPlateImage(stateName: stName)
                }
                return cell
            }else{
               let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ServiceCollectionViewCell", for: indexPath) as! ServiceCollectionViewCell
                cell.viewContainer.layer.borderWidth = 1.0
                cell.viewContainer.layer.borderColor = colorGreen.cgColor

                cell.serviceImageview.image = self.additionalImage
                cell.serviceNameLabel.text  = "\(srtTittle.uppercased()) \n \(srtPrice)" + "app_currency".localized()
                return cell
            }
        }else if indexPath.section == 2{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ConfrimBookingAddressCollectionViewCell", for: indexPath) as! ConfrimBookingAddressCollectionViewCell
            cell.collectionAddAdressButton.addTarget(self, action: #selector(onClickCollectionPointAddressButton(_:)), for: .touchUpInside)
            cell.backView.backgroundColor = UIColor.init(red: 244.0/255.0, green: 245.0/255.0, blue: 248.0/255.0, alpha: 1.0)
            cell.collectAddressTextfield.placeholder = "Collection Point".localized()
            self.collectionAddressTextfield = cell.collectAddressTextfield
            let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
            if languageCode != "en" {
                cell.collectAddressTextfield.textAlignment = .right
            }
            cell.collectAddressTextfield.text = collectionAddress
            cell.collectAddressTextfield.isUserInteractionEnabled = false
            //self.setupCollectionPicker(textField: cell.collectAddressTextfield)
            return cell
        }else if indexPath.section == 1{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ConfrimBookingDateCollectionViewCell", for: indexPath) as! ConfrimBookingDateCollectionViewCell
            cell.setLeftIconForTextField(cell.dateAndTimeTextfield, leftIcon: UIImage())
            cell.dateAndTimeTextfield.placeholder = "Select Date And Time".localized()
            let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
            if languageCode != "en" {
                cell.dateAndTimeTextfield.textAlignment = .right
            }
            if txtSelectDateTime.text?.count == 0{
                cell.dateAndTimeTextfield.text = self.selectDate
            }
            txtSelectDateTime = cell.dateAndTimeTextfield

            self.showDatePicker()
            cell.setNeedsLayout()
            return cell
        } else if indexPath.section == 3 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PaymentInfoCollectionViewCell", for: indexPath) as! PaymentInfoCollectionViewCell
            
            cell.totalIncludingVatLabel.text = "Total Including Vat :".localized()
            cell.discountAmount.text = String(format: "%.2f",self.discountAmount ) +  " " + "app_currency".localized()
            cell.totalExcludingVatAmount.text = String(format: "%.2f",self.excludeVat) +  " " + "app_currency".localized()
            cell.vatAmount.text = String(format: "%.2f",self.vatPrice) +  " " + "app_currency".localized()
            cell.totalIncludingVatAmount.text =  String(format: "%.2f",self.includeVat ) +  " " + "app_currency".localized()
            
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0{
                let size:CGFloat = (self.view.frame.size.width - 20) / 3.0
                return CGSize(width : size, height: size+10)
        }else if indexPath.section == 3 {
            let size:CGFloat = (self.view.frame.size.width - 1) / 1
            return CGSize(width: size, height: 60)
        }
        else{
            let size:CGFloat = (self.view.frame.size.width - 1) / 1
            return CGSize(width: size, height: 60)
        }
    }
    
    func showDatePicker(){
        //Formate Date
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
            let loc = Locale(identifier: "\(languageCode)")
            myDatePicker.locale = loc
            var calendar = Calendar.current
            calendar.locale = loc
            self.myDatePicker.calendar = calendar
//        var calendar = Calendar.current
//        calendar.locale = Locale(identifier: "da")
//        let formatter = DateComponentsFormatter()
//        formatter.calendar = calendar
        
        myDatePicker.datePickerMode = .dateAndTime
        myDatePicker.minimumDate = Date.tomorrow
        let dateFormatter = DateFormatter()
        dateFormatter.locale = loc
        dateFormatter.dateFormat = "YYYY-MM-dd"
        

        
        //ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .black
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.white
        toolBar.sizeToFit()
        toolBar.backgroundColor = colorGreen
        
        let doneButton = UIBarButtonItem(title: "date_time_done".localized(), style: .plain, target: self, action: #selector(self.donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "date_time_cancel".localized(), style: .plain, target: self, action: #selector(self.cancelDatePicker))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.txtSelectDateTime.inputAccessoryView = toolBar
        self.txtSelectDateTime.inputView = myDatePicker
    }
    
    @objc func donedatePicker(){
        
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        let loc = Locale(identifier: "\(languageCode)")
        myDatePicker.locale = loc
        var calendar = Calendar.current
        calendar.locale = loc
        self.myDatePicker.calendar = calendar

        myDatePicker.datePickerMode = .dateAndTime
        myDatePicker.minimumDate = Date.tomorrow
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = loc
        dateFormatter.dateFormat = "EEEE d MMMM HH:mm a"
        
        let mydateFormatter = DateFormatter()
        mydateFormatter.locale = Locale(identifier: "en")
        mydateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        self.dateAndTime = mydateFormatter.string(from: myDatePicker.date)
        
        self.txtSelectDateTime.text = dateFormatter.string(from: myDatePicker.date)
//        guard let bookingTime = self.txtSelectDateTime.text else{
//            return
//        }
      

        //self.dateAndTime = bookingTime
        self.setUpdateBookingFields()
        
        self.aCollectionView.reloadData()
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.aCollectionView.reloadData()
        self.view.endEditing(true)
    }
    
    
}


extension ConfrimBookingServiceViewController{

    func getUserAddress(){
        if net.isReachable == false{
            
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }else{
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["userId" : nUserId as AnyObject]
            //let header = ["X-API-KEY" : "smc1989"] as [String:AnyObject]
            
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objGetUserAddress = HttpWrapper.init()
            self.objGetUserAddress.delegate = self
            
            self.objGetUserAddress.requestWithparamdictParamPostMethodwithHeaderAndDictGet(url: kgetUserAddress, dictParams: dictParameters, headers: header)
        }
    }
    
    
    // MARK: - HttpWrapperfetchDataSuccess
    
    func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary)
    {
        AppHelper.hideLoadingView()
        print(dicsResponse)
        if wrapper == objGetUserAddress{
            if dicsResponse.value(forKey: "status") as? NSString == "success"{
                arrAddresslist.removeAllObjects()
                
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray{
                    arrAddresslist = tempNames.mutableCopy() as! NSMutableArray
                }
                print(arrAddresslist)
                aCollectionView.reloadData()
            }else{
                // AppHelper.showAlert("Alert", message:dicsResponse.value(forKey: "msg") as! String)
            }
        }else if wrapper == objGetExtraVat{
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                
                
                if let tempNames: NSDictionary = dicsResponse.value(forKey: "data") as? NSDictionary
                {
                    if let vat = tempNames.value(forKey: "vat") as? String{
                            self.vatPercnt = Float(vat) ?? 0
                            self.addVatAndPrice()

                    }
                    else  if let vat = tempNames.value(forKey: "vat")  as? NSNumber
                    {
                        let price = Float(truncating: vat)
                        self.vatPercnt = price
                        self.addVatAndPrice()

                        
                    }
                    
                }
                
            }
            else
            {
                //                AppHelper.showAlert("app_alert_title".localized(), message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }

    }
    // MARK: - HttpWrapperfetchDataFail
    
    func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError){
        AppHelper.hideLoadingView()
        print(error.debugDescription)
    }
}


extension ConfrimBookingServiceViewController:AddAddress_VcDelegate{
    func getAddAddress(_ viewController: UIViewController, address isAdded: Bool) {
    }
    
    func getCollectionPointAddress(_ viewController: UIViewController, collectAddress address: String, newAddress: NSDictionary) {
        self.collectionAddress = address
        self.selectCollectionAddress = newAddress
        self.setUpdateBookingFields()

        self.aCollectionView.reloadData()
    }
    
    func getDropPointAddress(_ viewController: UIViewController, dropAddress address: String, newAddress: NSDictionary) {
        self.dropAddress = address

        self.aCollectionView.reloadData()
    }
}



extension ConfrimBookingServiceViewController:UIPickerViewDataSource,UIPickerViewDelegate{
    
    
    
    // Mark:- SetUp carCategory picker
    
    @IBAction func onClickSelectCollectAddressType(_ sender: UIBarButtonItem){
        if self.arrAddresslist.count == 0 {
            self.view.endEditing(true)
            return
            
        }
        let selectAddress  = arrAddresslist[collectionPicker.selectedRow(inComponent: 0)]
        if let address = (selectAddress as AnyObject).value(forKey: "varAddress") as? String, let buildName = (selectAddress as AnyObject).value(forKey: "varBuildingName") as? String, let additionalText = (selectAddress as AnyObject).value(forKey: "varAdditionalInstruction") as? String, let state = (selectAddress as AnyObject).value(forKey: "varState") as? String, let country = (selectAddress as AnyObject).value(forKey: "varCountry") as? String{
            self.collectionAddress = "\(address) \(buildName) \(additionalText) \(state) \(country)"
            self.collectionAddressTextfield.text = self.collectionAddress
        }
        self.aCollectionView.reloadData()
        self.view.endEditing(true)
    }
    
    
    @IBAction func onClickSelectCancelType(_ sender: UIBarButtonItem){
        self.aCollectionView.reloadData()
        self.view.endEditing(true)
        
    }
    
    func setupCollectionPicker(textField:UITextField){
        
        // UIPickerView
        self.collectionPicker = UIPickerView()
        self.collectionPicker.delegate = self
        self.collectionPicker.dataSource = self
        self.collectionPicker.backgroundColor = UIColor.white
        
        textField.inputView = self.collectionPicker
        collectionPicker.reloadAllComponents()
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .black
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.white
        toolBar.sizeToFit()
        toolBar.backgroundColor = colorGreen
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "add_your_car_done".localized(), style: .plain, target: self, action: #selector(onClickSelectCollectAddressType(_:)))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "add_your_car_cancel".localized(), style: .plain, target: self, action: #selector(onClickSelectCancelType(_:)))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    
    
    
    
    
    //MARK:- PickerView Delegate & DataSource
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if arrAddresslist.count != 0{
            return arrAddresslist.count
        }else{
            return 0
        }
        
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label: UILabel
        
        if let view = view {
            label = view as! UILabel
        }
        else {
            label = UILabel(frame: CGRect(x: 0, y: 0, width: pickerView.frame.width, height: 400))
        }
        
        label.lineBreakMode = .byWordWrapping;
        if arrAddresslist.count != 0{
            if pickerView == collectionPicker{
                self.selectCollectionAddress = (arrAddresslist.object(at: row) as! NSDictionary)
                if let address = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varAddress") as? String, let buildName = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varBuildingName") as? String, let additionalText = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varAdditionalInstruction") as? String, let state = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varState") as? String, let country = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varCountry") as? String{
                    label.text = "\(address) \(buildName) \(additionalText) \(state) \(country)"
                }
            }
        }else{
            label.text = "tet"
        }
        label.numberOfLines = 2;
        //label.text = arr[row]
        label.sizeToFit()
        return label;
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if arrAddresslist.count != 0{
            if pickerView == collectionPicker{
                self.selectCollectionAddress = (arrAddresslist.object(at: row) as! NSDictionary)
                if let address = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varAddress") as? String, let buildName = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varBuildingName") as? String, let additionalText = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varAdditionalInstruction") as? String, let state = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varState") as? String, let country = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varCountry") as? String{
                    self.collectionAddress = "\(address) \(buildName) \(additionalText) \(state) \(country)"
                    self.collectionAddressTextfield.text = self.collectionAddress
                }
            }
        }
        
    }
}

extension ConfrimBookingServiceViewController {
    
    func GetVat()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            
            AppHelper.showLoadingView()
            self.objGetExtraVat = HttpWrapper.init()
            self.objGetExtraVat.delegate = self
            //self.objGetExtraVat.requestWithparamdictParam(kgetVat)
            self.objGetExtraVat.requestWithparamdictParamPostMethodwithHeaderGet(url: kgetVat, headers: header)
        }
        
        
        
    }
    
    
    @IBAction func onClickApplyCouponButton(_ sender:UIButton) {
        let nextVC = objMain.instantiateViewController(withIdentifier: "ApplyCouponViewController") as! ApplyCouponViewController
        if isCoupanApply {
            nextVC.includeVatPrice = self.includeVat
            nextVC.excludeVatPrice = self.excludeVat
            nextVC.isApply = self.isCoupanApply
            nextVC.vatPercnt = self.vatPercnt
            nextVC.coupanCode = self.coupanCode
            nextVC.couponId = self.couponId
            nextVC.discountAmnt = self.discountAmount
            nextVC.srtPrice = self.srtPrice
            
        }else{
            nextVC.srtPrice = srtPrice
            nextVC.vatPercnt = self.vatPercnt
        }

   

        nextVC.delegate = self

        nextVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        let nav = UINavigationController(rootViewController: nextVC)
        nav.navigationBar.isHidden = true
        nav.modalPresentationStyle = .overCurrentContext
        nav.modalTransitionStyle = .crossDissolve
        self.present(nav, animated: true, completion: nil)
    }
    

    
    
    func addVatAndPrice()  {
        self.vatPrice =  excludeVat * Float(vatPercnt)/100
        self.includeVat =  vatPrice + excludeVat
        
        
    }
    
}

extension ConfrimBookingServiceViewController:ApplyCouponViewControllerDeledate {
    func applyCouponViewController(viewController: UIViewController, didIncludingVatAmount includingVatAmount: Float, didApplyCoupon isApplyCoupon: Bool, didDiscountAmount discountAmount: Float, didCoupanCode coupanCode: String, didSuccess success: Bool, cuopinId: String, didExcludingVatAmount excludingVatPrice: Float) {
        viewController.dismiss(animated: false, completion: nil)
        if success {
            
            self.couponId = cuopinId
            self.coupanCode = coupanCode
            self.isCoupanApply = isApplyCoupon
            self.discountAmount =  discountAmount
            self.includeVat = includingVatAmount
            self.excludeVat = excludingVatPrice
            
            if !isCoupanApply {
                self.excludeVat = Float(srtPrice) ?? 0
            }
            
            self.addVatAndPrice()

            self.aCollectionView.reloadData()
            
        }
    }
    
    
}


extension TelrController {
    @IBAction func onClickBackButton(_ sender:UIButton) {
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        if languageCode == "en" {
         
        }else {
          
        }
        self.navigationController?.popViewController(animated: true)
    }
}
