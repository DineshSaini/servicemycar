//
//  BasicServiceViewController.swift
//  ServiceMyCar
//
//  Created by admin on 18/06/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class BasicServiceViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,HttpWrapperDelegate {
    
    
    @IBOutlet weak var basicCollectionView:UICollectionView!
    
    @IBOutlet weak var bookNowButton:UIButton!
    @IBOutlet weak var partInfoLabel:UILabel!
    
    var basicCollection = NSDictionary()
    var basicIncludedArray = NSMutableArray()
    var basicNotIncludedArray = NSMutableArray()
    
    var objgetPackagesItem = HttpWrapper()
    var net:NetworkReachabilityManager = NetworkReachabilityManager()!
    
    override func viewDidAppear(_ animated: Bool) {
        self.getPackagesItems()
    }
    
    
    let columnLayout = FlowLayout(
        itemSize: CGSize(width: 200, height: 200),
        minimumInteritemSpacing: 0,
        minimumLineSpacing: 0,
        sectionInset: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    )
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.basicCollectionView.delegate = self
        self.basicCollectionView.delegate = self
        
        basicCollectionView?.collectionViewLayout = columnLayout
        basicCollectionView?.contentInsetAdjustmentBehavior = .automatic
        
        self.basicCollectionView.register(UINib(nibName: "ServiceCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ServiceCollectionViewCell")
        
        self.basicCollectionView.register(UINib(nibName: "HeaderSectionCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HeaderSectionCollectionViewCell")
        
        
         self.basicCollectionView.register(UINib(nibName: "EmptyCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "EmptyCollectionViewCell")

        
        self.basicCollectionView.register(UINib(nibName: "ServiceHeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "ServiceHeaderCollectionReusableView")
        AppHelper.makeViewCircularWithRespectToHeight(bookNowButton, borderColor: .clear, borderWidth: 0.0)
        
        if let dataArray = basicCollection.value(forKey: "partsIncludedSpec") as? NSArray{
            basicIncludedArray = dataArray.mutableCopy() as! NSMutableArray
        }
        
        if let dataArray = basicCollection.value(forKey: "partsNotIncludedSpec") as? NSArray{
            basicNotIncludedArray = dataArray.mutableCopy() as! NSMutableArray
        }
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.getServiceInfoNotification(_:)),
                                               name: NSNotification.Name(rawValue: "GetServiceInfoBasicNotification"),
                                               object: nil)
        //self.getPackagesItems()
    
        self.basicCollectionView.reloadData()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        self.bookNowButton.setTitle("Book Now".localized(), for: .normal)
        self.partInfoLabel.text = "All part are sourced at the lowest price and the highest quality".localized()
    }
    
    @objc func getServiceInfoNotification(_ notification: NSNotification) {
        if notification.name.rawValue == "GetServiceInfoBasicNotification" {
            //self.toggleAuthUI()
            if let serviceInfo = notification.userInfo as? [String:AnyObject] {
                if let service = serviceInfo["statusText"] as? NSDictionary{
                    self.basicCollection = service
                    self.getPackagesItems()
                }
            }
        }
    }
    
    
    
    @IBAction func onClickBookNowButton(_ sender:UIButton){
        let nextViewController = objServiceSB.instantiateViewController(withIdentifier:"SelectCarServiceViewController") as! SelectCarServiceViewController
        nextViewController.dataDictionay = self.basicCollection
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    
    // MARK: - getPackagesListByCategory API
    
    func getPackagesItems()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            var languageCode = ""
            var packageID = ""
            if UserDefaults.standard.value(forKey: kAppLanguage) != nil {
                languageCode = UserDefaults.standard.value(forKey: kAppLanguage) as! String
            }
            
            if let packageId = basicCollection.value(forKey: "intId") as? String{
                packageID = packageId
            }else if let packageId = basicCollection.value(forKey: "intId") as? Int{
                packageID = "\(packageId)"
            }
            
            
            //getPackageListByID?packageId=4&lang=en
            
            let dictParameters:[String:AnyObject] = ["packageId" : packageID as AnyObject,
                                                     "lang" : languageCode as AnyObject]
            
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objgetPackagesItem = HttpWrapper.init()
            self.objgetPackagesItem.delegate = self
            self.objgetPackagesItem.requestWithparamdictParamPostMethodwithHeaderAndDictGet(url: kgetPackageListByID, dictParams: dictParameters, headers: header)
        }
        
    }
    
    
    // MARK: - HttpWrapperfetchDataSuccess
    
    func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary)
    {
        AppHelper.hideLoadingView()
        print(dicsResponse)
        if wrapper == objgetPackagesItem {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                basicIncludedArray.removeAllObjects()
                basicNotIncludedArray.removeAllObjects()

                
                if let tempNames: NSDictionary = dicsResponse.value(forKey: "data") as? NSDictionary
                {
                    basicCollection = tempNames.mutableCopy() as! NSMutableDictionary
                    if let dataArray = basicCollection.value(forKey: "partsIncludedSpec") as? NSArray{
                        basicIncludedArray = dataArray.mutableCopy() as! NSMutableArray
                        self.basicCollectionView.reloadData()

                    }
                    
                    if let dataArray = basicCollection.value(forKey: "partsNotIncludedSpec") as? NSArray{
                        basicNotIncludedArray = dataArray.mutableCopy() as! NSMutableArray
                        self.basicCollectionView.reloadData()

                    }
                }
                print(basicIncludedArray)
                print(basicNotIncludedArray)
                //self.basicCollectionView.reloadData()
            }
            else
            {
                // AppHelper.showAlert("Alert", message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }
        
    }
    // MARK: - HttpWrapperfetchDataFail
    
    func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError)
    {
        AppHelper.hideLoadingView()
        print(error.debugDescription)
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else  if section == 1{
            if basicIncludedArray.count == 0{
                return 1
            }else{
                return basicIncludedArray.count
            }
        }else  if section == 2{
            return 1
        }else{
            if basicNotIncludedArray.count == 0{
                return 1
            }else{
                return basicNotIncludedArray.count
            }
        }
    }

    @IBAction func onClickInfoButton(_ sender:UIButton){
        if  let indexpath = sender.collectionViewIndexPath(self.basicCollectionView) as IndexPath?{
            if indexpath.section == 0{
                AppHelper.showAlert("app_alert_title".localized(), message: "This is what's included in the service package".localized())
                
            }
        }
    }
    
    
    @IBAction func onClickNotInfoButton(_ sender:UIButton){
        if  let indexpath = sender.collectionViewIndexPath(self.basicCollectionView) as IndexPath?{
            if indexpath.section == 2{
                AppHelper.showAlert("app_alert_title".localized(), message: "This is what's not included in the Service package".localized())
                
            }
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.section == 0{
            let headerView = collectionView.dequeueReusableCell(withReuseIdentifier: "HeaderSectionCollectionViewCell", for: indexPath) as! HeaderSectionCollectionViewCell
            headerView.headerTittle.text = "What included".localized()
            headerView.sendSubviewToBack(headerView.infoImage)
            headerView.infoImage.setImage(UIImage(named: "info"), for: .normal)
            headerView.infoImage.isHidden = false
                headerView.infoImage.addTarget(self, action: #selector(onClickInfoButton(_:)), for:.touchUpInside)
           
            return headerView
        }else if indexPath.section == 2{
            let headerView = collectionView.dequeueReusableCell(withReuseIdentifier: "HeaderSectionCollectionViewCell", for: indexPath) as! HeaderSectionCollectionViewCell
            headerView.headerTittle.text = "What's not Included (Parts)".localized()
            headerView.sendSubviewToBack(headerView.infoImage)
            headerView.infoImage.setImage(UIImage(named: "info"), for: .normal)
            headerView.infoImage.isHidden = false
            headerView.infoImage.addTarget(self, action: #selector(onClickNotInfoButton(_:)), for:.touchUpInside)
            return headerView
        }else{
            
            if basicIncludedArray.count == 0 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EmptyCollectionViewCell", for: indexPath) as! EmptyCollectionViewCell
                return cell
            }else if basicNotIncludedArray.count == 0{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EmptyCollectionViewCell", for: indexPath) as! EmptyCollectionViewCell
                return cell
            }else{
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ServiceCollectionViewCell", for: indexPath) as! ServiceCollectionViewCell
        
        cell.viewContainer.layer.cornerRadius = 5.0
        cell.viewContainer.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2).cgColor
        cell.viewContainer.backgroundColor = colorBackGroundColor//UIColor.white
        cell.viewContainer.layer.shadowOffset = CGSize.zero
        cell.viewContainer.layer.shadowOpacity = 1.0
        cell.viewContainer.layer.shadowRadius = 0.5
        cell.viewContainer.layer.masksToBounds = false
        cell.contentView.backgroundColor = colorBackGroundColor//UIColor.init(red: 244.0/255.0, green: 245.0/255.0, blue: 248.0/255.0, alpha: 1.0)
        
        if indexPath.section == 1{
            if let name = (basicIncludedArray.object(at: indexPath.row) as! NSDictionary).value(forKey: "varSpecification") as? String{
                cell.serviceNameLabel.text = name.uppercased()
            }
            
            if let UserProfilePic = (basicIncludedArray.object(at: indexPath.row) as! NSDictionary).value(forKey: "varImage") as? String{
                let url = "\(UserProfilePic)"
                print(url)
                
                if let url2 = URL(string: url) {
                    cell.serviceImageview.sd_setImage(with: url2, placeholderImage: UIImage(named: "img_plan1"))
                }
            }
            
        }else{
            if let name = (basicNotIncludedArray.object(at: indexPath.row) as! NSDictionary).value(forKey: "varSpecification") as? String{
                cell.serviceNameLabel.text = name.uppercased()
            }
            
            if let UserProfilePic = (basicNotIncludedArray.object(at: indexPath.row) as! NSDictionary).value(forKey: "varImage") as? String{
                let url = "\(UserProfilePic)"
                print(url)
                
                if let url2 = URL(string: url) {
                    cell.serviceImageview.sd_setImage(with: url2, placeholderImage: UIImage(named: "img_plan1"))
                }
            }
        }
        return cell
            }
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0{
            let size:CGFloat = (self.view.frame.size.width) / 1.0
            return CGSize(width: size, height: 50)
        }else if indexPath.section == 2{
            let size:CGFloat = (self.view.frame.size.width) / 1.0
            return CGSize(width: size, height: 50)
        }else{
            if indexPath.section == 1{
                if basicIncludedArray.count == 0{
                    let size:CGFloat = (self.view.frame.size.width) / 1.0
                    return CGSize(width: size, height: 150)
                    
                }else{
                    let size:CGFloat = (self.view.frame.size.width - 20) / 3.0
                    return CGSize(width: size, height: size)
                }
                
            }else{
                if basicNotIncludedArray.count == 0{
                    let size:CGFloat = (self.view.frame.size.width) / 1.0
                    return CGSize(width: size, height: 150)
                    
                }else{
                    let size:CGFloat = (self.view.frame.size.width - 20) / 3.0
                    return CGSize(width: size, height: size)
                }
            }
        }
        return CGSize()
    }
    
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        basicCollectionView?.collectionViewLayout.invalidateLayout()
        super.viewWillTransition(to: size, with: coordinator)
    }

}


class FlowLayout: UICollectionViewFlowLayout {
    
    required init(itemSize: CGSize, minimumInteritemSpacing: CGFloat = 0, minimumLineSpacing: CGFloat = 0, sectionInset: UIEdgeInsets = .zero) {
        super.init()
        
        self.itemSize = itemSize
        self.minimumInteritemSpacing = minimumInteritemSpacing
        self.minimumLineSpacing = minimumLineSpacing
        self.sectionInset = sectionInset
        sectionInsetReference = .fromSafeArea
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let layoutAttributes = super.layoutAttributesForElements(in: rect)!.map { $0.copy() as! UICollectionViewLayoutAttributes }
        guard scrollDirection == .vertical else { return layoutAttributes }
        
        // Filter attributes to compute only cell attributes
        let cellAttributes = layoutAttributes.filter({ $0.representedElementCategory == .cell })
        
        // Group cell attributes by row (cells with same vertical center) and loop on those groups
        for (_, attributes) in Dictionary(grouping: cellAttributes, by: { ($0.center.y / 10).rounded(.up) * 10 }) {
            // Get the total width of the cells on the same row
            let cellsTotalWidth = attributes.reduce(CGFloat(0)) { (partialWidth, attribute) -> CGFloat in
                partialWidth + attribute.size.width
            }
            
            // Calculate the initial left inset
            let totalInset = collectionView!.safeAreaLayoutGuide.layoutFrame.width - cellsTotalWidth - sectionInset.left - sectionInset.right - minimumInteritemSpacing * CGFloat(attributes.count - 1)
            var leftInset = (totalInset / 2 * 10).rounded(.down) / 10 + sectionInset.left
            
            // Loop on cells to adjust each cell's origin and prepare leftInset for the next cell
            for attribute in attributes {
                attribute.frame.origin.x = leftInset
                leftInset = attribute.frame.maxX + minimumInteritemSpacing
            }
        }
        
        return layoutAttributes
    }
    
}
