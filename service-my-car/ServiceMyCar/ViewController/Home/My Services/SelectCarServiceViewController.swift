//
//  SelectCarServiceViewController.swift
//  ServiceMyCar
//
//  Created by admin on 19/06/19.
//  Copyright © 2019 baps. All rights reserved.
//
import Foundation
import UIKit
import SwiftyJSON
import Alamofire

class SelectCarServiceViewController: UIViewController,HttpWrapperDelegate {
    
    @IBOutlet weak var aCollection:UICollectionView!
    @IBOutlet weak var confrimButton:UIButton!
    @IBOutlet weak var totalAmount:UILabel!
    @IBOutlet weak var totalAmountLabel :UILabel!
    
    
    @IBOutlet weak var showMoreButton:UIButton!
    var dataLoading = false
    
    var includeOrNotInclude = ["","Select Your Address","Select Your Parts"]
    
    
    var dataDictionay = NSDictionary()
    var carArrayList = NSMutableArray()
    var partArrayList = NSMutableArray()
    var arrAddresslist = NSMutableArray()
    
    
    var objgetServiceYourCar = HttpWrapper()
    var objGetPart = HttpWrapper()
    var objGetUserAddress = HttpWrapper()
    var net:NetworkReachabilityManager = NetworkReachabilityManager()!
    
    
    var selectCar = NSDictionary()
    var selectPart = NSDictionary()
    
    var collectionAddress:String = ""
    var dropAddress:String = ""
    
    var collectionAddressTextfield:UITextField!
    var dropAddressTextfield:UITextField!
    
    
    var collectionPicker = UIPickerView()
    var dropPicker = UIPickerView()
    
    var selectDropAddress = NSDictionary()
    var selectColectionAddress = NSDictionary()
    
    var selectCarIndex =  -1
    var selectPartIndex = -1
    
    let columnLayout = FlowLayout(
        itemSize: CGSize(width: 200, height: 200),
        minimumInteritemSpacing: 0,
        minimumLineSpacing: 0,
        sectionInset: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    )
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Select Your Car".localized()
        self.totalAmountLabel.text = "Total Amount:".localized()
        self.confrimButton.setTitle("confirm".localized(), for: .normal)
        
        let layout = JEKScrollableSectionCollectionViewLayout()
        layout.defaultScrollViewConfiguration.showsHorizontalScrollIndicator = false
        layout.itemSize = CGSize(width: 50, height: 50);
        layout.headerReferenceSize = CGSize(width: 0, height: 22)
        layout.minimumInteritemSpacing = 5
        aCollection.collectionViewLayout = layout
        aCollection.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)

        self.aCollection.register(UINib(nibName: "ServiceHeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: JEKCollectionElementKindSectionBackground, withReuseIdentifier: "ServiceHeaderCollectionReusableView")
        self.aCollection.register(UINib(nibName: "HeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HeaderCollectionReusableView")
        
        self.aCollection.register(UINib(nibName: "HeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "HeaderCollectionReusableView")
        
        self.aCollection.register(UINib(nibName: "ServiceCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ServiceCollectionViewCell")
        self.aCollection.register(UINib(nibName: "EmptyCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "EmptyCollectionViewCell")

        AppHelper.makeViewCircularWithRespectToHeight(confrimButton, borderColor: .clear, borderWidth: 0.0)
        if let packagePrice = dataDictionay.value(forKey: "varPrice") as? String{
            self.totalAmount.text = "\(packagePrice) "+"app_currency".localized()
        }else if let packagePrice = dataDictionay.value(forKey: "varPrice") as? NSNumber{
            self.totalAmount.text = "\(packagePrice) "+"app_currency".localized()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(scrollViewDidEndScrollingAnimation(_:)), name: NSNotification.Name("ScrollCollectionView"), object: nil)
        showMoreButton.addTarget(self, action: #selector(scrollViewDidEndScrollingAnimation(_:)), for: .touchDown)
        self.aCollection.dataSource = self
        self.aCollection.delegate = self
        if carArrayList.count<=3{
            self.showMoreButton.isHidden = true
            }else{
            self.showMoreButton.isHidden = false
        }
        
        let button = UIButton.init(type: .custom)
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        
        if languageCode == "en" {
            button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
        }
        else {
            button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
        }
        button.addTarget(self, action:#selector(onClickBackBarButton(_ :)), for: UIControl.Event.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton

        
        aCollection.reloadData()
        
        
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.setupNavigation()
        self.setupNavigationButton()
        self.getPartsListItems()
        self.getServiceCarItems()
        self.getUserAddress()

    }
    
    override func viewDidAppear(_ animated: Bool) {
       // self.setupNavigationButton()
//        self.getPartsListItems()
//        self.getServiceCarItems()
//        self.getUserAddress()
    }
    
    
    @IBAction func onClickBackBarButton(_ sender:UIBarButtonItem){
        self.navigationController?.popViewController(animated: true)
    }
    
    
   
    
    
    @IBAction func onClickCollectionPointAddressButton(_ sender:UIBarButtonItem){
        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "AddAddress_Vc") as! AddAddress_Vc
        nextViewController.addressTypeString = "Enter Collection Address"
        nextViewController.addressChrType = "C"
        nextViewController.delegate = self
        self.navigationController?.pushViewController(nextViewController, animated: true)    }
    
    
    @IBAction func onClickDropPointAddressButton(_ sender:UIBarButtonItem){
        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "AddAddress_Vc") as! AddAddress_Vc
        nextViewController.addressTypeString = "Enter Drop Address"
        nextViewController.addressChrType = "D"
        nextViewController.delegate = self
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    @IBAction func onClickConfrimButton(_ sender:UIButton){
        
        if selectCar.count == 0{
            AppHelper.showAlert("Important Message".localized(), message: "Please Select your Car!".localized())
            return
        }
        if collectionAddress.isEmpty{
            AppHelper.showAlert("Important Message".localized(), message: "Please Select your Collection Address !".localized())
            return
        }
        
        if dropAddress.isEmpty{
            AppHelper.showAlert("Important Message".localized(), message: "Please Select your Drop Address !".localized())
            return
        }
        
        if selectPart.count == 0{
            AppHelper.showAlert("Important Message".localized(), message: "Please Select your Parts !".localized())
            return
        }
        
        let nextViewController = objServiceSB.instantiateViewController(withIdentifier: "SelectYourCarBookingViewController") as! SelectYourCarBookingViewController
        nextViewController.collectionAddress = self.collectionAddress
        nextViewController.dropAddress = self.dropAddress
        nextViewController.selectCar = self.selectCar
        nextViewController.selectPart = self.selectPart
        nextViewController.dataDictionay = self.dataDictionay
        nextViewController.selectColectionAddress = self.selectColectionAddress
        nextViewController.selectDropAddress = self.selectDropAddress
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    
    
    
    func setupNavigationButton(){
        let button = UIButton.init(type: .custom)
        button.frame = CGRect.init(x: 0, y: 0, width:35, height: 35)
        button.setImage(UIImage(named: "plus_green"), for: .normal)
        button.addTarget(self, action: #selector(onClickAddNavigation(_:)), for: .touchUpInside)
        let barButton : UIBarButtonItem = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
        
    }
    
    @IBAction func onClickAddNavigation(_ sender: UIButton){
        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "AddCar_Vc") as! AddCar_Vc
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension SelectCarServiceViewController:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return includeOrNotInclude.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0{
            return (carArrayList.count == 0) ? 1 : carArrayList.count
        }else if section == 1{
            return 1
        }else{
            return (partArrayList.count == 0) ? 1 :  partArrayList.count
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let edgeInsets = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        if section == 0{
            if carArrayList.count == 2{
                let edgeInsets = UIEdgeInsets.init(top: 0, left: self.view.frame.size.width/6.0, bottom: 0, right: 0)
                return edgeInsets
            }else if carArrayList.count == 1{
                let edgeInsets = UIEdgeInsets.init(top: 0, left: self.view.frame.size.width/3.0, bottom: 0, right: 0)
                return edgeInsets
            }else{
                return edgeInsets
            }
        }else if section == 1{
            return edgeInsets
        }else{
            if partArrayList.count == 2{
                let edgeInsets = UIEdgeInsets.init(top: 0, left: self.view.frame.size.width/6.0, bottom: 0, right: 0)
                return edgeInsets
            }else{
                return edgeInsets
            }
        }
    }

    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if (kind == UICollectionView.elementKindSectionHeader) {
           
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderCollectionReusableView", for: indexPath) as! HeaderCollectionReusableView
            let index = indexPath.first ?? 0
            headerView.headerTittle.text = self.includeOrNotInclude[index].localized()
            headerView.infoImage.isHidden = true
            return headerView
        }else if (kind == JEKCollectionElementKindSectionBackground) {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ServiceHeaderCollectionReusableView", for: indexPath) as! ServiceHeaderCollectionReusableView
            return headerView
        }else{
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderCollectionReusableView", for: indexPath) as! HeaderCollectionReusableView
            headerView.infoImage.isHidden = true
            return headerView

        }
    }

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: self.view.frame.size.width, height: 0.0)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if section == 0{
            return CGSize(width: self.view.frame.size.width, height: 25.0)
            
        }else{
            return CGSize(width: self.view.frame.size.width, height: 35.0)
            
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0{
            if carArrayList.count == 0{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EmptyCollectionViewCell", for: indexPath) as! EmptyCollectionViewCell
                cell.noDataLabel.text = dataLoading ? "No Car Found!".localized() : "Loading..".localized()
                return cell
            }else{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectCarCollectionViewCell", for: indexPath) as! SelectCarCollectionViewCell
                
                //            cell.backView.backgroundColor = self.selectCarIndex == indexPath.item ? colorlight : UIColor.white
                cell.selectButton.isSelected = self.selectCarIndex == indexPath.item ? true : false
                cell.backView.layer.borderWidth = 1.0
                cell.backView.layer.borderColor = colorGreen.cgColor
                cell.backView.layer.cornerRadius = 5.0
                cell.backView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2).cgColor
                cell.backView.layer.shadowOffset = CGSize.zero
                cell.backView.layer.shadowOpacity = 1.0
                cell.backView.layer.shadowRadius = 1.0
                cell.backView.layer.masksToBounds = false
                cell.contentView.backgroundColor = UIColor.init(red: 244.0/255.0, green: 245.0/255.0, blue: 248.0/255.0, alpha: 1.0)
                if let name = (carArrayList.object(at: indexPath.item) as! NSDictionary).value(forKey: "varCar") as? String, let model = (carArrayList.object(at: indexPath.item) as! NSDictionary).value(forKey: "varModel") as? String{
                    cell.carName.text = "\(name.uppercased()) \(model.uppercased())"
                }
                
                if let name = (carArrayList.object(at: indexPath.item) as! NSDictionary).value(forKey: "varYear") as? String{
                    cell.yearLabel.text  = "\(name.uppercased())"
                }
                
                if let varPlateCode = (carArrayList.object(at: indexPath.item) as! NSDictionary).value(forKey: "varPlateCode") as? String, let varPlateNumber = (carArrayList.object(at: indexPath.item) as! NSDictionary).value(forKey: "varPlateNumber") as? String{
                    cell.carNumber.text = varPlateNumber.uppercased()
                    cell.categoryName.text = varPlateCode.uppercased()
                }
                if let stName = (carArrayList.object(at: indexPath.row) as! NSDictionary).value(forKey: "varCarState") as? String{
                    cell.plateImageView.image = AppHelper.setNumberPlateImage(stateName: stName)
                }
                
                return cell
            }
        }else if indexPath.section == 1{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectAddressCollectionViewCell", for: indexPath) as! SelectAddressCollectionViewCell
            cell.dropAddAdressButton.addTarget(self, action: #selector(onClickDropPointAddressButton(_:)), for: .touchUpInside)
            cell.collectionAddAdressButton.addTarget(self, action: #selector(onClickCollectionPointAddressButton(_:)), for: .touchUpInside)
            cell.backView.backgroundColor = UIColor.init(red: 244.0/255.0, green: 245.0/255.0, blue: 248.0/255.0, alpha: 1.0)
            cell.dropAddressTextfield.placeholder = "Select Delivery Address".localized()
            cell.collectAddressTextfield.placeholder = "Select Collection  Address".localized()
            let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
            if languageCode != "en" {
                cell.dropAddressTextfield.textAlignment = .right
                cell.collectAddressTextfield.textAlignment = .right
            }
            self.collectionAddressTextfield = cell.collectAddressTextfield
            self.dropAddressTextfield = cell.dropAddressTextfield
            cell.collectAddressTextfield.text = collectionAddress
            cell.dropAddressTextfield.text = dropAddress
            self.setupCollectionPicker(textField: cell.collectAddressTextfield)
            self.setupDropPicker(textField: cell.dropAddressTextfield)
            return cell
        }else{
            if partArrayList.count == 0{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EmptyCollectionViewCell", for: indexPath) as! EmptyCollectionViewCell
                cell.noDataLabel.text = dataLoading ? "No Parts Found!".localized() : "Loading..".localized()
                return cell
            }else{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ServiceCollectionViewCell", for: indexPath) as! ServiceCollectionViewCell
                if let UserProfilePic = (partArrayList.object(at: indexPath.item) as! NSDictionary).value(forKey: "icon") as? String{
                    let url = "\(UserProfilePic)"
                    print(url)
                    
                    if let url2 = URL(string: url) {
                        cell.serviceImageview.sd_setImage(with: url2, placeholderImage: UIImage(named: "img_plan1"))
                    }
                }
                
                if let name = (partArrayList.object(at: indexPath.item) as! NSDictionary).value(forKey: "title") as? String{
                    cell.serviceNameLabel.text  = "\(name.uppercased())"
                }
                cell.infoButton.isHidden = false
                //            cell.viewContainer.backgroundColor = self.selectPartIndex == indexPath.item ? colorlight : UIColor.init(red: 244.0/255.0, green: 245.0/255.0, blue: 248.0/255.0, alpha: 1.0)
                cell.selectButton.isSelected = self.selectPartIndex == indexPath.item ? true : false
                cell.viewContainer.layer.borderWidth = 1.0
                cell.viewContainer.layer.borderColor = colorGreen.cgColor
                cell.viewContainer.layer.cornerRadius = 5.0
                cell.viewContainer.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2).cgColor
                cell.viewContainer.layer.shadowOffset = CGSize.zero
                cell.viewContainer.layer.shadowOpacity = 1.0
                cell.viewContainer.layer.shadowRadius = 0.5
                cell.viewContainer.layer.masksToBounds = false
                cell.contentView.backgroundColor = UIColor.init(red: 244.0/255.0, green: 245.0/255.0, blue: 248.0/255.0, alpha: 1.0)
                cell.infoButton.addTarget(self, action: #selector(onClickInfoButton(_:)), for: .touchUpInside)
                return cell
            }
        }
    }
    
    
    
    
    @IBAction func onClickInfoButton(_ sender:UIButton){
        if let indexPath = sender.collectionViewIndexPath(self.aCollection) as IndexPath?{
            var title = ""
            var detail = ""
            if let lTitle = (partArrayList.object(at: indexPath.item) as! NSDictionary).value(forKey: "title") as? String{
                title  = lTitle
            }
            
            if let kdetail = (partArrayList.object(at: indexPath.item) as! NSDictionary).value(forKey: "details") as? String{
                detail = kdetail
            }
            
            AppHelper.showAlert(title, message: detail)
            
        }
        
    }
    
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        let visibleIndexPath = aCollection.indexPathsForVisibleItems
        let indexPathForSectionOne = visibleIndexPath.filter { (indexPath) -> Bool in
            return (indexPath.section == 0)
        }
        let shouldHideSeeMoreButton = indexPathForSectionOne.contains(where: { (indexPath) -> Bool in
            return indexPath.item == self.carArrayList.count-1
        })
        showMoreButton.isHidden = shouldHideSeeMoreButton
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        //NSObject.cancelPreviousPerformRequests(withTarget: scrollView)
        self.perform(#selector(scrollViewDidEndScrollingAnimation(_:)), with: scrollView, afterDelay: 0.3)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 1{
            let size:CGFloat = (self.view.frame.size.width - 5) / 1.0
            return CGSize(width: size, height: 120)
        }else if indexPath.section == 0{
            if carArrayList.count == 0{
                return CGSize(width: self.aCollection.frame.size.width, height: 70)
            }else{
                let size:CGFloat = (self.view.frame.size.width - 20) / 3.0
                return CGSize(width: size, height: size + 10)
            }
        }else{
            if partArrayList.count == 0{
                return CGSize(width: self.aCollection.frame.size.width, height: 70)
            }else{
                let size:CGFloat = (self.view.frame.size.width - 20) / 3.0
                return CGSize(width: size, height: size + 10)
            }
            
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0{
            if carArrayList.count == 0{return}
            
            let selectCarItem = carArrayList.object(at: indexPath.item) as? NSDictionary
            self.selectCar = selectCarItem!
            self.selectCarIndex = indexPath.item
           // aCollection.reloadItems(at: [indexPath])

            self.aCollection.reloadData()
            
        }else if indexPath.section == 2{
            if partArrayList.count == 0{return}
            let selectPartItem = partArrayList.object(at: indexPath.item) as? NSDictionary
            self.selectPart = selectPartItem!
            self.selectPartIndex = indexPath.item
          //  aCollection.reloadItems(at: [indexPath])

            self.aCollection.reloadData()
            
        }
    }
}

extension SelectCarServiceViewController{
    func getServiceCarItems(){
        if net.isReachable == false{
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["userId" : nUserId as AnyObject]
            //let header = ["X-API-KEY" : "smc1989"] as [String:AnyObject]
            
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objgetServiceYourCar = HttpWrapper.init()
            self.objgetServiceYourCar.delegate = self
            
            self.objgetServiceYourCar.requestWithparamdictParamPostMethodwithHeaderAndDictGet(url: kgetUserCars, dictParams: dictParameters, headers: header)
        }
        
    }
    
    
    func getPartsListItems(){
        if net.isReachable == false{
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            var languageCode = ""
            if UserDefaults.standard.value(forKey: kAppLanguage) != nil {
                languageCode = UserDefaults.standard.value(forKey: kAppLanguage) as! String
            }
            
            let dictParameters:[String:AnyObject] = ["lang" : languageCode as AnyObject]
            
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objGetPart = HttpWrapper.init()
            self.objGetPart.delegate = self
            self.objGetPart.requestWithparamdictParamPostMethodwithHeaderAndDictGet(url: kgetExtraPartImages, dictParams: dictParameters, headers: header)
        }
        
    }
    
    
    
    func getUserAddress(){
        if net.isReachable == false{
            
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }else{
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["userId" : nUserId as AnyObject]
            //let header = ["X-API-KEY" : "smc1989"] as [String:AnyObject]
            
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objGetUserAddress = HttpWrapper.init()
            self.objGetUserAddress.delegate = self
            
            self.objGetUserAddress.requestWithparamdictParamPostMethodwithHeaderAndDictGet(url: kgetUserAddress, dictParams: dictParameters, headers: header)
        }
    }
    
    
    
    // MARK: - HttpWrapperfetchDataSuccess
    
    func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary)
    {
        AppHelper.hideLoadingView()
        print(dicsResponse)
        if wrapper == objgetServiceYourCar {
            if dicsResponse.value(forKey: "status") as? NSString == "success"{
                carArrayList.removeAllObjects()
                dataLoading = true
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray{
                    carArrayList = tempNames.mutableCopy() as! NSMutableArray
                }
                print(carArrayList)
                if carArrayList.count>3{
                    self.showMoreButton.isHidden = false
                }
                self.aCollection.reloadData()
            }else{
                dataLoading = true
                self.aCollection.reloadData()
                // AppHelper.showAlert("Alert", message:dicsResponse.value(forKey: "msg") as! String)
            }
            
        }else if wrapper == objGetPart{
            if dicsResponse.value(forKey: "status") as? NSString == "success"{
                partArrayList.removeAllObjects()
                dataLoading = true
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray{
                    partArrayList = tempNames.mutableCopy() as! NSMutableArray
                }
                print(partArrayList)
                self.aCollection.reloadData()
            }else{
                dataLoading = true
                self.aCollection.reloadData()
            }
        }else if wrapper == objGetUserAddress{
            if dicsResponse.value(forKey: "status") as? NSString == "success"{
                arrAddresslist.removeAllObjects()
                dataLoading = true

                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray{
                    arrAddresslist = tempNames.mutableCopy() as! NSMutableArray
                }
                print(arrAddresslist)
                aCollection.reloadData()
            }else{
                dataLoading = true
                self.aCollection.reloadData()
                // AppHelper.showAlert("Alert", message:dicsResponse.value(forKey: "msg") as! String)
            }
        }
    }
    // MARK: - HttpWrapperfetchDataFail
    
    func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError){
        AppHelper.hideLoadingView()
        print(error.debugDescription)
    }
}

extension SelectCarServiceViewController:UIPickerViewDataSource,UIPickerViewDelegate{
    
    
    
    // Mark:- SetUp carCategory picker
    
    
    
    
    @IBAction func onClickSelectCollectAddressType(_ sender: UIBarButtonItem){
        if self.arrAddresslist.count == 0 {
            self.view.endEditing(true)
            return
            
        }
        let selectAddress  = arrAddresslist[collectionPicker.selectedRow(inComponent: 0)]
        if let address = (selectAddress as AnyObject).value(forKey: "varAddress") as? String, let buildName = (selectAddress as AnyObject).value(forKey: "varBuildingName") as? String, let additionalText = (selectAddress as AnyObject).value(forKey: "varAdditionalInstruction") as? String, let state = (selectAddress as AnyObject).value(forKey: "varState") as? String, let country = (selectAddress as AnyObject).value(forKey: "varCountry") as? String{
            self.collectionAddress = "\(address) \(buildName) \(additionalText) \(state) \(country)"
            self.collectionAddressTextfield.text = self.collectionAddress
            self.selectColectionAddress = selectAddress as! NSDictionary
            
        }
        self.aCollection.reloadData()
        self.view.endEditing(true)
    }
    
    @IBAction func onClickSelectDropAddressType(_ sender: UIBarButtonItem){
        if self.arrAddresslist.count == 0 {
            self.view.endEditing(true)
            return
            
        }
        let selectAddress  = arrAddresslist[dropPicker.selectedRow(inComponent: 0)]
        if let address = (selectAddress as AnyObject).value(forKey: "varAddress") as? String, let buildName = (selectAddress as AnyObject).value(forKey: "varBuildingName") as? String, let additionalText = (selectAddress as AnyObject).value(forKey: "varAdditionalInstruction") as? String, let state = (selectAddress as AnyObject).value(forKey: "varState") as? String, let country = (selectAddress as AnyObject).value(forKey: "varCountry") as? String{
            self.dropAddress = "\(address) \(buildName) \(additionalText) \(state) \(country)"
            self.dropAddressTextfield.text = self.dropAddress
            self.selectDropAddress = selectAddress as! NSDictionary
        }
        self.aCollection.reloadData()
        self.view.endEditing(true)
    }
    
    @IBAction func onClickSelectCancelType(_ sender: UIBarButtonItem){
        self.aCollection.reloadData()
        self.view.endEditing(true)
        
    }
    
    func setupCollectionPicker(textField:UITextField){
        
        // UIPickerView
        self.collectionPicker = UIPickerView()
        self.collectionPicker.delegate = self
        self.collectionPicker.dataSource = self
        self.collectionPicker.backgroundColor = UIColor.white
        
        textField.inputView = self.collectionPicker
        collectionPicker.reloadAllComponents()
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .black
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.white
        toolBar.sizeToFit()
        toolBar.backgroundColor = colorGreen
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "add_your_car_done".localized(), style: .plain, target: self, action: #selector(onClickSelectCollectAddressType(_:)))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "add_your_car_cancel".localized(), style: .plain, target: self, action: #selector(onClickSelectCancelType(_:)))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    
    
    func setupDropPicker(textField:UITextField){
        
        // UIPickerView
        self.dropPicker = UIPickerView()
        self.dropPicker.delegate = self
        self.dropPicker.dataSource = self
        self.dropPicker.backgroundColor = UIColor.white
        
        textField.inputView = self.dropPicker
        dropPicker.reloadAllComponents()
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .black
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.white
        toolBar.sizeToFit()
        toolBar.backgroundColor = colorGreen
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "add_your_car_done".localized(), style: .plain, target: self, action: #selector(onClickSelectDropAddressType(_:)))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "add_your_car_cancel".localized(), style: .plain, target: self, action: #selector(onClickSelectCancelType(_:)))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    
    //MARK:- PickerView Delegate & DataSource
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if arrAddresslist.count != 0{
            return arrAddresslist.count
        }else{
            return 0
        }
        
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label: UILabel
        
        if let view = view {
            label = view as! UILabel
        }
        else {
            label = UILabel(frame: CGRect(x: 0, y: 0, width: pickerView.frame.width, height: 400))
        }
        
        label.lineBreakMode = .byWordWrapping;
        if arrAddresslist.count != 0{
            if pickerView == collectionPicker{
                if let address = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varAddress") as? String, let buildName = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varBuildingName") as? String, let additionalText = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varAdditionalInstruction") as? String, let state = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varState") as? String, let country = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varCountry") as? String{
                    label.text = "\(address) \(buildName) \(additionalText) \(state) \(country)"
                }
            }else{
                if let address = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varAddress") as? String, let buildName = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varBuildingName") as? String, let additionalText = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varAdditionalInstruction") as? String, let state = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varState") as? String, let country = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varCountry") as? String{
                    label.text = "\(address) \(buildName) \(additionalText) \(state) \(country)"
                }
            }
        }else{
            label.text = "tet"
        }
        label.numberOfLines = 2;
        //label.text = arr[row]
        label.sizeToFit()
        return label;
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if arrAddresslist.count != 0{
            if pickerView == collectionPicker{
                if let address = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varAddress") as? String, let buildName = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varBuildingName") as? String, let additionalText = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varAdditionalInstruction") as? String, let state = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varState") as? String, let country = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varCountry") as? String{
                    self.collectionAddress = "\(address) \(buildName) \(additionalText) \(state) \(country)"
                    self.collectionAddressTextfield.text = self.collectionAddress
                }
                self.selectColectionAddress = (arrAddresslist.object(at: row) as! NSDictionary)
                
            }else{
                if let address = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varAddress") as? String, let buildName = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varBuildingName") as? String, let additionalText = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varAdditionalInstruction") as? String, let state = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varState") as? String, let country = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varCountry") as? String{
                    self.dropAddress = "\(address) \(buildName) \(additionalText) \(state) \(country)"
                    self.dropAddressTextfield.text = self.dropAddress
                }
                self.selectDropAddress = (arrAddresslist.object(at: row) as! NSDictionary)
            }
        }
        
    }
}


extension SelectCarServiceViewController:AddAddress_VcDelegate{
    func getAddAddress(_ viewController: UIViewController, address isAdded: Bool) {
    }
    
    func getCollectionPointAddress(_ viewController: UIViewController, collectAddress address: String, newAddress: NSDictionary) {
        self.collectionAddress = address
        self.selectColectionAddress = newAddress
        
        self.aCollection.reloadData()
    }
    
    func getDropPointAddress(_ viewController: UIViewController, dropAddress address: String, newAddress: NSDictionary) {
        self.dropAddress = address
        self.selectDropAddress = newAddress
        self.aCollection.reloadData()
    }
}
