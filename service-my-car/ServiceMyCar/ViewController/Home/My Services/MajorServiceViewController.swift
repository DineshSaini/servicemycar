//
//  MajorServiceViewController.swift
//  ServiceMyCar
//
//  Created by admin on 18/06/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class MajorServiceViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,HttpWrapperDelegate {
    
    @IBOutlet weak var majorCollectionView:UICollectionView!
    var includeOrNotInclude = ["What included","What's not Included (Parts)"]
    
    @IBOutlet weak var bookNowButton:UIButton!
    @IBOutlet weak var partInfoLabel:UILabel!

    
    
    var majorCollection = NSDictionary()
    var includedArray = NSMutableArray()
    var includedNotArray = NSMutableArray()


    var objgetPackagesItem = HttpWrapper()
    var net:NetworkReachabilityManager = NetworkReachabilityManager()!
    
    let columnLayout = FlowLayout(
        itemSize: CGSize(width: 200, height: 200),
        minimumInteritemSpacing: 0,
        minimumLineSpacing: 0,
        sectionInset: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    )
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.majorCollectionView.delegate = self
        self.majorCollectionView.delegate = self
        
        majorCollectionView?.collectionViewLayout = columnLayout
        majorCollectionView?.contentInsetAdjustmentBehavior = .automatic
        
        self.majorCollectionView.register(UINib(nibName: "ServiceCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ServiceCollectionViewCell")
        
        self.majorCollectionView.register(UINib(nibName: "HeaderSectionCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HeaderSectionCollectionViewCell")
        
        self.majorCollectionView.register(UINib(nibName: "EmptyCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "EmptyCollectionViewCell")

        self.majorCollectionView.register(UINib(nibName: "ServiceHeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "ServiceHeaderCollectionReusableView")
        
        AppHelper.makeViewCircularWithRespectToHeight(bookNowButton, borderColor: .clear, borderWidth: 0.0)
        if let dataArray = majorCollection.value(forKey: "partsIncludedSpec") as? NSArray{
            includedArray = dataArray.mutableCopy() as! NSMutableArray
        }
        
        if let dataArray = majorCollection.value(forKey: "partsNotIncludedSpec") as? NSArray{
            includedNotArray = dataArray.mutableCopy() as! NSMutableArray
        }
        //self.getPackagesItems()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.getServiceInfoNotification(_:)),
                                               name: NSNotification.Name(rawValue: "GetServiceInfoMajorNotification"),
                                               object: nil)
        majorCollectionView.reloadData()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.getPackagesItems()
    }
    
    override func viewDidLayoutSubviews() {
        self.bookNowButton.setTitle("Book Now".localized(), for: .normal)
        self.partInfoLabel.text = "All part are sourced at the lowest price and the highest quality".localized()
        
    }
    
    @objc func getServiceInfoNotification(_ notification: NSNotification) {
        if notification.name.rawValue == "GetServiceInfoMajorNotification" {
            //self.toggleAuthUI()
            if let serviceInfo = notification.userInfo as? [String:AnyObject] {
                if let service = serviceInfo["statusText"] as? NSDictionary{
                    self.majorCollection = service
                    self.getPackagesItems()
                }
            }
        }
    }
    
    
    
    
    // MARK: - getPackagesListByCategory API
    
    @IBAction func onClickBookNowButton(_ sender:UIButton){
        let nextViewController = objServiceSB.instantiateViewController(withIdentifier:"SelectCarServiceViewController") as! SelectCarServiceViewController
        nextViewController.dataDictionay = self.majorCollection
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    func getPackagesItems()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            var languageCode = ""
            var packageID = ""
            if UserDefaults.standard.value(forKey: kAppLanguage) != nil {
                languageCode = UserDefaults.standard.value(forKey: kAppLanguage) as! String
            }
            
            if let packageId = majorCollection.value(forKey: "intId") as? String{
                packageID = packageId
            }else if let packageId = majorCollection.value(forKey: "intId") as? Int{
                packageID = "\(packageId)"
            }
            
            let dictParameters:[String:AnyObject] = ["packageId" : packageID as AnyObject,
                                                     "lang" : languageCode as AnyObject]
            
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objgetPackagesItem = HttpWrapper.init()
            self.objgetPackagesItem.delegate = self
            self.objgetPackagesItem.requestWithparamdictParamPostMethodwithHeaderAndDictGet(url: kgetPackageListByID, dictParams: dictParameters, headers: header)
        }
        
    }
    
    
    // MARK: - HttpWrapperfetchDataSuccess
    
    func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary)
    {
        AppHelper.hideLoadingView()
        print(dicsResponse)
        if wrapper == objgetPackagesItem {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                includedArray.removeAllObjects()
                includedNotArray.removeAllObjects()
                
                
                if let tempNames: NSDictionary = dicsResponse.value(forKey: "data") as? NSDictionary
                {
                    majorCollection = tempNames.mutableCopy() as! NSMutableDictionary
                    if let dataArray = majorCollection.value(forKey: "partsIncludedSpec") as? NSArray{
                        includedArray = dataArray.mutableCopy() as! NSMutableArray
                        self.majorCollectionView.reloadData()
                    }
                    
                    if let dataArray = majorCollection.value(forKey: "partsNotIncludedSpec") as? NSArray{
                        includedNotArray = dataArray.mutableCopy() as! NSMutableArray
                        self.majorCollectionView.reloadData()
                    }
                    self.majorCollectionView.reloadData()
                }
                print(includedArray)
                print(includedNotArray)
                //self.majorCollectionView.reloadData()
            }
            else
            {
                // AppHelper.showAlert("Alert", message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }
        
    }
    // MARK: - HttpWrapperfetchDataFail
    
    func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError)
    {
        AppHelper.hideLoadingView()
        print(error.debugDescription)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else  if section == 1{
            if includedArray.count == 0{
                return 1
            }else{
                return includedArray.count
            }
        }else  if section == 2{
            return 1
        }else{
            if includedNotArray.count == 0{
                return 1
            }else{
            return includedNotArray.count
            }
        }
    }
    
    @IBAction func onClickInfoButton(_ sender:UIButton){
        if  let indexpath = sender.collectionViewIndexPath(self.majorCollectionView) as IndexPath?{
            if indexpath.section == 0{
                AppHelper.showAlert("app_alert_title".localized(), message: "This is what's included in the service package".localized())

            }
        }
    }
    
    
    @IBAction func onClickNotInfoButton(_ sender:UIButton){
        if  let indexpath = sender.collectionViewIndexPath(self.majorCollectionView) as IndexPath?{
            if indexpath.section == 2{
                AppHelper.showAlert("app_alert_title".localized(), message: "This is what's not included in the Service package".localized())

            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0{
            let headerView = collectionView.dequeueReusableCell(withReuseIdentifier: "HeaderSectionCollectionViewCell", for: indexPath) as! HeaderSectionCollectionViewCell
            headerView.headerTittle.text = "What included".localized()//self.includeOrNotInclude[indexPath.section]
            headerView.sendSubviewToBack(headerView.infoImage)
            headerView.infoImage.setImage(UIImage(named: "info"), for: .normal)
            headerView.infoImage.isHidden = false
            headerView.infoImage.tag = 0
            headerView.infoImage.addTarget(self, action: #selector(onClickInfoButton(_:)), for:.touchUpInside)
            
            return headerView
        }else if indexPath.section == 2{
            let headerView = collectionView.dequeueReusableCell(withReuseIdentifier: "HeaderSectionCollectionViewCell", for: indexPath) as! HeaderSectionCollectionViewCell
            headerView.headerTittle.text = "What's not Included (Parts)".localized()//self.includeOrNotInclude[indexPath.section]
            headerView.sendSubviewToBack(headerView.infoImage)
            headerView.infoImage.setImage(UIImage(named: "info"), for: .normal)
            headerView.infoImage.isHidden = false
            headerView.infoImage.tag = 0
            headerView.infoImage.addTarget(self, action: #selector(onClickNotInfoButton(_:)), for:.touchUpInside)
            return headerView
        }else{
            
            
            if includedArray.count == 0 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EmptyCollectionViewCell", for: indexPath) as! EmptyCollectionViewCell
                return cell
            }else if includedNotArray.count == 0{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EmptyCollectionViewCell", for: indexPath) as! EmptyCollectionViewCell
                return cell
            }else{
                
                
                
        

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ServiceCollectionViewCell", for: indexPath) as! ServiceCollectionViewCell
        cell.viewContainer.layer.cornerRadius = 5.0
        cell.viewContainer.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2).cgColor
        cell.viewContainer.backgroundColor = colorBackGroundColor//UIColor.white
        cell.viewContainer.layer.shadowOffset = CGSize.zero
        cell.viewContainer.layer.shadowOpacity = 1.0
        cell.viewContainer.layer.shadowRadius = 0.5
        cell.viewContainer.layer.masksToBounds = false
        cell.contentView.backgroundColor = colorBackGroundColor//UIColor.init(red: 244.0/255.0, green: 245.0/255.0, blue: 248.0/255.0, alpha: 1.0)
        if indexPath.section == 1{
        if let name = (includedArray.object(at: indexPath.row) as! NSDictionary).value(forKey: "varSpecification") as? String{
            cell.serviceNameLabel.text = name.uppercased()
        }
        
        if let UserProfilePic = (includedArray.object(at: indexPath.row) as! NSDictionary).value(forKey: "varImage") as? String{
            let url = "\(UserProfilePic)"
            print(url)
            
            if let url2 = URL(string: url) {
                cell.serviceImageview.sd_setImage(with: url2, placeholderImage: UIImage(named: "img_plan1"))
                }
            }
            
        }else{
            if let name = (includedNotArray.object(at: indexPath.row) as! NSDictionary).value(forKey: "varSpecification") as? String{
                cell.serviceNameLabel.text = name.uppercased()
            }
            
            if let UserProfilePic = (includedNotArray.object(at: indexPath.row) as! NSDictionary).value(forKey: "varImage") as? String{
                let url = "\(UserProfilePic)"
                print(url)
                
                if let url2 = URL(string: url) {
                    cell.serviceImageview.sd_setImage(with: url2, placeholderImage: UIImage(named: "img_plan1"))
                }
            }
        }

        
        return cell
            }
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0{
            let size:CGFloat = (self.view.frame.size.width) / 1.0
            return CGSize(width: size, height: 50)
        }else if indexPath.section == 2{
            let size:CGFloat = (self.view.frame.size.width) / 1.0
            return CGSize(width: size, height: 50)
        }else{
            if indexPath.section == 1{
                if includedArray.count == 0{
                    let size:CGFloat = (self.view.frame.size.width) / 1.0
                    return CGSize(width: size, height: 150)
                    
                }else{
                    let size:CGFloat = (self.view.frame.size.width - 20) / 3.0
                    return CGSize(width: size, height: size)
                }

            }else{
                if includedNotArray.count == 0{
                    let size:CGFloat = (self.view.frame.size.width) / 1.0
                    return CGSize(width: size, height: 150)

                }else{
                    let size:CGFloat = (self.view.frame.size.width - 20) / 3.0
                    return CGSize(width: size, height: size)
                }
            }
        }
        return CGSize()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        majorCollectionView?.collectionViewLayout.invalidateLayout()
        super.viewWillTransition(to: size, with: coordinator)
    }


}
