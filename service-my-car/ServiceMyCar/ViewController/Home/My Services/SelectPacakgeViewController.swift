//
//  SelectPacakgeViewController.swift
//  ServiceMyCar
//
//  Created by admin on 18/06/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SelectPacakgeViewController: UIViewController,HttpWrapperDelegate {
    
    
    @IBOutlet weak var basicButton:UIButton!
    @IBOutlet weak var fullButton:UIButton!
    @IBOutlet weak var majorButton:UIButton!
    
    
    @IBOutlet weak var twoHundredEADLabel:UILabel!
    @IBOutlet weak var threeFiftyEADLabel:UILabel!
    @IBOutlet weak var fiveHundredEADLabel:UILabel!
    
    @IBOutlet weak var movingView:UIView!
    
    @IBOutlet weak var aScrollView:UIScrollView!
    
    
    var objgetPackagesListByCategory = HttpWrapper()
    var net:NetworkReachabilityManager = NetworkReachabilityManager()!
    var arrCategorylist:NSMutableArray = NSMutableArray()
    
    
    var fullService = NSDictionary()
    var basicService = NSDictionary()
    var majorService = NSDictionary()
    
    var badgeButton : MIBadgeButton = MIBadgeButton(frame:CGRect(x:-10, y: 30, width: 40, height: 20)) // Step 1

    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.setupNavigation()
        self.setupNavigationLeftBarButton()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.setupNavigationButton()
        self.setupNavigationLeftBarButton()
        self.getPackagesListByCategory()
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setupNavigation()
        self.aScrollView.isPagingEnabled = true
        self.aScrollView.bounces = false
        self.title = "Services".localized()
        self.getPackagesListByCategory()

        self.toggleRateButton(button: basicButton)
        let basicVC = objHomeSB.instantiateViewController(withIdentifier: "BasicServiceViewController") as! BasicServiceViewController
        basicVC.basicCollection = basicService
        self.addChild(basicVC)
        
        let fullVC = objHomeSB.instantiateViewController(withIdentifier: "FullServiceViewController") as! FullServiceViewController
        fullVC.fullCollection = fullService
        self.addChild(fullVC)
        
        let majorVC = objHomeSB.instantiateViewController(withIdentifier: "MajorServiceViewController") as! MajorServiceViewController
        majorVC.majorCollection = majorService
        self.addChild(majorVC)
        self.perform(#selector(LoadScolllView), with: nil, afterDelay: 0.5)

        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidLayoutSubviews() {
        self.basicButton.setTitle("Basic".localized(), for: .normal)
        self.majorButton.setTitle("Major".localized(), for: .normal)
        self.fullButton.setTitle("Full".localized(), for: .normal)

    }
    
    
    
    
    func setupNavigationButton(){
        
        // Step 2
        badgeButton.edgeInsetTop = 10
        badgeButton.setImage(UIImage(named : "notification"), for: .normal)
        badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        print(appDelegate.NCount)
        badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        badgeButton.badgeString = appDelegate.NCount
        if appDelegate.NCount == "0"
        {
            badgeButton.badgeBackgroundColor = UIColor.clear
            badgeButton.badgeTextColor = UIColor.clear
        }
        else
        {
            badgeButton.badgeBackgroundColor = colorGreen
            badgeButton.badgeTextColor = UIColor.white
        }
        badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        let barButton2 : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton2
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)
        
    }
    
    
    func setupNavigationLeftBarButton(){
        
        
        let button = UIButton(frame: CGRect(x:0,y: 0,width: 50,height: 45))
        
        button.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.center
        button.addTarget(self, action: #selector(onClickBackBarButton(_:)), for: .touchUpInside)
        
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        if languageCode == "en" {
            button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
        }else {
            button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
        }

        button.backgroundRect(forBounds: CGRect(x: 0, y: 0, width: 10, height: 10))
        let leftBarButtonItem = UIBarButtonItem(customView: button)
       // self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.setLeftBarButtonItems([leftBarButtonItem], animated: false)
        
        
       
        
    }

    
    
    
    
    // MARK: - Google Api Calling
    
    // Step 4
    
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            //self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                
                badgeButton.badgeString = appDelegate.NCount
                if appDelegate.NCount == "0"
                {
                    badgeButton.badgeBackgroundColor = UIColor.clear
                    badgeButton.badgeTextColor = UIColor.clear
                }
                else
                {
                    badgeButton.badgeBackgroundColor = colorGreen
                    badgeButton.badgeTextColor = UIColor.white
                }
            }
        }
    }
    
    // step 3
    
    @objc func NotificationAlert()
    {
        guard let vcs = self.navigationController?.viewControllers else{return}
        let inStack = vcs.contains(where: { (vc) -> Bool in
            return vc is MyNotificationList_Vc
        })
        if inStack{
            self.performSegue(withIdentifier: unwindToNotificationController, sender: self)
        }else{
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        //        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
        //
        //        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }

    
    
    @IBAction func onClickBackBarButton(_ sender:UIBarButtonItem){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func setDataOnViewController(){
        for i in 0..<arrCategorylist.count{
            if let data = arrCategorylist.object(at:i) as? NSDictionary{
                if let name = data.value(forKey: "intId") as? String{
                    if name == "3"/*"Full Service"*/{
                        fullService = data
                        if let packagePrice = data.value(forKey: "varPrice") as? String{
                            self.threeFiftyEADLabel.text = "\(packagePrice) "+"app_currency".localized()
                        }else if let packagePrice = data.value(forKey: "varPrice") as? NSNumber{
                            self.threeFiftyEADLabel.text = "\(packagePrice) "+"app_currency".localized()
                        }
                        NotificationCenter.default.post(
                            name: Notification.Name(rawValue: "GetServiceInfoNotification"),
                            object: nil,
                            userInfo: ["statusText": data])
                        
                    }else if name == "1"/*"Basic Service"*/{
                        basicService = data
                        if let packagePrice = data.value(forKey: "varPrice") as? String{
                            self.twoHundredEADLabel.text = "\(packagePrice) "+"app_currency".localized()
                        }else if let packagePrice = data.value(forKey: "varPrice") as? NSNumber{
                            self.twoHundredEADLabel.text = "\(packagePrice) "+"app_currency".localized()
                        }
                        NotificationCenter.default.post(
                            name: Notification.Name(rawValue: "GetServiceInfoBasicNotification"),
                            object: nil,
                            userInfo: ["statusText": data])
                    }else if name == "2"/*"Major Service"*/{
                        majorService = data
                        if let packagePrice = data.value(forKey: "varPrice") as? String{
                            self.fiveHundredEADLabel.text = "\(packagePrice) "+"app_currency".localized()
                        }else if let packagePrice = data.value(forKey: "varPrice") as? NSNumber{
                            self.fiveHundredEADLabel.text = "\(packagePrice) "+"app_currency".localized()
                        }
                        NotificationCenter.default.post(
                            name: Notification.Name(rawValue: "GetServiceInfoMajorNotification"),
                            object: nil,
                            userInfo: ["statusText": data])
                    }
                }else if let name = data.value(forKey: "intId") as? NSNumber{
                    if name == 3/*"Full Service"*/{
                        fullService = data
                        if let packagePrice = data.value(forKey: "varPrice") as? String{
                            self.threeFiftyEADLabel.text = "\(packagePrice) "+"app_currency".localized()
                        }else if let packagePrice = data.value(forKey: "varPrice") as? NSNumber{
                            self.threeFiftyEADLabel.text = "\(packagePrice) "+"app_currency".localized()
                        }
                        NotificationCenter.default.post(
                            name: Notification.Name(rawValue: "GetServiceInfoNotification"),
                            object: nil,
                            userInfo: ["statusText": data])
                        
                    }else if name == 1/*"Basic Service"*/{
                        basicService = data
                        if let packagePrice = data.value(forKey: "varPrice") as? String{
                            self.twoHundredEADLabel.text = "\(packagePrice) "+"app_currency".localized()
                        }else if let packagePrice = data.value(forKey: "varPrice") as? NSNumber{
                            self.twoHundredEADLabel.text = "\(packagePrice) "+"app_currency".localized()
                        }
                        NotificationCenter.default.post(
                            name: Notification.Name(rawValue: "GetServiceInfoBasicNotification"),
                            object: nil,
                            userInfo: ["statusText": data])
                    }else if name == 2/*"Major Service"*/{
                        majorService = data
                        if let packagePrice = data.value(forKey: "varPrice") as? String{
                            self.fiveHundredEADLabel.text = "\(packagePrice) "+"app_currency".localized()
                        }else if let packagePrice = data.value(forKey: "varPrice") as? NSNumber{
                            self.fiveHundredEADLabel.text = "\(packagePrice) "+"app_currency".localized()
                        }
                        NotificationCenter.default.post(
                            name: Notification.Name(rawValue: "GetServiceInfoMajorNotification"),
                            object: nil,
                            userInfo: ["statusText": data])
                    }
                }
            }
        }
      
    }
    
    
    func toggleRateButton(button:UIButton){
        if button == basicButton{
            fullButton.isSelected = false
            threeFiftyEADLabel.textColor = UIColor.lightGray
            majorButton.isSelected = false
            fiveHundredEADLabel.textColor = UIColor.lightGray
            basicButton.isSelected = true
            twoHundredEADLabel.textColor = colorGreen
        }else if button == fullButton {
            fullButton.isSelected = true
            threeFiftyEADLabel.textColor = colorGreen
            majorButton.isSelected = false
            fiveHundredEADLabel.textColor = UIColor.lightGray
            basicButton.isSelected = false
            twoHundredEADLabel.textColor = UIColor.lightGray
            
        }else{
            fullButton.isSelected = false
            threeFiftyEADLabel.textColor = UIColor.lightGray
            majorButton.isSelected = true
            fiveHundredEADLabel.textColor = colorGreen
            basicButton.isSelected = false
            twoHundredEADLabel.textColor = UIColor.lightGray
            
        }
    }
    
    
    
    
    // MARK: - getPackagesListByCategory API
    
    func getPackagesListByCategory()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            var languageCode = ""
            if UserDefaults.standard.value(forKey: kAppLanguage) != nil {
                languageCode = UserDefaults.standard.value(forKey: kAppLanguage) as! String
            }
            
            let dictParameters:[String:AnyObject] = ["chrCategory" : "S" as AnyObject,
                                                     "lang" : languageCode as AnyObject]
       
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objgetPackagesListByCategory = HttpWrapper.init()
            self.objgetPackagesListByCategory.delegate = self
            self.objgetPackagesListByCategory.requestWithparamdictParamPostMethodwithHeaderAndDictGet(url: kgetPackagesListByCategory, dictParams: dictParameters, headers: header)
        }
        
    }
    
    
    // MARK: - HttpWrapperfetchDataSuccess
    
    func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary)
    {
        AppHelper.hideLoadingView()
        print(dicsResponse)
        if wrapper == objgetPackagesListByCategory {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                arrCategorylist.removeAllObjects()
                
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray
                {
                    arrCategorylist = tempNames.mutableCopy() as! NSMutableArray
                    self.setDataOnViewController()
                }
                print(arrCategorylist)
            }
            else
            {
                // AppHelper.showAlert("Alert", message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }
        
    }
    // MARK: - HttpWrapperfetchDataFail
    
    func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError)
    {
        AppHelper.hideLoadingView()
        print(error.debugDescription)
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension SelectPacakgeViewController:UIScrollViewDelegate{
    
    @IBAction func onClickBasicButton(_ sender: UIButton){
        self.toggleRateButton(button: basicButton)
        self.raceScrollTo(CGPoint(x:0,y:0), withSnapBack: false, delegate: nil, callback: nil)
        self.raceTo(CGPoint(x:self.basicButton.frame.origin.x,y: 0), withSnapBack: false, delegate: nil, callbackmethod: nil)
        
    }
    
    
    @IBAction func onClickFullButton(_ sender: UIButton){
        self.toggleRateButton(button: fullButton)
        self.raceScrollTo(CGPoint(x:1*self.view.frame.size.width,y: 0), withSnapBack: false, delegate: nil, callback: nil)
        
        self.raceTo(CGPoint(x:self.fullButton.frame.origin.x,y: 0), withSnapBack: false, delegate: nil, callbackmethod: nil)
    }
    
    @IBAction func onClickMajorButton(_ sender: UIButton){
        self.toggleRateButton(button: majorButton)
        self.raceScrollTo(CGPoint(x:2*self.view.frame.size.width,y: 0), withSnapBack: false, delegate: nil, callback: nil)
        self.raceTo(CGPoint(x:self.majorButton.frame.origin.x,y: 0), withSnapBack: false, delegate: nil, callbackmethod: nil)
        
    }
    
    @objc func LoadScolllView() {
        aScrollView.delegate = nil
        aScrollView.contentSize = CGSize(width:self.view.frame.size.width * 3, height:aScrollView.frame.size.height)
        for i in 0 ..< self.children.count {
            self.loadScrollViewWithPage(i)
        }
        aScrollView.delegate = self
    }
    
    func loadScrollViewWithPage(_ page: Int) {
        if page < 0 {
            return
        }
        if page >= self.children.count {
            return
        }
        var basicVC : BasicServiceViewController
        var fullVC : FullServiceViewController
        var majorVC : MajorServiceViewController
        var frame: CGRect = aScrollView.frame
        switch page {
        case 0:
            basicVC = self.children[page] as! BasicServiceViewController
            basicVC.viewWillAppear(true)
            
            frame.origin.x = frame.size.width * CGFloat(page)
            frame.origin.y = 0
            basicVC.view.frame = frame
            aScrollView.addSubview(basicVC.view!)
            basicVC.view.setNeedsLayout()
            basicVC.view.layoutIfNeeded()
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
            basicVC.view.setNeedsLayout()
            basicVC.view.layoutIfNeeded()
            
        case 1:
            fullVC = self.children[page] as! FullServiceViewController
            fullVC.viewWillAppear(true)
            
            frame.origin.x = frame.size.width * CGFloat(page)
            frame.origin.y = 0
            fullVC.view.frame = frame
            aScrollView.addSubview(fullVC.view!)
            fullVC.view.setNeedsLayout()
            fullVC.view.layoutIfNeeded()
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
            fullVC.view.setNeedsLayout()
            fullVC.view.layoutIfNeeded()
            
        case 2:
            majorVC = self.children[page] as! MajorServiceViewController
            majorVC.viewWillAppear(true)
            
            frame.origin.x = frame.size.width * CGFloat(page)
            frame.origin.y = 0
            majorVC.view.frame = frame
            aScrollView.addSubview(majorVC.view!)
            majorVC.view.setNeedsLayout()
            majorVC.view.layoutIfNeeded()
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
            majorVC.view.setNeedsLayout()
            majorVC.view.layoutIfNeeded()
        default:
            break
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageCurrent = Int(floor(aScrollView.contentOffset.x / self.view.frame.size.width))
        switch pageCurrent {
        case 0:
            self.toggleRateButton(button: basicButton)
            self.raceScrollTo(CGPoint(x:0,y:0), withSnapBack: false, delegate: nil, callback: nil)
            self.raceTo(CGPoint(x:self.basicButton.frame.origin.x,y: 0), withSnapBack: false, delegate: nil, callbackmethod: nil)
            
            
        case 1:
            self.toggleRateButton(button: fullButton)
            self.raceScrollTo(CGPoint(x:1*self.view.frame.size.width,y: 0), withSnapBack: false, delegate: nil, callback: nil)
            self.raceTo(CGPoint(x:self.fullButton.frame.origin.x,y: 0), withSnapBack: false, delegate: nil, callbackmethod: nil)
            
        case 2:
            self.toggleRateButton(button: majorButton)
            self.raceScrollTo(CGPoint(x:2*self.view.frame.size.width,y: 0), withSnapBack: false, delegate: nil, callback: nil)
            self.raceTo(CGPoint(x:self.majorButton.frame.origin.x,y: 0), withSnapBack: false, delegate: nil, callbackmethod: nil)
            
        default:
            break
        }
        
        self.calledViewWillAppear()
    }
    
    func raceTo(_ destination: CGPoint, withSnapBack: Bool, delegate: AnyObject?, callbackmethod : (()->Void)?) {
        var stopPoint: CGPoint = destination
        if withSnapBack {
            let diffx = destination.x - movingView.frame.origin.x
            let diffy = destination.y - movingView.frame.origin.y
            if diffx < 0 {
                stopPoint.x -= 10.0
            }
            else if diffx > 0 {
                stopPoint.x += 10.0
            }
            
            if diffy < 0 {
                stopPoint.y -= 10.0
            }
            else if diffy > 0 {
                stopPoint.y += 10.0
            }
        }
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.2)
        UIView.setAnimationCurve(UIView.AnimationCurve.easeIn)
        movingView.frame = CGRect(x:stopPoint.x, y:stopPoint.y, width: movingView.frame.size.width,height: movingView.frame.size.height)
        UIView.commitAnimations()
        let firstDelay = 0.1
        let startTime = firstDelay * Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: .now() + startTime) {
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationDuration(0.1)
            UIView.setAnimationCurve(UIView.AnimationCurve.linear)
            self.movingView.frame = CGRect(x:destination.x, y:destination.y,width: self.movingView.frame.size.width, height: self.movingView.frame.size.height)
            UIView.commitAnimations()
        }
    }
    
    func raceScrollTo(_ destination: CGPoint, withSnapBack: Bool, delegate: AnyObject?, callback method:(()->Void)?) {
        var stopPoint = destination
        var isleft: Bool = false
        if withSnapBack {
            let diffx = destination.x - aScrollView.contentOffset.x
            if diffx < 0 {
                isleft = true
                stopPoint.x -= 10.0
            }
            else if diffx > 0 {
                isleft = false
                stopPoint.x += 10.0
            }
        }
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        UIView.setAnimationCurve(UIView.AnimationCurve.easeIn)
        if isleft {
            aScrollView.contentOffset = CGPoint(x:destination.x - 5, y:destination.y)
        }
        else {
            aScrollView.contentOffset = CGPoint(x:destination.x + 5, y:destination.y)
        }
        
        UIView.commitAnimations()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0) {() -> Void in
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationDuration(0.1)
            UIView.setAnimationCurve(UIView.AnimationCurve.linear)
            if isleft {
                self.aScrollView.contentOffset = CGPoint(x:destination.x + 5, y:destination.y)
            }
            else {
                self.aScrollView.contentOffset = CGPoint(x:destination.x - 5,y: destination.y)
            }
            UIView.commitAnimations()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0){() -> Void in
                UIView.beginAnimations(nil, context: nil)
                UIView.setAnimationDuration(0.1)
                UIView.setAnimationCurve(.easeInOut)
                self.aScrollView.contentOffset = CGPoint(x:destination.x, y:destination.y)
                UIView.commitAnimations()
                self.calledViewWillAppear()
            }
        }
    }
    
    func calledViewWillAppear() {
        
    }
    
}

