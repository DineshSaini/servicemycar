//
//  SelectCarBookingRepairViewController.swift
//  ServiceMyCar
//
//  Created by admin on 21/06/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

extension Notification.Name{
    
public static let Scroll_Collection_View_Cell_Notification =  Notification.Name("ScrollCollectionViewCellNotification")
    
}
class SelectCarBookingRepairViewController: UIViewController, HttpWrapperDelegate {
    
    
    @IBOutlet weak var aCollectionView:UICollectionView!
    @IBOutlet weak var payButton:UIButton!
    @IBOutlet weak var amountLabel:UILabel!
    
    @IBOutlet weak var seeMoreButton:UIButton!
    @IBOutlet weak var needHelpButton:UIButton!
    @IBOutlet weak var totalLabel:UILabel!
    
    var dataLoading = false
    
    

    
    var additionalImage = UIImage()
    
    var includeOrNotInclude = ["","Select Your Location","Select Your Date and Time"]
    
    var srtTittle = ""
    var srtPrice = ""
    
    var dataDictionay = NSDictionary()
    var carArrayList = NSMutableArray()
    var partArrayList = NSMutableArray()
    
    var extraPrice = NSDictionary()
    
    var objgetServiceYourCar = HttpWrapper()
    var objGetPart = HttpWrapper()
    var objGetExtraPrice = HttpWrapper()
    var objGetUserAddress = HttpWrapper()
    var net:NetworkReachabilityManager = NetworkReachabilityManager()!
    
    
    var selectCar = NSDictionary()
    var selectPart = NSDictionary()
    
    var collectionAddress:String = ""
    var dropAddress:String = ""
    var selectCollectionAddress = NSDictionary()
    
    var selectCarIndex = -1
    
    var isCarWash:Bool = false
    var isCarFuel:Bool = false
    
    let myDatePicker = UIDatePicker()
    
    
    var txtSelectDateTime = UITextField()
    
    var collectionAddressTextfield:UITextField!
    
    
    var collectionPicker = UIPickerView()
    var arrAddresslist = NSMutableArray()
    
    
    var dateAndTime = ""
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let layout = JEKScrollableSectionCollectionViewLayout()
        layout.defaultScrollViewConfiguration.showsHorizontalScrollIndicator = false
        
        layout.itemSize = CGSize(width: 50, height: 50);
        layout.headerReferenceSize = CGSize(width: 0, height: 22)
        layout.minimumInteritemSpacing = 5
        aCollectionView.collectionViewLayout = layout
        aCollectionView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)

        self.view.bringSubviewToFront(seeMoreButton)
        self.aCollectionView.register(UINib(nibName: "ServiceHeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: JEKCollectionElementKindSectionBackground, withReuseIdentifier: "ServiceHeaderCollectionReusableView")
        self.aCollectionView.register(UINib(nibName: "HeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HeaderCollectionReusableView")
        
        self.aCollectionView.register(UINib(nibName: "HeaderCollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "HeaderCollectionReusableView")

        self.aCollectionView.register(UINib(nibName: "ServiceCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ServiceCollectionViewCell")
        self.aCollectionView.register(UINib(nibName: "EmptyCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "EmptyCollectionViewCell")
        
        NotificationCenter.default.addObserver(self, selector: #selector(scrollViewDidEndScrollingAnimation(_:)), name: NSNotification.Name("ScrollCollectionView"), object: nil)
        
        self.aCollectionView.dataSource = self
        self.aCollectionView.delegate = self
        
        AppHelper.makeViewCircularWithRespectToHeight(payButton, borderColor: .clear, borderWidth: 0.0)

        self.amountLabel.text = "\(srtPrice)" + "app_currency".localized()
        if carArrayList.count<=3{
            self.seeMoreButton.isHidden = true
        }else{
            self.seeMoreButton.isHidden = false
        }
        self.title = "Select Your Car".localized()
        self.payButton.setTitle("confirm".localized(), for: .normal)
        self.totalLabel.text = "Total : ".localized()
        self.needHelpButton.setTitle("I need help now".localized(), for: .normal)
        
        let button = UIButton.init(type: .custom)
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        
        if languageCode == "en" {
            button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
        }
        else {
            button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
        }
        button.addTarget(self, action:#selector(onClickBackBarButton(_ :)), for: UIControl.Event.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton

        
        
        self.aCollectionView.reloadData()
        // Do any additional setup after loading the view.
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.setupNavigation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.setupNavigationButton()
        //        self.getPartsListItems()
        self.getServiceCarItems()
        self.getUserAddress()
        self.getExtraPriceData()
    }
    
    
    @IBAction func onClickBackBarButton(_ sender:UIBarButtonItem){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction private func onClickSeeMoreButton(_ sender: UIButton) {
        if carArrayList.count == 0 {return}
        let lastItemIndex = self.aCollectionView.numberOfItems(inSection: 0)
        let indexPath:IndexPath = IndexPath(item: lastItemIndex-1, section: 0)
        JEKScrollViewConfiguration.accessibilityScroll(.next)
        self.aCollectionView.scrollToItem(at: indexPath, at: UICollectionView.ScrollPosition.left, animated: true)
        self.seeMoreButton.isHidden = true
    }
    
    

    
    
    @IBAction func onClickCollectionPointAddressButton(_ sender:UIBarButtonItem){
                let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "AddAddress_Vc") as! AddAddress_Vc
                nextViewController.addressTypeString = "Enter Collection Address"
                nextViewController.addressChrType = "C"
                nextViewController.delegate = self
                self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    
    @IBAction func onClickConfrimButton(_ sender:UIButton){
        
        if selectCar.count == 0{
            AppHelper.showAlert("Important Message".localized().localized(), message: "Please Select Your Car!".localized())
            return
        }
        if collectionAddress.isEmpty{
            AppHelper.showAlert("Important Message".localized(), message: "Please Select Your Address !".localized())
            return
        }
        
        if self.dateAndTime.isEmpty && !self.needHelpButton.isSelected {
            AppHelper.showAlert("Important Message".localized(), message: "Please Select Your Date and Time or Check Need Help".localized())
            return
        }
        let selectDate = self.txtSelectDateTime.text ?? ""
        
        let nextViewController = objServiceSB.instantiateViewController(withIdentifier: "ConfrimBookingServiceViewController") as! ConfrimBookingServiceViewController
        nextViewController.collectionAddress = self.collectionAddress
        nextViewController.selectCar = self.selectCar
        nextViewController.additionalImage = self.additionalImage
        nextViewController.dateAndTime = dateAndTime
        nextViewController.srtTittle = self.srtTittle
        nextViewController.srtPrice = self.srtPrice
        nextViewController.selectDate = selectDate
        nextViewController.selectCollectionAddress = self.selectCollectionAddress
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    
    @IBAction func tappedOnNeedHelp(_ sender:UIButton){
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
            let loc = Locale(identifier: "\(languageCode)")
            let now = Date()
            let formatter = DateFormatter()
            formatter.locale = loc
            formatter.dateFormat = "EEEE d MMMM HH:mm a"
            
            let mydateFormatter = DateFormatter()
            mydateFormatter.locale = Locale(identifier: "en")
            mydateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            self.dateAndTime = mydateFormatter.string(from: now)
            
            let dateString = formatter.string(from: now)
            self.txtSelectDateTime.text = dateString
            self.aCollectionView.reloadData()
            self.view.endEditing(true)

        }
    }
    
    
    
    
    func setupNavigationButton(){
        let button = UIButton.init(type: .custom)
        button.frame = CGRect.init(x: 0, y: 0, width:35, height: 35)
        button.setImage(UIImage(named: "plus_green"), for: .normal)
        button.addTarget(self, action: #selector(onClickAddNavigation(_:)), for: .touchUpInside)
        let barButton : UIBarButtonItem = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
        
    }
    
    @IBAction func onClickAddNavigation(_ sender: UIButton){
        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "AddCar_Vc") as! AddCar_Vc
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension SelectCarBookingRepairViewController:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return (carArrayList.count != 0) ? carArrayList.count : 1
        }else{
            return 1
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let edgeInsets = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        if section == 0{
            if carArrayList.count == 2{
                let edgeInsets = UIEdgeInsets.init(top: 0, left: self.view.frame.size.width/6.0, bottom: 0, right: 0)
                return edgeInsets
            }else if carArrayList.count == 1{
                let edgeInsets = UIEdgeInsets.init(top: 0, left: self.view.frame.size.width/3.0, bottom: 0, right: 0)
                return edgeInsets
            }else{
                return edgeInsets
            }
        }else{
            return edgeInsets
        }
    }
  
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if section == 0{
            return CGSize(width: self.view.frame.size.width, height: 25.0)
            
        }else{
            return CGSize(width: self.view.frame.size.width, height: 35.0)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return CGSize(width: self.view.frame.size.width, height: 0.0)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if (kind == UICollectionView.elementKindSectionHeader) {
            
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderCollectionReusableView", for: indexPath) as! HeaderCollectionReusableView
            let index = indexPath.first ?? 0
            headerView.headerTittle.text = self.includeOrNotInclude[index].localized()
            headerView.infoImage.isHidden = true
            return headerView
        }else{
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderCollectionReusableView", for: indexPath) as! HeaderCollectionReusableView
            headerView.infoImage.isHidden = true
            return headerView
            
        }
    }

    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        let visibleIndexPath = aCollectionView.indexPathsForVisibleItems
        let indexPathForSectionOne = visibleIndexPath.filter { (indexPath) -> Bool in
            return (indexPath.section == 0)
        }
        let shouldHideSeeMoreButton = indexPathForSectionOne.contains(where: { (indexPath) -> Bool in
            return indexPath.item == self.carArrayList.count-1
        })
        seeMoreButton.isHidden = shouldHideSeeMoreButton
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        self.perform(#selector(scrollViewDidEndScrollingAnimation(_:)), with: scrollView, afterDelay: 0.3)
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0{
            if carArrayList.count == 0{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EmptyCollectionViewCell", for: indexPath) as! EmptyCollectionViewCell
                cell.noDataLabel.text = dataLoading ? "No Car Found!".localized() : "Loading..".localized()
                return cell
            }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CarCollectionViewCell", for: indexPath) as! CarCollectionViewCell
            cell.selectButton.isSelected = self.selectCarIndex == indexPath.item ? true : false
            cell.backView.layer.borderColor = colorGreen.cgColor
            cell.backView.layer.borderWidth = 1.0
            cell.backView.layer.cornerRadius = 5.0
            cell.backView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2).cgColor
            cell.backView.layer.shadowOffset = CGSize.zero
            cell.backView.layer.shadowOpacity = 1.0
            cell.backView.layer.shadowRadius = 1.0
            cell.backView.layer.masksToBounds = false
            cell.contentView.backgroundColor = UIColor.init(red: 244.0/255.0, green: 245.0/255.0, blue: 248.0/255.0, alpha: 1.0)
            if let name = (carArrayList.object(at: indexPath.item) as! NSDictionary).value(forKey: "varCar") as? String, let model = (carArrayList.object(at: indexPath.item) as! NSDictionary).value(forKey: "varModel") as? String{
                cell.carName.text = "\(name.uppercased()) \(model.uppercased())"
            }
            
            if let name = (carArrayList.object(at: indexPath.item) as! NSDictionary).value(forKey: "varYear") as? String{
                cell.yearLabel.text  = "\(name.uppercased())"
            }
            
            if let varPlateCode = (carArrayList.object(at: indexPath.item) as! NSDictionary).value(forKey: "varPlateCode") as? String, let varPlateNumber = (carArrayList.object(at: indexPath.item) as! NSDictionary).value(forKey: "varPlateNumber") as? String{
                cell.carNumber.text = varPlateNumber.uppercased()
                cell.categoryName.text = varPlateCode.uppercased()
            }
                
            if let stName = (carArrayList.object(at: indexPath.row) as! NSDictionary).value(forKey: "varCarState") as? String{
                    cell.plateImageView.image = AppHelper.setNumberPlateImage(stateName: stName)
                }

            return cell
            }
        }else if indexPath.section == 1{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BookingAddressCollectionViewCell", for: indexPath) as! BookingAddressCollectionViewCell
            cell.collectionAddAdressButton.addTarget(self, action: #selector(onClickCollectionPointAddressButton(_:)), for: .touchUpInside)
            cell.backView.backgroundColor = UIColor.init(red: 244.0/255.0, green: 245.0/255.0, blue: 248.0/255.0, alpha: 1.0)
            cell.collectAddressTextfield.placeholder = "Select Address".localized()
            let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
            if languageCode != "en" {
                cell.collectAddressTextfield.textAlignment = .right
            }
            self.collectionAddressTextfield = cell.collectAddressTextfield
            cell.collectAddressTextfield.text = collectionAddress
            self.setupCollectionPicker(textField: cell.collectAddressTextfield)
            return cell
        }else if indexPath.section == 2{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BookingDateCollectionViewCell", for: indexPath) as! BookingDateCollectionViewCell
            cell.setLeftIconForTextField(cell.dateAndTimeTextfield, leftIcon: UIImage())
            cell.dateAndTimeTextfield.placeholder = "Select Date And Time".localized()
            let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
            if languageCode != "en" {
                cell.dateAndTimeTextfield.textAlignment = .right
            }
            txtSelectDateTime = cell.dateAndTimeTextfield
            self.showDatePicker()
            cell.setNeedsLayout()
                return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0{
            if carArrayList.count == 0{
                let size:CGFloat = (self.view.frame.size.width - 20) / 1.0
                return CGSize(width: size, height: 140)
            }else{
            let size:CGFloat = (self.view.frame.size.width - 20) / 3.0
            return CGSize(width: size, height: size + 10)
            }
        }else{
            let size:CGFloat = (self.view.frame.size.width - 1) / 1
            return CGSize(width: size, height: 60)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0{
            if carArrayList.count == 0 {return}
            let selectCarItem = carArrayList.object(at: indexPath.item) as? NSDictionary
            self.selectCar = selectCarItem!
            self.selectCarIndex = indexPath.item
           // aCollectionView.reloadItems(at: [indexPath])

            aCollectionView.reloadData()
            }
    }
    
    
    func isFuelAndIsCarWash(){
        var carWashAmount:Float = 0.0
        var carFuelAmount:Float = 0.0
        var serviceAmount:Float = 0.0
        if let getPrice = extraPrice.value(forKey: "carFuel") as? String{
            carFuelAmount = Float(getPrice) ?? 0.0
        }
        if let getPrice = extraPrice.value(forKey: "carValet") as? String{
            carWashAmount = Float(getPrice) ?? 0.0
        }
        if let packagePrice = dataDictionay.value(forKey: "varPrice") as? String{
            serviceAmount = Float(packagePrice) ?? 0.0
        }else if let packagePrice = dataDictionay.value(forKey: "varPrice") as? NSNumber{
            serviceAmount = Float(truncating: packagePrice)
        }
        if isCarWash && isCarFuel{
            let totalAmount = serviceAmount+carFuelAmount+carWashAmount
            self.amountLabel.text = "\(totalAmount)" + "app_currency".localized()
            
        }else if isCarWash{
            let totalAmount = serviceAmount+carWashAmount
            self.amountLabel.text = "\(totalAmount)" + "app_currency".localized()
        }else if isCarFuel{
            let totalAmount = serviceAmount+carFuelAmount
            self.amountLabel.text = "\(totalAmount)" + "app_currency".localized()
        }else{
            let totalAmount = serviceAmount
            self.amountLabel.text = "\(totalAmount)" + "app_currency".localized()
        }
    }
    
    
    func showDatePicker(){
        //Formate Date
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        let loc = Locale(identifier: "\(languageCode)")
        myDatePicker.locale = loc
        var calendar = Calendar.current
        calendar.locale = loc
        self.myDatePicker.calendar = calendar
        
        myDatePicker.datePickerMode = .dateAndTime
        myDatePicker.minimumDate = Date.tomorrow
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        dateFormatter.locale = loc
        
        //ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .black
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.white
        toolBar.sizeToFit()
        toolBar.backgroundColor = colorGreen
        
        let doneButton = UIBarButtonItem(title: "date_time_done".localized(), style: .plain, target: self, action: #selector(self.donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "date_time_cancel".localized(), style: .plain, target: self, action: #selector(self.cancelDatePicker))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.txtSelectDateTime.inputAccessoryView = toolBar
        self.txtSelectDateTime.inputView = myDatePicker
    }
    
    @objc func donedatePicker(){
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        let loc = Locale(identifier: "\(languageCode)")
        myDatePicker.locale = loc
        var calendar = Calendar.current
        calendar.locale = loc
        self.myDatePicker.calendar = calendar
        
        myDatePicker.datePickerMode = .dateAndTime
        myDatePicker.minimumDate = Date.tomorrow
        let dateFormatter = DateFormatter()
        dateFormatter.locale = loc
        dateFormatter.dateFormat = "EEEE d MMMM HH:mm a"
        
        let mydateFormatter = DateFormatter()
        mydateFormatter.locale = Locale(identifier: "en")
        mydateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        self.dateAndTime = mydateFormatter.string(from: myDatePicker.date)
        self.txtSelectDateTime.text = dateFormatter.string(from: myDatePicker.date)

        self.aCollectionView.reloadData()
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.aCollectionView.reloadData()
        self.view.endEditing(true)
    }
    
    
}


extension SelectCarBookingRepairViewController{
    func getServiceCarItems(){
        if net.isReachable == false{
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["userId" : nUserId as AnyObject]
            //let header = ["X-API-KEY" : "smc1989"] as [String:AnyObject]
            
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objgetServiceYourCar = HttpWrapper.init()
            self.objgetServiceYourCar.delegate = self
            
            self.objgetServiceYourCar.requestWithparamdictParamPostMethodwithHeaderAndDictGet(url: kgetUserCars, dictParams: dictParameters, headers: header)
        }
        
    }
    
    
    func getPartsListItems(){
        if net.isReachable == false{
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            var languageCode = ""
            if UserDefaults.standard.value(forKey: kAppLanguage) != nil {
                languageCode = UserDefaults.standard.value(forKey: kAppLanguage) as! String
            }
            
            let dictParameters:[String:AnyObject] = ["lang" : languageCode as AnyObject]
            
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objGetPart = HttpWrapper.init()
            self.objGetPart.delegate = self
            self.objGetPart.requestWithparamdictParamPostMethodwithHeaderAndDictGet(url: kgetExtraPartImages, dictParams: dictParameters, headers: header)
        }
        
    }
    
    
    func getExtraPriceData(){
        if net.isReachable == false{
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            var languageCode = ""
            if UserDefaults.standard.value(forKey: kAppLanguage) != nil {
                languageCode = UserDefaults.standard.value(forKey: kAppLanguage) as! String
            }
            
            let dictParameters:[String:AnyObject] = ["lang" : languageCode as AnyObject]
            
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objGetExtraPrice = HttpWrapper.init()
            self.objGetExtraPrice.delegate = self
            self.objGetExtraPrice.requestWithparamdictParamPostMethodwithHeaderGet(url: kGetExtraPrice, headers: header)
        }
        
    }
    
    
    
    func getUserAddress(){
        if net.isReachable == false{
            
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }else{
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["userId" : nUserId as AnyObject]
            //let header = ["X-API-KEY" : "smc1989"] as [String:AnyObject]
            
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objGetUserAddress = HttpWrapper.init()
            self.objGetUserAddress.delegate = self
            
            self.objGetUserAddress.requestWithparamdictParamPostMethodwithHeaderAndDictGet(url: kgetUserAddress, dictParams: dictParameters, headers: header)
        }
    }
    
    
    // MARK: - HttpWrapperfetchDataSuccess
    
    func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary)
    {
        AppHelper.hideLoadingView()
        print(dicsResponse)
        if wrapper == objgetServiceYourCar {
            dataLoading = true
            if dicsResponse.value(forKey: "status") as? NSString == "success"{
                carArrayList.removeAllObjects()
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray{
                    carArrayList = tempNames.mutableCopy() as! NSMutableArray
                }
                print(carArrayList)

                if carArrayList.count>3{
                    self.seeMoreButton.isHidden = false
                }
                if carArrayList.count<=3{
                    self.seeMoreButton.isHidden = true
                }else{
                    self.seeMoreButton.isHidden = false
                }
                self.aCollectionView.reloadData()
            }else{
                dataLoading = true
                self.aCollectionView.reloadData()

                // AppHelper.showAlert("Alert", message:dicsResponse.value(forKey: "msg") as! String)
            }
            
        }else if wrapper == objGetPart{
            if dicsResponse.value(forKey: "status") as? NSString == "success"{
                partArrayList.removeAllObjects()
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray{
                    partArrayList = tempNames.mutableCopy() as! NSMutableArray
                }
                print(partArrayList)
                self.aCollectionView.reloadData()
            }else{
                
            }
        }else if wrapper == objGetExtraPrice{
            if dicsResponse.value(forKey: "status") as? NSString == "success"{
                if let tempNames = dicsResponse.value(forKey: "data") as? NSDictionary{
                    extraPrice = tempNames.mutableCopy() as! NSDictionary
                }
                self.aCollectionView.reloadData()
            }else{
                
            }
        }else if wrapper == objGetUserAddress{
            if dicsResponse.value(forKey: "status") as? NSString == "success"{
                arrAddresslist.removeAllObjects()
                
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray{
                    arrAddresslist = tempNames.mutableCopy() as! NSMutableArray
                }
                print(arrAddresslist)
                aCollectionView.reloadData()
            }else{
                
                // AppHelper.showAlert("Alert", message:dicsResponse.value(forKey: "msg") as! String)
            }
        }
    }
    // MARK: - HttpWrapperfetchDataFail
    
    func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError){
        AppHelper.hideLoadingView()
        print(error.debugDescription)
    }
}


extension SelectCarBookingRepairViewController:AddAddress_VcDelegate{
    func getAddAddress(_ viewController: UIViewController, address isAdded: Bool) {
    }
    
    func getCollectionPointAddress(_ viewController: UIViewController, collectAddress address: String, newAddress: NSDictionary) {
        self.collectionAddress = address
        self.selectCollectionAddress = newAddress
        self.aCollectionView.reloadData()
    }
    
    func getDropPointAddress(_ viewController: UIViewController, dropAddress address: String, newAddress: NSDictionary) {
        self.dropAddress = address
        self.aCollectionView.reloadData()
    }

}



extension SelectCarBookingRepairViewController:UIPickerViewDataSource,UIPickerViewDelegate{
    
    
    
    // Mark:- SetUp carCategory picker
    
    @IBAction func onClickSelectCollectAddressType(_ sender: UIBarButtonItem){
        if self.arrAddresslist.count == 0 {
            self.view.endEditing(true)
            return
            
        }
        let selectAddress  = arrAddresslist[collectionPicker.selectedRow(inComponent: 0)]
        self.selectCollectionAddress = selectAddress as! NSDictionary
        if let address = (selectAddress as AnyObject).value(forKey: "varAddress") as? String, let buildName = (selectAddress as AnyObject).value(forKey: "varBuildingName") as? String, let additionalText = (selectAddress as AnyObject).value(forKey: "varAdditionalInstruction") as? String, let state = (selectAddress as AnyObject).value(forKey: "varState") as? String, let country = (selectAddress as AnyObject).value(forKey: "varCountry") as? String{
            self.collectionAddress = "\(address) \(buildName) \(additionalText) \(state) \(country)"
            self.collectionAddressTextfield.text = self.collectionAddress
        }
        self.aCollectionView.reloadData()
        self.view.endEditing(true)
    }
    
    
    @IBAction func onClickSelectCancelType(_ sender: UIBarButtonItem){
        self.aCollectionView.reloadData()
        self.view.endEditing(true)
        
    }
    
    func setupCollectionPicker(textField:UITextField){
        
        // UIPickerView
        self.collectionPicker = UIPickerView()
        self.collectionPicker.delegate = self
        self.collectionPicker.dataSource = self
        self.collectionPicker.backgroundColor = UIColor.white
        
        textField.inputView = self.collectionPicker
        collectionPicker.reloadAllComponents()
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .black
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.white
        toolBar.sizeToFit()
        toolBar.backgroundColor = colorGreen
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "add_your_car_done".localized(), style: .plain, target: self, action: #selector(onClickSelectCollectAddressType(_:)))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "add_your_car_cancel".localized(), style: .plain, target: self, action: #selector(onClickSelectCancelType(_:)))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    
    
  
    
    
    //MARK:- PickerView Delegate & DataSource
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if arrAddresslist.count != 0{
            return arrAddresslist.count
        }else{
            return 0
        }
        
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label: UILabel
        
        if let view = view {
            label = view as! UILabel
        }
        else {
            label = UILabel(frame: CGRect(x: 0, y: 0, width: pickerView.frame.width, height: 400))
        }
        
        label.lineBreakMode = .byWordWrapping;
        if arrAddresslist.count != 0{
            if pickerView == collectionPicker{
                if let address = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varAddress") as? String, let buildName = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varBuildingName") as? String, let additionalText = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varAdditionalInstruction") as? String, let state = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varState") as? String, let country = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varCountry") as? String{
                    label.text = "\(address) \(buildName) \(additionalText) \(state) \(country)"
                }
            }
        }else{
            label.text = "tet"
        }
        label.numberOfLines = 2;
        //label.text = arr[row]
        label.sizeToFit()
        return label;
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if arrAddresslist.count != 0{
            if pickerView == collectionPicker{
                if let address = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varAddress") as? String, let buildName = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varBuildingName") as? String, let additionalText = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varAdditionalInstruction") as? String, let state = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varState") as? String, let country = (arrAddresslist.object(at: row) as! NSDictionary).value(forKey: "varCountry") as? String{
                    self.collectionAddress = "\(address) \(buildName) \(additionalText) \(state) \(country)"
                    self.collectionAddressTextfield.text = self.collectionAddress
                }
                self.selectCollectionAddress = (arrAddresslist.object(at: row) as! NSDictionary)
            }
        }
        
    }
}



