//
//  MyServices_Vc.swift
//  ServiceMyCar
//
//  Created by baps on 12/02/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import SJSwiftSideMenuController
import Alamofire
import SwiftyJSON

class MyServices_Vc: UIViewController ,UIScrollViewDelegate , UITableViewDelegate , UITableViewDataSource ,HttpWrapperDelegate {
   
    
    @IBOutlet weak var btnBookServicePage2: UIButtonX!
    @IBOutlet weak var btnBookServicePage1: UIButtonX!
    
    @IBOutlet weak var tblPage2: UITableView!
    @IBOutlet weak var tblPage1: UITableView!
    @IBOutlet weak var btnPage2: UIButton!
    @IBOutlet weak var btnPage1: UIButton!
    @IBOutlet var viewPage2: UIView!
    @IBOutlet var viewPage1: UIView!
    @IBOutlet weak var ViewPager: UIScrollView!
     @IBOutlet weak var viewMain: UIView!
    var isHistory = true
    @IBOutlet weak var lblSwipeWhiteViewConstant: NSLayoutConstraint!
    var scrollPage:UIScrollView = UIScrollView()
    var objGetHistory = HttpWrapper()
    var objGetScheduled = HttpWrapper()
    let badgeButton : MIBadgeButton = MIBadgeButton(frame:CGRect(x: -10, y: 15, width: 40, height: 20))
    var net:NetworkReachabilityManager = NetworkReachabilityManager()!
    var arrHistory:NSMutableArray = NSMutableArray()
    var arrScheduled:NSMutableArray = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.tblPage1.backgroundColor = colorlight
        self.tblPage2.backgroundColor = colorlight
        
        let button = UIButton.init(type: .custom)
        //backarrow , menu
        if appDelegate.isFromNotification == true
        {
            button.setImage(UIImage.init(named: "menu"), for: UIControl.State.normal)
        }
        else
        {
            let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
            
            if languageCode == "en" {
                button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
            }
            else {
                button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
            }
        }
       // button.addTarget(self, action:#selector(SideMenu), for: UIControl.Event.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        
        badgeButton.edgeInsetTop = 10
        badgeButton.setImage(UIImage(named : "notification"), for: .normal)
        print(appDelegate.NCount)
        //badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        badgeButton.badgeString = appDelegate.NCount
        if appDelegate.NCount == "0"
        {
            badgeButton.badgeBackgroundColor = UIColor.clear
            badgeButton.badgeTextColor = UIColor.clear
        }
        else
        {
            badgeButton.badgeBackgroundColor = colorGreen
            badgeButton.badgeTextColor = UIColor.white
        }
        //badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        let barButton2 : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton2
        
        btnBookServicePage1.addTarget(self, action:#selector(onclickBtnBookServiceNow), for: UIControl.Event.touchUpInside)
        btnBookServicePage2.addTarget(self, action:#selector(onclickBtnBookServiceNow), for: UIControl.Event.touchUpInside)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)
        
        btnPage1.setTitle("my_service_history".localized(), for: .normal)
        btnPage2.setTitle("my_service_scheduled".localized(), for: .normal)
        
        self.tblPage2.register(UINib(nibName: "MyServiceTableViewCell", bundle: nil), forCellReuseIdentifier:"MyServiceTableViewCell")
        self.tblPage1.register(UINib(nibName: "MyServiceTableViewCell", bundle: nil), forCellReuseIdentifier:"MyServiceTableViewCell")
        self.tblPage1.register(UINib(nibName: "NoDataCell", bundle: nil), forCellReuseIdentifier:"NoDataCell")
        self.tblPage2.register(UINib(nibName: "NoDataCell", bundle: nil), forCellReuseIdentifier:"NoDataCell")
        
    }
    
    
    // MARK: - Google Api Calling
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            //self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                
                badgeButton.badgeString = appDelegate.NCount
                if appDelegate.NCount == "0"
                {
                    badgeButton.badgeBackgroundColor = UIColor.clear
                    badgeButton.badgeTextColor = UIColor.clear
                }
                else
                {
                    badgeButton.badgeBackgroundColor = colorGreen
                    badgeButton.badgeTextColor = UIColor.white
                }
            }
        }
    }
    
    // MARK:- goBack
    
    @objc func onclickBtnBookServiceNow()
    {
        appDelegate.loginSuccess()
    }
    
    // MARK:- goBack
    
    @objc func SideMenu()
    {
        if appDelegate.isFromNotification == true
        {
            SJSwiftSideMenuController.toggleLeftSideMenu()
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    @objc func NotificationAlert()
    {
        guard let vcs = self.navigationController?.viewControllers else{return}
        let inStack = vcs.contains(where: { (vc) -> Bool in
            return vc is MyNotificationList_Vc
        })
        if inStack{
            self.performSegue(withIdentifier: unwindToNotificationController, sender: self)
        }else{
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        //        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
        //
        //        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.scrollPage = ViewPager
        var arrview = [viewPage1 , viewPage2]
        // self.view.addSubview(ViewPager)
        for index in 0..<2 {
            let imageView = UIView()
            
            let x = self.view.frame.size.width * CGFloat(index)
            imageView.frame = CGRect(x: x, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            imageView.contentMode = .scaleAspectFit
            imageView.clipsToBounds = true
            let view = arrview[index]
            view?.frame = self.view.frame
            view?.frame.origin.y = 0
            //view?.frame = CGRect(x: 0, y: 0, width: self.viewMain.frame.width, height: self.viewMain.frame.height)
            imageView.addSubview(view!)
            
            ViewPager.contentSize.width = self.view.frame.size.width * CGFloat(index + 1)
            ViewPager.contentSize.height = self.viewMain.frame.height
            //viewPage1.bringSubviewToFront(tblPage1)
            ViewPager.addSubview(imageView)
            //            print(index)
            //            frame.origin.x = self.ViewPager.frame.size.width * CGFloat(index)
            //            frame.size = self.ViewPager.frame.size
            //
            //            let subView = UIView(frame: frame)
            //            subView.backgroundColor = colors[index]
            //            let view = arrview[index]
            //            let x = self.one.frame.size.width * CGFloat(index)
            //            view?.frame = CGRect(x: x, y: 0, width: self.one.frame.width, height: self.one.frame.height)
            //            view?.frame = one.frame
            //            ViewPager.contentSize.width = ViewPager.frame.size.width * CGFloat(index + 1)
            //            //ViewPager.addSubview(view)
            //            print(view)
            //            self.ViewPager.addSubview(view!)
            
            
        }
        tblPage1.dataSource = self
        tblPage1.delegate = self
        tblPage1.reloadData()
        if isHistory == false
        {
            let frame = scrollPage.frame
            let offset:CGPoint = CGPoint(x: CGFloat(1) * frame.size.width, y: 0)
            self.scrollPage.setContentOffset(offset, animated: true)
            lblSwipeWhiteViewConstant.constant = btnPage1.frame.width
            btnPage2.setTitleColor(colorGreen, for: .normal)
            btnPage1.setTitleColor(UIColor.darkGray, for: .normal)
        }
        else
        {
            
        }
        getUserHistory()
        getUserScheduled()
    }
    
    // MARK: - onClickBtnPage2
    
    @IBAction func onClickBtnPage2(_ sender: UIButton) {
        let frame = scrollPage.frame
        let offset:CGPoint = CGPoint(x: CGFloat(1) * frame.size.width, y: 0)
        self.scrollPage.setContentOffset(offset, animated: true)
        lblSwipeWhiteViewConstant.constant = btnPage1.frame.width
        btnPage2.setTitleColor(colorGreen, for: .normal)
        btnPage1.setTitleColor(UIColor.darkGray, for: .normal)
    }
    
    // MARK: - onClickBtnPage1
    @IBAction func onClickBtnPage1(_ sender: UIButton) {
        
        
        let frame = scrollPage.frame
        let offset:CGPoint = CGPoint(x: CGFloat(0) * frame.size.width, y: 0)
        self.scrollPage.setContentOffset(offset, animated: true)
        lblSwipeWhiteViewConstant.constant = 0
        btnPage1.setTitleColor(colorGreen, for: .normal)
        btnPage2.setTitleColor(UIColor.darkGray, for: .normal)
    }
    
    // MARK: - scrollViewDidScroll
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblPage1
        {
            return arrHistory.count == 0 ? 1 : arrHistory.count
        }
        else
        {
            return arrScheduled.count == 0 ? 1:  arrScheduled.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblPage2
        {
            
            if arrScheduled.count == 0 {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath) as! NoDataCell
                cell.messageLabel.text = "No data found"
                return cell
                
            }else{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "MyServiceTableViewCell" , for: indexPath) as! MyServiceTableViewCell
                AppHelper.makeCircularWithRightTopTwoCornerRadius(cell.backView, cornerRadius: 25.0)
                
                
                cell.backView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).cgColor
                cell.backView.backgroundColor = UIColor.white
                cell.backView.layer.shadowOffset = CGSize.zero
                cell.backView.layer.shadowOpacity = 1.0
                cell.backView.layer.shadowRadius = 3.0
                cell.backView.layer.masksToBounds = false
                cell.documentsButton.layer.cornerRadius = 20
                cell.documentsButton.layer.masksToBounds = false
                cell.setNeedsLayout()
                cell.setNeedsDisplay()
                
                
                if let name = (arrScheduled.object(at: indexPath.row) as! NSDictionary).value(forKey: "date") as? String
                {
                    cell.dateLbl.text  = name
                }
                
                
                
                if let name = (arrScheduled.object(at: indexPath.row) as! NSDictionary).value(forKey: "netAmount") as? String
                {
                    cell.netAmountLbl.text  = name
                }
                
                if let name = (arrScheduled.object(at: indexPath.row) as! NSDictionary).value(forKey: "intPackage") as? String
                {
                    cell.packageLbl.text  = name
                }
                
                
                if let varCar = (arrScheduled.object(at: indexPath.row) as! NSDictionary).value(forKey: "varCar") as? String         {
                    cell.carNameLbl.text  = varCar
                }
                
                
                if let varModel = (arrScheduled.object(at: indexPath.row) as! NSDictionary).value(forKey: "varModel") as? String         {
                    cell.carModelLbl.text  = varModel
                }
                
                cell.documentsButton.addTarget(self, action: #selector(onClickDocumentButton(_:)), for: .touchUpInside)
                
                
                return cell
            }
            
        }
        else
        {
            
            if arrHistory.count == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath) as! NoDataCell
                cell.messageLabel.text = "No data found"
                return cell
                
            }else{
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "MyServiceTableViewCell" , for: indexPath) as! MyServiceTableViewCell
                AppHelper.makeCircularWithRightTopTwoCornerRadius(cell.backView, cornerRadius: 25.0)
                cell.backView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).cgColor
                cell.backView.backgroundColor = UIColor.white
                cell.backView.layer.shadowOffset = CGSize.zero
                cell.backView.layer.shadowOpacity = 1.0
                cell.backView.layer.shadowRadius = 3.0
                cell.backView.layer.masksToBounds = false
                cell.documentsButton.layer.cornerRadius = 20
                cell.documentsButton.layer.masksToBounds = false
                
                
                
                if let name = (arrHistory.object(at: indexPath.row) as! NSDictionary).value(forKey: "date") as? String
                {
                    cell.dateLbl.text  = name
                }
                
                
                
                if let name = (arrHistory.object(at: indexPath.row) as! NSDictionary).value(forKey: "netAmount") as? String
                {
                    cell.netAmountLbl.text  = name
                }
                
                if let name = (arrHistory.object(at: indexPath.row) as! NSDictionary).value(forKey: "intPackage") as? String
                {
                    cell.packageLbl.text  = name
                }
                
                
                if let varCar = (arrHistory.object(at: indexPath.row) as! NSDictionary).value(forKey: "varCar") as? String         {
                    cell.carNameLbl.text  = varCar
                }
                
                
                if let varModel = (arrHistory.object(at: indexPath.row) as! NSDictionary).value(forKey: "varModel") as? String         {
                    cell.carModelLbl.text  = varModel
                }
                
                cell.documentsButton.addTarget(self, action: #selector(onClickDocumentButton(_:)), for: .touchUpInside)
                
                
                
                return cell
                
            }
        }
    }
    
    
    
    
    @IBAction func onClickDocumentButton(_ sender:UIButton) {
        
        var carName = ""
        var carModel = ""
        
        if let indexpath = sender.tableViewIndexPath(self.tblPage1){
            if let car = (arrHistory.object(at: indexpath.row) as! NSDictionary).value(forKey: "varCar") as? String
            {
                carName = car
            }
            
            if let model = (arrHistory.object(at: indexpath.row) as! NSDictionary).value(forKey: "varModel") as? String
            {
                carModel  = model
            }
            self.navigateTocategory(carName: carName, model: carModel)
            
        }else if let indexpath = sender.tableViewIndexPath(self.tblPage2){
            if let car = (arrHistory.object(at: indexpath.row) as! NSDictionary).value(forKey: "varCar") as? String
            {
                carName = car
            }
            
            if let model = (arrHistory.object(at: indexpath.row) as! NSDictionary).value(forKey: "varModel") as? String
            {
                carModel  = model
            }
            self.navigateTocategory(carName: carName, model: carModel)
            
        }
    }
    
    
    func navigateTocategory(carName:String,model:String){
        
        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "CategoryDocViewController") as! CategoryDocViewController
        nextViewController.carName = carName
        nextViewController.carModel = model
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    
    // MARK: - scrollViewDidScroll
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == ViewPager
        {
            self.scrollPage = scrollView
            //                let verticalIndicator: UIImageView = (scrollView.subviews[(scrollView.subviews.count - 1)] as! UIImageView)
            //                verticalIndicator.backgroundColor = UIColor(red: 211/255.0, green: 138/255.0, blue: 252/255.0, alpha: 1)
            let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
            
            if pageNumber > 0
            {
                lblSwipeWhiteViewConstant.constant = scrollView.contentOffset.x/2
                btnPage2.setTitleColor(colorGreen, for: .normal)
                btnPage1.setTitleColor(UIColor.darkGray, for: .normal)
            }
            else
            {
                lblSwipeWhiteViewConstant.constant = scrollView.contentOffset.x/2
                btnPage1.setTitleColor(colorGreen, for: .normal)
                btnPage2.setTitleColor(UIColor.darkGray, for: .normal)
            }
            print(pageNumber)
            
            //        verticalIndicator.backgroundColor = UIColor.blue
        }
        
        
    }
    
    
    // MARK: - getUserHistory API
    
    func getUserHistory()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["userId" : nUserId as AnyObject]
            //            let dictHeaaderPera:[String:AnyObject] = [
            //                "platform" : "IOS" as AnyObject,
            //                "modelNumber" : "ML2000" as AnyObject
            //                ,"serialNumber" :"1234567891" as AnyObject,
            //                 "deviceId" : "12345" as AnyObject]
            //
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objGetHistory = HttpWrapper.init()
            self.objGetHistory.delegate = self
            //self.objGetHistory.requestWithparamdictParamPostMethod(url: kgetPastBookings, dicsParams: dictParameters)
            self.objGetHistory.requestWithparamdictParamMethodwithHeaderAndDictGet(url: kgetBookings, dictParams: dictParameters, headers: header)
            
            
        }
        
    }
    
    // MARK: - getUserScheduled API
    
    func getUserScheduled()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["userId" : nUserId as AnyObject]
            //            let dictHeaaderPera:[String:AnyObject] = [
            //                "platform" : "IOS" as AnyObject,
            //                "modelNumber" : "ML2000" as AnyObject
            //                ,"serialNumber" :"1234567891" as AnyObject,
            //                 "deviceId" : "12345" as AnyObject]
            //
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objGetScheduled = HttpWrapper.init()
            self.objGetScheduled.delegate = self
            self.objGetScheduled.requestWithparamdictParamMethodwithHeaderAndDictGet(url: kgetPastBookings, dictParams: dictParameters, headers: header)
            
            
            
        }
        
    }
    
    
    // MARK: - HttpWrapperfetchDataSuccess
    
    func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary)
    {
        AppHelper.hideLoadingView()
        print(dicsResponse)
        if wrapper == objGetHistory {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                arrHistory.removeAllObjects()
                
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray
                {
                    arrHistory = tempNames.mutableCopy() as! NSMutableArray
                    
                }
                print(arrHistory)
                tblPage1.isHidden = false
                btnBookServicePage1.isHidden = true
                tblPage1.reloadData()
                
            }
            else
            {
                //                 tblPage1.isHidden = true
                //                 btnBookServicePage1.isHidden = false
                //AppHelper.showAlert("Alert", message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }
        else if wrapper == objGetScheduled {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                arrScheduled.removeAllObjects()
                
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray
                {
                    arrScheduled = tempNames.mutableCopy() as! NSMutableArray
                }
                print(arrScheduled)
                tblPage2.isHidden = false
                btnBookServicePage2.isHidden = true
                tblPage2.reloadData()
            }
            else
            {
                //                tblPage2.isHidden = true
                //                btnBookServicePage2.isHidden = false
                //AppHelper.showAlert("Alert", message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }
        
        
    }
    // MARK: - HttpWrapperfetchDataFail
    
    func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError)
    {
        AppHelper.hideLoadingView()
        print(error.debugDescription)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
