//
//  AdditionalServicesApply_Vc.swift
//  ServiceMyCar
//
//  Created by baps on 26/02/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit

class AdditionalServicesApply_Vc: UIViewController {
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblPriceTopConstant: NSLayoutConstraint!
    @IBOutlet weak var btnTCApply: UIButtonX!
    @IBOutlet weak var btnBookNow: UIButtonX!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var btnCheck: UIButtonX!
    @IBOutlet weak var totalLabel : UILabel!
    //@IBOutlet weak var viewMainCenter: UIView!
    @IBOutlet weak var txtviewDetails: UITextView!
    var isCheck = false
    var isFlatTyre = false
    var strtitle:String = String()
    var strPrice:String = String()
    var strSerViceDetails:String = String()
    let badgeButton : MIBadgeButton = MIBadgeButton(frame:CGRect(x: -10, y: 15, width: 40, height: 20))
    
    
    var additionalImage:UIImage!
    @IBOutlet weak var serviceImageView:UIImageView!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = strtitle
        lblPrice.text = "\(strPrice) \("app_currency".localized())"
        txtviewDetails.text = strSerViceDetails
        txtviewDetails.textAlignment = .center
        txtviewDetails.isUserInteractionEnabled = false
        self.navigationController?.isNavigationBarHidden = false
        
        self.title = strtitle.localized()
        
        let button = UIButton.init(type: .custom)
        //backarrow , menu
        
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        
        if languageCode == "en" {
            button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
        }
        else {
            button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
        }
        
        button.addTarget(self, action:#selector(SideMenu), for: UIControl.Event.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        if isFlatTyre == true{
            btnCheck.isHidden = false
            btnCheck.setTitle("additional_services_spare_tyre".localized(), for: .normal)
        }else{
            btnCheck.isHidden = true
        }
        badgeButton.edgeInsetTop = 10
        badgeButton.setImage(UIImage(named : "notification"), for: .normal)
        print(appDelegate.NCount)
        badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        badgeButton.badgeString = appDelegate.NCount
        if appDelegate.NCount == "0"{
            badgeButton.badgeBackgroundColor = UIColor.clear
            badgeButton.badgeTextColor = UIColor.clear
        }else{
            badgeButton.badgeBackgroundColor = colorGreen
            badgeButton.badgeTextColor = UIColor.white
        }
        
         badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        let barButton2 : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton2
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)
        
        btnBookNow.setTitle("additional_services_book_now".localized(), for: .normal)
        btnTCApply.setTitle("additional_services_T_and_C_apply".localized(), for: .normal)
        totalLabel.text = "Total : ".localized()
        btnCheck.setTitle("I have a spare tyre in my Car".localized(), for: .normal)
        self.serviceImageView.image = additionalImage
        
    }
    
    
    // MARK: - Google Api Calling
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            //self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                
                badgeButton.badgeString = appDelegate.NCount
                if appDelegate.NCount == "0"
                {
                    badgeButton.badgeBackgroundColor = UIColor.clear
                    badgeButton.badgeTextColor = UIColor.clear
                }
                else
                {
                    badgeButton.badgeBackgroundColor = colorGreen
                    badgeButton.badgeTextColor = UIColor.white
                }
                
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        navigationController?.navigationBar.clipsToBounds = false
        self.navigationItem.setHidesBackButton(true, animated:true)
        self.navigationController?.makeAdditionalPageTransPrent()
        AppHelper.makeViewCircularWithRespectToHeight(btnBookNow, borderColor: .clear, borderWidth: 1.0)
        
        //resetCountTotalNotification()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.makeAdditionalPageTransPrent()
    }
    
    // MARK:- goBack
    
    @objc func SideMenu()
    {
        appDelegate.loginSuccess()
    }
    
    
    @objc func NotificationAlert()
    {
        guard let vcs = self.navigationController?.viewControllers else{return}
        let inStack = vcs.contains(where: { (vc) -> Bool in
            return vc is MyNotificationList_Vc
        })
        if inStack{
            self.performSegue(withIdentifier: unwindToNotificationController, sender: self)
        }else{
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
//        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
//        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    // MARK: - onClickBtnTCapply
    @IBAction func onClickBtnTCapply(_ sender: UIButton) {
        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "webview_Vc") as! webview_Vc
        nextViewController.isTermsAndContion = true
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    // MARK: - onClickBtnBookNow
    @IBAction func onClickBtnBookNow(_ sender: Any) {
        if isFlatTyre == true {
            if isCheck{
                let nextViewController = objServiceSB.instantiateViewController(withIdentifier: "SelectCarBookingRepairViewController") as! SelectCarBookingRepairViewController
                nextViewController.srtTittle = self.strtitle
                nextViewController.srtPrice = self.strPrice
                nextViewController.additionalImage = self.additionalImage
                self.navigationController?.pushViewController(nextViewController, animated: true)
            }else{
              self.askForSparePart()
            }
            
        }
        else {
            let nextViewController = objServiceSB.instantiateViewController(withIdentifier: "SelectCarBookingRepairViewController") as! SelectCarBookingRepairViewController
            nextViewController.srtTittle = self.strtitle
            nextViewController.srtPrice = self.strPrice
            nextViewController.additionalImage = self.additionalImage
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
    }
    // MARK: - onClickBtnCheck
    @IBAction func onClickBtnCheck(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if isCheck == true{
            isCheck = false
            appDelegate.Bookings.Package.parts = "sParts"
        }else{
            isCheck = true
            appDelegate.Bookings.Package.parts = "uParts"
        }
    }
    
    
    func askForSparePart() {
        let alert = UIAlertController(title: "ServiceMyCar".localized(), message: "We are sorry, we do not provide spare tyres. We can only help you get back on the road, If you have spare tyre.".localized(), preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "OK".localized(), style: .default){(action) in
            alert.dismiss(animated: true, completion: nil)
            
//            let nextViewController = objServiceSB.instantiateViewController(withIdentifier: "SelectCarBookingRepairViewController") as! SelectCarBookingRepairViewController
//            nextViewController.srtTittle = self.strtitle
//            nextViewController.srtPrice = self.strPrice
//            nextViewController.additionalImage = self.additionalImage
//            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
      //  let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel){(action) in
      //      alert.dismiss(animated: true, completion: nil)
      
            // self.onClickCancelButton()
       // }
        alert.addAction(okayAction)
        //alert.addAction(cancelAction)
        let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
        appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
