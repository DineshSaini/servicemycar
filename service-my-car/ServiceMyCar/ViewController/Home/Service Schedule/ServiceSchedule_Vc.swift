//
//  ServiceSchedule_Vc.swift
//  ServiceMyCar
//
//  Created by baps on 12/02/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SJSwiftSideMenuController
class ServiceSchedule_Vc: UIViewController, UITableViewDataSource , UITableViewDelegate ,HttpWrapperDelegate{
   
   
    var objGetScheduleService = HttpWrapper()
    
    var net:NetworkReachabilityManager = NetworkReachabilityManager()!
     let badgeButton : MIBadgeButton = MIBadgeButton(frame:CGRect(x: -10, y: 15, width: 40, height: 20))
    var arrScheduled:NSMutableArray = NSMutableArray()
    @IBOutlet weak var tblMain: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        let lbNavTitle = UILabel(frame: CGRect(x: CGFloat(35), y:self.view.bounds.size.width/2-235, width: CGFloat(200), height: CGFloat(40)))
        lbNavTitle.textAlignment = .center
        lbNavTitle.text = "service_schedule_title".localized()
        lbNavTitle.textColor = Colorblack
        lbNavTitle.font = UIFont(name: Regular, size: 16)
        
        //        let logo = UIImage(named: "AppTopIcon")
        //        let imageView = UIImageView(image:logo)
        //        imageView.contentMode = .scaleAspectFit
        //        self.navigationItem.titleView = imageView
        self.navigationItem.titleView = lbNavTitle
        self.navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.white
        
        let titleDict: NSDictionary = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [NSAttributedString.Key : Any]
        
        let button = UIButton.init(type: .custom)
        //backarrow , menu
        
        if appDelegate.isFromNotification == true
        {
            button.setImage(UIImage.init(named: "menu"), for: UIControl.State.normal)
        }
        else
        {
            let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
            
            if languageCode == "en" {
                button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
            }
            else {
                button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
            }
        }
        
        button.addTarget(self, action:#selector(SideMenu), for: UIControl.Event.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        //        self.tblPage1.register(HistoryDatatable_Cell.self, forCellReuseIdentifier: "HistoryDatatable_Cell")
        //        self.tblPage2.register(HistoryDatatable_Cell.self, forCellReuseIdentifier: "HistoryDatatable_Cell")
        
        
       
        badgeButton.edgeInsetTop = 10
        badgeButton.setImage(UIImage(named : "notification"), for: .normal)
         badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        print(appDelegate.NCount)
        badgeButton.badgeString = appDelegate.NCount
        if appDelegate.NCount == "0"
        {
            badgeButton.badgeBackgroundColor = UIColor.clear
            badgeButton.badgeTextColor = UIColor.clear
        }
        else
        {
            badgeButton.badgeBackgroundColor = colorGreen
            badgeButton.badgeTextColor = UIColor.white
        }
        badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        let barButton2 : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton2
        getUserGetScheduleService()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)
    }
    
    
    // MARK: - Google Api Calling
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            //self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                
                badgeButton.badgeString = appDelegate.NCount
                if appDelegate.NCount == "0"
                {
                    badgeButton.badgeBackgroundColor = UIColor.clear
                    badgeButton.badgeTextColor = UIColor.clear
                }
                else
                {
                    badgeButton.badgeBackgroundColor = colorGreen
                    badgeButton.badgeTextColor = UIColor.white
                }
            }
        }
    }
    
    // MARK:- goBack
    
    @objc func SideMenu()
    {
        if appDelegate.isFromNotification == true
        {
             SJSwiftSideMenuController.toggleLeftSideMenu()
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    @objc func NotificationAlert()
    {
        guard let vcs = self.navigationController?.viewControllers else{return}
        let inStack = vcs.contains(where: { (vc) -> Bool in
            return vc is MyNotificationList_Vc
        })
        if inStack{
            self.performSegue(withIdentifier: unwindToNotificationController, sender: self)
        }else{
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        //        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
        //
        //        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    // MARK: - onClickDeliveryReport
    @IBAction func onClickDeliveryReport(_ sender:UIButton!) {
        // do cool stuff here
        
        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "webview_Vc") as! webview_Vc
        if let varCar = (arrScheduled.object(at: sender.tag) as! NSDictionary).value(forKey: "varCar") as? String ,let varModel = (arrScheduled.object(at: sender.tag) as! NSDictionary).value(forKey: "varModel") as? String
        {
            nextViewController.strName  = varCar + " " + varModel
        }
        
        if let name = (arrScheduled.object(at: sender.tag) as! NSDictionary).value(forKey: "servicePdf") as? String
        {
            nextViewController.URL  = name
        }
        
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    
     // MARK: - tableview Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrScheduled.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryDatatable_Cell" , for: indexPath) as! HistoryDatatable_Cell
        
      
        
        if let name = (arrScheduled.object(at: indexPath.row) as! NSDictionary).value(forKey: "varPackageDate") as? String
        {
            cell.lblDate.text  = name
        }
        
        if let name = (arrScheduled.object(at: indexPath.row) as! NSDictionary).value(forKey: "varYear") as? String
        {
            cell.lblYear.text  = name
        }
        
        cell.btnView.tag = indexPath.row
        cell.btnView.addTarget(self, action: #selector(self.onClickDeliveryReport(_:)), for: .touchUpInside)
        
        if let varCar = (arrScheduled.object(at: indexPath.row) as! NSDictionary).value(forKey: "varCar") as? String ,let varModel = (arrScheduled.object(at: indexPath.row) as! NSDictionary).value(forKey: "varModel") as? String
        {
            cell.lblCarName.text  = varCar + " " + varModel
        }
        
        cell.btnView.setTitle("service_view".localized(), for: .normal)
        
        return cell
    }
    
    
    // MARK: - getUserGetScheduleService API
    
    func getUserGetScheduleService()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["userId" : nUserId as AnyObject]
            //            let dictHeaaderPera:[String:AnyObject] = [
            //                "platform" : "IOS" as AnyObject,
            //                "modelNumber" : "ML2000" as AnyObject
            //                ,"serialNumber" :"1234567891" as AnyObject,
            //                 "deviceId" : "12345" as AnyObject]
            //
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objGetScheduleService = HttpWrapper.init()
            self.objGetScheduleService.delegate = self
            //self.objGetScheduleService.requestWithparamdictParamPostMethod(url: kgetServiceSchedule, dicsParams: dictParameters)
            self.objGetScheduleService.requestWithparamdictParamPostMethodwithHeaderAndDictGet(url: kgetServiceSchedule, dictParams: dictParameters, headers: header)
            //self.objGetScheduleService.requestWithparamdictParamPostMethodwithHeader(url: kgetServiceSchedule, dicsParams: dictParameters, headers: header)
        }
        
    }
    
    
    // MARK: - HttpWrapperfetchDataSuccess
    
    func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary)
    {
        AppHelper.hideLoadingView()
        print(dicsResponse)
       if wrapper == objGetScheduleService {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                arrScheduled.removeAllObjects()
                
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray
                {
                    arrScheduled = tempNames.mutableCopy() as! NSMutableArray
                }
                print(arrScheduled)
              
                tblMain.reloadData()
            }
            else
            {
                tblMain.isHidden = true
               
               // AppHelper.showAlert("Alert", message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }
        
        
    }
    // MARK: - HttpWrapperfetchDataFail
    
    func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError)
    {
        AppHelper.hideLoadingView()
        print(error.debugDescription)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
