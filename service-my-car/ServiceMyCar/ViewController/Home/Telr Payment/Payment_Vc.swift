//
//  Payment_Vc.swift
//  ServiceMyCar
//
//  Created by baps on 31/01/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import TelrSDK
import Alamofire
import SwiftyJSON

class Payment_Vc: UIViewController  , UICollectionViewDataSource , UICollectionViewDelegate ,UICollectionViewDelegateFlowLayout ,HttpWrapperDelegate{
    

    @IBOutlet weak var pageController: UIPageControl!
    let KEY:String = "9Std-fqjHP#Q6dW2"  // TODO fill key
    let STOREID:String = "20796"         // TODO fill store id
    var EMAIL:String = ""//isLive ? "accounts@servicemycar.ae" : "Accounts@servicemycar.ae"  // TODO fill email
    var couponId:String = String()
    var paymentRequest:PaymentRequest?
    let UserLName = UserDefaults.standard.value(forKey: kUserName) as! String
    let UserEmail = UserDefaults.standard.value(forKey: KUserLoginEmail) as! String
    let UserPhoneNo = UserDefaults.standard.value(forKey: kUserLoginPhone) as! String
    //var show:Bool = false;
    var objGetCardList = HttpWrapper()
    var objapplyPromoCode = HttpWrapper()
    var objRemovePromoCode = HttpWrapper()
    var net:NetworkReachabilityManager = NetworkReachabilityManager()!
    var arrCardList:NSMutableArray = NSMutableArray()
    var arrScheduled:NSMutableArray = NSMutableArray()
    var isFrom = ""
    let badgeButton : MIBadgeButton = MIBadgeButton(frame:CGRect(x: -10, y: 15, width: 40, height: 20))
    
    @IBOutlet weak var CardCollectionview: UICollectionView!
    @IBOutlet weak var btnConfirm: UIButtonX!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var btnApplyPromo: UIButtonX!
    @IBOutlet weak var txtpromocode: UITextField!
    @IBOutlet weak var codeViewContainer: UIView!

    @IBOutlet weak var btnAddCard: UIButtonX!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblAddCard: UILabel!
    var isApply = false
    
    var orderPriceAmount = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        AppHelper.makeViewCircularWithRespectToHeight(btnConfirm, borderColor: .clear, borderWidth: 0.0)
        AppHelper.makeViewCircularWithRespectToHeight(btnApplyPromo, borderColor: .lightGray, borderWidth: 1.0)
        AppHelper.makeViewCircularWithRespectToHeight(codeViewContainer, borderColor: .lightGray, borderWidth: 1.0)
        if let UserEmail = UserDefaults.standard.value(forKey: KUserLoginEmail) as? String{
            self.EMAIL = UserEmail
        }

                let lbNavTitle = UILabel(frame: CGRect(x: CGFloat(35), y:self.view.bounds.size.width/2-235, width: CGFloat(200), height: CGFloat(40)))
                lbNavTitle.textAlignment = .center
                lbNavTitle.text = "Payment"
                lbNavTitle.textColor = Colorblack
                lbNavTitle.font = UIFont(name: Regular, size: 16)
        
//        let logo = UIImage(named: "AppTopIcon")
//        let imageView = UIImageView(image:logo)
//        imageView.contentMode = .scaleAspectFit
//        self.navigationItem.titleView = imageView
        self.navigationItem.titleView = lbNavTitle
        self.navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.white
        
        let titleDict: NSDictionary = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [NSAttributedString.Key : Any]
        
        let button = UIButton.init(type: .custom)
        //backarrow , menu
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        
        if languageCode == "en" {
            button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
        }
        else {
            button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
        }
        button.addTarget(self, action:#selector(SideMenu), for: UIControl.Event.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
//        let buttonLogout = UIButton.init(type: .custom)
//        buttonLogout.setImage(UIImage.init(named: "notification"), for: UIControl.State.normal)
//        buttonLogout.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
//        buttonLogout.frame = CGRect.init(x: self.view.frame.size.width-30, y: 0, width: 25, height: 25)
//        // buttonLogout.titleLabel!.font =  UIFont(name:FontSemibold, size: 14)
//        let barButtonLog = UIBarButtonItem.init(customView: buttonLogout)
//        // self.navigationItem.rightBarButtonItem = barButtonLog
//

        
        //        if let value = DataDic.value(forKey: "varSpecification") as? NSArray
        //        {
        //            arrService = value.mutableCopy() as! NSMutableArray
        //        }
        
        if isFrom == "3" || isFrom == "4" {
            let strVat = "5"
            
            if strVat != "" {
                let vat2 = Float(appDelegate.Bookings.Price.total)! * Float(strVat)!/100
                let Total = Float(appDelegate.Bookings.Price.total)! + vat2
                lblPrice.text = "\(Total) \("app_currency".localized())"
            }
        }else if isFrom == "ConfrimBookingServiceViewController"{
            lblPrice.text = "\(orderPriceAmount) \("app_currency".localized())"
        }else if isFrom == "SelectYourCarBookingViewController"{
            lblPrice.text = "\(orderPriceAmount) \("app_currency".localized())"
        }
        else {
            lblPrice.text = "\(appDelegate.Bookings.Price.total) \("app_currency".localized())"
        }
        
        appDelegate.Bookings.couponId = "0"
        GetCardList()
        badgeButton.edgeInsetTop = 10
        badgeButton.setImage(UIImage(named : "notification"), for: .normal)
        print(appDelegate.NCount)
         badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        badgeButton.badgeString = appDelegate.NCount
        if appDelegate.NCount == "0"
        {
            badgeButton.badgeBackgroundColor = UIColor.clear
            badgeButton.badgeTextColor = UIColor.clear
        }
        else
        {
            badgeButton.badgeBackgroundColor = colorGreen
            badgeButton.badgeTextColor = UIColor.white
        }
        badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        let barButton2 : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton2
        //        getPackagesListByCategory()
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)
        txtpromocode.placeholder = "payment_have_a_promocode".localized()
        lblTotal.text = "payment_total".localized()
        lblAddCard.text = "payment_add_card".localized()
        btnConfirm.setTitle("payment_confirm".localized(), for: .normal)
        btnApplyPromo.setTitle("payment_apply".localized(), for: .normal)
        
        
        
        
    }
    
    // Step 3
    
    @objc func NotificationAlert()
    {
        guard let vcs = self.navigationController?.viewControllers else{return}
        let inStack = vcs.contains(where: { (vc) -> Bool in
            return vc is MyNotificationList_Vc
        })
        if inStack{
            self.performSegue(withIdentifier: unwindToNotificationController, sender: self)
        }else{
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        //        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
        //
        //        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    // Step 4
    
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            //self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                
                badgeButton.badgeString = appDelegate.NCount
                if appDelegate.NCount == "0"
                {
                    badgeButton.badgeBackgroundColor = UIColor.clear
                    badgeButton.badgeTextColor = UIColor.clear
                }
                else
                {
                    badgeButton.badgeBackgroundColor = colorGreen
                    badgeButton.badgeTextColor = UIColor.white
                }
            }
        }
    }
    
    // MARK:- goBack
    
    @objc func SideMenu()
    {
       self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickBtnAddCard(_ sender: Any) {
        AppHelper.showAlert("app_alert_title".localized(), message:"Functionality Pending!")
        return
        let nextViewController = objMain.instantiateViewController(withIdentifier: "AddCard_Vc") as! AddCard_Vc
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @IBAction func onClickBtnApplyPromo(_ sender: Any) {
        
        if isApply == true
        {
           RemovePromocode()
        }
        else
        {
            ApplyPromoCode()
        }
        
    }
    
    @IBAction func onClickBtnConfirm(_ sender: Any) {
        
        paymentRequest = preparePaymentRequest()
        performSegue(withIdentifier: "TelrSegue", sender: paymentRequest)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrCardList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Card_Cell", for: indexPath) as! Card_Cell
        
        if let name = (arrCardList.object(at: indexPath.row) as! NSDictionary).value(forKey: "varCardHolderName") as? String
        {
            cell.lblName.text  = name.uppercased()
        }
        
        if let name = (arrCardList.object(at: indexPath.row) as! NSDictionary).value(forKey: "varCardNumber") as? String
        {
            cell.lblCardNo.text  = name
        }
        
        if let name = (arrCardList.object(at: indexPath.row) as! NSDictionary).value(forKey: "dtCreated") as? String
        {
            
            let date = name.split(separator: "-")
            cell.lblValidDate.text = "\(date[1])/\(date[0])"
            print("EXACT_DATE : \(date)")
        }
        
       
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
        let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
        let size:CGFloat = (collectionView.frame.size.width - space)
        return CGSize(width: size, height: collectionView.frame.size.height - space)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageController.currentPage = indexPath.row
    }
   
    // MARK: - objGetCardList API
    
    func GetCardList()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["intUserId" : nUserId as AnyObject]
            //            let dictHeaaderPera:[String:AnyObject] = [
            //                "platform" : "IOS" as AnyObject,
            //                "modelNumber" : "ML2000" as AnyObject
            //                ,"serialNumber" :"1234567891" as AnyObject,
            //                 "deviceId" : "12345" as AnyObject]
            //
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objGetCardList = HttpWrapper.init()
            self.objGetCardList.delegate = self
            //self.objGetCardList.requestWithparamdictParamPostMethod(url: klistCards, dicsParams: dictParameters)
            self.objGetCardList.requestWithparamdictParamPostMethodwithHeaderAndDictGet(url: klistCards, dictParams: dictParameters, headers: header)
        }
        
    }
    
     // MARK: - ApplyPromoCode API
    func ApplyPromoCode()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else if isLoginDataValid()
        {
           
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["coupon" : txtpromocode.text as AnyObject,
                                                     "packageId" : appDelegate.Bookings.Package.Id as AnyObject,
                                                     "carModel" : "" as AnyObject]
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objapplyPromoCode = HttpWrapper.init()
            self.objapplyPromoCode.delegate = self
            //self.objapplyPromoCode.requestWithparamdictParamPostMethodwithHeaderAndDictGet(url: kapplyCoupons, dictParams: dictParameters, headers: header)
            self.objapplyPromoCode.requestWithparamdictParamPostMethodwithHeader(url: kapplyCoupons, dicsParams: dictParameters, headers: header)

        }
        
    }
    // MARK: - RemovePromocode API
    
    func RemovePromocode()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["couponId" : couponId as AnyObject]
            //            let dictHeaaderPera:[String:AnyObject] = [
            //                "platform" : "IOS" as AnyObject,
            //                "modelNumber" : "ML2000" as AnyObject
            //                ,"serialNumber" :"1234567891" as AnyObject,
            //                 "deviceId" : "12345" as AnyObject]
            //
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objRemovePromoCode = HttpWrapper.init()
            self.objRemovePromoCode.delegate = self
            self.objRemovePromoCode.requestWithparamdictParamPostMethodwithHeader(url: kremoveCoupon, dicsParams: dictParameters, headers: header)
            //self.objRemovePromoCode.requestWithparamdictParamPostMethod(url: kremoveCoupon, dicsParams: dictParameters)
        }
        
    }
    
    
    // MARK: - HttpWrapperfetchDataSuccess
    
    func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary)
    {
        AppHelper.hideLoadingView()
        print(dicsResponse)
        if wrapper == objGetCardList {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                arrCardList.removeAllObjects()
                CardCollectionview.isHidden = false
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray
                {
                    arrCardList = tempNames.mutableCopy() as! NSMutableArray
                }
                print(arrCardList)
                pageController.numberOfPages = arrCardList.count
//                tblPage1.isHidden = false
//                btnBookServicePage1.isHidden = true
             CardCollectionview.reloadData()
            }
            else
            {
               CardCollectionview.isHidden = true
                //AppHelper.showAlert("Alert", message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }
        else if wrapper == objapplyPromoCode {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
               

                if let tempNames: NSDictionary = dicsResponse.value(forKey: "data") as? NSDictionary
                {
                   if let discountAmount = tempNames.value(forKey: "discountAmount") as? String
                   {
                    
                    appDelegate.Bookings.discountAmount = discountAmount
                     let Price = Float(appDelegate.Bookings.Price.OderAmount)
                   
                   
                    let strVat = "5"
                    if strVat != ""
                    {
                        let disPrice = Float(discountAmount)
                        let finelPrice = CGFloat(Price!) - CGFloat(disPrice!)
                        let vat2 = Float(finelPrice) * Float(strVat)!/100
                        let Total = Float(finelPrice) + vat2
                        lblPrice.text = "\(Total) \("app_currency".localized())"
                        
                    }
                    
                    
                   }
                    
                    if let discountAmount = tempNames.value(forKey: "couponId") as? String
                    {
                        self.couponId = discountAmount
                        appDelegate.Bookings.couponId = discountAmount
                    }
                   isApply = true
                    btnApplyPromo.setTitle("payment_remove".localized(), for: .normal)
                }
               
            }
            else
            {
//                tblPage2.isHidden = true
//                btnBookServicePage2.isHidden = false
                AppHelper.showAlert("app_alert_title".localized(), message:dicsResponse.value(forKey: "msg") as! String)
            }


        }
        else if wrapper == objRemovePromoCode {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                
                
                if let tempNames: NSDictionary = dicsResponse.value(forKey: "data") as? NSDictionary
                {
                   
                    
                    
                }
                isApply = false
                 btnApplyPromo.setTitle("payment_apply".localized(), for: .normal)
                 lblPrice.text = "\(appDelegate.Bookings.Price.total) AED"
                
            }
            else
            {
                //                tblPage2.isHidden = true
                //                btnBookServicePage2.isHidden = false
                AppHelper.showAlert("app_alert_title".localized(), message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }
//
        
    }
    // MARK: - HttpWrapperfetchDataFail
    
    func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError)
    {
        AppHelper.hideLoadingView()
        print(error.debugDescription)
    }
    // MARK: - isLoginDataValid
    func isLoginDataValid() -> Bool
    {
        
        
        if (txtpromocode.text?.trimmingCharacters(in: CharacterSet.whitespaces).characters.count)! <= 0
        {
            AppHelper.showAlert("app_alert_title".localized(), message: "payment_promocode_validation".localized())
            return false
        }
        
     
        return true
    }
    private func preparePaymentRequest() -> PaymentRequest{
        if let UserEmail = UserDefaults.standard.value(forKey: KUserLoginEmail) as? String{
            self.EMAIL = UserEmail
        }
        let paymentReq = PaymentRequest()
        paymentReq.key = KEY
        paymentReq.store = STOREID
        paymentReq.appId = "123456789"
        paymentReq.appName = "ServiceMyCar"
        paymentReq.appUser = "123456"
        paymentReq.appVersion = "0.0.1"
        paymentReq.transTest = Istesting
        paymentReq.transType = "sale"
        paymentReq.transClass = "paypage"
        paymentReq.transCartid = String(arc4random())
        paymentReq.transDesc = isLive ? "Booking on Servicemycar.ae" : "service payment"
        paymentReq.transCurrency = "AED"
        
        let value = lblPrice.text?.split(separator: " ")
        appDelegate.Bookings.telr_value = String(value![0])
        paymentReq.transAmount = String(value![0])
        paymentReq.transLanguage = "en"
        paymentReq.billingEmail = EMAIL
        
        let name = UserLName.split(separator: " ")
        
        
        paymentReq.billingFName = "\(name[0])"
        paymentReq.billingLName = "\(name[1])"
        paymentReq.billingTitle = "Mr"
        paymentReq.city = "Dubai"
        paymentReq.country = "AE"
        paymentReq.region = "Dubai"
        paymentReq.address = "line 1"
        paymentReq.billingPhone = UserPhoneNo
        
        
        return paymentReq
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? TelrController{
            destination.paymentRequest = paymentRequest!
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
