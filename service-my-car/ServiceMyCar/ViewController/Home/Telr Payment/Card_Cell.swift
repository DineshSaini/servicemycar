//
//  Card_Cell.swift
//  ServiceMyCar
//
//  Created by baps on 14/02/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit

class Card_Cell: UICollectionViewCell {
    @IBOutlet weak var lblCardNo: UILabel!
    
    @IBOutlet weak var lblValidDate: UILabel!
    @IBOutlet weak var lblName: UILabel!
}
