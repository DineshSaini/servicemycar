//
//  ResultController.swift
//  Telr_SDK
//
//  Created by Staff on 5/25/17.
//  Copyright © 2017 Telr. All rights reserved.
//

import UIKit
import TelrSDK
import Alamofire
import SwiftyJSON
import ZAlertView
class ResultController: TelrResponseController ,HttpWrapperDelegate{
    
    
    @IBOutlet weak var lblBookingwithus: UILabel!
    @IBOutlet weak var lblThankyou: UILabel!
    @IBOutlet var statusLabel: UILabel!
    @IBOutlet weak var backButton:UIButton!
    
    
    @IBOutlet weak var invoiceViewContainer:UIView!
    @IBOutlet weak var headingTittleLabel:UILabel!
    @IBOutlet weak var thankulabel:UILabel!
    @IBOutlet weak var forChoosingLabel:UILabel!
    @IBOutlet weak var thanksForPayment:UILabel!
    @IBOutlet weak var backToHomeButton:UIButtonX!



    
    
    @IBOutlet weak var myOdersButton:UIButton!
    
    var objSubmitBooking = HttpWrapper()
    var objOtherSubmitBooking = HttpWrapper()
    var timer: Timer?
    var net:NetworkReachabilityManager = NetworkReachabilityManager()!
    var telr_key:String = String()
    
    let badgeButton : MIBadgeButton = MIBadgeButton(frame:CGRect(x: -10, y: 15, width: 40, height: 20))
    
  
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        //self.setupTabbarView()
        self.navigationController?.setupNavigation()
    }

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        lblBookingwithus.text = "for_booking_with_us".localized()
        lblThankyou.text = "Thank_you".localized()
        
        
        let logo = UIImage(named: "AppTopIcon")
        let imageView = UIImageView(image:logo)
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
        
        statusLabel.isHidden = false
        statusLabel.text = "Booking successfull".localized()
        myOdersButton.setTitle("My Orders".localized(), for: .normal)
        
        print(message,tranRef,trace)
        //statusLabel.text = message!
        if let ref = tranRef{
            telr_key = tranRef!
           // saveData(key: "ref", value: ref)
        }
        if let last4 = cardLast4{
           // saveData(key: "last4", value: last4)
        }
        
//        if appDelegate.isPayExtraInvoice == "2"{
//            self.invoiceViewContainer.isHidden = false
//            self.headingTittleLabel.text = "CONGRATULATION YOUR PAYMENT HAS BEEN RECEIVED".localized()
//            self.thankulabel.text = "THANK YOU!".localized()
//            self.forChoosingLabel.text = "For Choosing".localized()
//            self.thanksForPayment.text = "Thank for you payment".localized()
//            self.backToHomeButton.setTitle("BACK TO HOME".localized(), for: .normal)
//        }else{
//          self.invoiceViewContainer.isHidden = true
//        }
        
        if (message?.contains("successful"))!{

            if appDelegate.isPayExtraInvoice == "2"{
                self.invoiceViewContainer.isHidden = false
                self.headingTittleLabel.text = "CONGRATULATION YOUR PAYMENT HAS BEEN RECEIVED".localized()
                self.thankulabel.text = "THANK YOU!".localized()
                self.forChoosingLabel.text = "For Choosing".localized()
                self.thanksForPayment.text = "Thank for you payment".localized()
                self.backToHomeButton.setTitle("BACK TO HOME".localized(), for: .normal)
                //appDelegate.isPayExtraInvoice = "0"
                statusLabel.isHidden = false
                lblBookingwithus.text = "Our driver will be at your chosen location to collect your car on the date and time you selected".localized()
                payExtraInvoice()
            }else if appDelegate.isPayExtraInvoice == "1"{
                self.invoiceViewContainer.isHidden = true
            lblBookingwithus.text = "Our driver will be at your chosen location to collect your car on the date and time you selected".localized()
                statusLabel.isHidden = false

            statusLabel.text = "Booking successfull".localized()
            SubmitBooking()
            }else if appDelegate.isPayExtraInvoice == "3"{
                statusLabel.isHidden = false
                self.invoiceViewContainer.isHidden = true
                lblBookingwithus.text = "Our driver will be at your chosen location to collect your car on the date and time you selected".localized()
               submitOtherBooking()
            }
        }else{
            self.invoiceViewContainer.isHidden = true
            ShowAlert(Title: "ServiceMyCar".localized(), Msg: message!)
        }
        
        
        badgeButton.edgeInsetTop = 10
        badgeButton.setImage(UIImage(named : "notification"), for: .normal)
        print(appDelegate.NCount)
         badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        badgeButton.badgeString = appDelegate.NCount
        if appDelegate.NCount == "0"
        {
            badgeButton.badgeBackgroundColor = UIColor.clear
            badgeButton.badgeTextColor = UIColor.clear
        }
        else
        {
            badgeButton.badgeBackgroundColor = colorGreen
            badgeButton.badgeTextColor = UIColor.white
        }
        badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        let barButton2 : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton2
        //        getPackagesListByCategory()
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)
        

        
    }
    
    
    override func viewWillLayoutSubviews() {
        AppHelper.makeViewCircularWithRespectToHeight(myOdersButton, borderColor: .clear, borderWidth: 0.0)
        
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        if languageCode == "en" {
            backButton.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
        }
        else {
            backButton.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
        }


    }
    
    // Step 3
    
    @objc func NotificationAlert()
    {
        guard let vcs = self.navigationController?.viewControllers else{return}
        let inStack = vcs.contains(where: { (vc) -> Bool in
            return vc is MyNotificationList_Vc
        })
        if inStack{
            self.performSegue(withIdentifier: unwindToNotificationController, sender: self)
        }else{
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        //        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
        //
        //        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    // Step 4
    
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            //self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                
                badgeButton.badgeString = appDelegate.NCount
                if appDelegate.NCount == "0"
                {
                    badgeButton.badgeBackgroundColor = UIColor.clear
                    badgeButton.badgeTextColor = UIColor.clear
                }
                else
                {
                    badgeButton.badgeBackgroundColor = colorGreen
                    badgeButton.badgeTextColor = UIColor.white
                }
            }
        }
    }
    
    
    // MARK:- goBack
    
    @objc func goBack()
    {
        appDelegate.loginSuccess()
    }
    
    
    
    
    
    func startTimer() {
        timer = Timer.scheduledTimer(timeInterval: 8,
                                     target: self,
                                     selector: #selector(eventWith(timer:)),
                                     userInfo: [ "foo" : "bar" ],
                                     repeats: true)
    }
    
    // Timer expects @objc selector
    @objc func eventWith(timer: Timer!) {
        let info = timer.userInfo as Any
        print(info)
        timer.invalidate()
        appDelegate.loginSuccess()
    }
    // MARK: - Show Toast
    func ShowAlert(Title:String,Msg:String)
    {
      
        ZAlertView.showAnimation            = .bounceBottom
        ZAlertView.hideAnimation            = .bounceRight
        ZAlertView.initialSpringVelocity    = 0.9
        ZAlertView.duration                 = 2
        ZAlertView.titleColor = colorGreen
        
        let dialog = ZAlertView(title: Title, message: Msg, closeButtonText: "OK".localized(), closeButtonHandler: { (alertView) -> () in
            alertView.dismissAlertView()
            appDelegate.loginSuccess()
        })
        
        dialog.width = self.view.frame.width - 40
        dialog.show()
        dialog.allowTouchOutsideToDismiss = true
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
       appDelegate.Bookings = BookingDetails()
      appDelegate.loginSuccess()

    }
    
    @IBAction func onClickMyOrders(_ sender: Any) {
        appDelegate.Bookings = BookingDetails()
        appDelegate.loginSuccess()
        NotificationCenter.default.post(name: .BOOKING_SUCCESS_NOTIFICATION, object: nil, userInfo: ["booking":"success"])

        
    }
    
    
    // MARK: - getUserNewNotification API
    
    func SubmitBooking()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
           
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["intUser" : nUserId as AnyObject,
                                                     "varCar" : appDelegate.Bookings.Car.CarName as AnyObject,
                                                     "varModel" : appDelegate.Bookings.Car.carModel as AnyObject,
                                                     "varYear" : appDelegate.Bookings.Car.carYear as AnyObject,
                                                     "varTrimEngine" : appDelegate.Bookings.Car.varTrimEngine as AnyObject,
                                                     "varPackageDate" : appDelegate.Bookings.Package.varPackageDate as AnyObject,
                                                     "plateCode" : appDelegate.Bookings.Car.varPlateCode as AnyObject,
                                                     "plateNumber" :  appDelegate.Bookings.Car.varPlateNumber as AnyObject,
                                                     "discountAmount" : appDelegate.Bookings.discountAmount as AnyObject,
                                                     "carValet" : appDelegate.Bookings.carValet as AnyObject,
                                                     "carFuel" : appDelegate.Bookings.carFuel as AnyObject,
                                                     "intPackage" : appDelegate.Bookings.Package.Id as AnyObject,
                                                     "varParts" : appDelegate.Bookings.SelectedPart.varParts as AnyObject,
                                                     "carState" : appDelegate.Bookings.Car.varCarState  as AnyObject,
                                                     "varPackagePAddress" : appDelegate.Bookings.DAddress.Pickup.varPackagePAddress as AnyObject,
                                                     "varPackagePLatLong" : "\(appDelegate.Bookings.DAddress.Pickup.Latitude),\(appDelegate.Bookings.DAddress.Pickup.Longitude)" as AnyObject,
                                                     "varBuildingPName" : appDelegate.Bookings.DAddress.Pickup.varBuildingPName as AnyObject,
                                                     "varPackagePState" : appDelegate.Bookings.DAddress.Pickup.varPackagePState as AnyObject,
                                                     "varPackagePCountry" : appDelegate.Bookings.DAddress.Pickup.varPackagePCountry as AnyObject,
                                                     "additionalPInstruction" : appDelegate.Bookings.DAddress.Pickup.additionalPInstruction as AnyObject,
                                                     "additionalDInstruction" : appDelegate.Bookings.DAddress.Dropoff.additionalDInstruction as AnyObject,
                                                     "varPackageDAddress" : appDelegate.Bookings.DAddress.Dropoff.varPackageDAddress as AnyObject,
                                                     "varBuildingDName" : appDelegate.Bookings.DAddress.Dropoff.varBuildingDName as AnyObject,
                                                     "varPackageDState" : appDelegate.Bookings.DAddress.Dropoff.varPackageDState as AnyObject,
                                                     "varPackageDCountry" : appDelegate.Bookings.DAddress.Dropoff.varPackageDCountry as AnyObject,
                                                     "varPackageDLatLong" : "\(appDelegate.Bookings.DAddress.Dropoff.Latitude),\(appDelegate.Bookings.DAddress.Dropoff.Longitude)" as AnyObject,
                                                     "varDriveIn" : nUserId as AnyObject,
                                                     "intCouponId" : appDelegate.Bookings.couponId as AnyObject,
                                                     "packageAmount" : appDelegate.Bookings.Package.PackegePrice as AnyObject,
                                                     "telr_key" : telr_key as AnyObject,
                                                     "telr_value" : appDelegate.Bookings.telr_value as AnyObject,
                                                     "deviceOS" : "IOS" as AnyObject,
                                                     "deviceId" : appDelegate.deviceUDID as AnyObject,
                                                     "ipAddress" : "" as AnyObject]
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objSubmitBooking = HttpWrapper.init()
            self.objSubmitBooking.delegate = self
            self.objSubmitBooking.requestWithparamdictParamPostMethodwithHeader(url: ksubmitBooking, dicsParams: dictParameters, headers: header)
        }
        
    }
    
    func submitOtherBooking() {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["userId" : nUserId as AnyObject,
                                                     "intPackage" : appDelegate.Bookings.Package.Id as AnyObject,
                                                     "varPackageType" : appDelegate.Bookings.Package.type as AnyObject,
                                                     "varCar" : appDelegate.Bookings.Car.CarName as AnyObject,
                                                     "varModel" : appDelegate.Bookings.Car.carModel as AnyObject,
                                                     "varYear" : appDelegate.Bookings.Car.carYear as AnyObject,
                                                     "varPackageDate" : appDelegate.Bookings.Package.varPackageDate as AnyObject,
                                                     "code" :  appDelegate.Bookings.Car.varPlateCode as AnyObject,
                                                     "plateNumber" : appDelegate.Bookings.Car.varPlateNumber as AnyObject,
                                                     "dateType" : appDelegate.Bookings.Package.dateType as AnyObject,
                                                     "varParts" : appDelegate.Bookings.Package.parts as AnyObject,
                                                     "carState" : appDelegate.Bookings.Car.varCarState  as AnyObject,
                                                     "varPackagePAddress" : appDelegate.Bookings.DAddress.Pickup.varPackagePAddress as AnyObject,
                                                     "varPackagePLatLong" : "\(appDelegate.Bookings.DAddress.Pickup.Latitude),\(appDelegate.Bookings.DAddress.Pickup.Longitude)" as AnyObject,
                                                     "varBuildingPName" : appDelegate.Bookings.DAddress.Pickup.varBuildingPName as AnyObject,
                                                     "varPackagePState" : appDelegate.Bookings.DAddress.Pickup.varPackagePState as AnyObject,
                                                     "varPackagePCountry" : appDelegate.Bookings.DAddress.Pickup.varPackagePCountry as AnyObject,
                                                     "additionalPInstruction" : appDelegate.Bookings.DAddress.Pickup.additionalPInstruction as AnyObject,
                                                     "deviceId" : appDelegate.deviceUDID as AnyObject,
                                                     "deviceOS" : "IOS" as AnyObject,
                                                     "telr_key" : telr_key as AnyObject,
                                                     "telr_value" : appDelegate.Bookings.telr_value as AnyObject]
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objOtherSubmitBooking = HttpWrapper.init()
            self.objOtherSubmitBooking.delegate = self
            self.objOtherSubmitBooking.requestWithparamdictParamPostMethodwithHeader(url: ksubmitOtherBooking, dicsParams: dictParameters, headers: header)
            //self.objOtherSubmitBooking.requestWithparamdictParamPostMethod(url: ksubmitOtherBooking, dicsParams: dictParameters)
        }
    }
    
    // MARK: - getUserNewNotification API
    
    func payExtraInvoice()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
           
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["telr_key" : telr_key as AnyObject,
                                                     "telr_value" : appDelegate.Bookings.telr_value as AnyObject,
                                                     "deviceOS" : "IOS" as AnyObject,
                                                     "deviceId" : appDelegate.deviceUDID as AnyObject,
                                                     "orderId" : appDelegate.ExtraInvoiceOderId as AnyObject]
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objSubmitBooking = HttpWrapper.init()
            self.objSubmitBooking.delegate = self
            self.objSubmitBooking.requestWithparamdictParamPostMethodwithHeader(url: kpayExtraInvoice, dicsParams: dictParameters, headers: header)

        }
        
    }
    
    // MARK: - HttpWrapperfetchDataSuccess
    
    func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary)
    {
        AppHelper.hideLoadingView()
        print(dicsResponse)
        if wrapper == objSubmitBooking {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
//                arrNotificaiton.removeAllObjects()
                appDelegate.Bookings = BookingDetails()
                //startTimer()
//
//                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray
//                {
//                    arrNotificaiton = tempNames.mutableCopy() as! NSMutableArray
//                }
//                print(arrNotificaiton)
//
//                tblMain.reloadData()
            }
            else
            {
                //  tblMain.isHidden = true
                
               // AppHelper.showAlert("Alert", message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }
        if wrapper == objOtherSubmitBooking {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                //appDelegate.loginSuccess()
            }
            else {
                AppHelper.showAlert("Alert", message: dicsResponse.value(forKey: "msg") as! String)
            }
        }
        
        
    }
    // MARK: - HttpWrapperfetchDataFail
    
    func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError)
    {
        AppHelper.hideLoadingView()
        print(error.debugDescription)
    }
    private func saveData(key:String, value:String){
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key)
    }

}
