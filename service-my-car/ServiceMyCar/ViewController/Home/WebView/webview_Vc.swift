//
//  webview_Vc.swift
//  ServiceMyCar
//
//  Created by baps on 12/02/19.
//  Copyright © 2019 baps. All rights reserved.
//
import UIKit
import WebKit
import SJSwiftSideMenuController
import Alamofire
import SwiftyJSON
class webview_Vc: UIViewController, WKNavigationDelegate{
    
    
    var webview: WKWebView!
    
    var strName:String = String()
    var URL:String = String()
    var orderId:String = String()
    var isTermsAndContion:Bool = false
    var isNotification:Bool = false
    var fromNotificationView:Bool = false
    var fromInvoiceReceived = false
    let badgeButton : MIBadgeButton = MIBadgeButton(frame:CGRect(x: -10, y: 15, width: 40, height: 20))
    var net:NetworkReachabilityManager = NetworkReachabilityManager()!
    
    var objGetDelivery = HttpWrapper()
    var arrDelivery:NSMutableArray = NSMutableArray()
    var indexPath:Int = -1
    var order = NSDictionary()
    var viewOrder  = NSDictionary()
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setupNavigation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setupNavigation()
        
    }
    override func loadView() {
        webview = WKWebView()
        webview.navigationDelegate = self
        view = webview
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.title = strName.localized()
        badgeButton.edgeInsetTop = 10
        badgeButton.setImage(UIImage(named : "notification"), for: .normal)
        print(appDelegate.NCount)
        badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        badgeButton.badgeString = appDelegate.NCount
        if appDelegate.NCount == "0"
        {
            badgeButton.badgeBackgroundColor = UIColor.clear
            badgeButton.badgeTextColor = UIColor.clear
        }
        else
        {
            badgeButton.badgeBackgroundColor = colorGreen
            badgeButton.badgeTextColor = UIColor.white
        }
        badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        let barButton2 : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton2
        //        getPackagesListByCategory()
       // webview.isOpaque = false
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)
        
        if isTermsAndContion{
            self.title = "Terms & Conditions".localized()
            let url = NSURL(string: "https://servicemycar.ae/smc/terms-and-conditions")
            let requestObj = URLRequest(url: url! as URL)
            webview.load(requestObj)
            AppHelper.showLoadingView()

        }else if isNotification{
            
            if strName == "HEALTH_CHECK_VIDEO"{
                //https://www.youtube.com/watch?v=
                let videoUrl = "https://www.youtube.com/embed/"+self.URL
                let url = NSURL(string:videoUrl)
                let requestObj = URLRequest(url: url! as URL)
                webview.load(requestObj)
                AppHelper.showLoadingView()

            }else{
                let url = NSURL(string: self.URL)
                let requestObj = URLRequest(url: url! as URL)
                webview.load(requestObj)
                AppHelper.showLoadingView()

            }
            
        }else if fromNotificationView{
            var viewUrl = ""
            if "HEALTH_CHECK_VIDEO" == viewOrder.value(forKey: "varMsgType") as? String{
                if let url =  viewOrder.value(forKey: "url") as? String{
                  viewUrl = "https://www.youtube.com/embed/"+url
                }
            }else{
                if let url =  viewOrder.value(forKey: "url") as? String{
                    viewUrl = url
                }
            }
            let url = NSURL(string: "\(viewUrl)")
            let requestObj = URLRequest(url: url! as URL)
            webview.load(requestObj)
            AppHelper.showLoadingView()

        }else{
        self.getUserCollectionReports()
        }
        
        
        let button = UIButton.init(type: .custom)
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        
        if languageCode == "en" {
            button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
        }
        else {
            button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
        }
        button.addTarget(self, action:#selector(onClickBackButton(_ :)), for: UIControl.Event.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton

        
    }
    
    // Step 3
    
    @objc func NotificationAlert()
    {
        guard let vcs = self.navigationController?.viewControllers else{return}
        let inStack = vcs.contains(where: { (vc) -> Bool in
            return vc is MyNotificationList_Vc
        })
        if inStack{
            self.performSegue(withIdentifier: unwindToNotificationController, sender: self)
        }else{
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        //        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
        //
        //        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    // Step 4
    
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            //self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                
                badgeButton.badgeString = appDelegate.NCount
                if appDelegate.NCount == "0"
                {
                    badgeButton.badgeBackgroundColor = UIColor.clear
                    badgeButton.badgeTextColor = UIColor.clear
                }
                else
                {
                    badgeButton.badgeBackgroundColor = colorGreen
                    badgeButton.badgeTextColor = UIColor.white
                }
            }
        }
    }
    
    
    
    @IBAction func onClickBackButton(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        hidePageNumberView(v: scrollView)
    }
    
    func hidePageNumberView(v: UIView) {
        for subView in v.subviews {
            if subView.isKind(of: UIImageView.self) || subView.isKind(of: UILabel.self) || subView.isKind(of: UIVisualEffectView.self){
                subView.isHidden = true
                
                if subView.isKind(of: UILabel.self) {
                    if let sv = subView.superview {
                        sv.isHidden = true
                    }
                }
            } else {
                hidePageNumberView(v: subView)
            }
        }
    }
    
    
    
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        // Check if a link was clicked
        if navigationAction.navigationType == .linkActivated {
            
            // Verify the url
            guard let url = navigationAction.request.url else { return }
            let shared = UIApplication.shared
            
            // Check if opening in Safari is allowd
            if shared.canOpenURL(url) {
                
                // Ask the user if they would like to open link in Safari
                let alert = UIAlertController(title: "Do you want to open Safari?", message: nil, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (alert: UIAlertAction) -> Void in
                    // User wants to open in Safari
                    shared.open(url, options: [:], completionHandler: nil)
                }))
                alert.addAction(UIAlertAction(title: "Opps, no.", style: .cancel, handler: nil))
                
                present(alert, animated: true, completion: nil)
                
            }
            decisionHandler(.cancel)
        }
        decisionHandler(.allow)
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        AppHelper.hideLoadingView()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        AppHelper.hideLoadingView()

    }
//    private func webViewDidStartLoad(_ : WKWebView) {
//        AppHelper.showLoadingView()
//    }
//
//    private func webViewDidFinishLoad(_ : WKWebView) {
//        AppHelper.hideLoadingView()
//    }
    
    
    
    
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    func getUserCollectionReports(){
        self.setUserDataAndURl()
      /*  var apiUrlString:String = ""
        switch indexPath {
        case 0:
            apiUrlString = kgetCollectionReport
        case 1:
            apiUrlString = kgetDeliveryReport
        case 2:
            apiUrlString = kgetServiceSchedule
        case 3:
            
            break
        case 4:
            apiUrlString = kgetHealthCheckVideo
        case 5:
            apiUrlString = kgetInvoice
        default:
            break
        }
        
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            var odrID = ""
            if let orderID = order.value(forKey: "intId") as? String{
               odrID = orderID
            }else if let orderID = order.value(forKey: "intId") as? NSNumber{
                odrID = "\(orderID)"
            }
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["orderId" : odrID as AnyObject]
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objGetDelivery = HttpWrapper.init()
            self.objGetDelivery.delegate = self
            self.objGetDelivery.requestWithparamdictParamPostMethodwithHeaderAndDictGet(url: apiUrlString, dictParams: dictParameters, headers: header)
        }*/
        
    }
    
    
    
    func setUserDataAndURl() {
        if fromInvoiceReceived{
            if let tittleName = viewOrder.value(forKey: "title") as? String{
                strName = tittleName
                self.title = tittleName.localized()
            }
        }else{
        if let varCar = order.value(forKey: "varCar") as? String ,let varModel = order.value(forKey: "varModel") as? String{
            strName  = varCar + " " + varModel
            self.title = strName.lowercased()
            }
            
        }
        
            if let name = viewOrder.value(forKey: "collectionPdf") as? String{
                self.loadWebWiew(url: name)
            }else if let name = viewOrder.value(forKey: "deliveryPdf") as? String{
                self.loadWebWiew(url: name)
            }else if let name = viewOrder.value(forKey: "servicePdf") as? String{
                self.loadWebWiew(url: name)
            }else if let name = viewOrder.value(forKey: "link") as? String{
                self.loadWebWiew(url: "https://www.youtube.com/embed/\(name)")
            }else if let name = viewOrder.value(forKey: "invoicePdf") as? String{
                self.loadWebWiew(url: name)
            }else if let name = viewOrder.value(forKey: "extraPartsPdf") as? String{
                self.loadWebWiew(url: name)
        }
    }
    
    
    func loadWebWiew(url:String)  {
        let url = NSURL(string: url)
        let requestObj = URLRequest(url: url! as URL)
        webview.load(requestObj)
    }
    
    
}
extension webview_Vc:HttpWrapperDelegate {
    func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary) {
        AppHelper.hideLoadingView()
        print(dicsResponse)
        if wrapper == objGetDelivery {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray
                {
                    arrDelivery = tempNames.mutableCopy() as! NSMutableArray
                    self.setUserDataAndURl()
                }
                print(arrDelivery)
            }
            else
            {
                
                AppHelper.showAlert("Alert", message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }
    }
    
    func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError) {
        AppHelper.hideLoadingView()
        print(error.debugDescription)
    }
    
    
    
}
