//
//  CarRepairQuote_Vc.swift
//  ServiceMyCar
//
//  Created by baps on 25/02/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import ZAlertView
class CarRepairQuote_Vc: UIViewController,UICollectionViewDelegate , UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,HttpWrapperDelegate {

    @IBOutlet weak var btnCamera: UIButtonX!
    @IBOutlet weak var btnSend: UIButtonX!
    @IBOutlet weak var collectionviewPhoto: UICollectionView!
    @IBOutlet weak var txtviewQuate: UITextView!
    @IBOutlet weak var lblFaultWithTheCar: UILabel!
    
    var objpostQuoteRequest = HttpWrapper()

    var arrImageList:[UIImage] =  [UIImage]()
    var net:NetworkReachabilityManager = NetworkReachabilityManager()!
    let badgeButton : MIBadgeButton = MIBadgeButton(frame:CGRect(x: -10, y: 15, width: 40, height: 20))
    var arrNotificaiton:NSMutableArray = NSMutableArray()
    var imageData = NSData ()
    var imagePicker = UIImagePickerController()
    @IBOutlet weak var tblMain: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtviewQuate.text = ""
        
        self.navigationController?.isNavigationBarHidden = false
        let lbNavTitle = UILabel(frame: CGRect(x: CGFloat(35), y:self.view.bounds.size.width/2-235, width: CGFloat(200), height: CGFloat(40)))
        lbNavTitle.textAlignment = .center
        lbNavTitle.text = "car_repair_quoats_title".localized()
        lbNavTitle.textColor = Colorblack
        lbNavTitle.font = UIFont(name: FontMedium, size: 16)
        
        //        let logo = UIImage(named: "AppTopIcon")
        //        let imageView = UIImageView(image:logo)
        //        imageView.contentMode = .scaleAspectFit
        //        self.navigationItem.titleView = imageView
        self.navigationItem.titleView = lbNavTitle
        self.navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.white
        
        let titleDict: NSDictionary = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [NSAttributedString.Key : Any]
        
        let button = UIButton.init(type: .custom)
        //backarrow , menu
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        
        if languageCode == "en" {
            button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
        }
        else {
            txtviewQuate.textAlignment = .right
            button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
        }
        button.addTarget(self, action:#selector(SideMenu), for: UIControl.Event.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        //        self.tblPage1.register(HistoryDatatable_Cell.self, forCellReuseIdentifier: "HistoryDatatable_Cell")
        //        self.tblPage2.register(HistoryDatatable_Cell.self, forCellReuseIdentifier: "HistoryDatatable_Cell")
        
        
        
        badgeButton.edgeInsetTop = 10
        badgeButton.setImage(UIImage(named : "notification"), for: .normal)
        print(appDelegate.NCount)
        badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        badgeButton.badgeString = appDelegate.NCount
        if appDelegate.NCount == "0"
        {
            badgeButton.badgeBackgroundColor = UIColor.clear
            badgeButton.badgeTextColor = UIColor.clear
        }
        else
        {
            badgeButton.badgeBackgroundColor = colorGreen
            badgeButton.badgeTextColor = UIColor.white
        }
        
         badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        let barButton2 : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton2
        
        //        let buttonLogout = UIButton.init(type: .custom)
        //        buttonLogout.setImage(UIImage.init(named: "notification"), for: UIControl.State.normal)
        //        badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        //        buttonLogout.frame = CGRect.init(x: self.view.frame.size.width-30, y: 0, width: 25, height: 25)
        //        // buttonLogout.titleLabel!.font =  UIFont(name:FontSemibold, size: 14)
        //        let barButtonLog = UIBarButtonItem.init(customView: buttonLogout)
        //        self.navigationItem.rightBarButtonItem = barButtonLog
        //
        //        let count = appDelegate.remoteLogin
        //        print(count)
        //getUserNewNotification()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)
        btnSend.setTitle("Send".localized(), for: .normal)
        lblFaultWithTheCar.text = "Fault with the car".localized()
    }
    
    
    // MARK: - Google Api Calling
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            //self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                
                badgeButton.badgeString = appDelegate.NCount
                if appDelegate.NCount == "0"
                {
                    badgeButton.badgeBackgroundColor = UIColor.clear
                    badgeButton.badgeTextColor = UIColor.clear
                }
                else
                {
                    badgeButton.badgeBackgroundColor = colorGreen
                    badgeButton.badgeTextColor = UIColor.white
                }
                
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
        //resetCountTotalNotification()
    }
    // MARK:- goBack
    
    @objc func SideMenu()
    {
        appDelegate.loginSuccess()
    }
    
    @IBAction func onClickBtnSend(_ sender: Any) {
        
        if isLoginDataValid()
        {
         postQuoteRequest()
        }
        
    }
    
    @IBAction func onClickBtnCamera(_ sender: UIButton) {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: NSLocalizedString("CAMERA".localized(), comment: ""), style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("GALLERY".localized(), comment: ""), style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: NSLocalizedString("CANCEL".localized(), comment: ""), style: .cancel, handler: nil))
        
        //If you want work actionsheet on ipad then you have to use popoverPresentationController to present the actionsheet, otherwise app will crash in iPad
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender
            alert.popoverPresentationController?.sourceRect = sender.bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func NotificationAlert()
    {
        guard let vcs = self.navigationController?.viewControllers else{return}
        let inStack = vcs.contains(where: { (vc) -> Bool in
            return vc is MyNotificationList_Vc
        })
        if inStack{
            self.performSegue(withIdentifier: unwindToNotificationController, sender: self)
        }else{
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
//        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
//        
//        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    // MARK:- CollectionView Methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      
            return arrImageList.count
       
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Ouatephoto_Cell", for: indexPath) as! Ouatephoto_Cell
       
        
        if let imageData = arrImageList[indexPath.row] as? UIImage
        {
            
            cell.imgCar.image = imageData
        }
        cell.btnDelete.addTarget(self, action: #selector(onCLickImageDeleteButton(_:)), for: .touchUpInside)
//        if let UserProfilePic = (arrService.object(at: indexPath.row) as! NSDictionary).value(forKey: "varImage") as? String
//        {
//            let url = "\(UserProfilePic)"
//            print(url)
//
//            if let url2 = URL(string: url) {
//                cell.imgCar.sd_setImage(with: url2, placeholderImage: UIImage(named: "img_plan1"))
//            }
//        }
        
        return cell
   
        
    }
    
    
     @IBAction func onCLickImageDeleteButton(_ sender: UIButton){
       if  let indexPath = sender.collectionViewIndexPath(self.collectionviewPhoto) as IndexPath? {
        if arrImageList.count != 0 {
        self.arrImageList.remove(at: indexPath.item)
            self.collectionviewPhoto.reloadData()
        }
            
        }
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
      
        let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
        let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
        let size:CGFloat = (collectionView.frame.size.width - space - 15) / 3.0
        return CGSize(width: size, height: size)
    }
    
    
    // MARK: - Show Toast
    func ShowAlert(Title:String,Msg:String)
    {
        //        ZAlertView.positiveColor            = UIColor.color("#669999")
        //        ZAlertView.negativeColor            = UIColor.color("#CC3333")
        //        ZAlertView.blurredBackground        = true
        ZAlertView.showAnimation            = .bounceBottom
        ZAlertView.hideAnimation            = .bounceRight
        ZAlertView.initialSpringVelocity    = 0.9
        ZAlertView.duration                 = 2
        ZAlertView.titleColor = colorGreen
        
        let dialog = ZAlertView(title: Title, message: Msg, closeButtonText: "OK".localized(), closeButtonHandler: { (alertView) -> () in
            alertView.dismissAlertView()
        })
        
        dialog.width = self.view.frame.width - 40
        dialog.show()
        dialog.allowTouchOutsideToDismiss = true
    }
    
    // MARK: - postQuoteRequest API
    
    func postQuoteRequest()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let UserLName = UserDefaults.standard.value(forKey: kUserName) as! String
            let UserEmail = UserDefaults.standard.value(forKey: KUserLoginEmail) as! String
            let UserPhoneNo = UserDefaults.standard.value(forKey: kUserLoginPhone) as! String
            let dictParameters:[String:AnyObject] = ["name" : UserLName as AnyObject,
                                                     "userId" : nUserId as AnyObject,
                                                     "number" : UserPhoneNo as AnyObject,
                                                     "fault" : txtviewQuate.text as AnyObject,
                                                     "varYear" : appDelegate.CarRepair.Car.carYear as AnyObject,
                                                     "varCar" : appDelegate.CarRepair.Car.CarName as AnyObject,
                                                     "varModel" : appDelegate.CarRepair.Car.carModel as AnyObject,
                                                     "varTrimEngine" : appDelegate.CarRepair.Car.varTrimEngine as AnyObject,
                                                     "varRegisterCharacter" : appDelegate.CarRepair.Car.varPlateCode as AnyObject,
                                                     "varRegisterPlateNumber" : appDelegate.CarRepair.Car.varPlateNumber as AnyObject,
                                                     "file[]" : arrImageList as AnyObject]
            
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objpostQuoteRequest = HttpWrapper.init()
            self.objpostQuoteRequest.delegate = self
            self.objpostQuoteRequest.requestMultipartFormDataWithImageAndVideo(kpostQuoteRequest, dicsParams: dictParameters, dictHeader: header)
        }
        
    }
    
   
    // MARK: - HttpWrapperfetchDataSuccess
    
    func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary)
    {
        AppHelper.hideLoadingView()
        print(dicsResponse)
        if wrapper == objpostQuoteRequest {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "Thankyou_Vc") as! Thankyou_Vc
                nextViewController.isFrom = "1"
                self.navigationController?.pushViewController(nextViewController, animated: true)
            }
            else
            {
                
                //  tblMain.isHidden = true

                 AppHelper.showAlert("ServiceMyCar".localized(), message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }
       
        
    }
    // MARK: - HttpWrapperfetchDataFail
    
    func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError)
    {
        AppHelper.hideLoadingView()
        print(error.debugDescription)
    }
    // MARK: - isLoginDataValid
    func isLoginDataValid() -> Bool
    {
        
        
        if (txtviewQuate.text?.trimmingCharacters(in: CharacterSet.whitespaces).count)! <= 0
        {
            AppHelper.showAlert("ServiceMyCar".localized(), message: PleaseEnterQuote)
            return false
        }
        
        return true
    }
    //MARK: - Open the camera
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            //If you dont want to edit the photo then you can set allowsEditing to false
            imagePicker.allowsEditing = false
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        }
        else{
            let alert  = UIAlertController(title: "Warning".localized(), message: "You don't have camera".localized(), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK".localized(), style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: - Choose image from camera roll
    func openGallary(){
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        //If you dont want to edit the photo then you can set allowsEditing to false
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
//MARK: - UIImagePickerControllerDelegate
extension CarRepairQuote_Vc:  UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var imgData:Data = Data()

        
        if arrImageList.count > 5 {
            ShowAlert(Title: "ServiceMyCar".localized(), Msg: "Sorry, only 6 images allowed. Please submit the request and our team will contact you shortly.".localized())
        }
        else if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{

            //let image = UIImage(named: "imageName")

           
           
            let resizeData = editedImage.jpegData(compressionQuality:0.5)
            self.imageData = resizeData! as NSData

            arrImageList.append(editedImage)
           
            //uploadImagewithData()
        }
        else  if let editedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{

            //let image = UIImage(named: "imageName")

            let resizeData = editedImage.jpegData(compressionQuality:0.5)
            self.imageData = resizeData! as NSData
             arrImageList.append(editedImage)
            //uploadImagewithData()
            
        }
        
        //Dismiss the UIImagePicker after selection
        picker.dismiss(animated: true) {
            self.collectionviewPhoto.reloadData()
            //            let userid =  UserDefaults.standard.object(forKey: kUser_id) as! String
            //            let Ktoken = UserDefaults.standard.object(forKey: kToken) as! String
            //            //AppHelper.showLoadingView()
            //            self.requestWith(endUrl: kuploadprofilepicture, imageData: imgData, parameters: ["nUserId":userid as AnyObject,"cToken": Ktoken as AnyObject], onCompletion: { (true) in
            //                AppHelper.hideLoadingView()
            //                var loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            //                loadingNotification?.mode = MBProgressHUDModeText
            //                loadingNotification?.labelText = "Data updated successfully"
            //                loadingNotification?.margin = 10;
            //                loadingNotification?.yOffset = 150;
            //                loadingNotification?.hide(true, afterDelay: 3)
            //
            //                self.getUserProfile()
            //
            //            }) { (true) in
            //
            //            }
        }
    }
  
    func uploadImage(){
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
}
    
}
