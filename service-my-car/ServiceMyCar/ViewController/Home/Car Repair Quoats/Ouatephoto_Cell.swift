//
//  Ouatephoto_Cell.swift
//  ServiceMyCar
//
//  Created by baps on 25/02/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit

class Ouatephoto_Cell: UICollectionViewCell {
    
    @IBOutlet weak var imgCar: UIImageView!
    @IBOutlet weak var btnDelete: UIButton!
}
