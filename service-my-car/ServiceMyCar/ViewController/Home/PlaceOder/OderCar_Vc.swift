//
//  OderCar_Vc.swift
//  ServiceMyCar
//
//  Created by baps on 28/01/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit

class OderCar_Vc: UIViewController ,UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    
    @IBOutlet weak var collectionviewselect: UICollectionView!
    var arrService = ["AUDI Model : A3","Basic Service","2019-01-01","Car Wash"]
    var arrImage = ["car-tools","accident","weel","battery","car-wash","car-option"]
    var arrServiceImage = ["car-wash-1","petrol-1"]
    var DataDic:NSDictionary = NSDictionary()
    //var arrService:NSMutableArray = NSMutableArray()
    var strcarValet :String = String()
    var strcarFuel :String = String()
    var strAmount:Float = Float()
    var isCarWas = false
    var isCarFuel = false
    var isFrom = ""
    let badgeButton : MIBadgeButton = MIBadgeButton(frame:CGRect(x: -10, y: 15, width: 40, height: 20))
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false

        let lbNavTitle = UILabel(frame: CGRect(x: CGFloat(35), y:self.view.bounds.size.width/2-235, width: CGFloat(200), height: CGFloat(40)))
        lbNavTitle.textAlignment = .center
        lbNavTitle.text = "confirmation_title".localized()
        lbNavTitle.textColor = Colorblack
        lbNavTitle.font = UIFont(name: Regular, size: 16)
        
//        let logo = UIImage(named: "AppTopIcon")
//        let imageView = UIImageView(image:logo)
//        imageView.contentMode = .scaleAspectFit
//        self.navigationItem.titleView = imageView
        self.navigationItem.titleView = lbNavTitle
        self.navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.white
        
        let titleDict: NSDictionary = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [NSAttributedString.Key : Any]
        
        let button = UIButton.init(type: .custom)
        //backarrow , menu
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        
        if languageCode == "en" {
            button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
        }
        else {
            button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
        }
        button.addTarget(self, action:#selector(SideMenu), for: UIControl.Event.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        let buttonLogout = UIButton.init(type: .custom)
        buttonLogout.setImage(UIImage.init(named: "notification"), for: UIControl.State.normal)
        buttonLogout.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        buttonLogout.frame = CGRect.init(x: self.view.frame.size.width-30, y: 0, width: 25, height: 25)
        // buttonLogout.titleLabel!.font =  UIFont(name:FontSemibold, size: 14)
        let barButtonLog = UIBarButtonItem.init(customView: buttonLogout)
        // self.navigationItem.rightBarButtonItem = barButtonLog
        
        self.collectionviewselect.register(UINib(nibName: "OrderTotal_Cell", bundle: nil), forCellWithReuseIdentifier: "OrderTotal_Cell")
        
        //        if let value = DataDic.value(forKey: "varSpecification") as? NSArray
        //        {
        //            arrService = value.mutableCopy() as! NSMutableArray
        //        }
        
        badgeButton.edgeInsetTop = 10
        badgeButton.setImage(UIImage(named : "notification"), for: .normal)
        print(appDelegate.NCount)
         badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        badgeButton.badgeString = appDelegate.NCount
        if appDelegate.NCount == "0"
        {
            badgeButton.badgeBackgroundColor = UIColor.clear
            badgeButton.badgeTextColor = UIColor.clear
        }
        else
        {
            badgeButton.badgeBackgroundColor = colorGreen
            badgeButton.badgeTextColor = UIColor.white
        }
        badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        let barButton2 : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton2
        //        getPackagesListByCategory()
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)

        
    }
    
    // Step 3
    
//    @objc func NotificationAlert()
//    {
//        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
//
//        self.navigationController?.pushViewController(nextViewController, animated: true)
//
//    }
    
    // Step 4
    
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            //self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                
                badgeButton.badgeString = appDelegate.NCount
                if appDelegate.NCount == "0"
                {
                    badgeButton.badgeBackgroundColor = UIColor.clear
                    badgeButton.badgeTextColor = UIColor.clear
                }
                else
                {
                    badgeButton.badgeBackgroundColor = colorGreen
                    badgeButton.badgeTextColor = UIColor.white
                }
            }
        }
    }
    
    // MARK:- goBack
    
    @objc func SideMenu()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK:- OnClickBookNow
    @objc func OnClickBookNow()
    {
        let nextViewController = objMain.instantiateViewController(withIdentifier: "Payment_Vc") as! Payment_Vc
        nextViewController.isFrom = isFrom
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    // MARK:- goBack
    
    @objc func NotificationAlert()
    {
        guard let vcs = self.navigationController?.viewControllers else{return}
        let inStack = vcs.contains(where: { (vc) -> Bool in
            return vc is MyNotificationList_Vc
        })
        if inStack{
            self.performSegue(withIdentifier: unwindToNotificationController, sender: self)
        }else{
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        //        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
        //
        //        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    // MARK:- CollectionView Methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0
        {
            if isCarFuel == true && isCarWas == true
            {
                return 4
            }
            else if isCarFuel == true && isCarWas == false
            {
                return 3
            }
            else if isCarFuel == false && isCarWas == true
            {
                return 3
            }
            else
            {
                return 2
            }
            
        }
        else
        {
            return 1
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0
        {
            
            var FCell = UICollectionViewCell()
            
            
            
//            if indexPath.row == 0
//            {
//                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectCar_Cell", for: indexPath) as! SelectCar_Cell
//                cell.viewBack.layer.borderColor = Colorblack.cgColor
//                cell.viewBack.layer.cornerRadius = 5
//                cell.viewBack.layer.borderWidth = 1
//                cell.imgCar.image = UIImage.init(named: "car")
//                cell.imgCar.image = UIImage.init(named: "car")
//                cell.btnClose.tag = indexPath.row
//                cell.btnClose.isHidden = true
//                //                cell.btnClose.addTarget(self, action:#selector(onClickbtnDeleteCar), for: UIControl.Event.touchUpInside)
//
//                cell.lblCarname.text = appDelegate.Bookings.Car.CarName
//
//                cell.lblModel.text = appDelegate.Bookings.Car.carModel
//
//                cell.lblPlateNo.text = appDelegate.Bookings.Car.carNo
//
//                FCell = cell
//            }

            if indexPath.row == 0
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectService_cell", for: indexPath) as! SelectService_cell
                cell.viewWidthConstant.constant = (collectionView.frame.size.width - 20) / 2.0
                cell.viewBack.layer.borderColor = Colorblack.cgColor
                cell.viewBack.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6).cgColor
                cell.viewBack.backgroundColor = UIColor.white
                cell.viewBack.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
                cell.viewBack.layer.shadowOpacity = 2.0
                cell.viewBack.layer.shadowRadius = 3.0
                cell.viewBack.layer.masksToBounds = false
                cell.viewBack.layer.cornerRadius = 5.0

                if let url2 = URL(string: appDelegate.Bookings.Package.PackageImageUrl) {
                    cell.imgCar.sd_setImage(with: url2, placeholderImage: UIImage(named: "img_plan1"))
                }
                
               
                cell.lblTitle.text = appDelegate.Bookings.Package.PackageName + " \(appDelegate.Bookings.Package.PackegePrice)"
               // cell.lblPrice.text = "\(appDelegate.Bookings.Package.PackegePrice) AED"
                FCell = cell
                
            }else if indexPath.row == 1
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectService_cell", for: indexPath) as! SelectService_cell
                cell.viewWidthConstant.constant = (collectionView.frame.size.width - 20) / 2.0
                cell.viewBack.layer.borderColor = Colorblack.cgColor
                cell.viewBack.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6).cgColor
                cell.viewBack.backgroundColor = UIColor.white
                cell.viewBack.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
                cell.viewBack.layer.shadowOpacity = 2.0
                cell.viewBack.layer.shadowRadius = 3.0
                cell.viewBack.layer.masksToBounds = false
                cell.viewBack.layer.cornerRadius = 5.0
                cell.imgCar.image = UIImage.init(named: "calendar")
                cell.lblTitle.text = appDelegate.Bookings.Date.Date
                
                FCell = cell
                
            }
            else if indexPath.row == 2
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectService_cell", for: indexPath) as! SelectService_cell
                cell.viewWidthConstant.constant = (collectionView.frame.size.width - 20) / 2.0
                cell.viewBack.layer.borderColor = Colorblack.cgColor
                cell.viewBack.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6).cgColor
                cell.viewBack.backgroundColor = UIColor.white
                cell.viewBack.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
                cell.viewBack.layer.shadowOpacity = 2.0
                cell.viewBack.layer.shadowRadius = 3.0
                cell.viewBack.layer.masksToBounds = false
                cell.viewBack.layer.cornerRadius = 5.0
                
                if isCarWas == true
                {
                    cell.imgCar.image = UIImage.init(named: arrServiceImage[0])
                    cell.lblTitle.text = "confirmation_car_wash".localized() + "-\(strcarValet) \("app_currency".localized())"
                    //cell.lblPrice.text = "\(strcarValet) AED"
                }
                else
                {
                    cell.imgCar.image = UIImage.init(named: arrServiceImage[1])
                    cell.lblTitle.text = "confirmation_fuel".localized() + "-\(strcarFuel) \("app_currency".localized())"
                    //cell.lblPrice.text = "\(strcarFuel) AED"
                }
                
                
                FCell = cell
                
            }
            else
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectService_cell", for: indexPath) as! SelectService_cell
                cell.viewWidthConstant.constant = (collectionView.frame.size.width - 20) / 2.0
                cell.viewBack.layer.borderColor = Colorblack.cgColor
                cell.viewBack.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6).cgColor
                cell.viewBack.backgroundColor = UIColor.white
                cell.viewBack.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
                cell.viewBack.layer.shadowOpacity = 2.0
                cell.viewBack.layer.shadowRadius = 3.0
                cell.viewBack.layer.masksToBounds = false
                cell.viewBack.layer.cornerRadius = 5.0
                cell.imgCar.image = UIImage.init(named: arrServiceImage[1])
                cell.lblTitle.text = "confirmation_fuel".localized() + "-\(strcarFuel) \("app_currency".localized())"
              //  cell.lblPrice.text = "\(strcarFuel) AED"
                
                FCell = cell
                
            }
            
            //            if let name = (arrService.object(at: indexPath.row) as! NSDictionary).value(forKey: "varSpecification") as? String
            //            {
            //                cell.lblTitle.text = name
            //            }
            //
            //            if let UserProfilePic = (arrService.object(at: indexPath.row) as! NSDictionary).value(forKey: "varImage") as? String
            //            {
            //                let url = "\(UserProfilePic)"
            //                print(url)
            //
            //                if let url2 = URL(string: url) {
            //                    cell.imgCar.sd_setImage(with: url2, placeholderImage: UIImage(named: "img_plan1"))
            //                }
            //            }
            
            return FCell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OrderTotal_Cell", for: indexPath) as! OrderTotal_Cell
            
            cell.lblDeliveryAddress.text = "confirmation_delivery_address".localized()
            cell.lblCollectionAddress.text = "confirmation_collection_address".localized()
            
            cell.lblPickFrom.text = appDelegate.Bookings.DAddress.PickupAddress
            if isFrom == "3" || isFrom == "4" {
                
                appDelegate.Bookings.Price.total = appDelegate.Bookings.Package.PackegePrice
                
                cell.lblPrice.text = "\(appDelegate.Bookings.Price.total) \("app_currency".localized())"
                cell.view2.isHidden = true
            }
            else {
                cell.lblDropOff.text = appDelegate.Bookings.DAddress.DropOffAddress
                cell.lblPrice.text = "\(appDelegate.Bookings.Price.total) \("app_currency".localized())"
                
                if appDelegate.Bookings.isDrop == true
                {
                    // cell.ViewDropOffHeightConstant.constant = 60
                    cell.view2.isHidden = false
                }
                else
                {
                    cell.view2.isHidden = true
                    
                    //                cell.ViewDropOffHeightConstant.constant = 0
                }
            }
            
            
            //            if let value = DataDic.value(forKey: "varPrice") as? String
            //            {
            //                cell.lblPrice.text = value + " AED"
            //            }
            //
            cell.btnBookNow.setTitle("confirmation_next".localized(), for: .normal)
            cell.btnBookNow.addTarget(self, action:#selector(OnClickBookNow), for: UIControl.Event.touchUpInside)
            cell.lblTotal.text = "confirmation_total".localized()
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0
        {
            let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
            let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
            let size:CGFloat = (collectionView.frame.size.width - space) / 2.0
            return CGSize(width: size, height: size)
        }
        else
        {
            let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
            let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
            let size:CGFloat = (collectionView.frame.size.width - space) / 2.0
            return CGSize(width: collectionView.frame.size.width, height: 333)
            
        }
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
