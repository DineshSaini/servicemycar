//
//  OrderTotal_Cell.swift
//  ServiceMyCar
//
//  Created by baps on 28/01/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit

class OrderTotal_Cell: UICollectionViewCell {

    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view2: UIViewX!
    @IBOutlet weak var view1: UIViewX!
    @IBOutlet weak var btnBookNow: UIButton!
    @IBOutlet weak var ViewDropOffHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblDropOff: UILabel!
    @IBOutlet weak var lblPickFrom: UILabel!
    @IBOutlet weak var lblCollectionAddress: UILabel!
    @IBOutlet weak var lblDeliveryAddress: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        view1.layer.borderColor = Colorblack.cgColor
        view1.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6).cgColor
        view1.backgroundColor = UIColor.white
        view1.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        view1.layer.shadowOpacity = 2.0
        view1.layer.shadowRadius = 3.0
        view1.layer.masksToBounds = false
        view1.layer.cornerRadius = 5.0
 
        view2.layer.borderColor = Colorblack.cgColor
        view2.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6).cgColor
        view2.backgroundColor = UIColor.white
        view2.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        view2.layer.shadowOpacity = 2.0
        view2.layer.shadowRadius = 3.0
        view2.layer.masksToBounds = false
        view2.layer.cornerRadius = 5.0
        
        view3.layer.borderColor = Colorblack.cgColor
        view3.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6).cgColor
        view3.backgroundColor = UIColor.white
        view3.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        view3.layer.shadowOpacity = 2.0
        view3.layer.shadowRadius = 3.0
        view3.layer.masksToBounds = false
        view3.layer.cornerRadius = 5.0
        
        // Initialization code
    }

}
