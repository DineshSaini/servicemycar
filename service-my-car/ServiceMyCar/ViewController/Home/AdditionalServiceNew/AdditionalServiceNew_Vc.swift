//
//  AdditionalServiceNew_Vc.swift
//  ServiceMyCar
//
//  Created by baps on 13/02/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import SwiftyJSON
import ZAlertView
import Alamofire

class AdditionalServiceNew_Vc: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout,HttpWrapperDelegate{

    @IBOutlet weak var btnNext: UIButtonX!
    @IBOutlet weak var lblTotalAmountValue: UILabel!
    @IBOutlet weak var lblVatValue: UILabel!
    @IBOutlet weak var lblOderAmountValue: UILabel!
    @IBOutlet weak var collectionviewMain: UICollectionView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var lblOrderAmountTitle: UILabel!
    @IBOutlet weak var lblVatTitle: UILabel!
    @IBOutlet weak var lblTotalAmountTitle: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    var objGetExtraPrice = HttpWrapper()
    var objGetExtraVat = HttpWrapper()
    var OderPrice:String = String()
    var net:NetworkReachabilityManager = NetworkReachabilityManager()!
    var arrTitle = ["additional_services_car_wash".localized(),"additional_services_fuel".localized()]
   
    var arrDetail = ["alert_Car_Wash_Msg".localized(),"alert_fuel_msg".localized()]
    var arrimage = ["car-wash-1","petrol-1"]
    var strcarValet :String = String()
    var strcarFuel :String = String()
    var strAmount:Float = Float()
    var isCarWas = false
    var isCarFuel = false
    var strVat :String = String()
    var FinelPrice:String = String()
    let badgeButton : MIBadgeButton = MIBadgeButton(frame:CGRect(x: -10, y: -5, width: 40, height: 20))
    func setShdow()
    {
        self.view1.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6).cgColor
        self.view1.backgroundColor = UIColor.white
        self.view1.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.view1.layer.shadowOpacity = 2.0
        self.view1.layer.shadowRadius = 3.0
        self.view1.layer.masksToBounds = false
        self.view1.layer.cornerRadius = 5.0
        
        self.view2.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6).cgColor
        self.view2.backgroundColor = UIColor.white
        self.view2.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.view2.layer.shadowOpacity = 2.0
        self.view2.layer.shadowRadius = 3.0
        self.view2.layer.masksToBounds = false
        self.view2.layer.cornerRadius = 5.0
        
        
        self.view3.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6).cgColor
        self.view3.backgroundColor = UIColor.white
        self.view3.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.view3.layer.shadowOpacity = 2.0
        self.view3.layer.shadowRadius = 3.0
        self.view3.layer.masksToBounds = false
        self.view3.layer.cornerRadius = 5.0
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setShdow()
        self.navigationController?.isNavigationBarHidden = false
        let lbNavTitle = UILabel(frame: CGRect(x: CGFloat(35), y:self.view.bounds.size.width/2-235, width: CGFloat(200), height: CGFloat(40)))
        lbNavTitle.textAlignment = .center
        lbNavTitle.text = "additional_services_title".localized()
        lbNavTitle.textColor = Colorblack
        lbNavTitle.font = UIFont(name: Regular, size: 16)
        
        //        let logo = UIImage(named: "AppTopIcon")
        //        let imageView = UIImageView(image:logo)
        //        imageView.contentMode = .scaleAspectFit
        // self.navigationItem.titleView = imageView
        self.navigationItem.titleView = lbNavTitle
        self.navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.white
        
        let titleDict: NSDictionary = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [NSAttributedString.Key : Any]
        
        let button = UIButton.init(type: .custom)
        //backarrow , menu
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        
        if languageCode == "en" {
            button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
        }
        else {
            button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
        }
        button.addTarget(self, action:#selector(goBack), for: UIControl.Event.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        //        let buttonLogout = UIButton.init(type: .custom)
        //        buttonLogout.setImage(UIImage.init(named: "notification"), for: UIControl.State.normal)
        //        buttonLogout.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        //        buttonLogout.frame = CGRect.init(x: self.view.frame.size.width-30, y: 0, width: 25, height: 25)
        //        // buttonLogout.titleLabel!.font =  UIFont(name:FontSemibold, size: 14)
        //        let barButtonLog = UIBarButtonItem.init(customView: buttonLogout)
        //        // self.navigationItem.rightBarButtonItem = barButtonLog
        
        strAmount = appDelegate.selectedPackagePrice
        badgeButton.edgeInsetTop = 10
        badgeButton.setImage(UIImage(named : "notification"), for: .normal)
        print(appDelegate.NCount)
         badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        badgeButton.badgeString = appDelegate.NCount
        if appDelegate.NCount == "0"
        {
            badgeButton.badgeBackgroundColor = UIColor.clear
            badgeButton.badgeTextColor = UIColor.clear
        }
        else
        {
            badgeButton.badgeBackgroundColor = colorGreen
            badgeButton.badgeTextColor = UIColor.white
        }
        badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        let barButton2 : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton2
        //        getPackagesListByCategory()
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)

        
        lblOrderAmountTitle.text = "additional_services_order_amount".localized()
        lblVatTitle.text = "additional_services_vat".localized()
        lblTotalAmountTitle.text = "additional_services_total_amount".localized()
        lblTitle.text = "additional_services_add_extra_services".localized()
        btnNext.setTitle("additional_services_next".localized(), for: .normal)
        
        GetVat()
        GetExtraPrice()
        
    }
    
    
    // Step 3
    
    @objc func NotificationAlert()
    {
        guard let vcs = self.navigationController?.viewControllers else{return}
        let inStack = vcs.contains(where: { (vc) -> Bool in
            return vc is MyNotificationList_Vc
        })
        if inStack{
            self.performSegue(withIdentifier: unwindToNotificationController, sender: self)
        }else{
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
//        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
//        
//        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    // Step 4
    
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            //self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                
                badgeButton.badgeString = appDelegate.NCount
                if appDelegate.NCount == "0"
                {
                    badgeButton.badgeBackgroundColor = UIColor.clear
                    badgeButton.badgeTextColor = UIColor.clear
                }
                else
                {
                    badgeButton.badgeBackgroundColor = colorGreen
                    badgeButton.badgeTextColor = UIColor.white
                }
            }
        }
    }
    
    // MARK:- goBack
    
    @IBAction func onClickBtnNext(_ sender: Any) {
   
        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "OderCar_Vc") as! OderCar_Vc
        appDelegate.Bookings.Price.total = FinelPrice
        appDelegate.Bookings.Price.OderAmount = OderPrice
        nextViewController.isCarWas = isCarWas
        nextViewController.isCarFuel = isCarFuel
        nextViewController.strcarFuel = strcarFuel
        nextViewController.strcarValet = strcarValet
        //        if let data = arrCategorylist.object(at: indexPath.row) as? NSDictionary
        //        {
        //            nextViewController.DataDic = data
        //        }
        //let nextViewController = objPostJob.instantiateViewController(withIdentifier:"PostYourJob") as! PostYourJob
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    // MARK:- goBack
    
    @objc func goBack()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func onClickBtnDetails(_sender:UIButton)
    {
       ShowAlert(Title: arrTitle[_sender.tag], Msg: arrDetail[_sender.tag])
    }

    @objc func onClickBtnCheckMark(_sender:UIButton)
    {
        // _sender.setImage(UIImage.init(named: "unticked"), for: .normal)
        
        if _sender.tag == 0
        {
            if isCarWas == true
            {
                isCarWas = false
                 appDelegate.Bookings.carValet = "0"
            }
            else
            {
                appDelegate.Bookings.carValet = "1"
                isCarWas = true
            }
        }
        else
        {
            if isCarFuel == true
            {
                isCarFuel = false
                appDelegate.Bookings.carFuel = "0"
            }
            else
            {
                appDelegate.Bookings.carFuel = "1"
                isCarFuel = true
            }
        }
       collectionviewMain.reloadData()
    }
    
    // MARK:- CollectionView Methods
    
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
            return arrTitle.count
       
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AdditionalNew_cell", for: indexPath) as! AdditionalNew_cell
        cell.viewBack.layer.cornerRadius = 4.0
        cell.viewBack.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
        cell.viewBack.backgroundColor = UIColor.white
        cell.viewBack.layer.shadowOffset = CGSize.zero
        cell.viewBack.layer.shadowOpacity = 2.0
        cell.viewBack.layer.shadowRadius = 3.0
        cell.viewBack.layer.masksToBounds = false
        cell.lbltitle.text = arrTitle[indexPath.row]
        cell.imgCar.image = UIImage.init(named: arrimage[indexPath.row])
        cell.btnCheck.tag = indexPath.row
        cell.btnShowDetails.tag = indexPath.row
        cell.btnCheck.addTarget(self, action:#selector(onClickBtnCheckMark(_sender:)), for: UIControl.Event.touchUpInside)
        cell.btnShowDetails.addTarget(self, action:#selector(onClickBtnDetails(_sender:)), for: UIControl.Event.touchUpInside)
        
        if indexPath.row == 0
        {
            if isCarWas == true
            {
               
                cell.imgCheck.image = UIImage.init(named: "CheckedGreen")
            }
            else
            {
                cell.imgCheck.image = UIImage.init(named: "unCheckGreen")
               
            }
            
            
        }
        else
        {
            if isCarFuel == true
            {
                
                 cell.imgCheck.image = UIImage.init(named: "CheckedGreen")
            }
            else
            {
                
                cell.imgCheck.image = UIImage.init(named: "unCheckGreen")
            }
        }
        
        
        
        lblVatValue.text        = "\(strVat)%"
       
        
        
        if isCarWas == true
        {
            if strcarValet != ""
            {
                strAmount = appDelegate.selectedPackagePrice + Float(strcarValet)!
            }
        }
        else
        {
            if strcarValet != ""
            {
                strAmount = appDelegate.selectedPackagePrice
            }
        }
        
        
        if isCarFuel == true
        {
            if strcarFuel != ""
            {
                strAmount = strAmount + Float(strcarFuel)!
            }
        }
        else
        {
            //                if strcarFuel != ""
            //                {
            //                    strAmount = strAmount
            //                }
        }
        
        lblOderAmountValue.text = "\(strAmount) AED"
        OderPrice = "\(strAmount)"
        if strVat != ""
        {
            let vat = strAmount * Float(strVat)!/100
            
            
            
            lblVatValue.text = "\(vat) AED"
            //total Price
            let Total = strAmount + vat
            lblTotalAmountValue.text = "\(Total) AED"
            FinelPrice = "\(Total)"
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == 0
        {
            if isCarWas == true
            {
                isCarWas = false
                appDelegate.Bookings.carValet = "0"
            }
            else
            {
                appDelegate.Bookings.carValet = "1"
                isCarWas = true
            }
        }
        else
        {
            if isCarFuel == true
            {
                isCarFuel = false
                appDelegate.Bookings.carFuel = "0"
            }
            else
            {
                appDelegate.Bookings.carFuel = "1"
                isCarFuel = true
            }
        }
        collectionviewMain.reloadData()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
        let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
        let size:CGFloat = (collectionView.frame.size.width - space) / 2.0
        return CGSize(width: size, height: size+30)
    }
    
    
    // MARK: - GetExtraPrice API
    
    func GetExtraPrice()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            
            //            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            //            let dictParameters:[String:AnyObject] = ["userId" : nUserId as AnyObject,
            //                                                     "carId" : carId as AnyObject]
            //            let dictHeaaderPera:[String:AnyObject] = [
            //                "platform" : "IOS" as AnyObject,
            //                "modelNumber" : "ML2000" as AnyObject
            //                ,"serialNumber" :"1234567891" as AnyObject,
            //                 "deviceId" : "12345" as AnyObject]
            //
            // print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objGetExtraPrice = HttpWrapper.init()
            self.objGetExtraPrice.delegate = self
            //self.objGetExtraPrice.requestWithparamdictParam(kGetExtraPrice)
            self.objGetExtraPrice.requestWithparamdictParamPostMethodwithHeaderGet(url: kGetExtraPrice, headers: header)
        }
        
    }
    
    // MARK: - GetVat API
    
    func GetVat()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            
            //            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            //            let dictParameters:[String:AnyObject] = ["userId" : nUserId as AnyObject,
            //                                                     "carId" : carId as AnyObject]
            //            let dictHeaaderPera:[String:AnyObject] = [
            //                "platform" : "IOS" as AnyObject,
            //                "modelNumber" : "ML2000" as AnyObject
            //                ,"serialNumber" :"1234567891" as AnyObject,
            //                 "deviceId" : "12345" as AnyObject]
            //
            // print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objGetExtraVat = HttpWrapper.init()
            self.objGetExtraVat.delegate = self
            //self.objGetExtraVat.requestWithparamdictParam(kgetVat)
            self.objGetExtraVat.requestWithparamdictParamPostMethodwithHeaderGet(url: kgetVat, headers: header)
        }
        
    }
    
    // MARK: - HttpWrapperfetchDataSuccess
    
    func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary)
    {
        AppHelper.hideLoadingView()
        print(dicsResponse)
        if wrapper == objGetExtraPrice {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                //                arrCarlist.removeAllObjects()
                //
                if let tempNames: NSDictionary = dicsResponse.value(forKey: "data") as? NSDictionary
                {
                    if let value = tempNames.value(forKey: "carValet") as? String
                    {
                        strcarValet = value
                        
                    }
                    else if let value = tempNames.value(forKey: "carValet") as? NSNumber
                    {
                        strcarValet = "\(value)"
                    }
                    
                    if let value = tempNames.value(forKey: "carFuel") as? String
                    {
                        strcarFuel = value
                    }
                    else if let value = tempNames.value(forKey: "carFuel") as? NSNumber
                    {
                        strcarFuel = "\(value)"
                    }
                    
                }
                
                
                collectionviewMain.reloadData()
                //                print(arrCarlist)
                //                collectionviewselect.reloadData()
            }
            else
            {
               // AppHelper.showAlert("Alert", message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }
        else if wrapper == objGetExtraVat {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                //                arrCarlist.removeAllObjects()
                //
                if let tempNames: NSDictionary = dicsResponse.value(forKey: "data") as? NSDictionary
                {
                    if let value = tempNames.value(forKey: "vat") as? String
                    {
                        strVat = value
                        
                    }
                    else if let value = tempNames.value(forKey: "vat") as? NSNumber
                    {
                        strVat = "\(value)"
                    }
                    
                    
                    
                }
                
                collectionviewMain.reloadData()
                //                print(arrCarlist)
                //                collectionviewselect.reloadData()
            }
            else
            {
                //AppHelper.showAlert("Alert", message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }
        
    }
    // MARK: - HttpWrapperfetchDataFail
    
    func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError)
    {
        AppHelper.hideLoadingView()
        print(error.debugDescription)
    }
    
    // MARK: - Show Toast
    func ShowAlert(Title:String,Msg:String)
    {
        //        ZAlertView.positiveColor            = UIColor.color("#669999")
        //        ZAlertView.negativeColor            = UIColor.color("#CC3333")
        //        ZAlertView.blurredBackground        = true
        ZAlertView.showAnimation            = .bounceBottom
        ZAlertView.hideAnimation            = .bounceRight
        ZAlertView.initialSpringVelocity    = 0.9
        ZAlertView.duration                 = 2
        ZAlertView.titleColor = colorGreen
        
        let dialog = ZAlertView(title: Title, message: Msg, closeButtonText: "service_parts_confirm".localized(), closeButtonHandler: { (alertView) -> () in
            alertView.dismissAlertView()
        })
        
        dialog.width = self.view.frame.width - 40
        dialog.show()
        dialog.allowTouchOutsideToDismiss = true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
