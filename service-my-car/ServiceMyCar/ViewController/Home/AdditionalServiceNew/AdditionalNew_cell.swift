//
//  AdditionalNew_cell.swift
//  ServiceMyCar
//
//  Created by baps on 13/02/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit

class AdditionalNew_cell: UICollectionViewCell {
    
    @IBOutlet weak var imgCheck: UIImageView!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var btnShowDetails: UIButton!
    @IBOutlet weak var imgCar: UIImageView!
    @IBOutlet weak var viewBack: UIView!
}
