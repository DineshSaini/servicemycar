//
//  AdditionalServices_Cell.swift
//  ServiceMyCar
//
//  Created by baps on 22/01/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit

class AdditionalServices_Cell: UITableViewCell {

    @IBOutlet weak var imgCar: UIImageView!
    @IBOutlet weak var btnNo: UIButton!
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var lblDetails: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var ViewBack: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.ViewBack.backgroundColor = UIColor.white
        self.ViewBack.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.ViewBack.layer.shadowOpacity = 1.0
        self.ViewBack.layer.shadowRadius = 2.0
        self.ViewBack.layer.masksToBounds = false
        self.ViewBack.layer.cornerRadius = 5.0
        self.ViewBack.layer.borderWidth = 0.5
        self.ViewBack.layer.borderColor = Colorblack.cgColor
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
