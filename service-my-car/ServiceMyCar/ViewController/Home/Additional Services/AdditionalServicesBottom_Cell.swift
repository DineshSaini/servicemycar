//
//  AdditionalServicesBottom_Cell.swift
//  ServiceMyCar
//
//  Created by baps on 22/01/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit

class AdditionalServicesBottom_Cell: UITableViewCell {

    @IBOutlet weak var AnsTotalAmount: UILabel!
    @IBOutlet weak var AnsVat: UILabel!
    @IBOutlet weak var AnsOderAmount: UILabel!
    @IBOutlet weak var lblOderAmount: UILabel!
    @IBOutlet weak var lblVat: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var btnNext: UIButtonX!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
