//
//  AdditionalServices_Vc.swift
//  ServiceMyCar
//
//  Created by baps on 22/01/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import SwiftyJSON
import ZAlertView
import Alamofire
class AdditionalServices_Vc: UIViewController , UITableViewDelegate , UITableViewDataSource ,HttpWrapperDelegate{
    
    
    @IBOutlet weak var tblMain: UITableView!
    
    var objGetExtraPrice = HttpWrapper()
     var objGetExtraVat = HttpWrapper()
   
    var net:NetworkReachabilityManager = NetworkReachabilityManager()!
    var arrTitle = ["Car Wash","Fuel"]
    var arrDetail = ["Treat your car to a spa by our team of proffesionals before we hand your car clean and shiny on the inside and outside leaving both you and your car happy.","Our driver will stop at the filling station and fill your cars fuel to full.all fuel will be charges extra and a reciipt will be provided.(the 10 AED is our service charge)"]
    var arrimage = ["carwash","petrol"]
    var strcarValet :String = String()
    var strcarFuel :String = String()
    var strAmount:Float = Float()
    var isCarWas = false
    var isCarFuel = false
    var strVat :String = String()
    var FinelPrice:String = String()
    var OderPrice:String = String()
    let badgeButton : MIBadgeButton = MIBadgeButton(frame:CGRect(x: -10, y: 15, width: 40, height: 20))
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        let lbNavTitle = UILabel(frame: CGRect(x: CGFloat(35), y:self.view.bounds.size.width/2-235, width: CGFloat(200), height: CGFloat(40)))
        lbNavTitle.textAlignment = .center
        lbNavTitle.text = "Additional Services"
        lbNavTitle.textColor = Colorblack
        lbNavTitle.font = UIFont(name: Regular, size: 16)
        
        //        let logo = UIImage(named: "AppTopIcon")
        //        let imageView = UIImageView(image:logo)
        //        imageView.contentMode = .scaleAspectFit
        // self.navigationItem.titleView = imageView
        self.navigationItem.titleView = lbNavTitle
        self.navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.white
        
        let titleDict: NSDictionary = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [NSAttributedString.Key : Any]
        
        let button = UIButton.init(type: .custom)
        //backarrow , menu
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        
        if languageCode == "en" {
            button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
        }
        else {
            button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
        }
        button.addTarget(self, action:#selector(goBack), for: UIControl.Event.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        //        let buttonLogout = UIButton.init(type: .custom)
        //        buttonLogout.setImage(UIImage.init(named: "notification"), for: UIControl.State.normal)
        //        buttonLogout.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        //        buttonLogout.frame = CGRect.init(x: self.view.frame.size.width-30, y: 0, width: 25, height: 25)
        //        // buttonLogout.titleLabel!.font =  UIFont(name:FontSemibold, size: 14)
        //        let barButtonLog = UIBarButtonItem.init(customView: buttonLogout)
        //        // self.navigationItem.rightBarButtonItem = barButtonLog
        
        GetVat()
        GetExtraPrice()
        strAmount = appDelegate.selectedPackagePrice
        badgeButton.edgeInsetTop = 10
        badgeButton.setImage(UIImage(named : "notification"), for: .normal)
         badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        print(appDelegate.NCount)
        badgeButton.badgeString = appDelegate.NCount
        if appDelegate.NCount == "0"
        {
            badgeButton.badgeBackgroundColor = UIColor.clear
            badgeButton.badgeTextColor = UIColor.clear
        }
        else
        {
            badgeButton.badgeBackgroundColor = colorGreen
            badgeButton.badgeTextColor = UIColor.white
        }
        badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        let barButton2 : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton2
        //        getPackagesListByCategory()
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)
    }
    
    // Step 3
    
    @objc func NotificationAlert()
    {
        guard let vcs = self.navigationController?.viewControllers else{return}
        let inStack = vcs.contains(where: { (vc) -> Bool in
            return vc is MyNotificationList_Vc
        })
        if inStack{
            self.performSegue(withIdentifier: unwindToNotificationController, sender: self)
        }else{
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        //        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
        //
        //        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    // Step 4
    
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            //self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                
                badgeButton.badgeString = appDelegate.NCount
                if appDelegate.NCount == "0"
                {
                    badgeButton.badgeBackgroundColor = UIColor.clear
                    badgeButton.badgeTextColor = UIColor.clear
                }
                else
                {
                    badgeButton.badgeBackgroundColor = colorGreen
                    badgeButton.badgeTextColor = UIColor.white
                }
            }
        }
    }
    
    // MARK:- goBack
    
    @objc func onClickBtnNext()
    {
        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "OderCar_Vc") as! OderCar_Vc
        appDelegate.Bookings.Price.total = FinelPrice
        print(OderPrice)
        
        appDelegate.Bookings.Price.OderAmount = OderPrice
        
         print(appDelegate.Bookings.Price.OderAmount )
        nextViewController.isCarWas = isCarWas
        nextViewController.isCarFuel = isCarFuel
        nextViewController.strcarFuel = strcarFuel
        nextViewController.strcarValet = strcarValet
        //        if let data = arrCategorylist.object(at: indexPath.row) as? NSDictionary
        //        {
        //            nextViewController.DataDic = data
        //        }
        //let nextViewController = objPostJob.instantiateViewController(withIdentifier:"PostYourJob") as! PostYourJob
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    // MARK:- goBack
    
    @objc func goBack()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func onClickBtnYes(_sender:UIButton)
    {
       // _sender.setImage(UIImage.init(named: "unticked"), for: .normal)
        
        if _sender.tag == 0
        {
            isCarWas = true
        }
        else
        {
            isCarFuel = true
        }
        tblMain.reloadData()
    }
    
    @objc func onClickBtnNo(_sender:UIButton)
    {
        if _sender.tag == 0
        {
            isCarWas = false
        }
        else
        {
            isCarFuel = false
        }
        
        tblMain.reloadData()
        //_sender.setImage(UIImage.init(named: "ticked"), for: .normal)
    }
    // MARK: - GetExtraPrice API
    
    func GetExtraPrice()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            
//            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
//            let dictParameters:[String:AnyObject] = ["userId" : nUserId as AnyObject,
//                                                     "carId" : carId as AnyObject]
            //            let dictHeaaderPera:[String:AnyObject] = [
            //                "platform" : "IOS" as AnyObject,
            //                "modelNumber" : "ML2000" as AnyObject
            //                ,"serialNumber" :"1234567891" as AnyObject,
            //                 "deviceId" : "12345" as AnyObject]
            //
           // print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objGetExtraPrice = HttpWrapper.init()
            self.objGetExtraPrice.delegate = self
            self.objGetExtraPrice.requestWithparamdictParam(kGetExtraPrice)
        }
        
    }
    
    // MARK: - GetVat API
    
    func GetVat()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            
            //            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            //            let dictParameters:[String:AnyObject] = ["userId" : nUserId as AnyObject,
            //                                                     "carId" : carId as AnyObject]
            //            let dictHeaaderPera:[String:AnyObject] = [
            //                "platform" : "IOS" as AnyObject,
            //                "modelNumber" : "ML2000" as AnyObject
            //                ,"serialNumber" :"1234567891" as AnyObject,
            //                 "deviceId" : "12345" as AnyObject]
            //
            // print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objGetExtraVat = HttpWrapper.init()
            self.objGetExtraVat.delegate = self
            self.objGetExtraVat.requestWithparamdictParam(kgetVat)
        }
        
    }
    
    // MARK: - HttpWrapperfetchDataSuccess
    
    func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary)
    {
        AppHelper.hideLoadingView()
        print(dicsResponse)
        if wrapper == objGetExtraPrice {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
//                arrCarlist.removeAllObjects()
//
                if let tempNames: NSDictionary = dicsResponse.value(forKey: "data") as? NSDictionary
                {
                    if let value = tempNames.value(forKey: "carValet") as? String
                    {
                        strcarValet = value
                        
                    }
                    else if let value = tempNames.value(forKey: "carValet") as? NSNumber
                    {
                        strcarValet = "\(value)"
                    }
                    
                    if let value = tempNames.value(forKey: "carFuel") as? String
                    {
                         strcarFuel = value
                    }
                    else if let value = tempNames.value(forKey: "carFuel") as? NSNumber
                    {
                        strcarFuel = "\(value)"
                    }
                    
                }
                
                
                
                tblMain.reloadData()
//                print(arrCarlist)
//                collectionviewselect.reloadData()
            }
            else
            {
                AppHelper.showAlert("Alert", message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }
       else if wrapper == objGetExtraVat {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                //                arrCarlist.removeAllObjects()
                //
                if let tempNames: NSDictionary = dicsResponse.value(forKey: "data") as? NSDictionary
                {
                    if let value = tempNames.value(forKey: "vat") as? String
                    {
                        strVat = value
                        
                    }
                    else if let value = tempNames.value(forKey: "vat") as? NSNumber
                    {
                        strVat = "\(value)"
                    }
                    
                    appDelegate.StrVat = strVat
                    
                }
                
                tblMain.reloadData()
                //                print(arrCarlist)
                //                collectionviewselect.reloadData()
            }
            else
            {
                AppHelper.showAlert("Alert", message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }
        
    }
    // MARK: - HttpWrapperfetchDataFail
    
    func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError)
    {
        AppHelper.hideLoadingView()
        print(error.debugDescription)
    }
    
// MARK: - Tableview Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0
        {
             return 2
        }
        else
        {
             return 1
        }
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AdditionalServices_Cell", for: indexPath) as! AdditionalServices_Cell
            cell.imgCar.image = UIImage.init(named: arrimage[indexPath.row])
            cell.lblDetails.text = arrDetail[indexPath.row]
            if indexPath.row == 0
            {
                cell.lblTitle.text = "\(arrTitle[indexPath.row]) - \(strcarValet) AED"
            }
            else
            {
                cell.lblTitle.text = "\(arrTitle[indexPath.row]) - \(strcarFuel) AED"
            }
            
            
            if indexPath.row == 0
            {
                if isCarWas == true
                {
                    cell.btnNo.setImage(UIImage.init(named: "unticked"), for: .normal)
                    cell.btnYes.setImage(UIImage.init(named: "ticked"), for: .normal)
                    
                }
                else
                {
                    cell.btnNo.setImage(UIImage.init(named: "ticked"), for: .normal)
                    cell.btnYes.setImage(UIImage.init(named: "unticked"), for: .normal)
                }
                
                
            }
            else
            {
                if isCarFuel == true
                {
                    cell.btnNo.setImage(UIImage.init(named: "unticked"), for: .normal)
                    cell.btnYes.setImage(UIImage.init(named: "ticked"), for: .normal)
                }
                else
                {
                    cell.btnNo.setImage(UIImage.init(named: "ticked"), for: .normal)
                    cell.btnYes.setImage(UIImage.init(named: "unticked"), for: .normal)
                }
            }
            
            
            
            cell.btnNo.tag = indexPath.row
            cell.btnYes.tag = indexPath.row
            cell.btnNo.addTarget(self, action:#selector(onClickBtnNo(_sender:)), for: UIControl.Event.touchUpInside)
            cell.btnYes.addTarget(self, action:#selector(onClickBtnYes(_sender:)), for: UIControl.Event.touchUpInside)
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AdditionalServicesBottom_Cell", for: indexPath) as! AdditionalServicesBottom_Cell
            cell.lblOderAmount.text  = "Oder Amount :"
            cell.lblVat.text         = "Goverment \(strVat)% tax :"
            cell.lblTotalAmount.text = "Total Amount :"
           
            
            if isCarWas == true
            {
                if strcarValet != ""
                {
                    strAmount = appDelegate.selectedPackagePrice + Float(strcarValet)!
                }
            }
            else
            {
                if strcarValet != ""
                {
                    strAmount = appDelegate.selectedPackagePrice
                }
            }
            
            
            if isCarFuel == true
            {
                if strcarFuel != ""
                {
                    strAmount = strAmount + Float(strcarFuel)!
                }
            }
            else
            {
//                if strcarFuel != ""
//                {
//                    strAmount = strAmount
//                }
            }
            
            cell.AnsOderAmount.text = "\(strAmount) AED"
            OderPrice = "\(strAmount)"
           // appDelegate.Bookings.OderAmount = strAmount
            if strVat != ""
            {
                let vat = strAmount * Float(strVat)!/100
            
            
            
            cell.AnsVat.text = "\(vat) AED"
            //total Price
            let Total = strAmount + vat
            cell.AnsTotalAmount.text = "\(Total) AED"
                FinelPrice = "\(Total)"
            }
            cell.btnNext.addTarget(self, action:#selector(onClickBtnNext), for: UIControl.Event.touchUpInside)
            return cell
        }
       
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
