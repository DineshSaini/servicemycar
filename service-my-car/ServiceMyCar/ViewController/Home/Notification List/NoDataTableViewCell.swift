//
//  NoDataTableViewCell.swift
//  ServiceMyCar
//
//  Created by admin on 26/07/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit

class NoDataTableViewCell: UITableViewCell {
    
    @IBOutlet weak var noDataLabel:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
