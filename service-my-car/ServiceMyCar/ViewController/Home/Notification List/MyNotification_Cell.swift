//
//  MyNotification_Cell.swift
//  ServiceMyCar
//
//  Created by baps on 16/02/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit

class MyNotification_Cell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var tittleName: UILabel!
    @IBOutlet weak var datelabel: UILabel!



    @IBOutlet weak var viewBack: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewBack.layer.borderColor = Colorblack.cgColor
        viewBack.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6).cgColor
        viewBack.backgroundColor = UIColor.white
        viewBack.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        viewBack.layer.shadowOpacity = 2.0
        viewBack.layer.shadowRadius = 3.0
        viewBack.layer.masksToBounds = false
        viewBack.layer.cornerRadius = 5.0
        datelabel.text = "Date And Time".localized()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
