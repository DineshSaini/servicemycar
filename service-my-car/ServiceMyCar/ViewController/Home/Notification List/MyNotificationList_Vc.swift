//
//  MyNotificationList_Vc.swift
//  ServiceMyCar
//
//  Created by baps on 16/02/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


let unwindToNotificationController = "unwindToNotificationController"

class MyNotificationList_Vc:  UIViewController, UITableViewDataSource , UITableViewDelegate ,HttpWrapperDelegate{
    
    
    var objgetUserNewNotification = HttpWrapper()
    var objsetSeenNotification = HttpWrapper()
    var objresetCounter = HttpWrapper()
    var net:NetworkReachabilityManager = NetworkReachabilityManager()!
    let badgeButton : MIBadgeButton = MIBadgeButton(frame:CGRect(x: -10, y: 15, width: 40, height: 20))
    var arrNotificaiton:NSMutableArray = NSMutableArray()
    var dataLoading = false

    @IBOutlet weak var tblMain: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        let lbNavTitle = UILabel(frame: CGRect(x: CGFloat(35), y:self.view.bounds.size.width/2-235, width: CGFloat(200), height: CGFloat(40)))
        lbNavTitle.textAlignment = .center
        lbNavTitle.text = "notification_title".localized()
        lbNavTitle.textColor = Colorblack
        lbNavTitle.font = UIFont(name: FontBold, size: 16)
        self.navigationItem.titleView = lbNavTitle
        self.navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.white
        
        let titleDict: NSDictionary = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [NSAttributedString.Key : Any]
        
        let button = UIButton.init(type: .custom)
        //backarrow , menu
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        
        if languageCode == "en" {
            button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
        }
        else {
            button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
        }
        button.addTarget(self, action:#selector(SideMenu), for: UIControl.Event.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        self.tblMain.register(UINib(nibName: "NoDataTableViewCell", bundle: nil), forCellReuseIdentifier:"NoDataTableViewCell")
//        badgeButton.edgeInsetTop = 10
//        badgeButton.setImage(UIImage(named : "notification"), for: .normal)
//        print(appDelegate.NCount)
//         badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
//        badgeButton.badgeString = appDelegate.NCount
//        if appDelegate.NCount == "0"{
//            badgeButton.badgeBackgroundColor = UIColor.clear
//            badgeButton.badgeTextColor = UIColor.clear
//        }else{
//            badgeButton.badgeBackgroundColor = colorGreen
//            badgeButton.badgeTextColor = UIColor.white
//        }
        tblMain.backgroundColor = backGroundColor

       // badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
//        let barButton2 : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
//        self.navigationItem.rightBarButtonItem = barButton2
        
//        let buttonLogout = UIButton.init(type: .custom)
//        buttonLogout.setImage(UIImage.init(named: "notification"), for: UIControl.State.normal)
//        badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
//        buttonLogout.frame = CGRect.init(x: self.view.frame.size.width-30, y: 0, width: 25, height: 25)
//        // buttonLogout.titleLabel!.font =  UIFont(name:FontSemibold, size: 14)
//        let barButtonLog = UIBarButtonItem.init(customView: buttonLogout)
//        self.navigationItem.rightBarButtonItem = barButtonLog
//
//        let count = appDelegate.remoteLogin
//        print(count)
        //getUserNewNotification()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)
    }
    
    
    // MARK: - Google Api Calling
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            //self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                
                badgeButton.badgeString = appDelegate.NCount
                if appDelegate.NCount == "0"{
                    badgeButton.badgeBackgroundColor = UIColor.clear
                    badgeButton.badgeTextColor = UIColor.clear
                }else{
                    badgeButton.badgeBackgroundColor = colorGreen
                    badgeButton.badgeTextColor = UIColor.white
                }
                
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setupNavigation()
        getUserNewNotification()
        //resetCountTotalNotification()
    }
    // MARK:- goBack
    
    @objc func SideMenu()
    {
        //appDelegate.loginSuccess()
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func NotificationAlert()
    {
        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
        
        self.navigationController?.pushViewController(nextViewController, animated: true)

    }
    
    // MARK: - onClickDeliveryReport
    @IBAction func onClickDeliveryReport(_ sender:UIButton!) {
        // do cool stuff here
        
        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "webview_Vc") as! webview_Vc
        if let varCar = (arrNotificaiton.object(at: sender.tag) as! NSDictionary).value(forKey: "varCar") as? String ,let varModel = (arrNotificaiton.object(at: sender.tag) as! NSDictionary).value(forKey: "varModel") as? String
        {
            nextViewController.strName  = varCar + " " + varModel
        }
        
        if let name = (arrNotificaiton.object(at: sender.tag) as! NSDictionary).value(forKey: "servicePdf") as? String
        {
            nextViewController.URL  = name
        }
        
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    
    // MARK: - tableview Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrNotificaiton.count == 0{
            return 1
        }else{
            return arrNotificaiton.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (arrNotificaiton.count != 0) ? 160.0 : self.tblMain.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if arrNotificaiton.count == 0{
             let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataTableViewCell" , for: indexPath) as! NoDataTableViewCell
            cell.contentView.backgroundColor = backGroundColor
            cell.noDataLabel.text = dataLoading ? "No data found".localized() : "Loading..".localized()
            return cell
            
        }else{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyNotification_Cell" , for: indexPath) as! MyNotification_Cell
        AppHelper.makeCircularWithRightTopTwoCornerRadius(cell.viewBack, cornerRadius: 25.0)
        cell.viewBack.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).cgColor
        cell.viewBack.backgroundColor = UIColor.white
        cell.viewBack.layer.shadowOffset = CGSize.zero
        cell.viewBack.layer.shadowOpacity = 1.0
        cell.viewBack.layer.shadowRadius = 3.0
        cell.viewBack.layer.masksToBounds = false

        cell.contentView.backgroundColor = backGroundColor
        
        if let name = (arrNotificaiton.object(at: indexPath.row) as! NSDictionary).value(forKey: "varMessage") as? String{
            cell.lblTitle.text  = name
        }
            
            if let dtDateTime = (arrNotificaiton.object(at: indexPath.row) as! NSDictionary).value(forKey: "dtDateTime") as? String{
                cell.dateTimeLabel.text = dtDateTime
            }
            
            if let tittleName = (arrNotificaiton.object(at: indexPath.row) as! NSDictionary).value(forKey: "varTitle") as? String{
                cell.tittleName.text = tittleName.uppercased()
            }
        
        return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if arrNotificaiton.count == 0{
            return
        }
        
        if let intId = (arrNotificaiton.object(at: indexPath.row) as! NSDictionary).value(forKey: "intId") as? String
        {
           setSeenNotification(intId:intId)
        }
        
        if let varMsgType = (arrNotificaiton.object(at: indexPath.row) as! NSDictionary).value(forKey: "varMsgType") as? String{
            
            if varMsgType == "BOOKING_SUCCESS"{
                let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyServiceViewController") as! MyServiceViewController
                self.navigationController?.pushViewController(nextViewController, animated: true)
                
            }else if varMsgType == "COLLECTION_REPORT"{
                
                let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "webview_Vc") as! webview_Vc
                if let name = (arrNotificaiton.object(at: indexPath.row) as! NSDictionary).value(forKey: "varTitle") as? String{
                    nextViewController.strName  = name
                }
                if let url = (arrNotificaiton.object(at: indexPath.row) as! NSDictionary).value(forKey: "url") as? String{
                    nextViewController.URL  = url
                }
                nextViewController.viewOrder = (arrNotificaiton.object(at: indexPath.row) as! NSDictionary)
                nextViewController.fromNotificationView = true
                self.navigationController?.pushViewController(nextViewController, animated: true)
                
            }else if varMsgType == "DELIVERY_REPORT"{
                
                let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "webview_Vc") as! webview_Vc
                if let name = (arrNotificaiton.object(at: indexPath.row) as! NSDictionary).value(forKey: "varTitle") as? String{
                    nextViewController.strName  = name
                }
                if let url = (arrNotificaiton.object(at: indexPath.row) as! NSDictionary).value(forKey: "url") as? String{
                    nextViewController.URL  = url
                }
                nextViewController.viewOrder = (arrNotificaiton.object(at: indexPath.row) as! NSDictionary)
                nextViewController.fromNotificationView = true
                self.navigationController?.pushViewController(nextViewController, animated: true)
                
            }else if varMsgType == "HEALTH_CHECK_VIDEO"{
                
                let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "webview_Vc") as! webview_Vc
                
                if let name = (arrNotificaiton.object(at: indexPath.row) as! NSDictionary).value(forKey: "varTitle") as? String{
                    nextViewController.strName  = name
                }
                if let url = (arrNotificaiton.object(at: indexPath.row) as! NSDictionary).value(forKey: "url") as? String{
                    nextViewController.URL  = url
                }
                nextViewController.viewOrder = (arrNotificaiton.object(at: indexPath.row) as! NSDictionary)
                nextViewController.fromNotificationView = true
                self.navigationController?.pushViewController(nextViewController, animated: true)
                
            }else if varMsgType == "INVOICE_RECEIVED"{
                
                let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "CategoryDocViewController") as! CategoryDocViewController
                nextViewController.order = (arrNotificaiton.object(at: indexPath.row) as! NSDictionary)
                if let name = (arrNotificaiton.object(at: indexPath.row) as! NSDictionary).value(forKey: "varTitle") as? String{
                    nextViewController.carName  = name
                }
                nextViewController.fromNotification = true
                self.navigationController?.pushViewController(nextViewController, animated: true)
                
            }else if varMsgType == "INVOICE_PAID"{
                
                let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "webview_Vc") as! webview_Vc
                
                if let name = (arrNotificaiton.object(at: indexPath.row) as! NSDictionary).value(forKey: "varTitle") as? String{
                    nextViewController.strName  = name
                }
                if let url = (arrNotificaiton.object(at: indexPath.row) as! NSDictionary).value(forKey: "url") as? String{
                    nextViewController.URL  = url
                }
                nextViewController.viewOrder = (arrNotificaiton.object(at: indexPath.row) as! NSDictionary)
                nextViewController.fromNotificationView = true
                self.navigationController?.pushViewController(nextViewController, animated: true)
                
            }else if varMsgType == "EXTRAPART_APPROVAL"{
                
                let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "AdditionRepairsViewController") as! AdditionRepairsViewController
                if let intId = (arrNotificaiton.object(at: indexPath.row) as! NSDictionary).value(forKey: "intOrderId") as? String{
                    nextViewController.orderId = intId
                }else if let intId = (arrNotificaiton.object(at: indexPath.row) as! NSDictionary).value(forKey: "intOrderId") as? NSNumber{
                    nextViewController.orderId = "\(intId)"
                }
                nextViewController.fromNotification = true
                nextViewController.order = (arrNotificaiton.object(at: indexPath.row) as! NSDictionary)
                self.navigationController?.pushViewController(nextViewController, animated: true)
                
            }else if varMsgType == "SERVICE_SCHADULE"{
                
                let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "webview_Vc") as! webview_Vc
                
                if let name = (arrNotificaiton.object(at: indexPath.row) as! NSDictionary).value(forKey: "varTitle") as? String{
                    nextViewController.strName  = name
                }
                if let url = (arrNotificaiton.object(at: indexPath.row) as! NSDictionary).value(forKey: "url") as? String{
                    nextViewController.URL  = url
                }
                nextViewController.viewOrder = (arrNotificaiton.object(at: indexPath.row) as! NSDictionary)
                nextViewController.fromNotificationView = true
                self.navigationController?.pushViewController(nextViewController, animated: true)
            }
            
        }
        
    }
    
    // MARK: - getUserNewNotification API
    
    func getUserNewNotification()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["userId" : nUserId as AnyObject]
  
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objgetUserNewNotification = HttpWrapper.init()
            self.objgetUserNewNotification.delegate = self
            self.objgetUserNewNotification.requestWithparamdictParamMethodwithHeaderAndDictGet(url: kgetAllUserNotification, dictParams: dictParameters, headers: header)

        }
        
    }
    
    // MARK: - getUserNewNotification API
    
    func resetCountTotalNotification()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["userId" : nUserId as AnyObject]
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objresetCounter = HttpWrapper.init()
            self.objresetCounter.delegate = self
            self.objresetCounter.requestWithparamdictParamMethodwithHeaderAndDictGet(url: kresetCountTotalNotification, dictParams: dictParameters, headers: header)

        }
        
    }
    
    // MARK: - segue handling
    @IBAction func unwindToNotificationController(_ segue: UIStoryboardSegue){
        
    }

    // MARK: - getUserNewNotification API
    
    func setSeenNotification(intId:String)
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["intId" : intId as AnyObject]
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objsetSeenNotification = HttpWrapper.init()
            self.objsetSeenNotification.delegate = self            
            self.objsetSeenNotification.requestWithparamdictParamMethodwithHeaderAndDictGet(url: ksetSeenNotification, dictParams: dictParameters, headers: header)

        }
        
    }
    
    
    // MARK: - HttpWrapperfetchDataSuccess
    
    func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary)
    {
        AppHelper.hideLoadingView()
        print(dicsResponse)
        if wrapper == objgetUserNewNotification {
            dataLoading = true
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                arrNotificaiton.removeAllObjects()
                
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray
                {
                    arrNotificaiton = tempNames.mutableCopy() as! NSMutableArray
                }
                print(arrNotificaiton)
                
                tblMain.reloadData()
            }
            else
            {
                arrNotificaiton.removeAllObjects()
                tblMain.reloadData()
              //  tblMain.isHidden = true
                
               // AppHelper.showAlert("Alert", message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }
        else if wrapper == objsetSeenNotification {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
//                arrNotificaiton.removeAllObjects()
//                
//                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray
//                {
//                    arrNotificaiton = tempNames.mutableCopy() as! NSMutableArray
//                }
//                print(arrNotificaiton)
                 appDelegate.getNotificationCount()
//                
//                tblMain.reloadData()
            }
            else
            {
                //  tblMain.isHidden = true
                
                // AppHelper.showAlert("Alert", message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }
        else if wrapper == objresetCounter {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                
                
//                appDelegate.NCount = "0"
//                badgeButton.badgeString = appDelegate.NCount
//                if appDelegate.NCount == "0"
//                {
//                    badgeButton.badgeBackgroundColor = UIColor.clear
//                    badgeButton.badgeTextColor = UIColor.clear
//                }
//                else
//                {
//                    badgeButton.badgeBackgroundColor = colorGreen
//                    badgeButton.badgeTextColor = UIColor.white
//                }
                
               
                //                arrNotificaiton.removeAllObjects()
                //
                //                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray
                //                {
                //                    arrNotificaiton = tempNames.mutableCopy() as! NSMutableArray
                //                }
                //                print(arrNotificaiton)
                //
                //                tblMain.reloadData()
            }
            else
            {
                //  tblMain.isHidden = true
                
                // AppHelper.showAlert("Alert", message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }
        
        
    }
    // MARK: - HttpWrapperfetchDataFail
    
    func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError)
    {
        AppHelper.hideLoadingView()
        print(error.debugDescription)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
