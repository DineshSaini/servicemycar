//
//  MyInvoice_Vc.swift
//  ServiceMyCar
//
//  Created by baps on 13/02/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import TelrSDK
import SJSwiftSideMenuController

class MyInvoice_Vc:UIViewController, UITableViewDataSource , UITableViewDelegate ,HttpWrapperDelegate{
    
    
    var objGetScheduleService = HttpWrapper()
    var net:NetworkReachabilityManager = NetworkReachabilityManager()!
   
    let KEY:String = "9Std-fqjHP#Q6dW2"  // TODO fill key
    let STOREID:String = "20796"         // TODO fill store id
    var EMAIL:String = ""//isLive ? "accounts@servicemycar.ae" : "Accounts@servicemycar.ae" // TODO fill email
    let UserLName = UserDefaults.standard.value(forKey: kUserName) as! String
    let UserEmail = UserDefaults.standard.value(forKey: KUserLoginEmail) as! String
    let UserPhoneNo = UserDefaults.standard.value(forKey: kUserLoginPhone) as! String
    var couponId:String = String()
    var paymentRequest:PaymentRequest?
    var strPaidAmaunt :String = String()
    var arrScheduled:NSMutableArray = NSMutableArray()
    let badgeButton : MIBadgeButton = MIBadgeButton(frame:CGRect(x: -10, y: 15, width: 40, height: 20))
    @IBOutlet weak var tblMain: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        if let UserEmail = UserDefaults.standard.value(forKey: KUserLoginEmail) as? String{
            self.EMAIL = UserEmail
        }
        self.navigationController?.isNavigationBarHidden = false
        let lbNavTitle = UILabel(frame: CGRect(x: CGFloat(35), y:self.view.bounds.size.width/2-235, width: CGFloat(200), height: CGFloat(40)))
        lbNavTitle.textAlignment = .center
        lbNavTitle.text = "invoice_title".localized()
        lbNavTitle.textColor = Colorblack
        lbNavTitle.font = UIFont(name: FontMedium, size: 16)
        
        //        let logo = UIImage(named: "AppTopIcon")
        //        let imageView = UIImageView(image:logo)
        //        imageView.contentMode = .scaleAspectFit
        //        self.navigationItem.titleView = imageView
        self.navigationItem.titleView = lbNavTitle
        self.navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.white
        
        let titleDict: NSDictionary = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [NSAttributedString.Key : Any]
        
        let button = UIButton.init(type: .custom)
        //backarrow , menu
        if appDelegate.isFromNotification == true
        {
            button.setImage(UIImage.init(named: "menu"), for: UIControl.State.normal)
        }
        else
        {
            let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
            
            if languageCode == "en" {
                button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
            }
            else {
                button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
            }
        }
        button.addTarget(self, action:#selector(SideMenu), for: UIControl.Event.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        //        self.tblPage1.register(HistoryDatatable_Cell.self, forCellReuseIdentifier: "HistoryDatatable_Cell")
        //        self.tblPage2.register(HistoryDatatable_Cell.self, forCellReuseIdentifier: "HistoryDatatable_Cell")
        
        
        
        badgeButton.edgeInsetTop = 10
        badgeButton.setImage(UIImage(named : "notification"), for: .normal)
        print(appDelegate.NCount)
         badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        badgeButton.badgeString = appDelegate.NCount
        if appDelegate.NCount == "0"
        {
            badgeButton.badgeBackgroundColor = UIColor.clear
            badgeButton.badgeTextColor = UIColor.clear
        }
        else
        {
            badgeButton.badgeBackgroundColor = colorGreen
            badgeButton.badgeTextColor = UIColor.white
        }
        badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        let barButton2 : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton2
        getUserGetUserInvoice()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)
    }
    
    
    // MARK: - Google Api Calling
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            //self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                
                badgeButton.badgeString = appDelegate.NCount
                if appDelegate.NCount == "0"
                {
                    badgeButton.badgeBackgroundColor = UIColor.clear
                    badgeButton.badgeTextColor = UIColor.clear
                }
                else
                {
                    badgeButton.badgeBackgroundColor = colorGreen
                    badgeButton.badgeTextColor = UIColor.white
                }
            }
        }
    }
    
    // MARK:- goBack
    
    @objc func SideMenu()
    {
//        if appDelegate.isFromNotification == true
//        {
//            SJSwiftSideMenuController.toggleLeftSideMenu()
//        }
//        else
//        {
            self.navigationController?.popViewController(animated: true)
       // }

    }
    
    @objc func NotificationAlert()
    {
        guard let vcs = self.navigationController?.viewControllers else{return}
        let inStack = vcs.contains(where: { (vc) -> Bool in
            return vc is MyNotificationList_Vc
        })
        if inStack{
            self.performSegue(withIdentifier: unwindToNotificationController, sender: self)
        }else{
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
//        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
//
//        self.navigationController?.pushViewController(nextViewController, animated: true)

    }
    
    // MARK: - onClickPayOrPaid
    @IBAction func onClickPayOrPaid(_ sender:UIButton!) {
        
        if let paidAmount = (arrScheduled.object(at: sender.tag) as! NSDictionary).value(forKey: "totaldue") as? NSNumber
        {
           strPaidAmaunt = "\(paidAmount)"
        }
        
        if let paidAmount = (arrScheduled.object(at: sender.tag) as! NSDictionary).value(forKey: "intId") as? NSNumber
        {
            appDelegate.ExtraInvoiceOderId = "\(paidAmount)"
        }
        else if let paidAmount = (arrScheduled.object(at: sender.tag) as! NSDictionary).value(forKey: "intId") as? String
        {
            appDelegate.ExtraInvoiceOderId = paidAmount
        }
        
        
        paymentRequest = preparePaymentRequest()
        appDelegate.isPayExtraInvoice = "2"
        performSegue(withIdentifier: "TelrSegue", sender: paymentRequest)
        
//        if let name = (arrScheduled.object(at: sender.tag) as! NSDictionary).value(forKey: "chrExtraPayment") as? String
//        {
////            if name == "Y"
////            {
//                paymentRequest = preparePaymentRequest()
//                appDelegate.isPayExtraInvoice = true
//                performSegue(withIdentifier: "TelrSegue", sender: paymentRequest)
//           //}
//
//        }
       
    }
    // MARK: - onClickView
    @IBAction func onClickView(_ sender:UIButton!) {
        // do cool stuff here
        
        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "webview_Vc") as! webview_Vc
        if let varCar = (arrScheduled.object(at: sender.tag) as! NSDictionary).value(forKey: "varCar") as? String ,let varModel = (arrScheduled.object(at: sender.tag) as! NSDictionary).value(forKey: "varModel") as? String
        {
            nextViewController.strName  = varCar + " " + varModel
        }
        
        if let name = (arrScheduled.object(at: sender.tag) as! NSDictionary).value(forKey: "invoicePdf") as? String
        {
            if name == ""
            {
               AppHelper.showAlert("app_alert_title".localized(), message: "invoice_pdf".localized())
            }
            else
            {
               nextViewController.URL  = name
                 self.navigationController?.pushViewController(nextViewController, animated: true)
            }
            
        }
        
       
    }
    
    
    // MARK: - tableview Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrScheduled.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Myinvoice_Cell" , for: indexPath) as! Myinvoice_Cell
        
        
        
        if let name = (arrScheduled.object(at: indexPath.row) as! NSDictionary).value(forKey: "varPackageDate") as? String
        {
            cell.lblDate.text  = name
            
            let expectedLabelSize = name.boundingRect(with: CGSize(width: 50, height:cell.lblDate.frame.height), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: cell.lblDate.font], context: nil)
            cell.PlateNoWidthContant.constant = expectedLabelSize.width
            cell.viewPlateWidthConstant.constant = 75 + expectedLabelSize.width
            view.layoutIfNeeded()
        }
        
        if let name = (arrScheduled.object(at: indexPath.row) as! NSDictionary).value(forKey: "varYear") as? String
        {
            cell.lblYear.text  = name
        }
        
        
        if let paidAmount = (arrScheduled.object(at: indexPath.row) as! NSDictionary).value(forKey: "totaldue") as? NSNumber
        {
            cell.lblPrice.text  = "\("invoice_due_amount".localized()) \(paidAmount) \("app_currency".localized())"
        }
        
       
        if let  totaldue = (arrScheduled.object(at: indexPath.row) as! NSDictionary).value(forKey: "totaldue") as? NSNumber
        {
            if totaldue == 0
            {
               cell.btnPay.isHidden = true
                cell.lblPrice.isHidden = true
                cell.lblPriceHeightConstant.constant = 0
                cell.btnViewCenterConstant.constant = 0
            }
            else
            {
                 cell.btnPay.isHidden = false
                 cell.lblPrice.isHidden = false
                cell.lblPriceHeightConstant.constant = 20
                cell.btnViewCenterConstant.constant = -20
            }
        }
        else if let  totaldue = (arrScheduled.object(at: indexPath.row) as! NSDictionary).value(forKey: "totaldue") as? String
        {
            if totaldue == "0"
            {
                cell.btnPay.isHidden = true
                cell.lblPrice.isHidden = true
            }
            else
            {
                cell.btnPay.isHidden = false
                cell.lblPrice.isHidden = false
            }
        }
       
        
        if let name = (arrScheduled.object(at: indexPath.row) as! NSDictionary).value(forKey: "chrExtraPayment") as? String
        {
            if name == "Y"
            {
                cell.btnPay.backgroundColor = colorGreen
                cell.btnPay.setTitle("invoice_paid".localized(), for: .normal)
            }
            else
            {
                cell.btnPay.setTitle("invoice_pay".localized(), for: .normal)
                cell.btnPay.backgroundColor = colorRed
            }
        }
        
        cell.btnView.tag = indexPath.row
        cell.btnPay.tag = indexPath.row
        cell.btnView.setTitle("invoice_view".localized(), for: .normal)
        
        cell.btnView.addTarget(self, action: #selector(self.onClickView(_:)), for: .touchUpInside)
        cell.btnPay.addTarget(self, action: #selector(self.onClickPayOrPaid(_:)), for: .touchUpInside)
        
        if let varCar = (arrScheduled.object(at: indexPath.row) as! NSDictionary).value(forKey: "varCar") as? String ,let varModel = (arrScheduled.object(at: indexPath.row) as! NSDictionary).value(forKey: "varModel") as? String
        {
            cell.lblCarName.text  = varCar + " " + varModel
        }
        
        
        return cell
    }
    
    
    // MARK: - getUserGetUserInvoice API
    
    func getUserGetUserInvoice()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["userId" : nUserId as AnyObject]
          
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objGetScheduleService = HttpWrapper.init()
            self.objGetScheduleService.delegate = self
            self.objGetScheduleService.requestWithparamdictParamPostMethodwithHeaderAndDictGet(url: kgetInvoice, dictParams: dictParameters, headers: header)
        }
        
    }
    
    
    // MARK: - HttpWrapperfetchDataSuccess
    
    func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary)
    {
        AppHelper.hideLoadingView()
        print(dicsResponse)
        if wrapper == objGetScheduleService {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                arrScheduled.removeAllObjects()
                
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray
                {
                    arrScheduled = tempNames.mutableCopy() as! NSMutableArray
                }
                print(arrScheduled)
                
                tblMain.reloadData()
            }
            else
            {
                tblMain.isHidden = true
                
                AppHelper.showAlert("app_alert_title".localized(), message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }
        
        
    }
    // MARK: - HttpWrapperfetchDataFail
    
    func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError)
    {
        AppHelper.hideLoadingView()
        print(error.debugDescription)
    }
    
    private func preparePaymentRequest() -> PaymentRequest{
        if let UserEmail = UserDefaults.standard.value(forKey: KUserLoginEmail) as? String{
            self.EMAIL = UserEmail
        }
        let paymentReq = PaymentRequest()
        paymentReq.key = KEY
        paymentReq.store = STOREID
        paymentReq.appId = "123456789"
        paymentReq.appName = "ServiceMyCar"
        paymentReq.appUser = "123456"
        paymentReq.appVersion = "0.0.1"
        paymentReq.transTest = Istesting
        paymentReq.transType = "sale"
        paymentReq.transClass = "paypage"
        paymentReq.transCartid = String(arc4random())
        paymentReq.transDesc = isLive ? "Booking on Servicemycar.ae" : "service payment"
        paymentReq.transCurrency = "AED"
        
       
        appDelegate.Bookings.telr_value = strPaidAmaunt
        paymentReq.transAmount = strPaidAmaunt
        
        paymentReq.transLanguage = "en"
        paymentReq.billingEmail = EMAIL
        
        let name = UserLName.split(separator: " ")
        
        
        paymentReq.billingFName = "\(name[0])"
        paymentReq.billingLName = "\(name[1])"
        paymentReq.billingTitle = "Mr"
        paymentReq.city = "Dubai"
        paymentReq.country = "AE"
        paymentReq.region = "Dubai"
        paymentReq.address = "line 1"
        paymentReq.billingPhone = UserPhoneNo
        
        return paymentReq
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? TelrController{
            destination.paymentRequest = paymentRequest!
            
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
