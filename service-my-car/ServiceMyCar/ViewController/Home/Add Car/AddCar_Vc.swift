//
//  AddCar_Vc.swift
//  ServiceMyCar
//
//  Created by baps on 22/01/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


protocol AddCar_VcDelegate {
   func editCarInCarList(_ viewController:UIViewController, didSuccess success:Bool)
}

class AddCar_Vc: UIViewController ,UITextFieldDelegate , UIPickerViewDelegate , UIPickerViewDataSource ,HttpWrapperDelegate{

    
   
    @IBOutlet weak var txtPlateNumber: UITextField!
    @IBOutlet weak var btnSaveMyCarDetails: UIButtonX!
    @IBOutlet weak var txtEngineCC: UITextField!
    @IBOutlet weak var txtYear: UITextField!
    @IBOutlet weak var txtPlateCode: UITextField!
    @IBOutlet weak var txtModel: UITextField!
    @IBOutlet weak var txtSelectCar: UITextField!
    @IBOutlet weak var txtState: UITextField!

    
    @IBOutlet weak var plateNumberView: UIView!
    @IBOutlet weak var engineCCView: UIView!
    @IBOutlet weak var yearView: UIView!
    @IBOutlet weak var plateCodeView: UIView!
    @IBOutlet weak var modelView: UIView!
    @IBOutlet weak var selectCarView: UIView!
    @IBOutlet weak var stateView: UIView!
    
    var myPickerView : UIPickerView!
    var SelectedCarIndex : Int = Int()
    var objGetAllCar = HttpWrapper()
     var objGetCarModel = HttpWrapper()
    var objGetAllYears = HttpWrapper()
    var objgetTrimEngine = HttpWrapper()
    var objaddCar = HttpWrapper()
    var objGetState = HttpWrapper()
    
    var objEditCar = HttpWrapper()
    
    var stateItems = [State]()
    
    var net:NetworkReachabilityManager = NetworkReachabilityManager()!
    var arrCarlist:NSMutableArray = NSMutableArray()
    var arrCarModel:NSMutableArray = NSMutableArray()
    var arrCarYears:NSMutableArray = NSMutableArray()
     var arrCarEngine:NSMutableArray = NSMutableArray()
    var arrStateList:NSMutableArray = NSMutableArray()
    let badgeButton : MIBadgeButton = MIBadgeButton(frame:CGRect(x: -10, y: 15, width: 40, height: 20))
    
    
    var isCarEdit:Bool = false
    var editCar = NSDictionary()
    
    var delegate: AddCar_VcDelegate?
    
    
    func setFontAndColor() {
        //CORNER RADIUS:
        
        btnSaveMyCarDetails.backgroundColor = colorGreen
        txtSelectCar.placeholder = "add_your_car_select_car_placeholder".localized()
        txtYear.placeholder = "add_your_car_select_year_placeholder".localized()
        txtModel.placeholder = "add_your_car_choose_model_placeholder".localized()
        txtEngineCC.placeholder = "add_your_car_choose_engine_size_cc_placeholder".localized()
        txtPlateCode.placeholder = "add_your_car_plate_code_placeholder".localized()
        txtPlateNumber.placeholder = "add_your_car_plate_number_placeholder".localized()
        txtState.placeholder = "Select State".localized()
        if !isCarEdit{
        btnSaveMyCarDetails.setTitle("Save My Car".localized(), for: .normal)
        }else{
            btnSaveMyCarDetails.setTitle("UPDATE".localized(), for: .normal)
        }
        
        txtState.delegate = self
        txtModel.delegate = self
        txtYear.delegate = self
        txtSelectCar.delegate = self
        txtEngineCC.delegate = self
        txtState.textColor = Colorblack
        txtModel.textColor = Colorblack
        txtPlateCode.textColor = Colorblack
        txtSelectCar.textColor = Colorblack
        txtYear.textColor = Colorblack
        txtEngineCC.textColor = Colorblack
        txtPlateNumber.textColor = Colorblack
        
        
        
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        if languageCode != "en" {
            txtState.textAlignment = .right
            txtModel.textAlignment = .right
            txtYear.textAlignment = .right
            txtSelectCar.textAlignment = .right
            txtEngineCC.textAlignment = .right
            txtPlateCode.textAlignment = .right
            txtPlateNumber.textAlignment = .right
        }

        
        //txtState.text = "add_your_car_dubai".localized()
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.clipsToBounds = false
        self.navigationItem.setHidesBackButton(true, animated:true)
        self.navigationController?.makeTransparent()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.makeTransparent()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setFontAndColor()
        setDrawView()
        self.navigationController?.isNavigationBarHidden = false
        let lbNavTitle = UILabel(frame: CGRect(x: CGFloat(35), y:self.view.bounds.size.width/2-235, width: CGFloat(200), height: CGFloat(40)))
        lbNavTitle.textAlignment = .center
        if isCarEdit{
           lbNavTitle.text = "Edit Car Details".localized()
        }else{
        lbNavTitle.text = "add_your_car_title".localized()
        }
        txtPlateNumber.delegate = self
        txtPlateCode.delegate = self
        
        lbNavTitle.textColor = Colorblack
        lbNavTitle.font = UIFont(name: FontBold, size: 16)
     
        self.navigationItem.titleView = lbNavTitle
        self.navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.white
        
        let titleDict: NSDictionary = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [NSAttributedString.Key : Any]
        
        let button = UIButton.init(type: .custom)
        //backarrow , menu
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        
        if languageCode == "en" {
            button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
        }
        else {
            button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
        }
        button.addTarget(self, action:#selector(goBack), for: UIControl.Event.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton

        GetAllCar()
        getYears()
        GetAllState()
        badgeButton.edgeInsetTop = 10
        badgeButton.setImage(UIImage(named : "notification"), for: .normal)
        print(appDelegate.NCount)
         badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        badgeButton.badgeString = appDelegate.NCount
        if appDelegate.NCount == "0"{
            badgeButton.badgeBackgroundColor = UIColor.clear
            badgeButton.badgeTextColor = UIColor.clear
        }else{
            badgeButton.badgeBackgroundColor = colorGreen
            badgeButton.badgeTextColor = UIColor.white
        }
        badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        let barButton2 : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton2
        //        getPackagesListByCategory()
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)
        
        
    }
    
    // Step 3
    
    @objc func NotificationAlert()
    {
        guard let vcs = self.navigationController?.viewControllers else{return}
        let inStack = vcs.contains(where: { (vc) -> Bool in
            return vc is MyNotificationList_Vc
        })
        if inStack{
            self.performSegue(withIdentifier: unwindToNotificationController, sender: self)
        }else{
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        //        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
        //
        //        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    // Step 4
    
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            //self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                
                badgeButton.badgeString = appDelegate.NCount
                if appDelegate.NCount == "0"{
                    badgeButton.badgeBackgroundColor = UIColor.clear
                    badgeButton.badgeTextColor = UIColor.clear
                }else{
                    badgeButton.badgeBackgroundColor = colorGreen
                    badgeButton.badgeTextColor = UIColor.white
                }
            }
        }
    }
    
    // MARK:- goBack
    
    @objc func goBack()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    //Mark:- SetDrawVeiwLayout
    
    func setDrawView(){
        AppHelper.makeViewCircularWithRespectToHeight(stateView, borderColor: .lightGray, borderWidth: 1.0)
        AppHelper.makeViewCircularWithRespectToHeight(plateCodeView, borderColor: .lightGray, borderWidth: 1.0)
        AppHelper.makeViewCircularWithRespectToHeight(plateNumberView, borderColor: .lightGray, borderWidth: 1.0)
        AppHelper.makeViewCircularWithRespectToHeight(modelView, borderColor: .lightGray, borderWidth: 1.0)
        AppHelper.makeViewCircularWithRespectToHeight(engineCCView, borderColor: .lightGray, borderWidth: 1.0)
        AppHelper.makeViewCircularWithRespectToHeight(yearView, borderColor: .lightGray, borderWidth: 1.0)
        AppHelper.makeViewCircularWithRespectToHeight(selectCarView, borderColor: .lightGray, borderWidth: 1.0)
        AppHelper.makeViewCircularWithRespectToHeight(btnSaveMyCarDetails, borderColor: .lightGray, borderWidth: 1.0)

        if isCarEdit{
            self.txtYear.text = editCar.value(forKey: "varYear") as? String
            self.txtPlateCode.text = editCar.value(forKey: "varPlateCode") as? String
            self.txtModel.text = editCar.value(forKey: "varModel") as? String
            self.txtState.text = editCar.value(forKey: "varCarState") as? String
            self.txtSelectCar.text = editCar.value(forKey: "varCar") as? String
            self.txtPlateNumber.text = editCar.value(forKey: "varPlateNumber") as? String
            self.txtEngineCC.text = editCar.value(forKey: "varTrimEngine") as? String
            getCarModels()
            getTrimEngine()
        }
        

    }
    
    
    
    
    
    
    
   
 // MARK: - Button Actions
    
    
    // MARK: - onClickBtnSaveMyCar Actions
    
    @IBAction func onClickBtnSaveMyCar(_ sender: Any) {
        if !isCarEdit{
        addCar()
        }else{
            editCarByUser()
        }
    }
    
    func pickUp(_ textField : UITextField){
        
        // UIPickerView
        self.myPickerView = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.myPickerView.delegate = self
        self.myPickerView.dataSource = self
        self.myPickerView.backgroundColor = UIColor.white
        
        textField.inputView = self.myPickerView
        myPickerView.reloadAllComponents()
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .black
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.white
        toolBar.sizeToFit()
        toolBar.backgroundColor = colorGreen
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "add_your_car_done".localized(), style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "add_your_car_cancel".localized(), style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    @objc func doneClick() {
        txtState.resignFirstResponder()
        txtModel.resignFirstResponder()
        txtYear.resignFirstResponder()
        txtEngineCC.resignFirstResponder()
        txtSelectCar.resignFirstResponder()
    }
    @objc func cancelClick() {
        
        txtState.resignFirstResponder()
        txtModel.resignFirstResponder()
        txtYear.resignFirstResponder()
        txtEngineCC.resignFirstResponder()
        txtSelectCar.resignFirstResponder()
    }
    
    //MARK:- PickerView Delegate & DataSource
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1
        {
            return arrStateList.count
        }
        else if pickerView.tag == 2
        {
            return arrCarModel.count
        }
        else if pickerView.tag == 3
        {
            return arrCarYears.count
        }
        else if pickerView.tag == 4
        {
            return arrCarEngine.count
        }
        else if pickerView.tag == 5
        {
            return arrCarlist.count
        }
        else
        {
            return 0
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label: UILabel
        
        if let view = view {
            label = view as! UILabel
        }
        else {
            label = UILabel(frame: CGRect(x: 0, y: 0, width: pickerView.frame.width, height: 400))
        }
        
        label.lineBreakMode = .byWordWrapping;
        if pickerView.tag == 1
        {
            label.text = (arrStateList.object(at: row) as! NSDictionary).value(forKey: "varStateName") as? String
        }
        else if pickerView.tag == 2
        {
            label.text = (arrCarModel.object(at: row) as! NSDictionary).value(forKey: "varMake") as? String
        }
        else if pickerView.tag == 3
        {
            label.text = "\(arrCarYears.object(at: row) as! NSNumber)"
        }
        else if pickerView.tag == 4
        {
            label.text = (arrCarEngine.object(at: row) as! NSDictionary).value(forKey: "trimengine") as? String
        }
        else if pickerView.tag == 5
        {
            label.text = (arrCarlist.object(at: row) as! NSDictionary).value(forKey: "varModel") as? String
        }
        else
        {
            label.text = "tet"
        }
        label.numberOfLines = 0;
        //label.text = arr[row]
        label.sizeToFit()
        return label;
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1
        {
            self.txtState.text = (arrStateList.object(at: row) as! NSDictionary).value(forKey: "varStateName") as? String
//            let selectState = self.txtState.text ?? ""
//            if self.stateItems.contains(where: {($0.state.capitalized == selectState.capitalized) && ($0.activeState == "Y")}){
//            }else{
//                self.txtState.text = ""
//                AppHelper.showAlert("SerivceMyCar".localized(), message: "Sorry we are currently not operating in your emirate and will be launching soon. Keep upto date by following us on Instagram @servicemycar.ae thank you".localized())
//            }

        }
        else  if pickerView.tag == 2
        {
            self.txtModel.text = (arrCarModel.object(at: row) as! NSDictionary).value(forKey: "varMake") as? String
        }
        else  if pickerView.tag == 3
        {
            self.txtYear.text = "\(arrCarYears.object(at: row) as! NSNumber)"
        }
        else  if pickerView.tag == 4
        {
            self.txtEngineCC.text = (arrCarEngine.object(at: row) as! NSDictionary).value(forKey: "trimengine") as? String
        }
        else  if pickerView.tag == 5
        {
            self.txtSelectCar.text = (arrCarlist.object(at: row) as! NSDictionary).value(forKey: "varModel") as? String
            SelectedCarIndex = row
        }
        else
        {
           // self.txtCarType.text = arrCarType[row]

        }
        
        
        
        // strCountryCode = (arrStores.object(at: row) as! NSDictionary).value(forKey: "cLocationName") as? String
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtState
        {
          
        }
        else if textField == txtSelectCar
        {
//            myPickerView.tag = 2
//            getCarModels()
           // getTrimEngine()

        //***********Mark:- Comment by Dinesh
            if textField.text?.count != 0{
                getCarModels()
                
            }
            
            if let value =  (arrCarlist.object(at: SelectedCarIndex) as! NSDictionary).value(forKey: "isExpensive") as? NSString
            {
                if value != "0"
                {
                    let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "AddExpensiveCar_Vc") as! AddExpensiveCar_Vc

                    self.navigationController?.pushViewController(nextViewController, animated: true)
                }

            }
            else
            {

            }
            

            
        }
        else if textField == txtModel
        {
            //            myPickerView.tag = 2
            //            getCarModels()
            
            if textField.text?.count != 0 {
                getTrimEngine()

            }
           // getCarModels()
        }
    }
    
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtState{
            if arrStateList.count == 0{return}
            self.pickUp(txtState)
            myPickerView.tag = 1
        }else if textField == txtModel{
           if txtSelectCar.text?.count == 0{
                AppHelper.showAlert("SerivceMyCar".localized(), message: "Please first select your car".localized())
                return
            }
            if arrCarModel.count == 0{return}
            txtEngineCC.text = ""
            self.pickUp(txtModel)
            myPickerView.tag = 2
        }else if textField == txtYear{
            if arrCarYears.count == 0{return}
            self.pickUp(txtYear)
            myPickerView.tag = 3
        }else if textField == txtEngineCC{
            if txtSelectCar.text?.count == 0{
                AppHelper.showAlert("SerivceMyCar".localized(), message: "Please first select your car".localized())
                return
            }
            if txtModel.text?.count == 0{
                AppHelper.showAlert("SerivceMyCar".localized(), message: "Please first select your car model".localized())
                return
            }
            if arrCarEngine.count == 0{return}
            self.pickUp(txtEngineCC)
            myPickerView.tag = 4
        }else if textField == txtSelectCar{
            if arrCarlist.count == 0{return}
            self.pickUp(txtSelectCar)
             txtModel.text = ""
            myPickerView.tag = 5
        }

        
    }
    
    
    // MARK: - GetAllState API
    
    func GetAllState()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["userId" : nUserId as AnyObject]
          
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objGetState = HttpWrapper.init()
            self.objGetState.delegate = self
         
            self.objGetState.requestWithparamdictParamPostMethodwithHeaderGet(url: kstateList, headers: header)
        }
        
    }
    
    // MARK: - getUserCars API
    
    func GetAllCar()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["userId" : nUserId as AnyObject]
       
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objGetAllCar = HttpWrapper.init()
            self.objGetAllCar.delegate = self
            self.objGetAllCar.requestWithparamdictParamPostMethodwithHeaderGet(url: kgetCars, headers: header)
        }
        
    }
    
    // MARK: - getCarModels API
    
    func getCarModels()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["carName" : txtSelectCar.text as AnyObject]
           
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objGetCarModel = HttpWrapper.init()
            self.objGetCarModel.delegate = self
            self.objGetCarModel.requestWithparamdictParamPostMethodwithHeaderAndDictGet(url: kgetCarModels, dictParams: dictParameters, headers: header)
        }
        
    }
    
    // MARK: - getTrimEngine API
    
    func getTrimEngine()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["modelName" : txtModel.text as AnyObject]
          
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objgetTrimEngine = HttpWrapper.init()
            self.objgetTrimEngine.delegate = self
           
            self.objgetTrimEngine.requestWithparamdictParamPostMethodwithHeaderAndDictGet(url: kgetTrimEngine, dictParams: dictParameters, headers: header)

        }
        
    }
    
    // MARK: - addCar API
    
    func addCar()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else if isLoginDataValid()
        {
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["userId" : nUserId as AnyObject,
                                                     "varPlateCode" : txtPlateCode.text as AnyObject,
                                                     "varPlateNumber" : txtPlateNumber.text as AnyObject,
                                                     "varYear" : txtYear.text as AnyObject,
                                                     "varCar" : txtSelectCar.text as AnyObject,
                                                     "varModel" : txtModel.text as AnyObject,
                                                     "varTrimEngine" : txtEngineCC.text as AnyObject,
                                                     "varCarState" : txtState.text as AnyObject]
          
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objaddCar = HttpWrapper.init()
            self.objaddCar.delegate = self
            self.objaddCar.requestWithparamdictParamPostMethodwithHeader(url: kaddUserCar, dicsParams: dictParameters, headers: header)
        }
        
    }
    
    
    // MARK: - getYears API
    
    func getYears()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["carName" : txtSelectCar.text as AnyObject]
          
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objGetAllYears = HttpWrapper.init()
            self.objGetAllYears.delegate = self
            self.objGetAllYears.requestWithparamdictParamPostMethodwithHeaderGet(url: kgetLaunchYears, headers: header)

        }
        
    }
    
    
    // MARK: - Edit Car API
    
    func editCarByUser(){
        if net.isReachable == false{
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            var carID = ""
            if let carId = editCar.value(forKey: "id") as? String{
                carID = carId
            }else if let carId = editCar.value(forKey: "id") as? Int{
                carID = "\(carId)"
            }
            let dictParameters:[String:AnyObject] = ["carId" : carID as AnyObject,
                                                     "varPlateCode" : txtPlateCode.text as AnyObject,
                                                     "varPlateNumber" : txtPlateNumber.text as AnyObject,
                                                     "varYear" : txtYear.text as AnyObject,
                                                     "varCar" : txtSelectCar.text as AnyObject,
                                                     "varModel" : txtModel.text as AnyObject,
                                                     "varTrimEngine" : txtEngineCC.text as AnyObject,
                                                     "varCarState" : txtState.text as AnyObject]

            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objEditCar = HttpWrapper.init()
            self.objEditCar.delegate = self
            self.objEditCar.requestWithparamdictParamPutMethodwithHeader(url: kEditUserCar, dicsParams: dictParameters, headers: header)
            //self.objEditCar.requestWithparamdictParamPostMethodwithHeaderGet(url: kEditUserCar, headers: header)
            
        }
        

        
    }

    
    
    // MARK: - HttpWrapperfetchDataSuccess
    
    func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary)
    {
        AppHelper.hideLoadingView()
        print(dicsResponse)
        if wrapper == objGetAllCar {
            if dicsResponse.value(forKey: "status") as! NSString == "success"{
                arrCarlist.removeAllObjects()
                
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray{
                    arrCarlist = tempNames.mutableCopy() as! NSMutableArray
                }
                print(arrCarlist)
               // collectionviewselect.reloadData()
            }
            else{
                AppHelper.showAlert("app_alert_title".localized(), message:dicsResponse.value(forKey: "msg") as! String)
            }
            
        }else if wrapper == objGetCarModel {
            if dicsResponse.value(forKey: "status") as! NSString == "success"{
                arrCarModel.removeAllObjects()
               // txtModel.resignFirstResponder()
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray{
                    arrCarModel = tempNames.mutableCopy() as! NSMutableArray
                }
                print(arrCarModel)
                // collectionviewselect.reloadData()
            }else{
                AppHelper.showAlert("app_alert_title".localized(), message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }else if wrapper == objGetAllYears {
            if dicsResponse.value(forKey: "status") as! NSString == "success"{
                arrCarYears.removeAllObjects()
                // txtModel.resignFirstResponder()
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray{
                    arrCarYears = tempNames.mutableCopy() as! NSMutableArray
                }
                print(arrCarYears)
                
                // collectionviewselect.reloadData()
            }else{
                AppHelper.showAlert("app_alert_title".localized(), message:dicsResponse.value(forKey: "msg") as! String)
            }
            
        }else if wrapper == objgetTrimEngine {
            if dicsResponse.value(forKey: "status") as! NSString == "success"{
                arrCarEngine.removeAllObjects()
                // txtModel.resignFirstResponder()
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray{
                    arrCarEngine = tempNames.mutableCopy() as! NSMutableArray
                }
                print(arrCarEngine)
                // collectionviewselect.reloadData()
            }else{
                AppHelper.showAlert("app_alert_title".localized(), message:dicsResponse.value(forKey: "msg") as! String)
            }
        }else if wrapper == objaddCar {
            if dicsResponse.value(forKey: "status") as! NSString == "success"{
//                arrCarEngine.removeAllObjects()
//                // txtModel.resignFirstResponder()
//                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray
//                {
//                    arrCarEngine = tempNames.mutableCopy() as! NSMutableArray
//                }
//                print(arrCarEngine)
                
//                self.navigationController?.popViewController(animated: true)
                
                // collectionviewselect.reloadData()
                self.delegate?.editCarInCarList(self, didSuccess: true)
                self.navigationController?.popViewController(animated: true)


            }else{
                AppHelper.showAlert("app_alert_title".localized(), message:dicsResponse.value(forKey: "msg") as! String)
            }
        }else if wrapper == objGetState {
            if dicsResponse.value(forKey: "status") as! NSString == "success"{
                        arrStateList.removeAllObjects()
                                // txtModel.resignFirstResponder()
                        if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray{
                            arrStateList = tempNames.mutableCopy() as! NSMutableArray
                        }
                    for dct in arrStateList {
                    let data = dct as! NSDictionary
                    // self.resultsArray.append(dct)
                    
                    let state = State(dict: data as! Dictionary<String, AnyObject>)
                    self.stateItems.append(state)
                    
                }
                        print(arrStateList)
                // collectionviewselect.reloadData()
            }else{
                AppHelper.showAlert("app_alert_title".localized(), message:dicsResponse.value(forKey: "msg") as! String)
            }
        }else if wrapper == objEditCar {
            if dicsResponse.value(forKey: "status") as! NSString == "success"{
                //self.navigationController?.popViewController(animated: true)
                self.delegate?.editCarInCarList(self, didSuccess: true)
                self.navigationController?.popViewController(animated: true)

            }else{
                AppHelper.showAlert("app_alert_title".localized(), message:dicsResponse.value(forKey: "msg") as! String)
            }
        }
        
    }
    // MARK: - HttpWrapperfetchDataFail
    
    func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError)
    {
        AppHelper.hideLoadingView()
        print(error.debugDescription)
    }
    
    // MARK: - isLoginDataValid
    func isLoginDataValid() -> Bool
    {
        
        if (txtSelectCar.text?.trimmingCharacters(in: CharacterSet.whitespaces).count)! <= 0
        {
            AppHelper.showAlert("app_alert_title".localized(), message: "add_your_car_car_validation".localized())
            return false
        }
        
        if (txtState.text?.trimmingCharacters(in: CharacterSet.whitespaces).count)! <= 0
        {
            AppHelper.showAlert("app_alert_title".localized(), message: "add_your_car_state_validation".localized())
            return false
        }
        
        if (txtModel.text?.trimmingCharacters(in: CharacterSet.whitespaces).count)! <= 0
        {
            AppHelper.showAlert("app_alert_title".localized(), message: "add_your_car_model_validation".localized())
            return false
        }
        
        
        if (txtPlateCode.text?.trimmingCharacters(in: CharacterSet.whitespaces).count)! <= 0
        {
            AppHelper.showAlert("app_alert_title".localized(), message: "add_your_car_plate_code_validation".localized())
            return false
        }
        
        if (txtPlateNumber.text?.trimmingCharacters(in: CharacterSet.whitespaces).count)! <= 0
        {
            AppHelper.showAlert("app_alert_title".localized(), message: "add_your_car_plate_number_validation".localized())
            return false
        }
        
        if (txtYear.text?.trimmingCharacters(in: CharacterSet.whitespaces).count)! <= 0
        {
            AppHelper.showAlert("app_alert_title".localized(), message: "add_your_car_year_validation".localized())
            return false
        }
        
        if (txtEngineCC.text?.trimmingCharacters(in: CharacterSet.whitespaces).count)! <= 0
        {
            AppHelper.showAlert("app_alert_title".localized(), message: "add_your_car_engine_type_validation".localized())
            return false
        }
        
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = textField.text
        let currentString:NSString =  textField.text! as NSString
        let newString = currentString.replacingCharacters(in: range, with: string) as NSString
        if textField == txtPlateCode{
            let maxLenght = 2
            if text!.count <= maxLenght{
                return newString.length <= maxLenght
            }
        }else if textField == txtPlateNumber{
            let maxLenght = 5
            if text!.count <= maxLenght{
                return newString.length <= maxLenght
            }
        }
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
