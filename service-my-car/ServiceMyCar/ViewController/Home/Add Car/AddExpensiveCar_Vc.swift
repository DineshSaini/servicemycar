//
//  AddExpensiveCar_Vc.swift
//  ServiceMyCar
//
//  Created by baps on 24/01/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class AddExpensiveCar_Vc: UIViewController, UIPickerViewDelegate , UIPickerViewDataSource ,HttpWrapperDelegate ,UITextFieldDelegate{

    @IBOutlet weak var btnSubmit: UIButtonX!
    @IBOutlet weak var txtRegistrationNo: UITextField!
    @IBOutlet weak var txtEngineCC: UITextField!
    @IBOutlet weak var txtModel: UITextField!
    @IBOutlet weak var txtSelectCar: UITextField!
    @IBOutlet weak var txtYear: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var lblDetailText: UILabel!
    
    
    @IBOutlet weak var nameView:UIView!
    @IBOutlet weak var yearView:UIView!
    @IBOutlet weak var registrationView:UIView!
    @IBOutlet weak var engineCCView:UIView!
    @IBOutlet weak var modelView:UIView!
    @IBOutlet weak var phoneView:UIView!
    @IBOutlet weak var emailView:UIView!
    @IBOutlet weak var selectCarView:UIView!
    
    
    var myPickerView : UIPickerView!
    var SelectedCarIndex : Int = Int()
    var objGetAllCar = HttpWrapper()
    var objGetCarModel = HttpWrapper()
    var objGetAllYears = HttpWrapper()
    var objgetTrimEngine = HttpWrapper()
    var objaddCar = HttpWrapper()
    var objGetState = HttpWrapper()
    
    var net:NetworkReachabilityManager = NetworkReachabilityManager()!
    var arrCarlist:NSMutableArray = NSMutableArray()
    var arrCarModel:NSMutableArray = NSMutableArray()
    var arrCarYears:NSMutableArray = NSMutableArray()
    var arrCarEngine:NSMutableArray = NSMutableArray()
    var arrStateList:NSMutableArray = NSMutableArray()
    let badgeButton : MIBadgeButton = MIBadgeButton(frame:CGRect(x: -10, y: 15, width: 40, height: 20))
    func setFontAndColor() {
        //CORNER RADIUS:
        
        btnSubmit.backgroundColor = colorGreen
        
        txtModel.delegate = self
        txtYear.delegate = self
        txtSelectCar.delegate = self
        txtEngineCC.delegate = self
        txtEmail.textColor = Colorblack
        txtModel.textColor = Colorblack
       txtName.textColor = Colorblack
        txtSelectCar.textColor = Colorblack
        txtYear.textColor = Colorblack
        txtEngineCC.textColor = Colorblack
        txtRegistrationNo.textColor = Colorblack
        txtPhoneNumber.textColor = Colorblack
        
        txtName.placeholder = "Name".localized()
        txtEmail.placeholder = "Email".localized()
        txtPhoneNumber.placeholder = "Phone Number".localized()
        txtYear.placeholder = "Select Year".localized()
        txtModel.placeholder = "Choose Model".localized()
        txtSelectCar.placeholder = "Select Car".localized()
        txtEngineCC.placeholder = "Choose engine size cc".localized()
        txtRegistrationNo.placeholder = "Registration Number".localized()
        btnSubmit.setTitle("Submit".localized(), for: .normal)
        lblDetailText.text = "Wow your car is expensive, Your car needs extra love & attention and for this you require a bescope service. please call +971(0)585762247 or fill in your details below and press submit. One our advisors will be more than happy to help you within 24 hours.".localized()
        
        
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        if languageCode != "en" {
            txtEmail.textAlignment = .right
            txtModel.textAlignment = .right
            txtYear.textAlignment = .right
            txtSelectCar.textAlignment = .right
            txtEngineCC.textAlignment = .right
            txtPhoneNumber.textAlignment = .right
            txtRegistrationNo.textAlignment = .right
            txtName.textAlignment = .right

        }

        
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setFontAndColor()
        self.navigationController?.isNavigationBarHidden = false
        setupRoundCornerTextField()
        let lbNavTitle = UILabel(frame: CGRect(x: CGFloat(35), y:self.view.bounds.size.width/2-235, width: CGFloat(200), height: CGFloat(40)))
        lbNavTitle.textAlignment = .center
        lbNavTitle.text = "Add Your Car".localized()
        lbNavTitle.textColor = Colorblack
        lbNavTitle.font = UIFont(name: FontBold, size: 16)
        
//                let logo = UIImage(named: "AppTopIcon")
//                let imageView = UIImageView(image:logo)
//                imageView.contentMode = .scaleAspectFit
//         self.navigationItem.titleView = imageView
        self.navigationItem.titleView = lbNavTitle
        self.navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.white
        
        let titleDict: NSDictionary = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [NSAttributedString.Key : Any]
        
        let button = UIButton.init(type: .custom)
        //backarrow , menu
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        
        if languageCode == "en" {
            button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
        }
        else {
            button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
        }
        button.addTarget(self, action:#selector(goBack), for: UIControl.Event.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        badgeButton.edgeInsetTop = 10
        badgeButton.setImage(UIImage(named : "notification"), for: .normal)
        print(appDelegate.NCount)
         badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        badgeButton.badgeString = appDelegate.NCount
        if appDelegate.NCount == "0"
        {
            badgeButton.badgeBackgroundColor = UIColor.clear
            badgeButton.badgeTextColor = UIColor.clear
        }
        else
        {
            badgeButton.badgeBackgroundColor = colorGreen
            badgeButton.badgeTextColor = UIColor.white
        }
        
         badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        let barButton2 : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton2
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)
        
        GetAllCar()
        getYears()
        GetAllState()
       
    }
    
    
    
    func setupRoundCornerTextField(){
        AppHelper.makeViewCircularWithRespectToHeight(nameView, borderColor: .lightGray, borderWidth: 1.0)
        AppHelper.makeViewCircularWithRespectToHeight(emailView, borderColor: .lightGray, borderWidth: 1.0)
        AppHelper.makeViewCircularWithRespectToHeight(engineCCView, borderColor: .lightGray, borderWidth: 1.0)
        AppHelper.makeViewCircularWithRespectToHeight(selectCarView, borderColor: .lightGray, borderWidth: 1.0)
        AppHelper.makeViewCircularWithRespectToHeight(registrationView, borderColor: .lightGray, borderWidth: 1.0)
        AppHelper.makeViewCircularWithRespectToHeight(phoneView, borderColor: .lightGray, borderWidth: 1.0)
        AppHelper.makeViewCircularWithRespectToHeight(yearView, borderColor: .lightGray, borderWidth: 1.0)
        AppHelper.makeViewCircularWithRespectToHeight(modelView, borderColor: .lightGray, borderWidth: 1.0)
        AppHelper.makeViewCircularWithRespectToHeight(btnSubmit, borderColor: .lightGray, borderWidth: 0.0)

        
    }
    
    
    
    // MARK: - Google Api Calling
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            //self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                
                badgeButton.badgeString = appDelegate.NCount
                if appDelegate.NCount == "0"
                {
                    badgeButton.badgeBackgroundColor = UIColor.clear
                    badgeButton.badgeTextColor = UIColor.clear
                }
                else
                {
                    badgeButton.badgeBackgroundColor = colorGreen
                    badgeButton.badgeTextColor = UIColor.white
                }
                
            }
        }
    }
    
    
    @objc func NotificationAlert()
    {
        guard let vcs = self.navigationController?.viewControllers else{return}
        let inStack = vcs.contains(where: { (vc) -> Bool in
            return vc is MyNotificationList_Vc
        })
        if inStack{
            self.performSegue(withIdentifier: unwindToNotificationController, sender: self)
        }else{
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        //        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
        //
        //        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    // MARK:- goBack
    
    @objc func goBack()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
// MARK: - onClickBtnSubmit actions
    
    
    @IBAction func onClickBtnSubmit(_ sender: Any) {
        addCar()
    }
    
    func pickUp(_ textField : UITextField){
        
        // UIPickerView
        self.myPickerView = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.myPickerView.delegate = self
        self.myPickerView.dataSource = self
        self.myPickerView.backgroundColor = UIColor.white
        
        textField.inputView = self.myPickerView
        myPickerView.reloadAllComponents()
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .black
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.white
        toolBar.sizeToFit()
        toolBar.backgroundColor = colorGreen
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done".localized(), style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel".localized(), style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    @objc func doneClick() {
       // txtState.resignFirstResponder()
        txtModel.resignFirstResponder()
        txtYear.resignFirstResponder()
        txtEngineCC.resignFirstResponder()
        txtSelectCar.resignFirstResponder()
    }
    @objc func cancelClick() {
       // txtState.resignFirstResponder()
        txtModel.resignFirstResponder()
        txtYear.resignFirstResponder()
        txtEngineCC.resignFirstResponder()
        txtSelectCar.resignFirstResponder()
    }
    
    //MARK:- PickerView Delegate & DataSource
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1
        {
            return arrStateList.count
        }
        else if pickerView.tag == 2
        {
            return arrCarModel.count
        }
        else if pickerView.tag == 3
        {
            return arrCarYears.count
        }
        else if pickerView.tag == 4
        {
            return arrCarEngine.count
        }
        else if pickerView.tag == 5
        {
            return arrCarlist.count
        }
        else
        {
            return 0
        }
        
    }
    //    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    //        if pickerView.tag == 1
    //        {
    //            return (arrCarlist.object(at: row) as! NSDictionary).value(forKey: "varModel") as? String
    //        }
    //        else if pickerView.tag == 2
    //        {
    //            return (arrCarModel.object(at: row) as! NSDictionary).value(forKey: "varMake") as? String
    //        }
    //        else if pickerView.tag == 3
    //        {
    //            return "\(arrCarYears.object(at: row) as! NSNumber)"
    //        }
    //        else if pickerView.tag == 4
    //        {
    //            return (arrCarEngine.object(at: row) as! NSDictionary).value(forKey: "trimengine") as? String
    //        }
    //        else
    //        {
    //            return "tet"
    //        }
    //
    //
    //
    //    }
    //
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label: UILabel
        
        if let view = view {
            label = view as! UILabel
        }
        else {
            label = UILabel(frame: CGRect(x: 0, y: 0, width: pickerView.frame.width, height: 400))
        }
        
        label.lineBreakMode = .byWordWrapping;
        if pickerView.tag == 1
        {
            label.text = (arrStateList.object(at: row) as! NSDictionary).value(forKey: "varStateName") as? String
        }
        else if pickerView.tag == 2
        {
            label.text = (arrCarModel.object(at: row) as! NSDictionary).value(forKey: "varMake") as? String
        }
        else if pickerView.tag == 3
        {
            label.text = "\(arrCarYears.object(at: row) as! NSNumber)"
        }
        else if pickerView.tag == 4
        {
            label.text = (arrCarEngine.object(at: row) as! NSDictionary).value(forKey: "trimengine") as? String
        }
        else if pickerView.tag == 5
        {
            label.text = (arrCarlist.object(at: row) as! NSDictionary).value(forKey: "varModel") as? String
        }
        else
        {
            label.text = "tet"
        }
        label.numberOfLines = 0;
        //label.text = arr[row]
        label.sizeToFit()
        return label;
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1
        {
           // self.txtState.text = (arrCarlist.object(at: row) as! NSDictionary).value(forKey: "varModel") as? String
        }
        else  if pickerView.tag == 2
        {
            self.txtModel.text = (arrCarModel.object(at: row) as! NSDictionary).value(forKey: "varMake") as? String
        }
        else  if pickerView.tag == 3
        {
            self.txtYear.text = "\(arrCarYears.object(at: row) as! NSNumber)"
        }
        else  if pickerView.tag == 4
        {
            self.txtEngineCC.text = (arrCarEngine.object(at: row) as! NSDictionary).value(forKey: "trimengine") as? String
        }
        else  if pickerView.tag == 5
        {
            self.txtSelectCar.text = (arrCarlist.object(at: row) as! NSDictionary).value(forKey: "varModel") as? String
            SelectedCarIndex = row
        }
        else
        {
            // self.txtCarType.text = arrCarType[row]
            
        }
        
        
        
        // strCountryCode = (arrStores.object(at: row) as! NSDictionary).value(forKey: "cLocationName") as? String
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtYear
        {
            
        }
        else if textField == txtSelectCar
        {
            //            myPickerView.tag = 2
            //            getCarModels()
            // getTrimEngine()
            
//            if let value =  (arrCarlist.object(at: SelectedCarIndex) as! NSDictionary).value(forKey: "isExpensive") as? NSString
//            {
//                if value != "0"
//                {
//                    let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "AddExpensiveCar_Vc") as! AddExpensiveCar_Vc
//
//                    self.navigationController?.pushViewController(nextViewController, animated: true)
//                }
//
//            }
//            else
//            {
//
//            }
            getCarModels()
        }
        else if textField == txtModel
        {
            //            myPickerView.tag = 2
            //            getCarModels()
            getTrimEngine()
            // getCarModels()
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtModel
        {
            txtEngineCC.text = ""
            self.pickUp(txtModel)
            myPickerView.tag = 2
            
        }
        else if textField == txtYear
        {
            
            self.pickUp(txtYear)
            myPickerView.tag = 3
            
        }
        else if textField == txtEngineCC
        {
            
            self.pickUp(txtEngineCC)
            myPickerView.tag = 4
            
        }
        else if textField == txtSelectCar
        {
            
            self.pickUp(txtSelectCar)
            txtModel.text = ""
            myPickerView.tag = 5
            
        }
        
    }
    
    
    // MARK: - GetAllState API
    
    func GetAllState()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["userId" : nUserId as AnyObject]
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objGetState = HttpWrapper.init()
            self.objGetState.delegate = self
            self.objGetState.requestWithparamdictParamPostMethodwithHeaderGet(url: kstateList, headers: header)
        }
        
    }
    
    // MARK: - getUserCars API
    
    func GetAllCar()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["userId" : nUserId as AnyObject]
          
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objGetAllCar = HttpWrapper.init()
            self.objGetAllCar.delegate = self
            self.objGetAllCar.requestWithparamdictParamPostMethodwithHeaderGet(url: kgetCars, headers: header)
        }
        
    }
    
    // MARK: - getCarModels API
    
    func getCarModels()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["carName" : txtSelectCar.text as AnyObject]
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objGetCarModel = HttpWrapper.init()
            self.objGetCarModel.delegate = self
            self.objGetCarModel.requestWithparamdictParamPostMethodwithHeaderAndDictGet(url: kgetCarModels, dictParams: dictParameters, headers: header)
        }
        
    }
    
    // MARK: - getTrimEngine API
    
    func getTrimEngine()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["modelName" : txtModel.text as AnyObject]
          
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objgetTrimEngine = HttpWrapper.init()
            self.objgetTrimEngine.delegate = self
            self.objgetTrimEngine.requestWithparamdictParamPostMethodwithHeaderAndDictGet(url: kgetTrimEngine, dictParams: dictParameters, headers: header)
        }
        
    }
    
    // MARK: - addCar API

    func addCar()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }

        else if isLoginDataValid()
        {
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            
           
            let dictParameters:[String:AnyObject] = ["userId" : nUserId as AnyObject,
                                                     "varUserName" : txtName.text as AnyObject,
                                                     "varEmail" : txtEmail.text as AnyObject,
                                                     "varYear" : txtYear.text as AnyObject,
                                                     "varCar" : txtSelectCar.text as AnyObject,
                                                     "varModel" : txtModel.text as AnyObject,
                                                     "varPhone" : txtPhoneNumber.text as AnyObject,
                                                     "varRegisterPlateNumber" : txtRegistrationNo.text as AnyObject]
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objaddCar = HttpWrapper.init()
            self.objaddCar.delegate = self
            self.objaddCar.requestWithparamdictParamPostMethodwithHeader(url: ksubmitExpensiveCar, dicsParams: dictParameters, headers: header)
        }

    }
    
    
    // MARK: - getYears API
    
    func getYears()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["carName" : txtSelectCar.text as AnyObject]
         
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objGetAllYears = HttpWrapper.init()
            self.objGetAllYears.delegate = self
            self.objGetAllYears.requestWithparamdictParamPostMethodwithHeaderGet(url: kgetLaunchYears, headers: header)
        }
        
    }
    
    // MARK: - HttpWrapperfetchDataSuccess
    
    func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary)
    {
        AppHelper.hideLoadingView()
        print(dicsResponse)
        if wrapper == objGetAllCar {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                arrCarlist.removeAllObjects()
                
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray
                {
                    arrCarlist = tempNames.mutableCopy() as! NSMutableArray
                }
                print(arrCarlist)
                // collectionviewselect.reloadData()
            }
            else
            {
                AppHelper.showAlert("ServiceMyCar".localized(), message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }
        else if wrapper == objGetCarModel {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                arrCarModel.removeAllObjects()
                // txtModel.resignFirstResponder()
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray
                {
                    arrCarModel = tempNames.mutableCopy() as! NSMutableArray
                }
                print(arrCarModel)
                
                
                
                // collectionviewselect.reloadData()
            }
            else
            {
                AppHelper.showAlert("ServiceMyCar".localized(), message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }
        else if wrapper == objGetAllYears {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                arrCarYears.removeAllObjects()
                // txtModel.resignFirstResponder()
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray
                {
                    arrCarYears = tempNames.mutableCopy() as! NSMutableArray
                }
                print(arrCarYears)
                
                
                
                // collectionviewselect.reloadData()
            }
            else
            {
                AppHelper.showAlert("ServiceMyCar".localized(), message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }
        else if wrapper == objgetTrimEngine {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                arrCarEngine.removeAllObjects()
                // txtModel.resignFirstResponder()
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray
                {
                    arrCarEngine = tempNames.mutableCopy() as! NSMutableArray
                }
                print(arrCarEngine)
                
                
                
                // collectionviewselect.reloadData()
            }
            else
            {
                AppHelper.showAlert("ServiceMyCar".localized(), message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }
        else if wrapper == objaddCar {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                //                arrCarEngine.removeAllObjects()
                //                // txtModel.resignFirstResponder()
                //                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray
                //                {
                //                    arrCarEngine = tempNames.mutableCopy() as! NSMutableArray
                //                }
                //                print(arrCarEngine)
                
               // self.navigationController?.popViewController(animated: true)
                let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "Thankyou_Vc") as! Thankyou_Vc
                nextViewController.isFrom = "3"
                self.navigationController?.pushViewController(nextViewController, animated: true)
                // collectionviewselect.reloadData()
            }
            else
            {
               // AppHelper.showAlert("Alert", message:dicsResponse.value(forKey: "msg") as! String)
                let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "Thankyou_Vc") as! Thankyou_Vc
                
                nextViewController.isFrom = "0"
                self.navigationController?.pushViewController(nextViewController, animated: true)
            }
            
            
        }
        else if wrapper == objGetState {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                arrStateList.removeAllObjects()
                // txtModel.resignFirstResponder()
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray
                {
                    arrStateList = tempNames.mutableCopy() as! NSMutableArray
                }
                print(arrStateList)
                
                
                
                // collectionviewselect.reloadData()
            }
            else
            {
                AppHelper.showAlert("ServiceMyCar".localized(), message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }
        
    }
    // MARK: - HttpWrapperfetchDataFail
    
    func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError)
    {
        AppHelper.hideLoadingView()
        print(error.debugDescription)
    }
    
    // MARK: - isLoginDataValid
    func isLoginDataValid() -> Bool
    {
        
        if (txtName.text?.trimmingCharacters(in: CharacterSet.whitespaces).count)! <= 0
        {
            AppHelper.showAlert("ServiceMyCar".localized(), message: SignUpfName)
            return false
        }

        if !AppHelper.isValidEmail(txtEmail.text!)
        {
            AppHelper.showAlert("ServiceMyCar".localized(), message: signUpValidEmail)
            return false
        }
        
       
        
        if (txtEmail.text?.trimmingCharacters(in: CharacterSet.whitespaces).count)! <= 0
        {
            AppHelper.showAlert("ServiceMyCar".localized(), message: signUpEmail)
            return false
        }
        
        if (txtPhoneNumber.text?.trimmingCharacters(in: CharacterSet.whitespaces).count)! <= 0
        {
            AppHelper.showAlert("ServiceMyCar".localized(), message: SignUpNumber)
            return false
        }
        
        if (txtPhoneNumber.text?.count)! > 8{
            AppHelper.showAlert("app_alert_title".localized(), message: "login_Phone_number_Digit_Validation".localized())
            return false
        }
        
        if (txtYear.text?.trimmingCharacters(in: CharacterSet.whitespaces).count)! <= 0
        {
            AppHelper.showAlert("ServiceMyCar".localized(), message: SelectCarYear)
            return false
        }
        
        if (txtSelectCar.text?.trimmingCharacters(in: CharacterSet.whitespaces).count)! <= 0
        {
            AppHelper.showAlert("ServiceMyCar".localized(), message: SelectCar)
            return false
        }
        
        if (txtModel.text?.trimmingCharacters(in: CharacterSet.whitespaces).count)! <= 0
        {
            AppHelper.showAlert("ServiceMyCar".localized(), message: SelectCarModel)
            return false
        }
        

        
        if (txtEngineCC.text?.trimmingCharacters(in: CharacterSet.whitespaces).count)! <= 0
        {
            AppHelper.showAlert("ServiceMyCar".localized(), message: SelectEngineType)
            return false
        }
        
        if (txtRegistrationNo.text?.trimmingCharacters(in: CharacterSet.whitespaces).count)! <= 0
        {
            AppHelper.showAlert("ServiceMyCar".localized(), message: RegisterNumber)
            return false
        }

        
        
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
