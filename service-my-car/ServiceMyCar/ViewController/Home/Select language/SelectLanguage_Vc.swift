//
//  SelectLanguage_Vc.swift
//  ServiceMyCar
//
//  Created by baps on 23/01/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import Localize_Swift
class SelectLanguage_Vc: UIViewController {

    
    @IBOutlet var viewBlack: UIView!
    @IBOutlet var viewLanguage: UIView!
    @IBOutlet weak var viewInnerLanguage: UIView!
    @IBOutlet weak var lblChooseLanguage: UILabel!
    @IBOutlet weak var btnEnglish: UIButton!
    @IBOutlet weak var btnArabic: UIButton!
    @IBOutlet weak var btnConfirm: UIButton!
    var languageCode = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.clear
        viewLanguage.isUserInteractionEnabled = true
        let viewBackTap = UITapGestureRecognizer(target: self, action: #selector(cloeseLangaugeOpetion))
        viewBackTap.numberOfTapsRequired = 1
        viewLanguage.addGestureRecognizer(viewBackTap)
        
        viewInnerLanguage.layer.cornerRadius = 3
        viewInnerLanguage.clipsToBounds = true
        lblChooseLanguage.text = "select_language_choose_language".localized()
        btnArabic.setTitle("app_arabic".localized(), for: .normal)
        btnConfirm.setTitle("select_language_confirm".localized(), for: .normal)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
    @objc func cloeseLangaugeOpetion() {
        self.dismiss(animated: false, completion: nil)
    }
    
    
    @IBAction func onClickEnglish(_ sender: Any) {
        languageCode = "en"
        btnEnglish.setTitleColor(UIColor.init(hexString: "#489D27"), for: .normal)
        btnArabic.setTitleColor(UIColor.black, for: .normal)
    }
    
    @IBAction func onClickArabic(_ sender: Any) {
        languageCode = "ar"
        btnEnglish.setTitleColor(UIColor.black, for: .normal)
        btnArabic.setTitleColor(UIColor.init(hexString: "#489D27"), for: .normal)
    }
    
    @IBAction func onClickConfirm(_ sender: Any) {
        let newLanguage:Languages!
        UserDefaults.standard.set(languageCode, forKey: kAppLanguage)
        if languageCode == "en" {
            newLanguage = .en
        }else{
            newLanguage = .ar
        }
        Localize.setCurrentLanguage(languageCode)
        LanguageManager.shared.setLanguage(language: newLanguage)
        appDelegate.arrCategorylist.removeAllObjects()
        appDelegate.loginSuccess()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
