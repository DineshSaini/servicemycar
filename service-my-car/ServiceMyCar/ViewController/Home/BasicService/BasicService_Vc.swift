//
//  BasicService_Vc.swift
//  ServiceMyCar
//
//  Created by baps on 21/01/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit

class BasicService_Vc: UIViewController,UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    
    @IBOutlet weak var collectionviewselect: UICollectionView!
   // var arrService = ["Car Service","Car Repair","Flat Tyre","Flat Battery","Car Wash","Recovery"]
    var arrImage = ["car-tools","accident","weel","battery","car-wash","car-option"]
    var DataDic:NSDictionary = NSDictionary()
    var arrService:NSMutableArray = NSMutableArray()
    let badgeButton : MIBadgeButton = MIBadgeButton(frame:CGRect(x: -10, y: 15, width: 40, height: 20))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        let lbNavTitle = UILabel(frame: CGRect(x: CGFloat(35), y:self.view.bounds.size.width/2-235, width: CGFloat(200), height: CGFloat(40)))
        lbNavTitle.textAlignment = .center
        lbNavTitle.text = "service_details_title".localized()
        lbNavTitle.textColor = Colorblack
        lbNavTitle.font = UIFont(name: Regular, size: 16)
        
        //        let logo = UIImage(named: "AppTopIcon")
        //        let imageView = UIImageView(image:logo)
        //        imageView.contentMode = .scaleAspectFit
        // self.navigationItem.titleView = imageView
        self.navigationItem.titleView = lbNavTitle
        self.navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.white
        
        let titleDict: NSDictionary = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [NSAttributedString.Key : Any]
        
        let button = UIButton.init(type: .custom)
        //backarrow , menu
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        
        if languageCode == "en" {
            button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
        }
        else {
            button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
        }
        button.addTarget(self, action:#selector(SideMenu), for: UIControl.Event.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        let buttonLogout = UIButton.init(type: .custom)
        buttonLogout.setImage(UIImage.init(named: "notification"), for: UIControl.State.normal)
        buttonLogout.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        buttonLogout.frame = CGRect.init(x: self.view.frame.size.width-30, y: 0, width: 25, height: 25)
        // buttonLogout.titleLabel!.font =  UIFont(name:FontSemibold, size: 14)
        let barButtonLog = UIBarButtonItem.init(customView: buttonLogout)
        // self.navigationItem.rightBarButtonItem = barButtonLog
        
        self.collectionviewselect.register(UINib(nibName: "BasicBookNow_Cell", bundle: nil), forCellWithReuseIdentifier: "BasicBookNow_Cell")
        
        if let value = DataDic.value(forKey: "varSpecification") as? NSArray
        {
            arrService = value.mutableCopy() as! NSMutableArray
        }
        badgeButton.edgeInsetTop = 10
        badgeButton.setImage(UIImage(named : "notification"), for: .normal)
         badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        print(appDelegate.NCount)
        badgeButton.badgeString = appDelegate.NCount
        if appDelegate.NCount == "0"
        {
            badgeButton.badgeBackgroundColor = UIColor.clear
            badgeButton.badgeTextColor = UIColor.clear
        }
        else
        {
            badgeButton.badgeBackgroundColor = colorGreen
            badgeButton.badgeTextColor = UIColor.white
        }
        badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        let barButton2 : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton2
        //        getPackagesListByCategory()
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)
        
    }
    
    // Step 3
    
//    @objc func NotificationAlert()
//    {
//        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
//        
//        self.navigationController?.pushViewController(nextViewController, animated: true)
//        
//    }
    
    // Step 4
    
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            //self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                
                badgeButton.badgeString = appDelegate.NCount
                if appDelegate.NCount == "0"
                {
                    badgeButton.badgeBackgroundColor = UIColor.clear
                    badgeButton.badgeTextColor = UIColor.clear
                }
                else
                {
                    badgeButton.badgeBackgroundColor = colorGreen
                    badgeButton.badgeTextColor = UIColor.white
                }
            }
        }
    }
    
    // MARK:- goBack
    
    @objc func SideMenu()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK:- OnClickBookNow
    @objc func OnClickBookNow()
    {
        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "CarList_Vc") as! CarList_Vc
       
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    // MARK:- goBack
    
    @objc func NotificationAlert()
    {
        guard let vcs = self.navigationController?.viewControllers else{return}
        let inStack = vcs.contains(where: { (vc) -> Bool in
            return vc is MyNotificationList_Vc
        })
        if inStack{
            self.performSegue(withIdentifier: unwindToNotificationController, sender: self)
        }else{
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        //        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
        //
        //        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    // MARK:- CollectionView Methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0
        {
           return arrService.count
        }
        else
        {
             return 1
        }
       
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectService_cell", for: indexPath) as! SelectService_cell
            cell.viewBack.layer.cornerRadius = 4.0
            cell.viewBack.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
            cell.viewBack.backgroundColor = UIColor.white
            cell.viewBack.layer.shadowOffset = CGSize.zero
            cell.viewBack.layer.shadowOpacity = 2.0
            cell.viewBack.layer.shadowRadius = 3.0
            cell.viewBack.layer.masksToBounds = false
            
            if let name = (arrService.object(at: indexPath.row) as! NSDictionary).value(forKey: "varSpecification") as? String
            {
                cell.lblTitle.text = name
            }
            
            if let UserProfilePic = (arrService.object(at: indexPath.row) as! NSDictionary).value(forKey: "varImage") as? String
            {
                let url = "\(UserProfilePic)"
                print(url)
                
                if let url2 = URL(string: url) {
                    cell.imgCar.sd_setImage(with: url2, placeholderImage: UIImage(named: "img_plan1"))
                }
            }
            
            return cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BasicBookNow_Cell", for: indexPath) as! BasicBookNow_Cell
            
             if let value = DataDic.value(forKey: "varPrice") as? String
             {
                let main_string = "\("service_details_total".localized())  \(value + " \("app_currency".localized())")"
                let string_to_color = "service_details_total".localized()
                
                let range = (main_string as NSString).range(of: string_to_color)
                
                let attribute = NSMutableAttributedString.init(string: main_string)
                attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: colorRed , range: range)
                cell.lblPrice.attributedText = attribute
                
            }
            
            let stringArray = ["service_details_all_service_parts_will_be_an_additional_charge".localized(), "service_details_any_additional_repairs".localized()]
            cell.txtView.attributedText = NSAttributedStringHelper.createBulletedList(fromStringArray: stringArray, font: cell.txtView.font)
            cell.btnBookNow.addTarget(self, action:#selector(OnClickBookNow), for: UIControl.Event.touchUpInside)
            cell.btnBookNow.setTitle("service_details_book_now".localized(), for: .normal)
            cell.txtView.isHidden = true
//            cell.viewBack.layer.borderColor = Colorblack.cgColor
//            cell.viewBack.layer.cornerRadius = 5
//            cell.viewBack.layer.borderWidth = 1
//            cell.lblTitle.text = arrService[indexPath.row]
//            cell.imgCar.image = UIImage.init(named: arrImage[indexPath.row])
            return cell
        }
        
    }
    
   

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         var size:CGFloat = CGFloat()
         let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
        let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
              size = (collectionView.frame.size.width - space-30) / 2.0
            case 1334:
                print("iPhone 6/6S/7/8")
                size = (collectionView.frame.size.width - space-30) / 2.0
            case 1920, 2208:
                print("iPhone 6+/6S+/7+/8+")
                size = (collectionView.frame.size.width - space-30) / 2.0
            case 2436:
                print("iPhone X, XS")
                size = (collectionView.frame.size.width - space) / 2.0
            case 2688:
                print("iPhone XS Max")
                 size = (collectionView.frame.size.width - space) / 2.0
            case 1792:
                print("iPhone XR")
                 size = (collectionView.frame.size.width - space) / 2.0
            default:
                print("Unknown")
                 size = (collectionView.frame.size.width - space) / 2.0
            }
        }
        
        if indexPath.section == 0
        {
           
            
            
            return CGSize(width: size, height: size)
        }
        else
        {
          //  let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
           // let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
            let size:CGFloat = (collectionView.frame.size.width - space) / 2.0
            return CGSize(width: collectionView.frame.size.width, height: 120)
            
        }
       
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
