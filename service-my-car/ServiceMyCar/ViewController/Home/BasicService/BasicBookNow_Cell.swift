//
//  BasicBookNow_Cell.swift
//  ServiceMyCar
//
//  Created by baps on 21/01/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit

class BasicBookNow_Cell: UICollectionViewCell {

    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var btnBookNow: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.btnBookNow.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6).cgColor
        self.btnBookNow.backgroundColor = colorGreen
        self.btnBookNow.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.btnBookNow.layer.shadowOpacity = 2.0
        self.btnBookNow.layer.shadowRadius = 3.0
        self.btnBookNow.layer.masksToBounds = false
        self.btnBookNow.layer.cornerRadius = 5.0
       // self.btnBookNow.setTitle("Resend Code", for: .normal)
        self.btnBookNow.tintColor = Colorblack
        // Initialization code
    }

}
