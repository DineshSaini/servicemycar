//
//  SelectCar_Cell.swift
//  ServiceMyCar
//
//  Created by baps on 31/01/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit

class SelectCar_Cell: UICollectionViewCell {
    @IBOutlet weak var viewWidthConstant: NSLayoutConstraint!
    @IBOutlet weak var lblyearLabel: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var lblPlateNo: UILabel!
    @IBOutlet weak var lblModel: UILabel!
    @IBOutlet weak var lblCarname: UILabel!
    @IBOutlet weak var imgCar: UIImageView!
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var lblModelLabel: UILabel!
    @IBOutlet weak var lblSerialNo: UILabel!
    @IBOutlet weak var lblYear: UILabel!
    @IBOutlet weak var editButton:UIButton!
    @IBOutlet weak var shadowView: UIView!
    
    @IBOutlet weak var plateImageView:UIImageView!

    
}
