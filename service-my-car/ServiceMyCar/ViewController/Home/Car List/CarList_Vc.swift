//
//  CarList_Vc.swift
//  ServiceMyCar
//
//  Created by baps on 23/01/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import ZAlertView

struct TotalPrice {
    var total:String = String()
    var OderAmount:String = String()
}

struct carDetail {
    var CarName :String = String()
    var carModel:String = String()
    var carNo:String = String()
    var carYear:String = String()
    var varTrimEngine:String = String()
    var varPlateCode:String = String()
    var varPlateNumber:String = String()
    var varCarState:String = String()
    
    
}

struct PartsList {
    var varParts:String = String()
   
}

struct PackageDetail {
    var PackageName:String = String()
    var PackegePrice:String = String()
    var PackageImageUrl:String = String()
    var Id:String = String()
    var varPackageDate:String = String()
    var type:String = String()
    var dateType:String = String()
    var parts:String = "sParts"
}
struct PickUpLocation {
    var varPackagePAddress:String = String()
    var varPackagePState:String = String()
    var varPackagePCountry:String = String()
    var varBuildingPName:String = String()
    var additionalPInstruction:String = String()
    var Latitude:String = String()
    var Longitude:String = String()
    
}

struct DropOffLocation {
    var varPackageDAddress:String = String()
    var varBuildingDName:String = String()
    var varPackageDState:String = String()
    var varPackageDCountry:String = String()
    var additionalDInstruction:String = String()
    var Latitude:String = String()
    var Longitude:String = String()
}

struct Address {
    var PickupAddress:String = String()
    var DropOffAddress:String = String()
    var Pickup = PickUpLocation()
    var Dropoff = DropOffLocation()
    var isPickUp :Bool = Bool()
    var isDropOff :Bool = Bool()
   
}

struct DateAndTime {
    
    var Date:String = String()
}

class CarRepairQuete {
    var Car = carDetail()
}
class BookingDetails {
    var Car = carDetail()
    var Package = PackageDetail()
    var Date = DateAndTime()
    var DAddress = Address()
    var Price = TotalPrice()
    var SelectedPart = PartsList()
    var interlaced = false
    var frameRate = 0.0
    var name: String?
    var isDrop:Bool = true
    var discountAmount:String?
    var carValet:String?
    var carFuel:String?
    var couponId:String?
    var telr_value:String?
    
    var strVat:String?
}
class CarList_Vc: UIViewController,UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout,HttpWrapperDelegate {
    //Note For Variable IsFrom
//    0 == select Service
//    1 == Car Repair Quate
    var isFrom = "0"
    @IBOutlet weak var collectionviewselect: UICollectionView!
    @IBOutlet weak var addNewCarButton:UIButton!
    
    var arrService = ["Car Service","Car Repair","Flat Tyre","Flat Battery","Car Wash","Recovery"]
    var arrImage = ["car-tools","accident","weel","battery","car-wash","car-option"]
    var objGetUserCars = HttpWrapper()
    var objdeleteUserCar = HttpWrapper()
    
    var net:NetworkReachabilityManager = NetworkReachabilityManager()!
    var arrCarlist:NSMutableArray = NSMutableArray()
    var arrayCount = 0
    let badgeButton : MIBadgeButton = MIBadgeButton(frame:CGRect(x: -10, y: 15, width: 40, height: 20))
    override func viewDidLoad() {
        super.viewDidLoad()
        if isFrom == "1"{
           self.title = "select_car_title".localized()
        }

        
        let button = UIButton.init(type: .custom)
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        
        if languageCode == "en" {
            button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
        }
        else {
            button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
        }
        button.addTarget(self, action:#selector(SideMenu), for: UIControl.Event.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton

         self.collectionviewselect.register(UINib(nibName: "AddPlus_Cell", bundle: nil), forCellWithReuseIdentifier: "AddPlus_Cell")
        
        badgeButton.edgeInsetTop = 10
        badgeButton.setImage(UIImage(named : "notification"), for: .normal)
         badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        print(appDelegate.NCount)
        badgeButton.badgeString = appDelegate.NCount
        if appDelegate.NCount == "0"{
            badgeButton.badgeBackgroundColor = UIColor.clear
            badgeButton.badgeTextColor = UIColor.clear
        }else{
            badgeButton.badgeBackgroundColor = colorGreen
            badgeButton.badgeTextColor = UIColor.white
        }
        badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        let barButton2 : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton2
        AppHelper.makeViewCircularWithRespectToHeight(addNewCarButton, borderColor: .clear, borderWidth: 0.0)
        //self.getPackagesListByCategory()
        self.getUserCars()

        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)
        
    }
    
    // Step 3
    
    @objc func NotificationAlert()
    {
        guard let vcs = self.navigationController?.viewControllers else{return}
        let inStack = vcs.contains(where: { (vc) -> Bool in
            return vc is MyNotificationList_Vc
        })
        if inStack{
            self.performSegue(withIdentifier: unwindToNotificationController, sender: self)
        }else{
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        //        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
        //
        //        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    // Step 4
    
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            //self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                
                badgeButton.badgeString = appDelegate.NCount
                if appDelegate.NCount == "0"
                {
                    badgeButton.badgeBackgroundColor = UIColor.clear
                    badgeButton.badgeTextColor = UIColor.clear
                }
                else
                {
                    badgeButton.badgeBackgroundColor = colorGreen
                    badgeButton.badgeTextColor = UIColor.white
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setupNavigation()
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        getUserCars()
    }
    
    override func viewDidLayoutSubviews() {
        addNewCarButton.setTitle("Add New".localized(), for: .normal)
    }
    // MARK:- goBack
    
    @objc func SideMenu()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK:- onClickBtnAddCar
    @IBAction func onClickBtnAddCar(_ sender: Any) {
        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "AddCar_Vc") as! AddCar_Vc
        nextViewController.delegate = self
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    // MARK:- editCarButton
    @IBAction func onClickBtnEditCar(_ sender: UIButton) {
        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "AddCar_Vc") as! AddCar_Vc
        let editCar = self.arrCarlist.object(at: sender.tag) as! NSDictionary
        nextViewController.editCar = editCar
        nextViewController.isCarEdit = true
        nextViewController.delegate = self

        self.navigationController?.pushViewController(nextViewController, animated: true)
    }

    
    
    // MARK:- goBack
    
    @objc func onClickbtnDeleteCar(sender:UIButton)
    {
//        ZAlertView.positiveColor            = UIColor.color("#669999")
//        ZAlertView.negativeColor            = UIColor.color("#CC3333")
//        ZAlertView.blurredBackground        = true
        ZAlertView.showAnimation            = .bounceBottom
        ZAlertView.hideAnimation            = .bounceRight
        ZAlertView.initialSpringVelocity    = 0.9
        ZAlertView.duration                 = 2
       
        let dialog = ZAlertView(title: "select_car_delete".localized(),
                                message: "are_you_sure_you_want_to_delete_thiscar".localized(),
                                isOkButtonLeft: true,
                                okButtonText: "app_yes".localized(),
                                cancelButtonText: "app_alert_cancel".localized(),
                                okButtonHandler: { (alertView) -> () in
                                    alertView.dismissAlertView()
                                    
                                    if let id = (self.arrCarlist.object(at: sender.tag) as! NSDictionary).value(forKey: "id") as? String
                                    {
                                        self.DeleteUserCars(carId: id)
                                    }
                                    else if let id = (self.arrCarlist.object(at: sender.tag) as! NSDictionary).value(forKey: "id") as? NSNumber
                                    {
                                        self.DeleteUserCars(carId: "\(id)")
                                    }
                                    
                                    
        },
                                cancelButtonHandler: { (alertView) -> () in
                                    alertView.dismissAlertView()
        }
        )
        dialog.show()
        dialog.allowTouchOutsideToDismiss = true
    }
   
    // MARK:- CollectionView Methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if section == 0
        {
           return arrCarlist.count
        }
        else
        {
            return 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        

            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectCar_Cell", for: indexPath) as! SelectCar_Cell

        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        if languageCode == "en" {
            
            if indexPath.row == 0{
                AppHelper.makeCircularWithLeftTopTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)
                
                
            }else if indexPath.row == 1{
                AppHelper.makeCircularWithRightTopTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)
            }else if (indexPath.row%2 == 0){
                if (indexPath.row == arrayCount-1) || indexPath.row == arrayCount-2{
                    AppHelper.makeCircularWithRightBottmTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)
                }else{
                    AppHelper.makeCircularWithLeftTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)
                }
            }else if (indexPath.row%2 == 1){
                if (indexPath.row == arrayCount-1) || indexPath.row == arrayCount-2{
                    AppHelper.makeCircularWithLeftBottmTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)
                    
                }else{
                    
                    AppHelper.makeCircularWithRightTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)
                }
            }
        }else{
            
            if indexPath.row == 0{
                AppHelper.makeCircularWithRightTopTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)
                
            }else if indexPath.row == 1{
                AppHelper.makeCircularWithLeftTopTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)

            }else if (indexPath.row%2 == 0){
                if (indexPath.row == arrayCount-1) || indexPath.row == arrayCount-2{
                    AppHelper.makeCircularWithLeftBottmTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)
                }else{
                    AppHelper.makeCircularWithRightTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)
                }
                
            }else if (indexPath.row%2 == 1){
                
                if (indexPath.row == arrayCount-1) || indexPath.row == arrayCount-2{
                    AppHelper.makeCircularWithRightBottmTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)
                }else{
                    AppHelper.makeCircularWithLeftTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)
                }
            }
            
        }

        
        cell.shadowView.backgroundColor = colorBackGroundColor
        cell.viewBack.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).cgColor
        cell.viewBack.backgroundColor = UIColor.white
        cell.viewBack.layer.shadowOffset = CGSize.zero
        cell.viewBack.layer.shadowOpacity = 1.0
        cell.viewBack.layer.shadowRadius = 3.0
        cell.viewBack.layer.masksToBounds = false
        cell.lblyearLabel.text = "select_car_year".localized()
        cell.lblModelLabel.text = "select_car_model".localized()
        if languageCode != "en" {
            cell.lblCarname.textAlignment = .right
            cell.lblYear.textAlignment  = .left
            cell.lblModel.textAlignment = .left
            cell.lblModelLabel.textAlignment = .right
            cell.lblyearLabel.textAlignment = .right
        }
        
        
                cell.btnClose.tag = indexPath.row
                cell.editButton.tag = indexPath.row
                cell.btnClose.addTarget(self, action:#selector(onClickbtnDeleteCar), for: UIControl.Event.touchUpInside)
                cell.editButton.addTarget(self, action:#selector(onClickBtnEditCar(_:)), for: UIControl.Event.touchUpInside)
                if let name = (arrCarlist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varCar") as? String
                {
                    cell.lblCarname.text = "\(name.uppercased())"
                    //\("select_car_make".localized())  :
                }
        
                if let name = (arrCarlist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varYear") as? String
                {
                    cell.lblYear.text  = "\(name.uppercased())"
                }
        
                if let name = (arrCarlist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varModel") as? String
                {
                    cell.lblModel.text = "\(name.uppercased()) "
                }
        
        
            if let stName = (arrCarlist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varCarState") as? String
            {
            cell.plateImageView.image = AppHelper.setNumberPlateImage(stateName: stName)
            }
        

        
        
                
                if let varPlateCode = (arrCarlist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varPlateCode") as? String, let varPlateNumber = (arrCarlist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varPlateNumber") as? String
                {
                    cell.lblPlateNo.text = varPlateNumber.uppercased()
                    cell.lblSerialNo.text = varPlateCode.uppercased()
                }
        
/*else if indexPath.row == (arrCarlist.count - 1){
            AppHelper.makeCircularWithRightBottmTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)
        }else if indexPath.row == (arrCarlist.count){
            AppHelper.makeCircularWithLeftBottmTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)
        }*/
        
                return cell
                
//            }
//
//
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if (indexPath.row).isEven{
            //even Number
            print(indexPath.row)
        }
        else {
            print(indexPath.row)
            // odd Number
        }
        let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
        let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
        let size:CGFloat = (collectionView.frame.size.width - space) / 2.0
        return CGSize(width: size, height: size+20)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
       /* if isFrom == "0"
        {
            
            if let name = (arrCarlist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varCar") as? String
            {
                appDelegate.Bookings.Car.CarName = name
            }
            
            if let name = (arrCarlist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varModel") as? String
            {
               appDelegate.Bookings.Car.carModel = name
            }
            
            if let varYear = (arrCarlist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varYear") as? String
            {
                appDelegate.Bookings.Car.carYear = varYear
            }
            if let varTrimEngine = (arrCarlist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varTrimEngine") as? String
            {
                appDelegate.Bookings.Car.varTrimEngine = varTrimEngine
            }
            
            if let varPlateCode = (arrCarlist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varPlateCode") as? String, let varPlateNumber = (arrCarlist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varPlateNumber") as? String
            {
                appDelegate.Bookings.Car.carNo = varPlateCode + "-" + varPlateNumber
                appDelegate.Bookings.Car.varPlateCode = varPlateCode
                appDelegate.Bookings.Car.varPlateNumber = varPlateNumber
            }
            
            
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "DateTime_Vc") as! DateTime_Vc
            nextViewController.isFrom = isFrom
           self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        else*/ if isFrom == "1"
        {
            if let name = (arrCarlist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varCar") as? String
            {
                appDelegate.CarRepair.Car.CarName = name
            }
            
            if let name = (arrCarlist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varModel") as? String
            {
                appDelegate.CarRepair.Car.carModel = name
            }
            
            if let varYear = (arrCarlist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varYear") as? String
            {
                appDelegate.CarRepair.Car.carYear = varYear
            }
            if let varTrimEngine = (arrCarlist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varTrimEngine") as? String
            {
                appDelegate.CarRepair.Car.varTrimEngine = varTrimEngine
            }
            
            if let varPlateCode = (arrCarlist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varPlateCode") as? String, let varPlateNumber = (arrCarlist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varPlateNumber") as? String
            {
                appDelegate.CarRepair.Car.carNo = varPlateCode + "-" + varPlateNumber
                appDelegate.CarRepair.Car.varPlateCode = varPlateCode
                appDelegate.CarRepair.Car.varPlateNumber = varPlateNumber
            }
            
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "CarRepairQuote_Vc") as! CarRepairQuote_Vc
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
            
        }/*
        else if isFrom == "3" {
            if let name = (arrCarlist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varCar") as? String
            {
                appDelegate.Bookings.Car.CarName = name
                print(name)
            }
            
            if let name = (arrCarlist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varModel") as? String
            {
                appDelegate.Bookings.Car.carModel = name
                print(name)
            }
            
            if let varYear = (arrCarlist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varYear") as? String
            {
                appDelegate.Bookings.Car.carYear = varYear
                print(varYear)
            }
            if let varTrimEngine = (arrCarlist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varTrimEngine") as? String
            {
                appDelegate.Bookings.Car.varTrimEngine = varTrimEngine
                print(varTrimEngine)
            }
            
            if let varPlateCode = (arrCarlist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varPlateCode") as? String, let varPlateNumber = (arrCarlist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varPlateNumber") as? String
            {
                appDelegate.Bookings.Car.carNo = varPlateCode + "-" + varPlateNumber
                appDelegate.Bookings.Car.varPlateCode = varPlateCode
                appDelegate.Bookings.Car.varPlateNumber = varPlateNumber
                print(varPlateCode)
                print(varPlateNumber)
            }
            
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "DateTime_Vc") as! DateTime_Vc
            nextViewController.isEmergency = true
            nextViewController.isFrom = isFrom
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }*/
        
       
//
        
//        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    
    
    // MARK: - getUserCars API
    
    func getUserCars()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["userId" : nUserId as AnyObject]
            //let header = ["X-API-KEY" : "smc1989"] as [String:AnyObject]

            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objGetUserCars = HttpWrapper.init()
            self.objGetUserCars.delegate = self
            
            self.objGetUserCars.requestWithparamdictParamPostMethodwithHeaderAndDictGet(url: kgetUserCars, dictParams: dictParameters, headers: header)
        }
        
    }
    
    
    // MARK: - DeleteUserCars API
    
    func DeleteUserCars(carId:String)
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            
            let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as! String
            let dictParameters:[String:AnyObject] = ["userId" : nUserId as AnyObject,
                                                     "carId" : carId as AnyObject]
            //let header = ["X-API-KEY" : "smc1989"] as [String:AnyObject]
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objdeleteUserCar = HttpWrapper.init()
            self.objdeleteUserCar.delegate = self
            self.objdeleteUserCar.requestWithparamdictParamDeleteMethodwithHeader(url: kdeleteUserCar, dicsParams: dictParameters, headers: header)
        }
    }
    
    
    // MARK: - HttpWrapperfetchDataSuccess
    
    func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary)
    {
        AppHelper.hideLoadingView()
        print(dicsResponse)
        if wrapper == objGetUserCars {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                arrCarlist.removeAllObjects()
                
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray
                {
                    arrCarlist = tempNames.mutableCopy() as! NSMutableArray
                    self.arrayCount = arrCarlist.count
                    
                    print(" this is total item in list \(self.arrayCount)")
                }
                print(arrCarlist)
                collectionviewselect.reloadData()
            }
            else
            {
               // AppHelper.showAlert("Alert", message:dicsResponse.value(forKey: "msg") as! String)
            }
        }
        else if wrapper == objdeleteUserCar {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
               getUserCars()
            }
            else
            {
                AppHelper.showAlert("app_alert_title".localized(), message:dicsResponse.value(forKey: "msg") as! String)
            }
        }
        
    }
    // MARK: - HttpWrapperfetchDataFail
    
    func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError)
    {
        AppHelper.hideLoadingView()
        print(error.debugDescription)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension CarList_Vc : AddCar_VcDelegate{
    func editCarInCarList(_ viewController: UIViewController, didSuccess success: Bool) {
        if success{
            getUserCars()

        }
    }
    
    
}

