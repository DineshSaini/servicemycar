//
//  Thankyou_Vc.swift
//  ServiceMyCar
//
//  Created by baps on 24/01/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit

class Thankyou_Vc: UIViewController {
    @IBOutlet weak var lblMsg: UILabel!
    
    
    @IBOutlet weak var lblThankyou: UILabel!
    var isFrom = "0"
    let badgeButton : MIBadgeButton = MIBadgeButton(frame:CGRect(x: -10, y: 15, width: 40, height: 20))
    override func viewDidLoad() {
        super.viewDidLoad()
        lblThankyou.text = "Thank_you".localized()
        self.navigationController?.isNavigationBarHidden = false
        
        
        
        
        

        let logo = UIImage(named: "AppTopIcon")
        let imageView = UIImageView(image:logo)
        
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
        
        
        let button = UIButton.init(type: .custom)
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        
        if languageCode == "en" {
            button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
        }
        else {
            button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
        }
        button.addTarget(self, action:#selector(onClickBackButton(_ :)), for: UIControl.Event.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton

        
//        let button = UIButton.init(type: .custom)
//        //backarrow , menu
//        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
//
//        if languageCode == "en" {
//            button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
//        }
//        else {
//            button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
//        }
//
//
//        //        let buttonLogout = UIButton.init(type: .custom)
//        //        buttonLogout.setImage(UIImage.init(named: "notification"), for: UIControl.State.normal)
//        //        buttonLogout.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
//        //        buttonLogout.frame = CGRect.init(x: self.view.frame.size.width-30, y: 0, width: 25, height: 25)
//        //        // buttonLogout.titleLabel!.font =  UIFont(name:FontSemibold, size: 14)
//        //        let barButtonLog = UIBarButtonItem.init(customView: buttonLogout)
//        //        // self.navigationItem.rightBarButtonItem = barButtonLog
        
    //badgeButton.edgeInsetTop = 10
//        badgeButton.setImage(UIImage(named : "notification"), for: .normal)
//        print(appDelegate.NCount)
//         badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
//        badgeButton.badgeString = appDelegate.NCount
//        if appDelegate.NCount == "0"
//        {
//            badgeButton.badgeBackgroundColor = UIColor.clear
//            badgeButton.badgeTextColor = UIColor.clear
//        }
//        else
//        {
//            badgeButton.badgeBackgroundColor = colorGreen
//            badgeButton.badgeTextColor = UIColor.white
//        }
//        //badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
//        let barButton2 : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
//        self.navigationItem.rightBarButtonItem = barButton2
//        //        getPackagesListByCategory()
//
//
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
//                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
//                                               object: nil)
        
        if isFrom == "1"{
            lblMsg.text = "thank_you_for_submit_request".localized()
        }else if isFrom == "2"{
            lblMsg.text = "your decision is received".localized()//"Our driver will be at your chosen location to collect your car on the date and time you selected".localized() //"your decision is received"
        }else if isFrom == "0"{
            lblMsg.text = "For_Choosing_Us".localized()//"Our driver will be at your chosen location to collect your car on the date and time you selected".localized() //"your decision is received"
        }else if isFrom == "3"{
            lblMsg.text = "For_Choosing_Us".localized()//"Our driver will be at your chosen location to collect your car on the date and time you selected".localized() //"your decision is received"
        }else{
            lblMsg.text = "Our driver will be at your chosen location to collect your car on the date and time you selected".localized()//"your decision is received".localized()
        }
    }
    
    // Step 3
    
    @objc func NotificationAlert()
    {
        guard let vcs = self.navigationController?.viewControllers else{return}
        let inStack = vcs.contains(where: { (vc) -> Bool in
            return vc is MyNotificationList_Vc
        })
        if inStack{
            self.performSegue(withIdentifier: unwindToNotificationController, sender: self)
        }else{
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        //        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
        //
        //        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    // Step 4
    
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            //self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                
                badgeButton.badgeString = appDelegate.NCount
                if appDelegate.NCount == "0"
                {
                    badgeButton.badgeBackgroundColor = UIColor.clear
                    badgeButton.badgeTextColor = UIColor.clear
                }
                else
                {
                    badgeButton.badgeBackgroundColor = colorGreen
                    badgeButton.badgeTextColor = UIColor.white
                }
            }
        }
    }
    
    // MARK:- goBack
    @IBAction func onClickBackButton(_ sender:UIButton) {
        
        if isFrom == "CategoryDocViewController" {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: CategoryDocViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
        else
        {
            self.navigationController?.popToRootViewController(animated: true)
        }
        
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
