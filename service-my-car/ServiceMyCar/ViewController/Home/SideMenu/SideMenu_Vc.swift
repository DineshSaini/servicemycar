//
//  SideMenu_Vc.swift
//  Waterwise
//
//  Created by Mandip Kanjiya on 15/08/18.
//  Copyright © 2018 Mandip Kanjiya. All rights reserved.
//

import UIKit
import SJSwiftSideMenuController
import Alamofire
import SDWebImage
import Localize_Swift
class SideMenu_Vc: UIViewController ,UITableViewDataSource , UITableViewDelegate {
    
    

    @IBOutlet weak var tblMain: UITableView!
    
    
    //BUTTONS
    
   // var arrName = [NSLocalizedString("HOME", comment: "") as String,NSLocalizedString("SEARCH", comment: "") as String,NSLocalizedString("PROFILE", comment: "") as String,NSLocalizedString("UPLOAD", comment: "") as String,NSLocalizedString("CHANGE_LANGUAGE", comment: "") as String,NSLocalizedString("LOGOUT", comment: "") as String]

    var arrName = ["menu_select_services".localized(),"menu_my_services".localized(),"menu_reports".localized(),"menu_service_schedule".localized(),"menu_my_invoices".localized(),"menu_my_health_checks".localized(),"menu_my_additional_repairs".localized(),"menu_notifications".localized(),"menu_profile".localized(),"menu_language".localized(),"menu_logout".localized()]
    //var arrImages = ["menu_sacn_prize","menu_vendor","menu_offer","menu_favorites","menu_favorites","menu_settings","menu_help","menu_logout"]
    var languageCode = ""
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
      self.view.backgroundColor = UIColor.white
        
        // Do any additional setup after loading the view.
    }

    
    
    override func viewWillAppear(_ animated: Bool) {
        
        tblMain.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0
        {
            return 1
        }
        else
        {
            return arrName.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0
        {
           let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuProfile_Cell", for: indexPath) as! SideMenuProfile_Cell
//            if let UserName = UserDefaults.standard.object(forKey: kcCustomerName)
//            {
//                print(UserDefaults.standard.object(forKey: kcCustomerName))
//                print(UserDefaults.standard.object(forKey: kcCustomerImage))
//               cell.lblName.text = UserName as! String
//            }
//
//            if let UserEmail = UserDefaults.standard.object(forKey: kcEmail)
//            {
//                print(UserEmail)
//                cell.lblEmail.text = UserEmail as! String
//            }
//            if let UserProfilePic = UserDefaults.standard.object(forKey: kcCustomerImage)
//            {
//                let url = "\(UserProfilePic)"
//                print(url)
//
////                if let url2 = URL(string: UserDefaults.standard.object(forKey:kProfile_pic) as! String) {
////                    cell.imgProfile.sd_setImage(with: url2, placeholderImage: UIImage(named: "bg-user"))
////                }
//
//                if let url2 = URL(string: url) {
//                    cell.imgProfile.sd_setImage(with: url2, placeholderImage: UIImage(named: "photo"))
//                }
//            }

            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "sideMenuButton_Cell", for: indexPath) as! sideMenuButton_Cell
            cell.btnActions.setTitle(arrName[indexPath.row], for: .normal)
            cell.btnActions.setTitleColor(Colorblue, for: .normal)
            //cell.imgICon.image = UIImage.init(named: arrImages[indexPath.row])
//
            return cell
        }
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       appDelegate.isFromNotification = false
       if indexPath.section == 0
       {
//        SJSwiftSideMenuController.hideLeftMenu()
//        let viewController = objMain.instantiateViewController(withIdentifier:"Payment_Vc") as! Payment_Vc
//        SJSwiftSideMenuController.pushViewController(viewController, animated: true)
       }
       else
       {
        
           print(indexPath.row)
         //  Select Services
           if indexPath.row == 0
           {
            SJSwiftSideMenuController.hideLeftMenu()
            let viewController = objHomeSB.instantiateViewController(withIdentifier:"Home_Vc") as! Home_Vc
            
            SJSwiftSideMenuController.pushViewController(viewController, animated: false)
           }
               //My Services
           else if indexPath.row == 1
           {
               SJSwiftSideMenuController.hideLeftMenu()
               let viewController = objHomeSB.instantiateViewController(withIdentifier:"MyServices_Vc") as! MyServices_Vc
               
               SJSwiftSideMenuController.pushViewController(viewController, animated: false)
           }
               // Reports
            else if indexPath.row == 2
            {
               
                SJSwiftSideMenuController.hideLeftMenu()
                let viewController = objHomeSB.instantiateViewController(withIdentifier:"Reports_Vc") as! Reports_Vc
               
                SJSwiftSideMenuController.pushViewController(viewController, animated: false)
            }
                //Service Schedule
            else if indexPath.row == 3
            {
                SJSwiftSideMenuController.hideLeftMenu()
                let viewController = objHomeSB.instantiateViewController(withIdentifier:"ServiceSchedule_Vc") as! ServiceSchedule_Vc

                SJSwiftSideMenuController.pushViewController(viewController, animated: false)
            }
                // My Invoices
            else if indexPath.row == 4
            {
                SJSwiftSideMenuController.hideLeftMenu()
                let viewController = objMain.instantiateViewController(withIdentifier:"MyInvoice_Vc") as! MyInvoice_Vc
                
                SJSwiftSideMenuController.pushViewController(viewController, animated: false)
            }
            
            // My Health checks
           else if indexPath.row == 5
           {
            SJSwiftSideMenuController.hideLeftMenu()
            let viewController = objHomeSB.instantiateViewController(withIdentifier:"HealthCheckUpVideo_Vc") as! HealthCheckUpVideo_Vc
           
            SJSwiftSideMenuController.pushViewController(viewController, animated: false)
           }  // My Additional Repairs
           else if indexPath.row == 6
           {
            SJSwiftSideMenuController.hideLeftMenu()
            let viewController = objHomeSB.instantiateViewController(withIdentifier:"AdditionalRepairs_Vc") as! AdditionalRepairs_Vc
            
            SJSwiftSideMenuController.pushViewController(viewController, animated: false)
           }//Notification
           else if indexPath.row == 7
           {
            SJSwiftSideMenuController.hideLeftMenu()
            let viewController = objHomeSB.instantiateViewController(withIdentifier:"MyNotificationList_Vc") as! MyNotificationList_Vc
            
            SJSwiftSideMenuController.pushViewController(viewController, animated: false)
           }
            // PROFILE
           else if indexPath.row == 8
           {
                        SJSwiftSideMenuController.hideLeftMenu()
                        let viewController = objHomeSB.instantiateViewController(withIdentifier:"Profile_Vc") as! Profile_Vc
            
                        SJSwiftSideMenuController.pushViewController(viewController, animated: false)
           }
            // PROFILE
           else if indexPath.row == 9
           {
            SJSwiftSideMenuController.hideLeftMenu()
            let viewController = objHomeSB.instantiateViewController(withIdentifier:"SelectLanguage_Vc") as! SelectLanguage_Vc
            viewController.modalPresentationStyle = .overCurrentContext
            viewController.modalTransitionStyle = .crossDissolve
            self.present(viewController, animated: false, completion: nil)
//            SJSwiftSideMenuController.pushViewController(viewController, animated: false)
           }
            
               // LogOut
            else if indexPath.row == 10
            {
               let alertController = UIAlertController(title: "app_alert_title".localized(), message: "menu_logout_message".localized(), preferredStyle: .alert)
                let settingsAction = UIAlertAction(title: "app_net_alert_ok".localized(), style: .default) { (_) -> Void in
                    let defaults = UserDefaults.standard

                    defaults.removeObject(forKey: kUserLoginUserId)
                    defaults.removeObject(forKey: kUserLoginPhone)
                    defaults.removeObject(forKey: kUserName)
                    defaults.removeObject(forKey: KUserLoginEmail)
                    defaults.removeObject(forKey: klatitude)
                    defaults.removeObject(forKey: kLongitude)
                   

                    defaults.synchronize()

                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.logout()
                }
                let cancelAction = UIAlertAction(title: "app_alert_cancel".localized(), style: .default, handler: {(alert: UIAlertAction!) in print("Foo")
//                     self.SwitchNotification.isOn = false
                })

                alertController.addAction(cancelAction)
                alertController.addAction(settingsAction)

                self.present(alertController, animated: true, completion: {
                    print("Foo")
                })
            }
        }
    }
     // MARK: - BUTTONS ACTIONS
    
  
    // MARK: - onClickBtnLogOut
    @IBAction func onClickBtnLogOut(_ sender: Any)
    {
//        let alertController = UIAlertController(title: "Staycurity", message: "Are sure you want to logout.", preferredStyle: .alert)
//        let settingsAction = UIAlertAction(title: "Ok", style: .default) { (_) -> Void in
//            let defaults = UserDefaults.standard
//
//            defaults.removeObject(forKey: kUser_id)
//            defaults.removeObject(forKey: kUserName)
//            defaults.removeObject(forKey: klatitude)
//            defaults.removeObject(forKey: kLongitude)
//            defaults.removeObject(forKey: kProfile_pic)
//            defaults.removeObject(forKey: kUserType)
//            defaults.removeObject(forKey: SignOutKey)
//            defaults.removeObject(forKey: kcEmail)
//            defaults.synchronize()
//
//            let appDelegate = UIApplication.shared.delegate as! AppDelegate
//            appDelegate.logout()
//        }
//        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {(alert: UIAlertAction!) in print("Foo")
//            // self.SwitchNotification.isOn = false
//        })
//
//        alertController.addAction(cancelAction)
//        alertController.addAction(settingsAction)
//
//        self.present(alertController, animated: true, completion: {
//            print("Foo")
//        })
//
       
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
}
