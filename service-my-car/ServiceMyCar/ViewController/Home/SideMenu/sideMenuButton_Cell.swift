//
//  sideMenuButton_Cell.swift
//  Waterwise
//
//  Created by Mandip Kanjiya on 21/08/18.
//  Copyright © 2018 Mandip Kanjiya. All rights reserved.
//

import UIKit

class sideMenuButton_Cell: UITableViewCell {

    @IBOutlet weak var imgICon: UIImageView!
    @IBOutlet weak var btnActions: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
