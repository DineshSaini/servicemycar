//
//  Home_Vc.swift
//  ServiceMyCar
//
//  Created by baps on 21/01/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import SJSwiftSideMenuController
import Alamofire
import SwiftyJSON
class Home_Vc: UIViewController,UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout,HttpWrapperDelegate {

    @IBOutlet weak var collectionviewselect: UICollectionView!
    @IBOutlet weak var userNameLabel:UILabel!
    @IBOutlet weak var serviceTypeLabel:UILabel!
    @IBOutlet weak var serviceDayLabel:UILabel!
    

    var objgetPackagesListByCategory = HttpWrapper()
    var objgetServicePackagesList = HttpWrapper()
    
    
    var carServiceArrayList = NSMutableArray()
    
    var net:NetworkReachabilityManager = NetworkReachabilityManager()!
    
    let badgeButton : MIBadgeButton = MIBadgeButton(frame:CGRect(x: -10, y: 15, width: 40, height: 20)) // Step 1
   
    var srtName:String = ""
    var redirectUrl:String = ""
    var orderID:String = ""
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        let UserLName = UserDefaults.standard.value(forKey: kUserName) as! String
        self.userNameLabel.text =  "Hi ".localized() + UserLName + ","

        if appDelegate.arrCategorylist.count == 0 {
            getServicePackagesList()
            getPackagesListByCategory()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false

        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "menu"), for: UIControl.State.normal)
        button.addTarget(self, action:#selector(SideMenu), for: UIControl.Event.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
       // Step 2
        badgeButton.edgeInsetTop = 10
        badgeButton.setImage(UIImage(named : "notification"), for: .normal)
         badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        print(appDelegate.NCount)
        badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        badgeButton.badgeString = appDelegate.NCount
        if appDelegate.NCount == "0"{
            badgeButton.badgeBackgroundColor = UIColor.clear
            badgeButton.badgeTextColor = UIColor.clear
        }else{
            badgeButton.badgeBackgroundColor = colorGreen
            badgeButton.badgeTextColor = UIColor.white
        }
        badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        let barButton2 : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton2
        
        if appDelegate.arrCategorylist.count == 0 {
            getServicePackagesList()
            getPackagesListByCategory()
        }
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(Home_Vc.reportDetailShowNotification(_:)), name: .SHOW_REPORT_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(Home_Vc.extraPartApprovalNotification(_:)), name: .EXTRA_PART_APPROVAL_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(Home_Vc.invoiceDetailNotification(_:)), name: .EXTRA_INVOICE_NOTIFICATION, object: nil)

        //
    }
    
    
    override func viewDidLayoutSubviews() {
        serviceTypeLabel.text = "what service would".localized()
        serviceDayLabel.text = "you like to book today?".localized()
    }
    
    
    
    
    @objc func reportDetailShowNotification(_ notification: Notification) {
        if let reportInfo = notification.userInfo as? [String:AnyObject]{
            if let detail = reportInfo["report"] as? Dictionary<String,String>{
                let webVC = objHomeSB.instantiateViewController(withIdentifier: "webview_Vc") as! webview_Vc
                if let strName = detail["type"] as String?{
                  webVC.strName = strName
                }
                
               if let url = detail["url"] as String?{
                    webVC.URL = url
                }
                if let orderID = detail["orderId"] as String?{
                    webVC.orderId = orderID
                }
                webVC.isNotification = true
                
               // webVC.strName = self.srtName
               // webVC.URL = self.redirectUrl
                self.navigationController?.pushViewController(webVC, animated: true)
                
            }
        }
    }
    
    
    @objc func invoiceDetailNotification(_ notification: Notification) {
        if let invoiceInfo = notification.userInfo as? [String:AnyObject]{
            if let invoice = invoiceInfo["invoice"] as? Dictionary<String,String>{
                let invoiceVC = objHomeSB.instantiateViewController(withIdentifier: "CategoryDocViewController") as! CategoryDocViewController
                if let _orderID = invoice["orderId"] as String?{
                   invoiceVC.orderId = _orderID
                }
                if let serviceType = invoice["serviceType"] as String?{
                  invoiceVC.servicetype = serviceType
                }
                invoiceVC.isOpenFromPushNotification = true
                invoiceVC.fromNotification = false
                self.navigationController?.pushViewController(invoiceVC, animated: true)
                
            }
        }
    }
    
    
    @objc func extraPartApprovalNotification(_ notification: Notification) {
        if let extraPartInfo = notification.userInfo as? [String:AnyObject]{
            if let extrapart = extraPartInfo["extrapart"] as? Dictionary<String,String>{
                let exPartVC = objHomeSB.instantiateViewController(withIdentifier: "AdditionRepairsViewController") as! AdditionRepairsViewController
            
                if let _orderID = extrapart["orderId"] as String?{
                    exPartVC.orderId = _orderID
                }
                //exPartVC.orderId = orderId//self.orderID
                exPartVC.isFromPushNotification = true
                exPartVC.fromNotification = false
                self.navigationController?.pushViewController(exPartVC, animated: true)
                
            }
        }
    }
    
    
    // MARK: - Google Api Calling
    
    // Step 4
    
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            //self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                
                badgeButton.badgeString = appDelegate.NCount
                if appDelegate.NCount == "0"
                {
                    badgeButton.badgeBackgroundColor = UIColor.clear
                    badgeButton.badgeTextColor = UIColor.clear
                }
                else
                {
                    badgeButton.badgeBackgroundColor = colorGreen
                    badgeButton.badgeTextColor = UIColor.white
                }
            }
        }
    }
    
    // MARK:- goBack
    
    @objc func SideMenu()
    {
       SJSwiftSideMenuController.toggleLeftSideMenu()
    }
    
    
    // MARK:- goBack
    
    // step 3
    
    @objc func NotificationAlert()
    {
        guard let vcs = self.navigationController?.viewControllers else{return}
        let inStack = vcs.contains(where: { (vc) -> Bool in
            return vc is MyNotificationList_Vc
        })
        if inStack{
            self.performSegue(withIdentifier: unwindToNotificationController, sender: self)
        }else{
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        //        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
        //
        //        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    
    
    // MARK:- CollectionView Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return appDelegate.arrCategorylist.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectService_cell", for: indexPath) as! SelectService_cell
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        if languageCode == "en" {
            switch indexPath.row{
            case 0:
                self.makeCircularWithLeftTopTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)
                break;
            case 1:
                self.makeCircularWithRightTopTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)
                break;
            case 2:
                self.makeCircularWithLeftTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)
                break;
            case 3:
                self.makeCircularWithRightTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)
                break;
            case 4:
                self.makeCircularWithRightBottmTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)
                break;
            case 5:
                self.makeCircularWithLeftBottmTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)
                break;
            default:
                break;
            }
        }else {
            switch indexPath.row{
            case 0:
                self.makeCircularWithRightTopTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)
                break;
            case 1:
                self.makeCircularWithLeftTopTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)
                break;
            case 2:
                self.makeCircularWithRightTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)
                break;
            case 3:
                self.makeCircularWithLeftTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)
                break;
            case 4:
                self.makeCircularWithLeftBottmTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)
                break;
            case 5:
                self.makeCircularWithRightBottmTwoCornerRadius(cell.viewBack, cornerRadius: 20.0)
                break;
            default:
                break;
            }
        }

        
        
       

        cell.shadowBackView.backgroundColor = colorBackGroundColor
        cell.viewBack.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).cgColor
        cell.viewBack.backgroundColor = UIColor.white
        cell.viewBack.layer.shadowOffset = CGSize.zero
        cell.viewBack.layer.shadowOpacity = 1.0
        cell.viewBack.layer.shadowRadius = 3.0
        cell.viewBack.layer.masksToBounds = false
        
            if let name = (appDelegate.arrCategorylist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varName") as? String
            {
                cell.lblTitle.text = name.uppercased()
            }
            
            if let UserProfilePic = (appDelegate.arrCategorylist.object(at: indexPath.row) as! NSDictionary).value(forKey: "icon") as? String
            {
                let url = "\(UserProfilePic)"
                print(url)
                
                if let url2 = URL(string: url) {
                    cell.imgCar.sd_setImage(with: url2, placeholderImage: UIImage(named: "img_plan1"))
                }
            }
                return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
            let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
            let size:CGFloat = (collectionView.frame.size.width - space) / 2.0
            return CGSize(width: size, height: self.collectionviewselect.frame.size.height/3 - 15)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == 0
        {
            
            if let Id = (appDelegate.arrCategorylist.object(at: indexPath.row) as! NSDictionary).value(forKey: "intId") as? NSNumber
            {
                appDelegate.Bookings.Package.Id = "\(Id)"
                print(Id)
            }
            else if let Id = (appDelegate.arrCategorylist.object(at: indexPath.row) as! NSDictionary).value(forKey: "intId") as? String
            {
                appDelegate.Bookings.Package.Id = "\(Id)"
                print(Id)
            }
            
           // let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "SelectPackages_Vc") as! SelectPackages_Vc
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier:"SelectPacakgeViewController") as! SelectPacakgeViewController
            appDelegate.isPayExtraInvoice = "1"
            nextViewController.arrCategorylist = self.carServiceArrayList
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        else  if indexPath.row == 1
        {
            if let Id = (appDelegate.arrCategorylist.object(at: indexPath.row) as! NSDictionary).value(forKey: "intId") as? NSNumber
            {
                appDelegate.Bookings.Package.Id = "\(Id)"
                print(Id)
            }
            else if let Id = (appDelegate.arrCategorylist.object(at: indexPath.row) as! NSDictionary).value(forKey: "intId") as? String
            {
                appDelegate.Bookings.Package.Id = "\(Id)"
                print(Id)
            }
            
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "CarList_Vc") as! CarList_Vc
            nextViewController.isFrom = "1"
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        else  if indexPath.row == 2
        {
            print(appDelegate.arrCategorylist.object(at: indexPath.row) as! NSDictionary)
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "AdditionalServicesApply_Vc") as! AdditionalServicesApply_Vc
            if let name = (appDelegate.arrCategorylist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varName") as? String
            {
                nextViewController.strtitle = name
                appDelegate.Bookings.Package.PackageName = name
            }
            if let price = (appDelegate.arrCategorylist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varPrice") as? String
            {
                nextViewController.strPrice = price
                appDelegate.Bookings.Package.PackegePrice = price
                appDelegate.selectedPackagePrice = Float(price)!
                appDelegate.Bookings.Price.OderAmount = "\(price)"
            }
            else  if let price = (appDelegate.arrCategorylist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varPrice") as? NSNumber
            {
                nextViewController.strPrice = "\(price)"
                appDelegate.Bookings.Package.PackegePrice = "\(price)"
                appDelegate.selectedPackagePrice = Float(price)
                 appDelegate.Bookings.Price.OderAmount = "\(price)"
            }
            
            if let icon = (appDelegate.arrCategorylist.object(at: indexPath.row) as! NSDictionary).value(forKey: "icon") as? String
            {
                appDelegate.Bookings.Package.PackageImageUrl = icon
            }
            
            if let Id = (appDelegate.arrCategorylist.object(at: indexPath.row) as! NSDictionary).value(forKey: "intId") as? NSNumber
            {
                appDelegate.Bookings.Package.Id = "\(Id)"
                print(Id)
            }
            else if let Id = (appDelegate.arrCategorylist.object(at: indexPath.row) as! NSDictionary).value(forKey: "intId") as? String
            {
                appDelegate.Bookings.Package.Id = "\(Id)"
                print(Id)
            }
            
            if let varOtherType = (appDelegate.arrCategorylist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varOtherType") as? String
            {
                appDelegate.Bookings.Package.type = varOtherType
            }
            
            nextViewController.isFlatTyre = true
            nextViewController.strSerViceDetails = "home_flat_tyre_description".localized()
            nextViewController.additionalImage = UIImage(named: "flat_tyre")
            appDelegate.isPayExtraInvoice = "3"
            
            
            
            //let nextViewController = objPostJob.instantiateViewController(withIdentifier:"PostYourJob") as! PostYourJob
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        else
        {
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "AdditionalServicesApply_Vc") as! AdditionalServicesApply_Vc
            
            if let name = (appDelegate.arrCategorylist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varName") as? String
            {
                nextViewController.strtitle = name
                appDelegate.Bookings.Package.PackageName = name
            }
            if let price = (appDelegate.arrCategorylist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varPrice") as? String
            {
                nextViewController.strPrice = price
                appDelegate.Bookings.Package.PackegePrice = price
                appDelegate.selectedPackagePrice = Float(price)!
                appDelegate.Bookings.Price.OderAmount = "\(price)"
            }
            else  if let price = (appDelegate.arrCategorylist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varPrice") as? NSNumber
            {
                nextViewController.strPrice = "\(price)"
                appDelegate.Bookings.Package.PackegePrice = "\(price)"
                appDelegate.selectedPackagePrice = Float(price)
                appDelegate.Bookings.Price.OderAmount = "\(price)"
            }
            
            if let icon = (appDelegate.arrCategorylist.object(at: indexPath.row) as! NSDictionary).value(forKey: "icon") as? String
            {
                appDelegate.Bookings.Package.PackageImageUrl = icon
            }
            
            if let Id = (appDelegate.arrCategorylist.object(at: indexPath.row) as! NSDictionary).value(forKey: "intId") as? NSNumber
            {
                appDelegate.Bookings.Package.Id = "\(Id)"
                print(Id)
            }
            else if let Id = (appDelegate.arrCategorylist.object(at: indexPath.row) as! NSDictionary).value(forKey: "intId") as? String
            {
                appDelegate.Bookings.Package.Id = "\(Id)"
                print(Id)
            }
            
            if let varOtherType = (appDelegate.arrCategorylist.object(at: indexPath.row) as! NSDictionary).value(forKey: "varOtherType") as? String
            {
                appDelegate.Bookings.Package.type = varOtherType
            }
            
            if indexPath.row == 3 {
                nextViewController.strSerViceDetails = "home_flat_battery_description".localized()
                nextViewController.additionalImage = UIImage(named:"flat_battery")

            }
            else if indexPath.row == 4 {
                nextViewController.strSerViceDetails = "home_car_wash_description".localized()
                nextViewController.additionalImage = UIImage(named: "car_wash")

            }
            else {
                nextViewController.strSerViceDetails = "home_recovery_description".localized()
                nextViewController.additionalImage = UIImage(named: "recovery")

            }
            
            appDelegate.isPayExtraInvoice = "3"
            nextViewController.isFlatTyre = false
            //let nextViewController = objPostJob.instantiateViewController(withIdentifier:"PostYourJob") as! PostYourJob
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
       
       

    }
    
    // MARK: - getServicePackagesList API
    
    func getServicePackagesList()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            
            var languageCode = ""
            if UserDefaults.standard.value(forKey: kAppLanguage) != nil {
                languageCode = UserDefaults.standard.value(forKey: kAppLanguage) as! String
            }
            
            let dictParameters:[String:AnyObject] = ["lang" : languageCode as AnyObject]
            
            //let header = ["X-API-KEY": "smc1989"] as [String:AnyObject]

            AppHelper.showLoadingView()
            self.objgetServicePackagesList = HttpWrapper.init()
            self.objgetServicePackagesList.delegate = self
            self.objgetServicePackagesList.requestWithparamdictParamPostMethodwithHeaderAndDictGet(url: kgetServicePackageList, dictParams: dictParameters, headers: header)

            //self.objgetServicePackagesList.requestWithparamdictParamPostMethodwithHeaderGet(url: kgetServicePackageList, headers: header)
        }
        
    }
    
    
    // MARK: - getPackagesListByCategory API
    
    func getPackagesListByCategory()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            var languageCode = ""
            if UserDefaults.standard.value(forKey: kAppLanguage) != nil {
                languageCode = UserDefaults.standard.value(forKey: kAppLanguage) as! String
            }
            
            let dictParameters:[String:AnyObject] = ["chrCategory" : "S" as AnyObject,
                                                     "lang" : languageCode as AnyObject]
            
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objgetPackagesListByCategory = HttpWrapper.init()
            self.objgetPackagesListByCategory.delegate = self
            self.objgetPackagesListByCategory.requestWithparamdictParamPostMethodwithHeaderAndDictGet(url: kgetPackagesListByCategory, dictParams: dictParameters, headers: header)
        }
        
    }

    
    
    
    
    
    
    
    // MARK: - HttpWrapperfetchDataSuccess
    
    func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary)
    {
        AppHelper.hideLoadingView()
        print(dicsResponse)
        if wrapper == objgetServicePackagesList {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                appDelegate.arrCategorylist.removeAllObjects()
                
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray
                {
                    appDelegate.arrCategorylist = tempNames.mutableCopy() as! NSMutableArray
                }
                print(appDelegate.arrCategorylist)
                collectionviewselect.reloadData()
            }
            else
            {
                //AppHelper.showAlert("Alert", message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }else if wrapper == objgetPackagesListByCategory {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                carServiceArrayList.removeAllObjects()
                
                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray
                {
                    carServiceArrayList = tempNames.mutableCopy() as! NSMutableArray
                }
                print(carServiceArrayList)
                collectionviewselect.reloadData()
            }
            else
            {
                //AppHelper.showAlert("Alert", message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }
        
    }
    // MARK: - HttpWrapperfetchDataFail
    
    func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError)
    {
        AppHelper.hideLoadingView()
        print(error.debugDescription)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension Home_Vc{
    
    
     func makeCircularWithLeftBottmTwoCornerRadius(_ view:UIView,cornerRadius:CGFloat){
        view.clipsToBounds = true
        view.layer.cornerRadius = cornerRadius
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
    }
    
    func makeCircularWithRightBottmTwoCornerRadius(_ view:UIView,cornerRadius:CGFloat){
        view.clipsToBounds = true
        view.layer.cornerRadius = cornerRadius
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        
    }

    
    func makeCircularWithLeftTopTwoCornerRadius(_ view:UIView,cornerRadius:CGFloat){
        view.clipsToBounds = true
        view.layer.cornerRadius = cornerRadius
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMinXMinYCorner, .layerMinXMaxYCorner]
    }
    
    func makeCircularWithRightTopTwoCornerRadius(_ view:UIView,cornerRadius:CGFloat){
        view.clipsToBounds = true
        view.layer.cornerRadius = cornerRadius
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner,.layerMinXMinYCorner]
        
    }
    
    
    func makeCircularWithLeftTwoCornerRadius(_ view:UIView,cornerRadius:CGFloat){
        view.clipsToBounds = true
        view.layer.cornerRadius = cornerRadius
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
    }
    
    func makeCircularWithRightTwoCornerRadius(_ view:UIView,cornerRadius:CGFloat){
        view.clipsToBounds = true
        view.layer.cornerRadius = cornerRadius
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner]
        
    }

    
    
     func makeCircularWithTopTwoCornerRadius(_ view:UIView,cornerRadius:CGFloat){
        view.clipsToBounds = true
        view.layer.cornerRadius = cornerRadius
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMinXMinYCorner]//[.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        
    }

}
