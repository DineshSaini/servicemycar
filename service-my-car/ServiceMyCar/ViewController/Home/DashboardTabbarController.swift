//
//  DashboardTabbarController.swift
//  ServiceMyCar
//
//  Created by admin on 10/06/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import SJSwiftSideMenuController
import Alamofire
import SwiftyJSON

let backGroundColor = UIColor(red: 244.0/255.0, green: 245.0/255.0, blue: 248.0/255.0, alpha: 1.0)

class DashboardTabbarController: UITabBarController {
    
    var myCarTabButton: UITabBarItem!
    var myAddressTabButton : UITabBarItem!
    var homeTabButton:UITabBarItem!
    var myServiceTabButton:UITabBarItem!
    var profileTabButton:UITabBarItem!
    
    var homeButton:UIButton!
    
    
    var srtName:String = ""
    var redirectUrl:String = ""
    var orderID:String = ""
    
    
    var myTababar = CustomizedTabBar()
    
    var badgeButton : MIBadgeButton = MIBadgeButton(frame:CGRect(x:-10, y: 30, width: 40, height: 20)) // Step 1
    
    var isTabBarSet:Bool = false
    
    var selectIndex = 0
    
        override var prefersStatusBarHidden: Bool {
            return false
        }
        override var preferredStatusBarStyle: UIStatusBarStyle {
            return .lightContent
    }
        
        override func viewWillAppear(_ animated: Bool) {
            self.navigationController?.isNavigationBarHidden = false
            //self.setupTabbarView()
            self.navigationController?.setupNavigation()
            self.setSelectedTabButton()
        }
    
    
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        if isTabBarSet {
            return
        }
        self.selectedIndex = self.selectIndex
        self.setupMiddleButton()
        homeButton.isSelected = (self.selectIndex == 2)
    }

        override func viewDidLoad() {
            super.viewDidLoad()
            self.selectIndex = 2
            self.navigationItem.title = "".localized()
            self.tabBar.isTranslucent = true
            self.tabBar.clipsToBounds = true
            self.setUpTabBarElement()
            self.setupNavigationButton()
            self.observeNotificationDetail()

            //self.setupTabbarView()
        }
        deinit {
            NotificationCenter.default.removeObserver(self)
        }
    
    
    
  
    
    func setupTabbarView(){
        var newTabBarFrame = tabBar.frame
        let newTabBarHeight: CGFloat = newTabBarFrame.size.height+10
        newTabBarFrame.size.height = newTabBarHeight
        newTabBarFrame.origin.y =  tabBar.frame.origin.y-10
        let image = UIImage(named: "tab_bar_image")
        if let image = image {
            var resizeImage: UIImage?
            let size = CGSize(width: UIScreen.main.bounds.size.width, height:newTabBarHeight+10)
            UIGraphicsBeginImageContextWithOptions(size, false, 0)
            image.draw(in: CGRect(origin: CGPoint.zero, size: size))
            resizeImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            tabBar.backgroundImage = resizeImage?.withRenderingMode(.alwaysOriginal)//resizableImage(withCapInsets: UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0), resizingMode: UIImage.ResizingMode.stretch)//(.alwaysOriginal)
        }
        if self.homeButton != nil{
            homeButton = UIButton(frame: CGRect(x: 0.0, y: 0.0, width: 50, height: 50))
            var menuButtonFrame = homeButton.frame
            
            menuButtonFrame.origin.y = self.view.bounds.height - (menuButtonFrame.height+35) - self.view.safeAreaInsets.bottom
            menuButtonFrame.origin.x = self.view.bounds.width/2 - menuButtonFrame.size.width/2
            homeButton.frame = menuButtonFrame
        }
        self.tabBar.frame = newTabBarFrame
        self.isTabBarSet = true

    }
    
    
    
    func observeNotificationDetail(){
        NotificationCenter.default.addObserver(self, selector: #selector(DashboardTabbarController.bookingScheduleNotification(_:)), name: .BOOKING_SUCCESS_NOTIFICATION, object: nil)

    }
    
    
    
    func setupNavigationButton(){

        // Step 2
        badgeButton.edgeInsetTop = 10
        badgeButton.setImage(UIImage(named : "notification"), for: .normal)
        badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        print(appDelegate.NCount)
        badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        badgeButton.badgeString = appDelegate.NCount
        if appDelegate.NCount == "0"
        {
            badgeButton.badgeBackgroundColor = UIColor.clear
            badgeButton.badgeTextColor = UIColor.clear
        }
        else
        {
            badgeButton.badgeBackgroundColor = colorGreen
            badgeButton.badgeTextColor = UIColor.white
        }
        badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        let barButton2 : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton2
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)
        
    }
    
    
    
    
    
    
    
    
    // MARK: - Google Api Calling
    
    // Step 4
    
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            //self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                
                badgeButton.badgeString = appDelegate.NCount
                if appDelegate.NCount == "0"
                {
                    badgeButton.badgeBackgroundColor = UIColor.clear
                    badgeButton.badgeTextColor = UIColor.clear
                }
                else
                {
                    badgeButton.badgeBackgroundColor = colorGreen
                    badgeButton.badgeTextColor = UIColor.white
                }
            }
        }
    }
    
    // MARK:- goBack
    
    @objc func SideMenu()
    {
        SJSwiftSideMenuController.toggleLeftSideMenu()
    }
    
    
    // MARK:- goBack
    
    // step 3
    
    @objc func NotificationAlert(){
        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
        
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    
    
    @objc func bookingScheduleNotification(_ notification: Notification) {
        if let bookingInfo = notification.userInfo as? [String:AnyObject]{
            if bookingInfo["booking"] != nil{
                self.navigationSetupView(tittle: "My SERVICES".localized())
                self.selectIndex = 3
                self.selectedIndex = 3
                self.selectedIndex = self.selectIndex
                self.tabBar(UITabBar.init(), didSelect: myServiceTabButton)
            }
        }
    }
    
    

    
    

    



        func setUpTabBarElement(){
        
            myCarTabButton = UITabBarItem()
            myCarTabButton.title = "My CARS".localized()
            self.myCarTabButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.lightText], for: .normal)
            self.myCarTabButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for:.selected)
            
            
            myAddressTabButton = UITabBarItem()
            myAddressTabButton.title = "My ADDRESS".localized()
            self.myAddressTabButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.lightText], for: .normal)
            self.myAddressTabButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for:.selected)

            
            homeTabButton = UITabBarItem()
            homeTabButton.title = "HOME".localized()
            self.homeTabButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.lightText], for: .normal)
            self.homeTabButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for:.selected)

            myServiceTabButton = UITabBarItem()
            let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
            if languageCode == "en" {
                self.myServiceTabButton.title = "My SERVICES"
            }else {
                self.myServiceTabButton.title = "الخدمات المطلوبة"
            }

            //myServiceTabButton.title = "My SERVICES".localized()
            self.myServiceTabButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.lightText], for: .normal)
            self.myServiceTabButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for:.selected)
            
            profileTabButton = UITabBarItem()
            profileTabButton.title = "PROFILE".localized()
            self.profileTabButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.lightText], for: .normal)
            self.profileTabButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for:.selected)
            
            setUpTabBarElements()
            
            let storyboard = UIStoryboard(name: "HomeSB", bundle:nil)
            
            let myCarVC = storyboard.instantiateViewController(withIdentifier: "CarList_Vc") as! CarList_Vc
            myCarVC.tabBarItem = myCarTabButton
            
            
            
            let myAddressVC = storyboard.instantiateViewController(withIdentifier: "MyAddressViewController") as! MyAddressViewController
            myAddressVC.tabBarItem = myAddressTabButton
            
            
            
            let homeVC = storyboard.instantiateViewController(withIdentifier: "Home_Vc") as! Home_Vc
            homeVC.tabBarItem = homeTabButton

            
            let myServiceVc = storyboard.instantiateViewController(withIdentifier: "MyServiceViewController") as! MyServiceViewController
            myServiceVc.tabBarItem = myServiceTabButton
            
            let profileVC = storyboard.instantiateViewController(withIdentifier: "Profile_Vc") as! Profile_Vc
            profileVC.tabBarItem = profileTabButton
            self.viewControllers = [myCarVC, myAddressVC, homeVC, myServiceVc, profileVC]
            self.setSelectedTabButton()
            tabBar.backgroundColor = backGroundColor
            self.tabBar.setNeedsDisplay()
            self.tabBar.layoutIfNeeded()
           // self.view.layoutIfNeeded()
            //self.view.setNeedsDisplay()
        }
        
        
        func setUpTabBarElements() {
            
            self.myCarTabButton.image = UIImage(named: "my_cars")?.withRenderingMode(.alwaysOriginal)
            self.myAddressTabButton.image = UIImage(named: "my_address")?.withRenderingMode(.alwaysOriginal)
            //self.homeTabButton.image = UIImage(named:"")?.withRenderingMode(.alwaysOriginal)
            self.myServiceTabButton.image = UIImage(named:"my_service")?.withRenderingMode(.alwaysOriginal)
            self.profileTabButton.image = UIImage(named:"profile")?.withRenderingMode(.alwaysOriginal)
            
            
            self.myCarTabButton.selectedImage = UIImage(named: "my_car")?.withRenderingMode(.alwaysOriginal)
            self.myAddressTabButton.selectedImage = UIImage(named: "my_address")?.withRenderingMode(.alwaysOriginal)
            //self.homeTabButton.selectedImage = UIImage(named:"")?.withRenderingMode(.alwaysOriginal)
            self.myServiceTabButton.selectedImage = UIImage(named:"my_service")?.withRenderingMode(.alwaysOriginal)
            self.profileTabButton.selectedImage = UIImage(named:"profile")?.withRenderingMode(.alwaysOriginal)
            
        }
        
        
        func setSelectedTabButton(){
            if self.selectedIndex == 0{
               // self.setupRightSwitchButtonStatusBarButton()
            }else if self.selectedIndex == 1{
                //self.setupRightAddCarBarButton()
            }else if self.selectedIndex == 4{
                //self.setupRightEditProfileBarButton()
            }else if self.selectIndex == 3{
                let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
                if languageCode == "en" {
                    self.myServiceTabButton.title = "My SERVICES"
                }else {
                    self.myServiceTabButton.title = "الخدمات المطلوبة"
                }

            }else{
               // self.setupRightBarButton()
            }
            
        }
    
    
    func setupMiddleButton() {
//        let width : CGFloat = self.view.frame.size.width/5
//        let height = tabBarHeight //self.tabBar.frame.size.height
        homeButton = UIButton(frame: CGRect(x: 0.0, y: 0.0, width: 50, height: 50))
        var menuButtonFrame = homeButton.frame
        
        menuButtonFrame.origin.y = self.view.bounds.height - (menuButtonFrame.height+25) - self.view.safeAreaInsets.bottom //self.view.bounds.height - menuButtonFrame.height - 30
        menuButtonFrame.origin.x = self.view.bounds.width/2 - menuButtonFrame.size.width/2
        homeButton.frame = menuButtonFrame
        homeButton.addTarget(self, action: #selector(onClickHomeButton(_:)), for: .touchUpInside)
        self.view.addSubview(homeButton)
        homeButton.setImage(UIImage(named: "home_ic")!.withRenderingMode(.alwaysOriginal), for: UIControl.State.normal)
      
        self.tabBar.setNeedsDisplay()
        self.tabBar.layoutIfNeeded()
        self.view.layoutIfNeeded()
        self.view.setNeedsDisplay()
    }
    
    
    @IBAction func onClickHomeButton(_ sender: UIButton){
        if self.selectedIndex == 2{return}
        self.navigationSetupView(tittle: "".localized())
        self.selectIndex = 2
        self.selectedIndex = self.selectIndex
        homeButton.isSelected = (self.selectIndex == 2)
        self.homeButton.isSelected = true

    }
        
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
    
        /*
         // MARK: - Navigation
         
         // In a storyboard-based application, you will often want to do a little preparation before navigation
         override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         // Get the new view controller using segue.destinationViewController.
         // Pass the selected object to the new view controller.
         }
         */
        
    }
    
    extension DashboardTabbarController{
        override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
              if item == myCarTabButton{
                if self.selectedIndex == 0{return}
                self.selectIndex = 0
                self.navigationSetupView(tittle: "MY CARS".localized())
                self.homeButton.isSelected = false
                self.navigationItem.hidesBackButton = true
                //self.setupRightAddCarBarButton()
                
              }else if item == myAddressTabButton{
                if self.selectedIndex == 1{return}
                self.selectIndex = 1
                self.navigationSetupView(tittle: "My ADDRESS".localized())
                self.homeButton.isSelected = false

                //self.setupRightSwitchButtonStatusBarButton()
              }
              else if item == homeTabButton{
                if self.selectedIndex == 2{return}
                self.selectIndex = 2
                self.navigationSetupView(tittle: "".localized())
                self.homeButton.isSelected = true

                //self.setupRightBarButton()
                
            }else if item == myServiceTabButton{
                 //self.myServiceTabButton.title = "My SERVICES".localized()
                let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
                if languageCode == "en" {
                    self.myServiceTabButton.title = "My SERVICES"
                }else {
                    self.myServiceTabButton.title = "الخدمات المطلوبة"
                }
               

                if self.selectedIndex == 3{return}
                self.selectIndex = 3
                self.navigationSetupView(tittle:"My SERVICES".localized())
                self.homeButton.isSelected = false
                setSelectedTabButton()
                
            }else if item == profileTabButton{
                if self.selectedIndex == 4{return}
                self.selectIndex = 4
                self.navigationSetupView(tittle: "PROFILE".localized())
                self.homeButton.isSelected = false

                //self.setupRightEditProfileBarButton()
            }
        }
        
        func navigationSetupView(tittle:String){
            self.navigationController?.isNavigationBarHidden = false
            self.navigationItem.title = tittle.uppercased()
            self.navigationController?.navigationBar.tintColor = backGroundColor
            self.navigationController?.navigationBar.barTintColor = backGroundColor
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        }
}

////////////////


@IBDesignable
class CustomizedTabBar: UITabBar {
    
    private var shapeLayer: CALayer?
    
    private func addShape() {
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = createPath()
        shapeLayer.strokeColor = colorGreen.cgColor//UIColor.lightGray.cgColor
        shapeLayer.fillColor = colorGreen.cgColor//UIColor.white.cgColor
        shapeLayer.lineWidth = 1.0
        
        if let oldShapeLayer = self.shapeLayer {
            self.layer.replaceSublayer(oldShapeLayer, with: shapeLayer)
        } else {
            self.layer.insertSublayer(shapeLayer, at: 0)
        }
        
        self.shapeLayer = shapeLayer
    }
    
    override func draw(_ rect: CGRect) {
        self.addShape()
    }
    
    func createPath() -> CGPath {
        
        let height: CGFloat = 30.0
        let path = UIBezierPath()
        let centerWidth = self.frame.width / 2
        
        path.move(to: CGPoint(x: 0, y: 0)) // start top left
        path.addLine(to: CGPoint(x: (centerWidth - height * 2), y: 0)) // the beginning of the trough
        
        // first curve down
        path.addCurve(to: CGPoint(x: centerWidth, y: height),
                      controlPoint1: CGPoint(x: (centerWidth - 30), y: 0), controlPoint2: CGPoint(x: centerWidth - 35, y: height))
        // second curve up
        path.addCurve(to: CGPoint(x: (centerWidth + height * 2), y: 0),
                      controlPoint1: CGPoint(x: centerWidth + 35, y: height), controlPoint2: CGPoint(x: (centerWidth + 30), y: 0))
        
        // complete the rect
        path.addLine(to: CGPoint(x: self.frame.width, y: 0))
        path.addLine(to: CGPoint(x: self.frame.width, y: self.frame.height))
        path.addLine(to: CGPoint(x: 0, y: self.frame.height))
        path.close()
        
        return path.cgPath
    }
    
//    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
//        let buttonRadius: CGFloat = 35
//        return abs(self.center.x - point.x) > buttonRadius || abs(point.y) > buttonRadius
//    }
    
    func createPathCircle() -> CGPath {
        
        let radius: CGFloat = 34.0
        let path = UIBezierPath()
        let centerWidth = self.frame.width / 2
        
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: (centerWidth - radius * 2), y: 0))
        path.addArc(withCenter: CGPoint(x: centerWidth, y: 0), radius: radius, startAngle: CGFloat(180).degreesToRadians, endAngle: CGFloat(0).degreesToRadians, clockwise: false)
        path.addLine(to: CGPoint(x: self.frame.width, y: 0))
        path.addLine(to: CGPoint(x: self.frame.width, y: self.frame.height))
        path.addLine(to: CGPoint(x: 0, y: self.frame.height))
        path.close()
        return path.cgPath
    }
}

extension CGFloat {
    var degreesToRadians: CGFloat { return self * .pi / 180 }
    var radiansToDegrees: CGFloat { return self * 180 / .pi }
}
