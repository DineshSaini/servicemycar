//
//  ApplyCouponViewController.swift
//  ServiceMyCar
//
//  Created by Parikshit on 23/06/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol ApplyCouponViewControllerDeledate {
    func applyCouponViewController(viewController: UIViewController, didIncludingVatAmount includingVatAmount:Float,didApplyCoupon isApplyCoupon:Bool,didDiscountAmount discountAmount:Float,didCoupanCode coupanCode:String, didSuccess success:Bool,cuopinId:String,didExcludingVatAmount excludingVatPrice:Float)
    
    
    
}

class ApplyCouponViewController: UIViewController,HttpWrapperDelegate{
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var btnApplyPromo: UIButtonX!
    @IBOutlet weak var txtpromocode: UITextField!
    @IBOutlet weak var codeViewContainer: UIView!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var totalIncludingAmount: UILabel!
    @IBOutlet weak var totalExcludingAmount: UILabel!
    
    @IBOutlet weak var couponTittleLabel:UILabel!
    @IBOutlet weak var totalExcludingVatLabel : UILabel!
     @IBOutlet weak var discountAmountLabel : UILabel!
     @IBOutlet weak var totalIncludingPriceLabel : UILabel!
    



    var objapplyPromoCode = HttpWrapper()
    var objRemovePromoCode = HttpWrapper()
    var net:NetworkReachabilityManager = NetworkReachabilityManager()!
    var isApply = false
    var couponId:String = String()
    var srtPrice = ""
    var delegate: ApplyCouponViewControllerDeledate?
    var discountAmnt:Float = 0
    
    var coupanCode: String = ""
    var vatPercnt:Float = 0.0
    var includeVatPrice: Float = 0
    var excludeVatPrice :Float = 0
    


    override func viewDidLoad() {
        super.viewDidLoad()
      self.backView.layer.cornerRadius = 5
        self.backView.layer.masksToBounds = true
        appDelegate.Bookings.couponId = "0"
        AppHelper.makeViewCircularWithRespectToHeight(confirmButton, borderColor: .clear, borderWidth: 0.0)
        AppHelper.makeViewCircularWithRespectToHeight(btnApplyPromo, borderColor: .clear, borderWidth: 0.0)
        if isApply {
            btnApplyPromo.setTitle("payment_remove".localized(), for: .normal)
            self.discountLabel.text = String(format: "%.2f",self.discountAmnt ) + "app_currency".localized()
            self.txtpromocode.text = self.coupanCode
            totalExcludingAmount.text = String(format: "%.2f",self.excludeVatPrice ) + "app_currency".localized()
            self.totalIncludingAmount.text = String(format: "%.2f",self.includeVatPrice ) + "app_currency".localized()
        }else{
            btnApplyPromo.setTitle("payment_apply".localized(), for: .normal)
            self.includeVatPrice = 0
            self.excludeVatPrice =  Float(srtPrice) ?? 0
            self.totalExcludingAmount.text = String(format: "%.2f",self.excludeVatPrice ) + "app_currency".localized()
            self.discountLabel.text = String(format: "%.2f",self.discountAmnt ) + "app_currency".localized()
            self.totalIncludingAmount.text = String(format: "%.2f",self.includeVatPrice ) + "app_currency".localized()

        }
        
        confirmButton.setTitle("confirm".localized(), for: .normal)
        couponTittleLabel.text = "Apply Coupon".localized()
        totalExcludingVatLabel.text = "Total excluding vat :".localized()
        discountAmountLabel.text = "Discount Amount :".localized()
        totalIncludingPriceLabel.text = "Total including Price".localized()
        txtpromocode.placeholder = "Have a Promocode ?".localized()
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        if languageCode != "en" {
            txtpromocode.textAlignment = .right
        }

    }
    
    
    
    
    @IBAction func onClickBtnApplyPromo(_ sender: Any) {
        
        if isApply == true{
            RemovePromocode()
        }
        else{
            ApplyPromoCode()
        }
        
    }
    
    
    @IBAction func onClickConfirmButton() {
        if isApply{

            self.delegate?.applyCouponViewController(viewController: self, didIncludingVatAmount: self.includeVatPrice, didApplyCoupon: self.isApply, didDiscountAmount:discountAmnt , didCoupanCode: self.coupanCode, didSuccess: true, cuopinId: self.couponId, didExcludingVatAmount: excludeVatPrice)
        }else{

            self.delegate?.applyCouponViewController(viewController: self, didIncludingVatAmount: 0, didApplyCoupon:self.isApply, didDiscountAmount: discountAmnt, didCoupanCode: self.coupanCode, didSuccess: true, cuopinId: self.couponId, didExcludingVatAmount: 0)

        }
        
    }
    
    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        guard let touch = touches.first else{return}
//        guard let touchedView = touch.view else{return}
//        if touchedView == self.view{
//            dismiss(animated: true, completion: nil)
//        }
//    }
    
    @IBAction func onClickCloseButton(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    

    
    
    // MARK: - ApplyPromoCode API
    func ApplyPromoCode()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else if isLoginDataValid()
        {
            
            let dictParameters:[String:AnyObject] = ["coupon" : txtpromocode.text as AnyObject,
                                                     "packageId" : appDelegate.Bookings.Package.Id as AnyObject,
                                                     "carModel" : "" as AnyObject]
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objapplyPromoCode = HttpWrapper.init()
            self.objapplyPromoCode.delegate = self
            self.objapplyPromoCode.requestWithparamdictParamPostMethodwithHeader(url: kapplyCoupons, dicsParams: dictParameters, headers: header)
            
        }
        
    }
    // MARK: - RemovePromocode API
    
    func RemovePromocode()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
            
            let dictParameters:[String:AnyObject] = ["couponId" : couponId as AnyObject]
            
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objRemovePromoCode = HttpWrapper.init()
            self.objRemovePromoCode.delegate = self
            self.objRemovePromoCode.requestWithparamdictParamPostMethodwithHeader(url: kremoveCoupon, dicsParams: dictParameters, headers: header)
        }
        
    }
    
    
    
    
    // MARK: - HttpWrapperfetchDataSuccess
    
    func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary)
    {
        AppHelper.hideLoadingView()
         if wrapper == objapplyPromoCode {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                
                
                if let tempNames: NSDictionary = dicsResponse.value(forKey: "data") as? NSDictionary
                {
                    if let discountAmount = tempNames.value(forKey: "discountAmount") as? String
                    {
                        self.discountAmnt = Float(discountAmount)!
                        appDelegate.Bookings.discountAmount = discountAmount
                        
                        self.includeVatPrice = self.totalAmountWithdiscountAndVat()
                        self.excludeVatPrice = self.calculateAmountWithDiscount()
                        self.totalExcludingAmount.text = String(format: "%.2f",self.excludeVatPrice ) + "app_currency".localized()
                       self.totalIncludingAmount.text = String(format: "%.2f",self.includeVatPrice) + "app_currency".localized()


                        
                    }else  if let discountAmount = tempNames.value(forKey: "discountAmount")  as? NSNumber
                    {
                        let price = Float(truncating: discountAmount)
                        self.discountAmnt = price
                        appDelegate.Bookings.discountAmount = "\(price)"
                        
                        self.includeVatPrice = self.totalAmountWithdiscountAndVat()
                        self.excludeVatPrice = self.calculateAmountWithDiscount()
                        totalExcludingAmount.text = String(format: "%.2f",self.excludeVatPrice ) + "app_currency".localized()
                         self.totalIncludingAmount.text = String(format: "%.2f",self.includeVatPrice) + "app_currency".localized()

                    }
                    
                    if let cpnID = tempNames.value(forKey: "couponId") as? String
                    {
                        self.couponId = cpnID
                        appDelegate.Bookings.couponId = cpnID
                    }
                    isApply = true
                    btnApplyPromo.setTitle("payment_remove".localized(), for: .normal)
                    self.discountLabel.text = String(format: "%.2f",self.discountAmnt ) + "app_currency".localized()
                    self.coupanCode = self.txtpromocode.text!
                    self.txtpromocode.resignFirstResponder()
                }
            }
            else
            {
                AppHelper.showAlert("app_alert_title".localized(), message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }
        else if wrapper == objRemovePromoCode {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
 
                self.txtpromocode.text = ""

                isApply = false
                btnApplyPromo.setTitle("payment_apply".localized(), for: .normal)
                self.excludeVatPrice = Float(srtPrice) ?? 0
                self.discountAmnt = 0
                self.includeVatPrice = 0
                self.couponId = "0"
                appDelegate.Bookings.couponId = "0"
                appDelegate.Bookings.discountAmount = "0"
                self.discountLabel.text = String(format: "%.2f",self.discountAmnt ) + "app_currency".localized()
                self.totalExcludingAmount.text = String(format: "%.2f",self.excludeVatPrice ) + "app_currency".localized()
                self.totalIncludingAmount.text = String(format: "%.2f",self.includeVatPrice ) + "app_currency".localized()

            }
            else
            {
                AppHelper.showAlert("app_alert_title".localized(), message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }
        //
        
    }
    // MARK: - HttpWrapperfetchDataFail
    
    func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError)
    {
        AppHelper.hideLoadingView()
        print(error.debugDescription)
    }
    
    
    
    // MARK: - isLoginDataValid
    func isLoginDataValid() -> Bool
    {
        
        
        if (txtpromocode.text?.trimmingCharacters(in: CharacterSet.whitespaces).count)! <= 0
        {
            AppHelper.showAlert("app_alert_title".localized(), message: "payment_promocode_validation".localized())
            return false
        }
        
        
        return true
    }
    
    
    func totalAmountWithdiscountAndVat() -> Float {
        var finalPrice :Float  = 0
        
        let amountWithDiscount  = (Float(srtPrice) ?? 0) - self.discountAmnt
        let makeVat = amountWithDiscount * vatPercnt/100
         finalPrice = amountWithDiscount + makeVat
        return finalPrice
        
    }
    
    func calculateAmountWithDiscount() -> Float {
        var finalPrice :Float  = 0
         finalPrice  = (Float(srtPrice) ?? 0) - discountAmnt

        return finalPrice
    }

}
