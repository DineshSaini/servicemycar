//
//  DateTime_Vc.swift
//  ServiceMyCar
//
//  Created by baps on 24/01/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit

class DateTime_Vc: UIViewController ,UITextFieldDelegate, UIPickerViewDelegate , UIPickerViewDataSource  {

    @IBOutlet weak var btnTransport2: UIButtonX!
    @IBOutlet weak var btntransport1: UIButtonX!
    @IBOutlet weak var btnNext: UIButtonX!
    @IBOutlet weak var txtChooseOption: UITextField!
    @IBOutlet weak var lblChooseOption: UILabel!
    @IBOutlet weak var lblDatails: UILabel!
    @IBOutlet weak var txtSelectDateTime: UITextField!
    @IBOutlet weak var lblSelectDateTime: UILabel!
    @IBOutlet weak var btnEmergency: UIButtonX!
    @IBOutlet weak var topMarginOfNext: NSLayoutConstraint!
    var isAnyOneSelected = false
    @IBOutlet var myDatePicker:UIDatePicker!
    var isDropSelection = true
    var arrService = ["date_time_arrange_a_free_collection_and_delivery".localized(),"date_time_drive_my_car_to_the_garage_and_wait_while_service".localized()]
    var myPickerView : UIPickerView!
    var strSelectedTime : String = ""
    var strSelectedDate : String = ""
    var isSelectOne = true
    let badgeButton : MIBadgeButton = MIBadgeButton(frame:CGRect(x: -10, y: 15, width: 40, height: 20))
    var isEmergency = false
    var isAssistance = false
    var isFrom = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        btnNext.backgroundColor = colorGreen
        txtSelectDateTime.delegate = self
       // txtChooseOption.delegate = self
       // txtChooseOption.text = arrService[0]
        btntransport1.setTitle(arrService[0], for: .normal)
        btnTransport2.setTitle(arrService[1], for: .normal)
        btntransport1.setTitleColor(UIColor.black, for: .normal)
        btnTransport2.setTitleColor(UIColor.black, for: .normal)
        
        btnEmergency.setTitle("date_time_i_need_urgent_assistance".localized(), for: .normal)
        btnEmergency.setTitleColor(UIColor.black, for: .normal)
        
        if isEmergency == true {
            topMarginOfNext.constant = 15
            btntransport1.isHidden = true
            btnTransport2.isHidden = true
            btnEmergency.isHidden = false
        }
        else {
            topMarginOfNext.constant = 72
            btnEmergency.isHidden = true
        }
        
        self.navigationController?.isNavigationBarHidden = false
        let lbNavTitle = UILabel(frame: CGRect(x: CGFloat(35), y:self.view.bounds.size.width/2-235, width: CGFloat(200), height: CGFloat(40)))
        lbNavTitle.textAlignment = .center
        lbNavTitle.text = "date_time_title".localized()
        lbNavTitle.textColor = Colorblack
        lbNavTitle.font = UIFont(name: Regular, size: 16)
        
        //        let logo = UIImage(named: "AppTopIcon")
        //        let imageView = UIImageView(image:logo)
        //        imageView.contentMode = .scaleAspectFit
        // self.navigationItem.titleView = imageView
        self.navigationItem.titleView = lbNavTitle
        self.navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.white
        
        let titleDict: NSDictionary = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [NSAttributedString.Key : Any]
        
        let button = UIButton.init(type: .custom)
        //backarrow , menu
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        
        if languageCode == "en" {
            button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
        }
        else {
            button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
        }
        button.addTarget(self, action:#selector(SideMenu), for: UIControl.Event.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        //        let buttonLogout = UIButton.init(type: .custom)
        //        buttonLogout.setImage(UIImage.init(named: "notification"), for: UIControl.State.normal)
        //        buttonLogout.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        //        buttonLogout.frame = CGRect.init(x: self.view.frame.size.width-30, y: 0, width: 25, height: 25)
        //        // buttonLogout.titleLabel!.font =  UIFont(name:FontSemibold, size: 14)
        //        let barButtonLog = UIBarButtonItem.init(customView: buttonLogout)
        // self.navigationItem.rightBarButtonItem = barButtonLog
        
      badgeButton.edgeInsetTop = 10
        badgeButton.setImage(UIImage(named : "notification"), for: .normal)
        print(appDelegate.NCount)
         badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        badgeButton.badgeString = appDelegate.NCount
        if appDelegate.NCount == "0"
        {
            badgeButton.badgeBackgroundColor = UIColor.clear
            badgeButton.badgeTextColor = UIColor.clear
        }
        else
        {
            badgeButton.badgeBackgroundColor = colorGreen
            badgeButton.badgeTextColor = UIColor.white
        }
        badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        let barButton2 : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton2
        //        getPackagesListByCategory()
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)
        
        
        lblSelectDateTime.text = "date_time_select_pickup_date_and_time".localized()
        if isEmergency == true {
            lblChooseOption.text = "date_time_emergency_assistance".localized()
        }
        else {
            lblChooseOption.text = "date_time_transport_option".localized()
        }
        btnNext.setTitle("date_time_next".localized(), for: .normal)
        txtSelectDateTime.placeholder = "date_time_select_date_and_time".localized()
        
    }
    
    // Step 3
    
    @objc func NotificationAlert()
    {
        guard let vcs = self.navigationController?.viewControllers else{return}
        let inStack = vcs.contains(where: { (vc) -> Bool in
            return vc is MyNotificationList_Vc
        })
        if inStack{
            self.performSegue(withIdentifier: unwindToNotificationController, sender: self)
        }else{
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        //        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
        //
        //        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    // Step 4
    
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            //self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                
                badgeButton.badgeString = appDelegate.NCount
                if appDelegate.NCount == "0"
                {
                    badgeButton.badgeBackgroundColor = UIColor.clear
                    badgeButton.badgeTextColor = UIColor.clear
                }
                else
                {
                    badgeButton.badgeBackgroundColor = colorGreen
                    badgeButton.badgeTextColor = UIColor.white
                }
            }
        }
    }
    
    @objc func doneClick() {
        txtChooseOption.resignFirstResponder()
        txtSelectDateTime.resignFirstResponder()
        
    }
    @objc func cancelClick() {
        txtChooseOption.resignFirstResponder()
        txtSelectDateTime.resignFirstResponder()
    }
    @IBAction func onClickBtnSelectDate(_ sender: Any) {
        
        pickefview(txtSelectDateTime)
    }
    
    // MARK:- Emergency Assistance
    
    @IBAction func onClickEmergencyAssistance(_ sender: Any) {
        if isAssistance == false {
            btnEmergency.backgroundColor = colorGreen
            btnEmergency.setTitleColor(UIColor.white, for: .normal)
            isAssistance = true
            let date = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let strDate = dateFormatter.string(from: date)
            let selectDate = appDelegate.SetDateFormat(strDate)
            
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm"
            let convertdate1: String = formatter.string(from: date)
            appDelegate.Bookings.Package.varPackageDate = "\(strDate) \(convertdate1)"
            
            let selectDate1 = appDelegate.SetDateFormat(strDate)
            strSelectedDate = strDate
            
            txtSelectDateTime.text = "\(selectDate)/\(convertdate1)"
            appDelegate.Bookings.Package.dateType = "2"
        }
        else {
            btnEmergency.backgroundColor = UIColor.white
            btnEmergency.setTitleColor(UIColor.black, for: .normal)
            isAssistance = false
            btntransport1.backgroundColor = UIColor.white
            btntransport1.setTitleColor(UIColor.black, for: .normal)
            self.strSelectedDate = ""
            txtSelectDateTime.text = ""
            appDelegate.Bookings.Package.dateType = "1"
        }
    }
    
    // MARK:- onClickBtnTransport1
    @IBAction func onClickBtnTransport1(_ sender: UIButton) {
        btntransport1.backgroundColor = colorGreen
        btntransport1.setTitleColor(UIColor.white, for: .normal)
         btnTransport2.setTitleColor(UIColor.black, for: .normal)
        btnTransport2.backgroundColor = UIColor.white
        isDropSelection = true
        isAnyOneSelected = true
    }
    // MARK:- onClickBtnTransport2
    @IBAction func onClickBtnTransport2(_ sender: UIButton) {
        btntransport1.backgroundColor = UIColor.white
        btnTransport2.backgroundColor = colorGreen
        btnTransport2.setTitleColor(UIColor.white, for: .normal)
        btntransport1.setTitleColor(UIColor.black, for: .normal)
        isDropSelection = false
        isAnyOneSelected = true
    }
    // MARK:- goBack
    
    @objc func SideMenu()
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickbtnNext(_ sender: Any) {
        
        if isLoginDataValid()
        {
            
            appDelegate.Bookings.Date.Date = txtSelectDateTime.text!
            appDelegate.Bookings.isDrop = isDropSelection
            
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "SelectAddress_Vc") as! SelectAddress_Vc
            nextViewController.isDropSelection = isDropSelection
            nextViewController.isFrom = isFrom
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        else
        {
            
        }
       
    }
    
    
    func pickUp(_ textField : UITextField){
        
        // UIPickerView
        self.myPickerView = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.myPickerView.delegate = self
        self.myPickerView.dataSource = self
        self.myPickerView.backgroundColor = UIColor.white
        
        textField.inputView = self.myPickerView
        myPickerView.reloadAllComponents()
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .black
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.white
        toolBar.sizeToFit()
        toolBar.backgroundColor = colorGreen
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "date_time_done".localized(), style: .plain, target: self, action: #selector(self.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "date_time_cancel".localized(), style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    //MARK:- PickerView Delegate & DataSource
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1
        {
            return arrService.count
        }
        else
        {
            return 0
        }
        
    }
    //    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    //        if pickerView.tag == 1
    //        {
    //            return (arrCarlist.object(at: row) as! NSDictionary).value(forKey: "varModel") as? String
    //        }
    //        else if pickerView.tag == 2
    //        {
    //            return (arrCarModel.object(at: row) as! NSDictionary).value(forKey: "varMake") as? String
    //        }
    //        else if pickerView.tag == 3
    //        {
    //            return "\(arrCarYears.object(at: row) as! NSNumber)"
    //        }
    //        else if pickerView.tag == 4
    //        {
    //            return (arrCarEngine.object(at: row) as! NSDictionary).value(forKey: "trimengine") as? String
    //        }
    //        else
    //        {
    //            return "tet"
    //        }
    //
    //
    //
    //    }
    //
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label: UILabel
        
        if let view = view {
            label = view as! UILabel
        }
        else {
            label = UILabel(frame: CGRect(x: 0, y: 0, width: pickerView.frame.width, height: 400))
        }
        
        label.lineBreakMode = .byWordWrapping;
        if pickerView.tag == 1
        {
            label.text = arrService[row]
        }
       
        else
        {
            label.text = "tet"
        }
        label.numberOfLines = 0;
        //label.text = arr[row]
        label.sizeToFit()
        return label;
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1
        {
            if row == 1
            {
               isDropSelection = false
            }
            else
            {
                isDropSelection = true
            }
            self.txtChooseOption.text = arrService[row]
        }
       
        // strCountryCode = (arrStores.object(at: row) as! NSDictionary).value(forKey: "cLocationName") as? String
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidEndEditing(_ textField: UITextField) {
       
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtSelectDateTime
        {
            pickefview(txtSelectDateTime)
            txtSelectDateTime.resignFirstResponder()
        }
        else
        {
            self.pickUp(txtChooseOption)
            
            myPickerView.tag = 1
        }
    
    }
    
    func pickefview(_ sender: AnyObject){
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad) {
            
            //            print("========= iPad =========")
            
            let alertView = UIAlertController(title: nil, message: "\n\n\n\n\n\n\n\n\n", preferredStyle: .alert)
            
            myDatePicker = UIDatePicker(frame: CGRect(x: 0, y: 50, width: 260, height: 162))
            
            alertView.view.addSubview(myDatePicker)
            myDatePicker.datePickerMode = .date
            myDatePicker.minimumDate = Date.tomorrow
            alertView.addAction(UIAlertAction(title: "date_time_done".localized(), style: .default, handler: { (alertAction) -> Void in
                self.mydatepice()
                
            }))
            
            
            alertView.addAction(UIAlertAction(title: "date_time_cancel".localized(), style: .cancel, handler: { (alertAction) -> Void in
                
                
            }))
            
            if let presenter = alertView.popoverPresentationController {
                presenter.sourceView = sender as? UIView
                presenter.sourceRect = sender.frame
            }
            
            present(alertView, animated: true, completion: nil)
            
            
        }
        else{
            
            let alertView = UIAlertController(title: nil, message: "\n\n\n\n\n\n\n\n\n", preferredStyle: .actionSheet)
            
            myDatePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: alertView.view.frame.size.width, height: 180))
            alertView.view.addSubview(myDatePicker)
            
            myDatePicker.datePickerMode = .dateAndTime
            myDatePicker.minimumDate = Date.tomorrow
            alertView.addAction(UIAlertAction(title: "date_time_done".localized(), style: .default, handler: { (alertAction) -> Void in
                self.mydatepice()
                
            }))
            
            
            alertView.addAction(UIAlertAction(title: "date_time_cancel".localized(), style: .cancel, handler: { (alertAction) -> Void in
                
                
            }))
            
            self.present(alertView, animated: true, completion: nil)
        }
    }
    
    
    // MARK: - isLoginDataValid
    func isLoginDataValid() -> Bool
    {
        
        if (txtSelectDateTime.text?.trimmingCharacters(in: CharacterSet.whitespaces).characters.count)! <= 0
        {
            AppHelper.showAlert("app_alert_title".localized(), message: validDate)
            return false
        }
        
        if isEmergency == false {
            if isAnyOneSelected == false
            {
                AppHelper.showAlert("app_alert_title".localized(), message: pleaseSelectTransport)
                return false
            }
        }
        
//        if (txtChooseOption.text?.trimmingCharacters(in: CharacterSet.whitespaces).characters.count)! <= 0
//        {
//            AppHelper.showAlert("Alert", message: SignUplName)
//            return false
//        }
//
        
      
        
        return true
    }
    //MARK: - DatePicker
    
    func mydatepice(){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let strDate = dateFormatter.string(from: myDatePicker.date)
        let selectDate = appDelegate.SetDateFormat(strDate)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        let convertdate1: String = formatter.string(from: myDatePicker.date)
        appDelegate.Bookings.Package.varPackageDate = "\(strDate) \(convertdate1)"
       // btnDate.setTitle(selectDate, for: UIControlState.normal)
       
        let selectDate1 = appDelegate.SetDateFormat(strDate)
        strSelectedDate = strDate
        
         txtSelectDateTime.text = "\(selectDate)/\(convertdate1)"
        appDelegate.Bookings.Package.dateType = "1"
        if !strSelectedTime.isEmpty
        {
           // btnTime.setTitle("Select Time", for: UIControlState())
            strSelectedTime = ""
        }
    }
    
    
    func mytimepice(){
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        let convertdate1: String = formatter.string(from: myDatePicker.date)
     //   btnTime.setTitle(convertdate1, for: UIControlState())
        strSelectedTime = convertdate1
        
        strSelectedDate =  strSelectedDate + "%20" + strSelectedTime
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
