//
//  SelectParts_Vc.swift
//  ServiceMyCar
//
//  Created by baps on 28/01/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import ZAlertView
import Alamofire
import SwiftyJSON

class SelectParts_Vc: UIViewController,UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout ,HttpWrapperDelegate{
    
    
    @IBOutlet weak var collectionviewselect: UICollectionView!
    var arrService1 = ["customer_parts","aftermarketparts","genuineparts"]
    var arrImage = ["wheel","spark_plug","toolss"]
    var arralerttxt = ["(Please be advised we cannot give any parts warranty with this option only labour )\nOn the day of the service please ensure that you have placed the correct amount of oil and the oil filter in the boot of the vehicle. Please make sure your parts are correct so there is no delay in servicing your vehicle. Please also read and follow the instructions on the email we will send you on completion of your booking.","Aftermarket parts will be the sourced with the highest quality and lowest price making sure we save you as much money as we can","Genuine parts are what vehicle manufacturers recommend."]
   var objGetExtraPartImage = HttpWrapper()
    var isSelectparts = false
   var net:NetworkReachabilityManager = NetworkReachabilityManager()!
    var arrPartslist:NSMutableArray = NSMutableArray()
    var isType = 2
    let badgeButton : MIBadgeButton = MIBadgeButton(frame:CGRect(x: -10, y: 15, width: 40, height: 20))
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        let lbNavTitle = UILabel(frame: CGRect(x: CGFloat(35), y:self.view.bounds.size.width/2-235, width: CGFloat(200), height: CGFloat(40)))
        lbNavTitle.textAlignment = .center
        
       
            lbNavTitle.text = "service_parts_title".localized()
       
        
        lbNavTitle.textColor = Colorblack
        lbNavTitle.font = UIFont(name: Regular, size: 16)
        
        //        let logo = UIImage(named: "AppTopIcon")
        //        let imageView = UIImageView(image:logo)
        //        imageView.contentMode = .scaleAspectFit
        // self.navigationItem.titleView = imageView
        self.navigationItem.titleView = lbNavTitle
        self.navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.white
        
        let titleDict: NSDictionary = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = titleDict as? [NSAttributedString.Key : Any]
        
        let button = UIButton.init(type: .custom)
        //backarrow , menu
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        
        if languageCode == "en" {
            button.setImage(UIImage.init(named: "backarrow"), for: UIControl.State.normal)
        }
        else {
            button.setImage(UIImage.init(named: "backarrowar"), for: UIControl.State.normal)
        }
        button.addTarget(self, action:#selector(SideMenu), for: UIControl.Event.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        let buttonLogout = UIButton.init(type: .custom)
        buttonLogout.setImage(UIImage.init(named: "notification"), for: UIControl.State.normal)
        buttonLogout.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        buttonLogout.frame = CGRect.init(x: self.view.frame.size.width-30, y: 0, width: 25, height: 25)
        // buttonLogout.titleLabel!.font =  UIFont(name:FontSemibold, size: 14)
        let barButtonLog = UIBarButtonItem.init(customView: buttonLogout)
        // self.navigationItem.rightBarButtonItem = barButtonLog
        
       getExtraPartImages()
        badgeButton.edgeInsetTop = 10
        badgeButton.setImage(UIImage(named : "notification"), for: .normal)
        print(appDelegate.NCount)
         badgeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        badgeButton.badgeString = appDelegate.NCount
        if appDelegate.NCount == "0"
        {
            badgeButton.badgeBackgroundColor = UIColor.clear
            badgeButton.badgeTextColor = UIColor.clear
        }
        else
        {
            badgeButton.badgeBackgroundColor = colorGreen
            badgeButton.badgeTextColor = UIColor.white
        }
        badgeButton.addTarget(self, action:#selector(NotificationAlert), for: UIControl.Event.touchUpInside)
        let barButton2 : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton2
        //        getPackagesListByCategory()
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.receiveToggleAuthUINotification(_:)),
                                               name: NSNotification.Name(rawValue: "ToggleAuthUINotification"),
                                               object: nil)
        
    }
    
    // Step 3
    
//    @objc func NotificationAlert()
//    {
//        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
//        
//        self.navigationController?.pushViewController(nextViewController, animated: true)
//        
//    }
    
    // Step 4
    
    @objc func receiveToggleAuthUINotification(_ notification: NSNotification) {
        if notification.name.rawValue == "ToggleAuthUINotification" {
            //self.toggleAuthUI()
            if notification.userInfo != nil {
                guard let userInfo = notification.userInfo as? [String:String] else { return }
                
                badgeButton.badgeString = appDelegate.NCount
                if appDelegate.NCount == "0"
                {
                    badgeButton.badgeBackgroundColor = UIColor.clear
                    badgeButton.badgeTextColor = UIColor.clear
                }
                else
                {
                    badgeButton.badgeBackgroundColor = colorGreen
                    badgeButton.badgeTextColor = UIColor.white
                }
            }
        }
    }
    
    // MARK:- goBack
    
    @objc func SideMenu()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func ShowAlert(Title:String,Msg:String,index:Int)
    {
        //        ZAlertView.positiveColor            = UIColor.color("#669999")
        //        ZAlertView.negativeColor            = UIColor.color("#CC3333")
        //        ZAlertView.blurredBackground        = true
        ZAlertView.showAnimation            = .bounceBottom
        ZAlertView.hideAnimation            = .bounceRight
        ZAlertView.initialSpringVelocity    = 0.9
        ZAlertView.duration                 = 2
        ZAlertView.titleColor = colorGreen
      
        let dialog = ZAlertView(title: Title,
                                message: Msg,
                                isOkButtonLeft: true,
                                okButtonText: "service_parts_confirm".localized(),
                                cancelButtonText: "service_parts_cancel".localized(),
                                okButtonHandler: { (alertView) -> () in
                                    alertView.dismissAlertView()
                                    appDelegate.Bookings.SelectedPart.varParts = self.arrService1[index]
                                    let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "AdditionalServiceNew_Vc") as! AdditionalServiceNew_Vc
                                    self.navigationController?.pushViewController(nextViewController, animated: true)

                                    
                                    
        },
                                cancelButtonHandler: { (alertView) -> () in
                                    alertView.dismissAlertView()
        }
        )
       
        dialog.width = self.view.frame.width - 40
         dialog.show()
        dialog.allowTouchOutsideToDismiss = true
    }
    // MARK:- goBack
    
    @objc func NotificationAlert()
    {
        guard let vcs = self.navigationController?.viewControllers else{return}
        let inStack = vcs.contains(where: { (vc) -> Bool in
            return vc is MyNotificationList_Vc
        })
        if inStack{
            self.performSegue(withIdentifier: unwindToNotificationController, sender: self)
        }else{
            let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
            
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        //        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyNotificationList_Vc") as! MyNotificationList_Vc
        //
        //        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    // MARK:- CollectionView Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
            return arrPartslist.count
      
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
      
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectService_cell", for: indexPath) as! SelectService_cell
            cell.viewBack.layer.borderColor = Colorblack.cgColor
            cell.viewBack.layer.cornerRadius = 5
            cell.viewBack.layer.borderWidth = 1
            cell.viewWidthConstant.constant = (collectionView.frame.size.width - 20) / 2.0
            if let name = (arrPartslist.object(at: indexPath.row) as! NSDictionary).value(forKey: "title") as? String
            {
                cell.lblTitle.text  = name
            }
        
            if let UserProfilePic = (arrPartslist.object(at: indexPath.row) as! NSDictionary).value(forKey: "icon") as? String
            {
                let url = "\(UserProfilePic)"
                print(url)
                
                if let url2 = URL(string: url) {
                    cell.imgCar.sd_setImage(with: url2, placeholderImage: UIImage(named: "img_plan1"))
                }
            }
        
            return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if isType == 1 || isType == 2
        {
            isType -= 1
            let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
            let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
            let size:CGFloat = (collectionView.frame.size.width - space) / 2.0
            return CGSize(width: size, height: size)
        }
        else
        {
            isType = 2
            let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
            let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
            let size:CGFloat = (collectionView.frame.size.width - space) / 2.0
            return CGSize(width: (collectionView.frame.size.width - space), height: size)
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let name = (arrPartslist.object(at: indexPath.row) as! NSDictionary).value(forKey: "title") as? String , let details = (arrPartslist.object(at: indexPath.row) as! NSDictionary).value(forKey: "details") as? String
        {
            ShowAlert(Title: name, Msg: details,index:indexPath.row )
        }
    }
    
    // MARK: - getExtraPartImages API

    func getExtraPartImages()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }

        else
        {

            var languageCode = ""
            if UserDefaults.standard.value(forKey: kAppLanguage) != nil {
                languageCode = UserDefaults.standard.value(forKey: kAppLanguage) as! String
            }
            
            let dictParameters:[String:AnyObject] = ["lang" : languageCode as AnyObject] //Arbic for er and English en
            print("URL :--\(dictParameters)")
            
            AppHelper.showLoadingView()
            self.objGetExtraPartImage = HttpWrapper.init()
            self.objGetExtraPartImage.delegate = self
            self.objGetExtraPartImage.requestWithparamdictParamPostMethodwithHeaderAndDictGet(url: kgetExtraPartImages, dictParams: dictParameters, headers: header)
        }

    }


    // MARK: - HttpWrapperfetchDataSuccess

    func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary)
    {
        AppHelper.hideLoadingView()
        print(dicsResponse)
        if wrapper == objGetExtraPartImage {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                arrPartslist.removeAllObjects()

                if let tempNames: NSArray = dicsResponse.value(forKey: "data") as? NSArray
                {
                    arrPartslist = tempNames.mutableCopy() as! NSMutableArray
                }
                print(arrPartslist)
                collectionviewselect.reloadData()
            }
            else
            {
                //AppHelper.showAlert("Alert", message:dicsResponse.value(forKey: "msg") as! String)
            }


        }

    }
    // MARK: - HttpWrapperfetchDataFail

    func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError)
    {
        AppHelper.hideLoadingView()
        print(error.debugDescription)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

