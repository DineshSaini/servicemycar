//
//  ViewController.swift
//  ServiceMyCar
//
//  Created by baps on 21/01/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import AVFoundation
class ViewController: UIViewController {

    var player: AVPlayer?
    override func viewDidLoad() {
        super.viewDidLoad()
       // self.view.backgroundColor = UIColor.green
        
        loadVideo()
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)

        notificationCenter.addObserver(self, selector: #selector(appCameToForeground), name: UIApplication.willEnterForegroundNotification, object: nil)

        // Do any additional setup after loading the view.
    }
    
    
    
    @objc func appMovedToBackground() {
        print("app enters background")
    }

    @objc func appCameToForeground() {
        print("app enters foreground")
     //   loadVideo()
    }

    private func loadVideo() {

        //this line is important to prevent background music stop
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.ambient, mode: AVAudioSession.Mode.default, options: AVAudioSession.CategoryOptions.defaultToSpeaker)
        } catch { }

        let path = Bundle.main.path(forResource: "WhatsApp Video 2019-01-21 at 8.52.43 AM", ofType:"mp4")

        player = AVPlayer(url: NSURL(fileURLWithPath: path!) as URL)
        let playerLayer = AVPlayerLayer(player: player)

        playerLayer.frame = self.view.frame
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspect
        playerLayer.zPosition = -1

        self.view.layer.addSublayer(playerLayer)
         NotificationCenter.default.addObserver(self, selector: #selector(finishVideo), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        player?.seek(to: CMTime.zero)
        player?.play()
    }
    
    @objc func finishVideo()
    {
        print("Video Finished")

        if (UserDefaults.standard.object(forKey: kUserLoginUserId) != nil)
        {
            appDelegate.loginSuccess()
        }
        else
        {
            //self.loginSuccess()
            
            appDelegate.LogInfromTutorial()
        }
    }
   
}

