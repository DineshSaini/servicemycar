//
//  TutorialViewController.swift
//  ServiceMyCar
//
//  Created by Tecorb Technologies on 08/06/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit

class TutorialViewController: UIViewController {
    
    @IBOutlet weak var backGroundImage: UIImageView!
    @IBOutlet weak var appLogo: UIImageView!
    @IBOutlet weak var logInImage: UIButton!
    @IBOutlet weak var signUpImage: UIButton!
    @IBOutlet weak var tutImage: UIImageView!
    @IBOutlet weak var tutLabel: UILabel!
    var index = 0
    
    fileprivate var waterView: YXWaveView?

    var arrChangetext = ["login_tag_line_service_your_car".localized(),"login_tag_line_vehicle_returned_same_day".localized(), "login_tag_line_free_collection_and_delivery".localized(),"login_tag_line_UAE_s_first_network".localized(),"login_tag_Flat_Batteries_Recovery_Breakdown".localized(),"login_tag_Number_Priority".localized()]
    var arrCarImage = ["car_1","car_2","car_3","car_4","car_5","car_6"]
    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.set(false, forKey: kFirstTime)
        self.perform(#selector(self.marquee), with: nil, afterDelay: 1.0)
        AppHelper.makeViewCircularWithRespectToHeight(logInImage, borderColor: colorGreen, borderWidth: 1.0)
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func logInTapped(_ sender: Any) {
        appDelegate.logout()
    }
    
    @IBAction func signUpTapped(_ sender: Any) {
        let nextViewController = objLogin_SignUpSB.instantiateViewController(withIdentifier: "SignUp_Vc") as! SignUp_Vc
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    
    @objc func animationOnBackGroundImage(){
        self.backGroundImage.isHidden = false
        var waveHeight = backGroundImage?.layer.presentation()?.position.y
        UIView.animate(withDuration: 1, delay: 0, options: [.curveLinear], animations: {
            self.backGroundImage.transform = CGAffineTransform(translationX: 0, y: 0)
            
            waveHeight = self.backGroundImage?.layer.presentation()?.position.y
        }, completion: nil)
        
        
        

    }
    
    
    @objc func marquee(){
        tutLabel.alpha = 0.02
        tutImage.alpha = 0.02
        if index > arrChangetext.count - 1{
            index = 0
        }else{
        }
        tutLabel.text = arrChangetext[index]
        tutImage.image = UIImage(named: arrCarImage[index])
        
        UIView.animate(withDuration: 3.0, animations: {
            self.tutLabel.alpha = 1
            self.tutImage.alpha = 1
 
        }) { (sucess) in
            UIView.animate(withDuration: 3.0, animations: {
                self.tutLabel.alpha = 0.02
                self.tutImage.alpha = 0.01
                self.index += 1
            })
        }
        self.perform(#selector(self.marquee), with: nil, afterDelay: 6.0)
    }
    
}



