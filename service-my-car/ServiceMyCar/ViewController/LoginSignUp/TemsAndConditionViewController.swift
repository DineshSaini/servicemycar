//
//  TemsAndConditionViewController.swift
//  ServiceMyCar
//
//  Created by admin on 23/06/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import WebKit

class TemsAndConditionViewController: UIViewController,WKNavigationDelegate,UIScrollViewDelegate{
    
    var webview : WKWebView!
    
    var isTermOfUse = false
    
    var url = ""
    let termsAndCondition = "https://servicemycar.ae/smc/terms-and-conditions"
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.setupNavigation()
    }
    
    override func loadView() {
        webview = WKWebView()
        webview.navigationDelegate = self
        view = webview
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.title =  "TERMS & CONDITIONS"
       // webview.delegate = self
        //webview.scalesPageToFit = true
        self.webview.scrollView.alwaysBounceHorizontal = false
        self.webview.scrollView.alwaysBounceVertical = false
        self.webview.scrollView.showsHorizontalScrollIndicator = false
        self.webview.scrollView.showsVerticalScrollIndicator = false
        //self.webview.scrollView.bounces = false
        self.webview.scrollView.bouncesZoom = false
        self.url =  termsAndCondition
        let req = URL(string: url)
        let webUrl = URLRequest(url: req!)
        self.webview.load(webUrl)
        
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickBackBarButton(_ sender: UIBarButtonItem){
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        AppHelper.hideLoadingView()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        AppHelper.hideLoadingView()
        
    }
    
//    func webViewDidStartLoad(_ webView: UIWebView) {
//        AppHelper.hideLoadingView()
//    }
//    
//    
//    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
//        AppHelper.hideLoadingView()
//
//    }
//    func webViewDidFinishLoad(_ webView: UIWebView) {
//        AppHelper.hideLoadingView()
//
//    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
