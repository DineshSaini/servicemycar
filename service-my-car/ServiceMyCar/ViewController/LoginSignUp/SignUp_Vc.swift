//
//  SignUp_Vc.swift
//  ServiceMyCar
//
//  Created by baps on 21/01/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class SignUp_Vc: UIViewController ,HttpWrapperDelegate{
    
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var btnSignUp: UIButtonX!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPhoneNo: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var btnBack: UIButton!
    
    
    @IBOutlet weak var userNameContiner: UIView!
    @IBOutlet weak var fullContiner: UIView!
    @IBOutlet weak var phoneContiner: UIView!
    @IBOutlet weak var emailContiner: UIView!
    @IBOutlet weak var passwordContiner: UIView!
    
    
    
    var objSignIn = HttpWrapper()
    var net:NetworkReachabilityManager = NetworkReachabilityManager()!
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.view.backgroundColor = colorGreen
        self.setDrawView()
         txtFirstName.placeholder = "signup_firstname_placeholder".localized()
         txtLastName.placeholder = "signup_lastname_placeholder".localized()
         txtPhoneNo.placeholder = "signup_phone_number_placeholder".localized()
        txtEmail.placeholder = "signup_email_placeholder".localized()
        txtPassword.placeholder = "signup_password_placeholder".localized()
        
        btnSignUp.setTitle("signup_title".localized(), for: .normal)
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func checkButtonTapped(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        checkButton.isSelected = sender.isSelected
    }
    
    func setDrawView(){
        AppHelper.makeViewCircularWithRespectToHeight(userNameContiner, borderColor: .lightGray, borderWidth: 1.0)
        AppHelper.makeViewCircularWithRespectToHeight(fullContiner, borderColor: .lightGray, borderWidth: 1.0)
        AppHelper.makeViewCircularWithRespectToHeight(phoneContiner, borderColor: .lightGray, borderWidth: 1.0)
        AppHelper.makeViewCircularWithRespectToHeight(emailContiner, borderColor: .lightGray, borderWidth: 1.0)
        AppHelper.makeViewCircularWithRespectToHeight(passwordContiner, borderColor: .lightGray, borderWidth: 1.0)
        
    }
    
    
    // MARK: - Button actions
    
    // MARK: - OnClickBtnBack actions
    @IBAction func OnClickBtnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - onClickBtnSignUp actions
    @IBAction func onClickBtnSignUp(_ sender: Any) {
        Login(isFrom: 0)
    }
    
    @IBAction func onClickTermsAndCondotionButton(_ sender:UIButton){
        let nextViewController = objLogin_SignUpSB.instantiateViewController(withIdentifier: "TemsAndConditionViewController") as! TemsAndConditionViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
       

    }
    
    
    @IBAction func onClickPasswordShow(_ sender:UIButton){
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            self.txtPassword.isSecureTextEntry = false
        }else{
            self.txtPassword.isSecureTextEntry = true
        }
    }
    
    
    // MARK: - Login
    func Login(isFrom:Int)
    {
        if isFrom == 0
        {
            if net.isReachable == false
            {
                let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
                
            else if isLoginDataValid()
            {
                
                
                let dictParameters:[String:AnyObject] = ["firstName" : self.txtFirstName.text as AnyObject
                    ,"lastName" : self.txtLastName.text as AnyObject,
                     "varRegisterPhone" : "05\(self.txtPhoneNo.text!)" as AnyObject,
                     "varRegisterEmail" : self.txtEmail.text as AnyObject,
                     "varRegisterPassword" : self.txtPassword.text as AnyObject]
                //            let dictHeaaderPera:[String:AnyObject] = [
                //                "platform" : "IOS" as AnyObject,
                //                "modelNumber" : "ML2000" as AnyObject
                //                ,"serialNumber" :"1234567891" as AnyObject,
                //                 "deviceId" : "12345" as AnyObject]
                //
                print("URL :--\(dictParameters)")
                AppHelper.showLoadingView()
                self.objSignIn = HttpWrapper.init()
                self.objSignIn.delegate = self
                self.objSignIn.requestWithparamdictParamPostMethodwithHeader(url: kregister, dicsParams: dictParameters, headers: header)
                //self.objSignIn.requestWithparamdictParamPostMethod(url: kregister, dicsParams: dictParameters)
            }
        }
        else
        {
            if net.isReachable == false
            {
                let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
                
            else
            {
                
                let dictParameters:[String:AnyObject] = ["cEmail" : self.txtEmail.text as AnyObject
                    ,"cPassword" : self.txtPassword.text as AnyObject]
                //            let dictHeaaderPera:[String:AnyObject] = [
                //                "platform" : "IOS" as AnyObject,
                //                "modelNumber" : "ML2000" as AnyObject
                //                ,"serialNumber" :"1234567891" as AnyObject,
                //                 "deviceId" : "12345" as AnyObject]
                //
                print("URL :--\(dictParameters)")
                AppHelper.showLoadingView()
                self.objSignIn = HttpWrapper.init()
                self.objSignIn.delegate = self
                self.objSignIn.requestWithparamdictParamPostMethod(url: kregister, dicsParams: dictParameters)
                
                
            }
        }
        
    }
    
    // MARK: - HttpWrapperfetchDataSuccess
    
    func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary)
    {
        AppHelper.hideLoadingView()
        print(dicsResponse)
        if wrapper == objSignIn {
            if dicsResponse.value(forKey: "status") as! NSString == "success"
            {
                
                
                //                UserDefaults.standard.setValue("\(json["nUserId"])", forKey: knUserId)
                //                // UserDefaults.standard.setValue((dicsResponse.value(forKey: "data") as! NSDictionary).value(forKey: "apiToken") as! String, forKey: "apiToken")
                //
                //                UserDefaults.standard.setValue("\(json["nLoginId"])", forKey: knLoginId)
                //
                //                UserDefaults.standard.setValue("\(json["cCustomerName"])", forKey: kcCustomerName)
                //
                //                UserDefaults.standard.setValue("\(json["nCustomerId"])", forKey: knCustomerId)
                //
                //                UserDefaults.standard.setValue("\(json["cEmail"])", forKey: kcEmail)
                //
                //                UserDefaults.standard.setValue("\(json["cCustomerImage"])", forKey: kcCustomerImage)
                //
                //                UserDefaults.standard.setValue("\(json["cToken"])", forKey: kcToken)
                //
                let nextViewController = objLogin_SignUpSB.instantiateViewController(withIdentifier: "VarifyMobile_Vc") as! VarifyMobile_Vc
                nextViewController.strvarRegisterPhone = "05\(txtPhoneNo.text!)"
                nextViewController.isFromRegister = true
                //let nextViewController = objPostJob.instantiateViewController(withIdentifier:"PostYourJob") as! PostYourJob
                nextViewController.navigationController?.isNavigationBarHidden = false
                self.navigationController?.pushViewController(nextViewController, animated: true)
                UserDefaults.standard.synchronize()
                
                
                // appDelegate.loginSuccess()
                
            }
            else
            {
                AppHelper.showAlert("app_alert_title".localized(), message:dicsResponse.value(forKey: "msg") as! String)
            }
            
            
        }
        
    }
    // MARK: - HttpWrapperfetchDataFail
    
    func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError)
    {
        AppHelper.hideLoadingView()
        print(error.debugDescription)
    }
    // MARK: - isLoginDataValid
    func isLoginDataValid() -> Bool
    {
        
        if (txtFirstName.text?.trimmingCharacters(in: CharacterSet.whitespaces).count)! <= 0
        {
            AppHelper.showAlert("app_alert_title".localized(), message: "signup_firstname_validation".localized())
            return false
        }
        
        if (txtLastName.text?.trimmingCharacters(in: CharacterSet.whitespaces).count)! <= 0
        {
            AppHelper.showAlert("app_alert_title".localized(), message: "signup_lastname_validation".localized())
            return false
        }
        
        
        if (txtPhoneNo.text?.trimmingCharacters(in: CharacterSet.whitespaces).count)! <= 0
        {
            AppHelper.showAlert("app_alert_title".localized(), message: "signup_phone_number_validation".localized())
            return false
        }
        if (txtPhoneNo.text?.count)! > 8{
            AppHelper.showAlert("app_alert_title".localized(), message: "login_Phone_number_Digit_Validation".localized())
            return false
        }
        
        if !AppHelper.isValidEmail(txtEmail.text!)
        {
            AppHelper.showAlert("app_alert_title".localized(), message: "signup_email_validation".localized())
            return false
        }
        
        
        if (txtPassword.text?.trimmingCharacters(in: CharacterSet.whitespaces).count)! < 6
        {
            AppHelper.showAlert("app_alert_title".localized(), message: "signup_password_validation".localized())
            return false
        }
        
        
        return true
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
