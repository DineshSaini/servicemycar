//
//  VarifyMobile_Vc.swift
//  ServiceMyCar
//
//  Created by baps on 21/01/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Localize_Swift

class VarifyMobile_Vc: UIViewController,HttpWrapperDelegate,UITextFieldDelegate {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var viewOTP: UIViewX!
    @IBOutlet weak var btnResendCode: UIButtonX!
    @IBOutlet weak var btnVerify: UIButtonX!
    @IBOutlet weak var txtOTP4: UITextField!
    @IBOutlet weak var txtOTP3: UITextField!
    @IBOutlet weak var txtOTP2: UITextField!
    @IBOutlet weak var txtOTP1: UITextField!
    @IBOutlet weak var lblVerifyYourPhone: UILabel!
    @IBOutlet weak var lblVarifyMessage: UILabel!
    
    @IBOutlet weak var otptextOneView:UIView!
    @IBOutlet weak var otptextTwoView:UIView!
    @IBOutlet weak var otptextThreeView:UIView!
    @IBOutlet weak var otptextFourView:UIView!
    
    var languageCode = ""

    
    var strvarRegisterPhone:String = String()
    var isFromRegister = false
    var objSendOTP = HttpWrapper()
    var objLoginWithOTP = HttpWrapper()
    var net:NetworkReachabilityManager = NetworkReachabilityManager()!
    func setFontAndColor() {
        //CORNER RADIUS:
        
        
       btnVerify.backgroundColor = colorGreen
        self.lblVerifyYourPhone.textColor = colorRed
       // btnResendCode.backgroundColor = colorGreen
//        viewOTP.backgroundColor = colorGreen
//        self.btnResendCode.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6).cgColor
//        self.btnResendCode.backgroundColor = UIColor.white
//        self.btnResendCode.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
//        self.btnResendCode.layer.shadowOpacity = 2.0
//        self.btnResendCode.layer.shadowRadius = 3.0
//        self.btnResendCode.layer.masksToBounds = false
//        self.btnResendCode.layer.cornerRadius = 5.0
//        self.btnResendCode.setTitle("phone_verify_resend_code".localized() , for: .normal)
//        self.btnResendCode.tintColor = Colorblack
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       setFontAndColor()
        self.navigationController?.isNavigationBarHidden = true
        setDrawView()
        txtOTP1?.delegate = self
        txtOTP3?.delegate = self
        txtOTP2?.delegate = self
        txtOTP4?.delegate = self

//        lblVerifyYourPhone.text = "phone_verify_your_phohe_number".localized()
//        lblVarifyMessage.text = "phone_verify_message".localized()
//        btnVerify.setTitle("phone_verify".localized(), for: .normal)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        view.layoutIfNeeded()
        //self.view.backgroundColor = colorGreen
        if isFromRegister == true {
            SendOTP()
        }
        //txtOTP1?.becomeFirstResponder()
    }
    
    
    func setDrawView(){
        AppHelper.makeViewCircularWithRespectToHeight(btnResendCode, borderColor: .lightGray, borderWidth: 1.0)
        AppHelper.makeViewCircularWithCornerRadius(otptextTwoView,borderColor:.clear,borderWidth:0.0, cornerRadius: 2.0)
        AppHelper.makeViewCircularWithCornerRadius(otptextThreeView,borderColor:.clear,borderWidth:0.0, cornerRadius: 2.0)
        AppHelper.makeViewCircularWithCornerRadius(otptextOneView,borderColor:.clear,borderWidth:0.0, cornerRadius: 2.0)
        AppHelper.makeViewCircularWithCornerRadius(otptextFourView,borderColor:.clear,borderWidth:0.0, cornerRadius: 2.0)
        
        
    }
    
    // MARK:- goBack

    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.isNavigationBarHidden = true
        _ = self.navigationController?.popViewController(animated: true)
    }
      
    

     // MARK: - button actions
    
    // MARK: - onClickBtnVerify actions
    @IBAction func onClickBtnVerify(_ sender: Any) {
        
        LoginWithOTP()
    }
    // MARK: - onclickBtnResendCode actions
    @IBAction func onclickBtnResendCode(_ sender: Any) {
        SendOTP()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range:NSRange, replacementString string: String) -> Bool {
        // Range.length == 1 means,clicking backspace
        if (string.count == 1){
            if textField == txtOTP1 {
                txtOTP1.text = string
                txtOTP2?.becomeFirstResponder()
            }
            if textField == txtOTP2 {
                txtOTP2.text = string
                txtOTP3?.becomeFirstResponder()
            }
            if textField == txtOTP3 {
                txtOTP3.text = string
                txtOTP4?.becomeFirstResponder()
            }
            if textField == txtOTP4 {
                txtOTP4.text = string
               txtOTP4.endEditing(true)
            }
            
            return false
        }else if (string.count == 0) {
            if textField == txtOTP4 {
                txtOTP4.text = ""
                txtOTP3?.becomeFirstResponder()
            }
            if textField == txtOTP3 {
                txtOTP3.text = ""
                txtOTP2?.becomeFirstResponder()
            }
            if textField == txtOTP2 {
                txtOTP2.text = ""
                txtOTP1?.becomeFirstResponder()
            }
            if textField == txtOTP1 {
                 txtOTP1.text = ""
                txtOTP1?.becomeFirstResponder()
            }
            
            
            return false
        }
        return true
    }
    // MARK: - SendOTP
    
    func SendOTP()
    {
            if net.isReachable == false
            {
                let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
                
            else
            {
                
                
                let dictParameters:[String:AnyObject] = ["varRegisterPhone" : strvarRegisterPhone as AnyObject]
                //let header:[String:AnyObject] = ["X-API-KEY":"smc1989" as AnyObject]

                //            let dictHeaaderPera:[String:AnyObject] = [
                //                "platform" : "IOS" as AnyObject,
                //                "modelNumber" : "ML2000" as AnyObject
                //                ,"serialNumber" :"1234567891" as AnyObject,
                //                 "deviceId" : "12345" as AnyObject]
                //
                print("URL :--\(dictParameters)")
                AppHelper.showLoadingView()
                self.objSendOTP = HttpWrapper.init()
                self.objSendOTP.delegate = self
                self.objSendOTP.requestWithparamdictParamPostMethodwithHeader(url: ksendOTPMobile, dicsParams: dictParameters, headers: header)

                //self.objSendOTP.requestWithparamdictParamPostMethod(url: ksendOTPMobile, dicsParams: dictParameters)
            }
     
    }
    
    
    // MARK: - Login With OTP
    func LoginWithOTP()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else
        {
           
            let OTP = txtOTP1.text! + txtOTP2.text! + txtOTP3.text! + txtOTP4.text!
            //let header = ["X-API-KEY": "smc1989"] as [String:AnyObject]
            let dictParameters:[String:AnyObject] = ["varRegisterPhone" : strvarRegisterPhone as AnyObject
                ,"OTPCode" : OTP as AnyObject,
                 "varDeviceId" : appDelegate.deviceUDID as AnyObject,
                 "varDeviceOS" : "IOS" as AnyObject]
            //,"Accept":"application/json","Content-Type":"application/json"] as [String:AnyObject]

            //            let dictHeaaderPera:[String:AnyObject] = [
            //                "platform" : "IOS" as AnyObject,
            //                "modelNumber" : "ML2000" as AnyObject
            //                ,"serialNumber" :"1234567891" as AnyObject,
            //                 "deviceId" : "12345" as AnyObject]
            //
            //print("URL :--\(kloginByOTP) \(dictParameters)")
            
            AppHelper.showLoadingView()
            self.objLoginWithOTP = HttpWrapper.init()
            self.objLoginWithOTP.delegate = self
            //self.objLoginWithOTP.requestWithparamdictParamPostMethod(url: kloginByOTP, dicsParams: dictParameters)
            self.objLoginWithOTP.requestWithparamdictParamPostMethodwithHeader(url: kloginByOTP, dicsParams: dictParameters, headers: header)

        
        }
        
    }
    
    // MARK: - HttpWrapperfetchDataSuccess
    
    func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary)
    {
        AppHelper.hideLoadingView()
        print(dicsResponse)
        if wrapper == objSendOTP {
            if dicsResponse.value(forKey: "status") as! NSString == "success"{
                // appDelegate.loginSuccess()
            }else{
                AppHelper.showAlert("app_alert_title".localized(), message:dicsResponse.value(forKey: "msg") as! String)
            }
        }else if wrapper == objLoginWithOTP {
            if dicsResponse.value(forKey: "status") as! NSString == "success"{
                
                                let data = dicsResponse.value(forKey: "data") as! NSDictionary
                                let json = JSON(data)
                                print(json["UserLoginUserId"])
                                UserDefaults.standard.setValue("\(json["UserLoginUserId"])", forKey: kUserLoginUserId)
                                // UserDefaults.standard.setValue((dicsResponse.value(forKey: "data") as! NSDictionary).value(forKey: "apiToken") as! String, forKey: "apiToken")

                                UserDefaults.standard.setValue("\(json["UserLoginPhone"])", forKey: kUserLoginPhone)

                                UserDefaults.standard.setValue("\(json["UserLoginName"])", forKey: kUserName)

                                UserDefaults.standard.setValue("\(json["UserLoginEmail"])", forKey: KUserLoginEmail)
                
                                UserDefaults.standard.setValue("\(json["UserLoginShortPhone"])", forKey: kUserShortLoginPhone)
                                UserDefaults.standard.setValue("\(json["lastLang"])", forKey: kAppLanguage)
                let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as? String ?? "en"
                let newLanguage:Languages!
                UserDefaults.standard.set(languageCode, forKey: kAppLanguage)
                if languageCode == "en" {
                    newLanguage = .en
                }else{
                    newLanguage = .ar
                }
                Localize.setCurrentLanguage(languageCode)
                LanguageManager.shared.setLanguage(language: newLanguage)
                appDelegate.arrCategorylist.removeAllObjects()

                UserDefaults.standard.synchronize()
                appDelegate.loginSuccess()
               showToast()
                
            }else{
                AppHelper.showAlert("app_alert_title".localized(), message:dicsResponse.value(forKey: "msg") as! String)
            }
        }
        
    }
    // MARK: - HttpWrapperfetchDataFail
    
    func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError)
    {
        AppHelper.hideLoadingView()
        print(error.debugDescription)
    }
    
    func showToast()
    {
        let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
        loadingNotification?.mode = MBProgressHUDModeText
        loadingNotification?.labelText = "phone_verify_sucsses".localized()
        loadingNotification?.margin = 10;
        loadingNotification?.yOffset = 250;
        
        loadingNotification?.isUserInteractionEnabled = true
        loadingNotification?.hide(true, afterDelay: 1.5)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
