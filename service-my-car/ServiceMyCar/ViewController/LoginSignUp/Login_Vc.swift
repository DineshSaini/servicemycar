//
//  Login_Vc.swift
//  ServiceMyCar
//
//  Created by baps on 21/01/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class Login_Vc: UIViewController ,HttpWrapperDelegate{
    
    // @IBOutlet weak var lblFadeText: UILabel!
    @IBOutlet weak var btnGoogle: UIButtonX!
    @IBOutlet weak var btnFaceBook: UIButtonX!
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var txtPhoneNo: UITextField!
    @IBOutlet weak var lblOr: UILabel!
    
    @IBOutlet weak var mobileContainerView:UIView!
    
    var objSendOTP = HttpWrapper()
    var objCurrentVersion = HttpWrapper()
    
    var net:NetworkReachabilityManager = NetworkReachabilityManager()!
    var index = 0
    var arrChangetext = ["login_tag_line_service_your_car".localized(),"login_tag_line_vehicle_returned_same_day".localized(), "login_tag_line_free_collection_and_delivery".localized(),"login_tag_line_UAE_s_first_network".localized()]
    func setFontAndColor() {
        //CORNER RADIUS:
        
        btnLogin.backgroundColor = colorGreen
        btnSignUp.backgroundColor = colorGreen
        btnFaceBook.backgroundColor = Colorblack
        btnGoogle.backgroundColor = Colorblack
        //       // txtPhoneNo.attributedPlaceholder = NSAttributedString(string: self.txtPhoneNo.placeholder!,
        //                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        
        
        
    }
    
    override func viewWillLayoutSubviews() {
        AppHelper.makeViewCircularWithRespectToHeight(mobileContainerView, borderColor: .lightGray, borderWidth: 1.0)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // self.view.backgroundColor = colorGreen
        self.navigationController?.navigationBar.isHidden = true
        txtPhoneNo.placeholder = "login_phone_number_placeholder".localized()
        //btnSignUp.setTitle("login_sign_up".localized(), for: .normal)
        //btnLogin.setTitle("login".localized(), for: .normal)
        // btnFaceBook.setTitle("login_facebook".localized(), for: .normal)
        // btnGoogle.setTitle("login_google".localized(), for: .normal)
        // lblOr.text = "login_or".localized()
        //setFontAndColor()
        // _ = Timer.scheduledTimer(timeInterval: 3.5, target: self, selector: #selector(marquee), userInfo: nil, repeats: true)
        self.checkAppVersion ()
        
        // Do any additional setup after loading the view.
    }
    
    @objc func marquee(){
        //  lblFadeText.alpha = 0.10
        if index > arrChangetext.count - 1
        {
            index = 0
        }
        else
        {
            
        }
        //  lblFadeText.text = arrChangetext[index]
        
        UIView.animate(withDuration: 2.0) {
            //      self.lblFadeText.alpha = 1
            self.index += 1
        }
        
    }
    
    
    
    func checkAppVersion (){
        if net.isReachable == false{
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }

            let dictParameters:[String:AnyObject] = ["deviceOS" : "iOS" as AnyObject]
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objCurrentVersion = HttpWrapper.init()
            self.objCurrentVersion.delegate = self
            self.objCurrentVersion.requestWithparamdictParamPostMethodwithHeaderAndDictGet(url: kGetCurrentVersion, dictParams: dictParameters, headers: header)
        
    }

    
    
    // MARK: - SendOTP
    
    func SendOTP()
    {
        if net.isReachable == false
        {
            let alert = UIAlertController(title: internetConnectedTitle, message: internetConnected, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "app_net_alert_ok".localized(), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
            
        else if isLoginDataValid()
        {
            
            
            let dictParameters:[String:AnyObject] = ["varRegisterPhone" : "05\(txtPhoneNo.text!)" as AnyObject]
            //let header:[String:AnyObject] = ["X-API-KEY":"smc1989" as AnyObject]
            //            let dictHeaaderPera:[String:AnyObject] = [
            //                "platform" : "IOS" as AnyObject,
            //                "modelNumber" : "ML2000" as AnyObject
            //                ,"serialNumber" :"1234567891" as AnyObject,
            //                 "deviceId" : "12345" as AnyObject]
            //
            print("URL :--\(dictParameters)")
            AppHelper.showLoadingView()
            self.objSendOTP = HttpWrapper.init()
            self.objSendOTP.delegate = self
            self.objSendOTP.requestWithparamdictParamPostMethodwithHeader(url: ksendOTPMobile, dicsParams: dictParameters, headers: header)
            //self.objSendOTP.requestWithparamdictParamPostMethod(url: ksendOTPMobile, dicsParams: dictParameters)
        }
        
    }
    
    
    // MARK: - HttpWrapperfetchDataSuccess
    
    func HttpWrapperfetchDataSuccess(wrapper: HttpWrapper, dicsResponse: NSMutableDictionary)
    {
        AppHelper.hideLoadingView()
        print(dicsResponse)
        if wrapper == objSendOTP {
            if dicsResponse.value(forKey: "status") as? NSString == "success"{
                let nextViewController = objLogin_SignUpSB.instantiateViewController(withIdentifier: "VarifyMobile_Vc") as! VarifyMobile_Vc
                nextViewController.strvarRegisterPhone = "05"+txtPhoneNo.text!
                //let nextViewController = objPostJob.instantiateViewController(withIdentifier:"PostYourJob") as! PostYourJob
                nextViewController.navigationController?.isNavigationBarHidden = false
                self.navigationController?.pushViewController(nextViewController, animated: true)
                
            }else{
                AppHelper.showAlert("app_alert_title".localized(), message:dicsResponse.value(forKey: "msg") as? String ?? "")
            }
        }else if wrapper == objCurrentVersion{
            if dicsResponse.value(forKey: "status") as? NSString == "success"{
                if let getVersionDetail = dicsResponse.value(forKey: "data") as? NSDictionary{
                    let currentVersion = UIApplication.appVersion()
                    print("currentVesion \(currentVersion)")
                    let appStoreVersion = getVersionDetail.value(forKey: "varVersion") as? String ?? ""
                    if appStoreVersion != "\(currentVersion)"{
                        showForceUpdateAlert()
                    }
                }
            }else{
                AppHelper.showAlert("app_alert_title".localized(), message:dicsResponse.value(forKey: "msg") as? String ?? "")
            }
        }
        
    }
    
    
    func showForceUpdateAlert(){
        AppHelper.hideLoadingView()
        let alert = UIAlertController(title:"ServiceMyCar".localized(), message:"A new version of ServiceMyCar is available on the App Store. Would you like to update it now?".localized(), preferredStyle: .alert)
        let updateAction = UIAlertAction(title: "Update".localized(), style: .default) {[alert] (action) in
            if let url = URL(string: "itms-apps://itunes.apple.com/app/id\(appID)"),
                UIApplication.shared.canOpenURL(url)
            {
                UIApplication.shared.open(url, options: [:], completionHandler: {[alert] (done) in
                    alert.dismiss(animated: false, completion: nil)
                })
            }
        }
        
          let cancelAction = UIAlertAction(title: "Ignore".localized(), style: .cancel){(action) in
                      alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(cancelAction)
        alert.addAction(updateAction)
        let toastShowingVC = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController
        toastShowingVC?.present(alert, animated: true, completion: nil)
    }

    
    
    
    
//    func askForUpdateAppVersion() {
//        let alert = UIAlertController(title: "ServiceMyCar".localized(), message: "We are sorry, we do not provide spare tyres. We can only help you get back on the road, If you have spare tyre.".localized(), preferredStyle: .alert)
//        let okayAction = UIAlertAction(title: "OK".localized(), style: .default){(action) in
//            alert.dismiss(animated: true, completion: nil)
//
//            //            let nextViewController = objServiceSB.instantiateViewController(withIdentifier: "SelectCarBookingRepairViewController") as! SelectCarBookingRepairViewController
//            //            nextViewController.srtTittle = self.strtitle
//            //            nextViewController.srtPrice = self.strPrice
//            //            nextViewController.additionalImage = self.additionalImage
//            //            self.navigationController?.pushViewController(nextViewController, animated: true)
//        }
//        //  let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel){(action) in
//        //      alert.dismiss(animated: true, completion: nil)
//
//        // self.onClickCancelButton()
//        // }
//        alert.addAction(okayAction)
//        //alert.addAction(cancelAction)
//        let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
//        appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
//    }
//
    
    
    
    // MARK: - HttpWrapperfetchDataFail
    
    func HttpWrapperfetchDataFail(wrapper: HttpWrapper, error: NSError)
    {
        AppHelper.hideLoadingView()
        print(error.debugDescription)
    }
    
    // MARK: - UiButton Actions
    
    // MARK: - onClickBtnLogin Action
    @IBAction func onClickBtnLogin(_ sender: Any) {
        SendOTP()
        
        
    }
    // MARK: - onClickBtnSignUp Action
    @IBAction func onClickBtnSignUp(_ sender: Any) {
        
        let nextViewController = objLogin_SignUpSB.instantiateViewController(withIdentifier: "SignUp_Vc") as! SignUp_Vc
        //let nextViewController = objPostJob.instantiateViewController(withIdentifier:"PostYourJob") as! PostYourJob
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    // MARK: - onClickBtnFaceBook Action
    @IBAction func onClickBtnFaceBook(_ sender: Any) {
    }
    // MARK: - onClickBtnGoogle Action
    @IBAction func onClickBtnGoogle(_ sender: Any) {
    }
    
    
    @IBAction func onClickBtnTwitter(_ sender: UIButton) {
    }
    
    
    // MARK: - isLoginDataValid
    func isLoginDataValid() -> Bool
    {
        
        
        
        
        if (txtPhoneNo.text?.trimmingCharacters(in: CharacterSet.whitespaces).count)! <= 0
        {
            AppHelper.showAlert("app_alert_title".localized(), message: "login_phone_number_validation".localized())
            return false
        }
        
        if (txtPhoneNo.text?.count)! > 8{
            AppHelper.showAlert("app_alert_title".localized(), message: "login_Phone_number_Digit_Validation".localized())
            return false
        }
        
        
        return true
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}



extension UIApplication {
    class func appVersion() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
    }
    
    class func appBuild() -> String {
        return Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as! String
    }
    
}

