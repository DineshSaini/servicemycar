//
//  SelectAddressCollectionViewCell.swift
//  ServiceMyCar
//
//  Created by admin on 19/06/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit

class SelectAddressCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var backView:UIView!
    @IBOutlet weak var collectAddressTextfield:UITextField!
    @IBOutlet weak var dropAddressTextfield:UITextField!
    @IBOutlet weak var collectionAddAdressButton:UIButton!
    @IBOutlet weak var dropAddAdressButton:UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        AppHelper.makeViewCircularWithRespectToHeight(collectAddressTextfield, borderColor: .lightGray, borderWidth: 1.0)
        AppHelper.makeViewCircularWithRespectToHeight(dropAddressTextfield, borderColor: .lightGray, borderWidth: 1.0)


        // Initialization code
    }

}
