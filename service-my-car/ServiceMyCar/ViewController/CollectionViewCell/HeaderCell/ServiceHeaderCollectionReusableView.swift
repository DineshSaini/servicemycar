//
//  ServiceHeaderCollectionReusableView.swift
//  ServiceMyCar
//
//  Created by admin on 18/06/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit

class ServiceHeaderCollectionReusableView: UICollectionReusableView {

    @IBOutlet weak var headerTittle : UILabel!
    @IBOutlet weak var infoImage : UIButton!
    @IBOutlet weak var nextImage : UIImageView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.infoImage.isHidden = true
        // Initialization code
    }
    
}
