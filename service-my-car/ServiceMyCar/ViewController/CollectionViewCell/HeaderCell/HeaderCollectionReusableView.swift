//
//  HeaderCollectionReusableView.swift
//  ServiceMyCar
//
//  Created by admin on 05/07/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit

class HeaderCollectionReusableView: UICollectionReusableView {
    @IBOutlet weak var headerTittle : UILabel!
    @IBOutlet weak var infoImage : UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
