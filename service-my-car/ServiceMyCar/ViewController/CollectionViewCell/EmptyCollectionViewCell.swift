//
//  EmptyCollectionViewCell.swift
//  ServiceMyCar
//
//  Created by admin on 20/06/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit

class EmptyCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var noDataLabel:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
