//
//  CategoryDocCollectionViewCell.swift
//  ServiceMyCar
//
//  Created by Parikshit on 19/06/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit

class CategoryDocCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var shadowView: UIView!

    @IBOutlet weak var firstLbl:UILabel!
    @IBOutlet weak var secondLbl:UILabel!
    @IBOutlet weak var firstBtn:UIButton!
    @IBOutlet weak var secondBtn:UIButton!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
