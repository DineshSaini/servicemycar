//
//  DateAndTimeCollectionViewCell.swift
//  ServiceMyCar
//
//  Created by admin on 20/06/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit

class DateAndTimeCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var backView:UIView!
    @IBOutlet weak var dateAndTimeTextfield:UITextField!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        AppHelper.makeViewCircularWithRespectToHeight(dateAndTimeTextfield, borderColor: .lightGray, borderWidth: 1.0)
        
        
        // Initialization code
    }
    
    
    func setLeftIconForTextField(_ textField:UITextField,leftIcon: UIImage){
        let leftImageView = UIImageView()
        leftImageView.image = leftIcon
        //let imgFrame = textField.frame
        leftImageView.frame = CGRect(x: 40, y: 5, width: 35, height:35)
        textField.leftView = leftImageView
        textField.leftViewMode = UITextField.ViewMode.always
    }

}




