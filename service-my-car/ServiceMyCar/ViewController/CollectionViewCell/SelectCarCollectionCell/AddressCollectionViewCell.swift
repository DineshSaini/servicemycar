//
//  AddressCollectionViewCell.swift
//  ServiceMyCar
//
//  Created by admin on 20/06/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit

class AddressCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var backView:UIView!
    @IBOutlet weak var collectAddressLabel:UILabel!
    @IBOutlet weak var dropAddressLabel:UILabel!
    @IBOutlet weak var collectionAddAdressButton:UIButton!
    @IBOutlet weak var dropAddAdressButton:UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        // Initialization code
    }

}
