//
//  AdditionalRepairsTableViewCell.swift
//  ServiceMyCar
//
//  Created by Parikshit on 20/06/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit

class AdditionalRepairsTableViewCell: UITableViewCell {
    @IBOutlet weak var backView:UIView!
    @IBOutlet weak var nameLabel:UILabel!
    @IBOutlet weak var totalCostLabel:UILabel!
    @IBOutlet weak var lChargeLabel:UILabel!
    @IBOutlet weak var costLabel:UILabel!
    @IBOutlet weak var acceptButton:UIButton!
    @IBOutlet weak var declineButton:UIButton!
    @IBOutlet weak var staticTotalCost:UILabel!
    @IBOutlet weak var staticCost:UILabel!
    @IBOutlet weak var staticLabourCharge:UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        AppHelper.makeCircularWithRightTopTwoCornerRadius(backView, cornerRadius: 25.0)
        backView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).cgColor
        backView.backgroundColor = UIColor.white
        backView.layer.shadowOffset = CGSize.zero
        backView.layer.shadowOpacity = 1.0
        backView.layer.shadowRadius = 3.0
        backView.layer.masksToBounds = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
