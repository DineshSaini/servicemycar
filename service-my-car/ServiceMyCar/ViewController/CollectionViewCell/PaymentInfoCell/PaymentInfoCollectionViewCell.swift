//
//  PaymentInfoCollectionViewCell.swift
//  ServiceMyCar
//
//  Created by Parikshit on 26/06/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit

class PaymentInfoCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var totalIncludingVatAmount:UILabel!
    @IBOutlet weak var vatAmount:UILabel!
    @IBOutlet weak var totalExcludingVatAmount:UILabel!
    @IBOutlet weak var discountAmount:UILabel!
    
    @IBOutlet weak var totalIncludingVatLabel:UILabel!
    


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
