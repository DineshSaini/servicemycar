//
//  MyServiceTableViewCell.swift
//  ServiceMyCar
//
//  Created by Parikshit on 18/06/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit

class MyServiceTableViewCell: UITableViewCell {
    @IBOutlet weak var backView:UIView!
    @IBOutlet weak var documentsButton:UIButton!
    @IBOutlet weak var carNameLbl:UILabel!
    @IBOutlet weak var carModelLbl:UILabel!
    @IBOutlet weak var dateLbl:UILabel!
    @IBOutlet weak var packageLbl:UILabel!
    @IBOutlet weak var netAmountLbl:UILabel!
    @IBOutlet weak var trackDricer:UILabel!
    
    @IBOutlet weak var totalLabel:UILabel!
    
    @IBOutlet weak var buttonStackView:UIStackView!

    @IBOutlet weak var trackOrderButton:UIButton!
    @IBOutlet weak var trackCollectionButton:UIButton!


    override func awakeFromNib() {
        super.awakeFromNib()
        AppHelper.makeViewCircularWithCornerRadius(trackOrderButton, borderColor: .clear, borderWidth: 0.0, cornerRadius: 5.0)
        AppHelper.makeViewCircularWithCornerRadius(trackCollectionButton, borderColor: .clear, borderWidth: 0.0, cornerRadius: 5.0)
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        if languageCode != "en" {
            packageLbl.textAlignment = .left
            trackDricer.textAlignment  = .left
        }
        
           
            
        

        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
