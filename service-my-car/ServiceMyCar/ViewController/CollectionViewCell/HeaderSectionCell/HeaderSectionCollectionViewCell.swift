//
//  HeaderSectionCollectionViewCell.swift
//  ServiceMyCar
//
//  Created by admin on 24/06/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit

class HeaderSectionCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var headerTittle : UILabel!
    @IBOutlet weak var infoImage : UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
