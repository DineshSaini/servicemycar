//
//  ServiceCollectionViewCell.swift
//  ServiceMyCar
//
//  Created by admin on 18/06/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit

class ServiceCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var serviceNameLabel:UILabel!
    @IBOutlet weak var serviceImageview:UIImageView!
    @IBOutlet weak var viewContainer:UIView!
    @IBOutlet weak var infoButton:UIButton!
    @IBOutlet weak var selectButton:UIButton!




    override func awakeFromNib() {
        super.awakeFromNib()
        self.infoButton.isHidden = true
        viewContainer.layer.cornerRadius = 5.0
        viewContainer.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2).cgColor
        viewContainer.backgroundColor = UIColor.white
        viewContainer.layer.shadowOffset = CGSize.zero
        viewContainer.layer.shadowOpacity = 1.0
        viewContainer.layer.shadowRadius = 1.0
        viewContainer.layer.masksToBounds = false
        contentView.backgroundColor = UIColor.init(red: 244.0/255.0, green: 245.0/255.0, blue: 248.0/255.0, alpha: 1.0)
        
        // Initialization code
    }
    
}
