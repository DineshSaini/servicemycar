//
//  CarCollectionViewCell.swift
//  ServiceMyCar
//
//  Created by admin on 21/06/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit

class CarCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var backView:UIView!
    @IBOutlet weak var yearLabel:UILabel!
    @IBOutlet weak var carName:UILabel!
    @IBOutlet weak var categoryName:UILabel!
    @IBOutlet weak var carNumber:UILabel!
    @IBOutlet weak var numberContainerView:UIView!
    @IBOutlet weak var selectButton:UIButton!
    
    @IBOutlet weak var plateImageView:UIImageView!


    
    override func awakeFromNib() {
        super.awakeFromNib()
        AppHelper.makeViewCircularWithCornerRadius(numberContainerView, borderColor: .black, borderWidth: 1.0, cornerRadius: 3.0)
        // Initialization code
    }
}
