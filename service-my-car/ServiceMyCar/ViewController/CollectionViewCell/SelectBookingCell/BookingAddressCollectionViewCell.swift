//
//  BookingAddressCollectionViewCell.swift
//  ServiceMyCar
//
//  Created by admin on 21/06/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit

class BookingAddressCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var backView:UIView!
    @IBOutlet weak var collectAddressTextfield:UITextField!
    @IBOutlet weak var collectionAddAdressButton:UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        AppHelper.makeViewCircularWithRespectToHeight(collectAddressTextfield, borderColor: .lightGray, borderWidth: 1.0)
        
        
        // Initialization code
    }
}



class ConfrimBookingAddressCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var backView:UIView!
    @IBOutlet weak var collectAddressTextfield:UITextField!
    @IBOutlet weak var collectionAddAdressButton:UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        AppHelper.makeViewCircularWithRespectToHeight(collectAddressTextfield, borderColor: .lightGray, borderWidth: 1.0)
        
        
        // Initialization code
    }
}
