//
//  ConstantURL.swift
//  FoodBook
//
//  Created by Mandip Kanjiya on 26/07/18.
//  Copyright © 2018 Mandip Kanjiya. All rights reserved.
//

import Foundation


let header = isLive ? ["X-API-KEY":"Bs%*3xR3Dau7yAU*"] as [String:AnyObject] : ["X-API-KEY":"smc1989"] as [String:AnyObject]

//Live = https://servicemycar.ae
//Testing = https://servicemytaxi.com
let kbaseURL = isLive ? "https://servicemycar.ae/smc/web_api_v2/" :"https://servicemytaxi.com/smc/web_api/"//"https://servicemycar.ae/smc/web_api/"
//MARK:- Webservice API list
let kregister                          = kbaseURL + "register"
let ksendOTPMobile                     = kbaseURL + "sendOTPMobile"
let kloginByOTP                        = kbaseURL + "loginByOTP"
let kgetPackageListByID                = kbaseURL + "getPackageListByID"
let kgetPackagesListByCategory         = kbaseURL + "getPackagesListByCategory"
let kgetServicePackageList             = kbaseURL + "getServicePackageList"
let kgetUserCars                       = kbaseURL + "getUserCars"
let kgetCars                           = kbaseURL + "getCars"
let kgetCarModels                      = kbaseURL + "getCarModels"
let kgetLaunchYears                    = kbaseURL + "getLaunchYears"
let kgetTrimEngine                     = kbaseURL + "getTrimEngine"
let kaddUserCar                        = kbaseURL + "addUserCar"
let kEditUserCar                       = kbaseURL + "editUserCar"
let kstateList                         = kbaseURL + "stateList"
let kdeleteUserCar                     = kbaseURL + "deleteUserCar"
let ksubmitExpensiveCar                = kbaseURL + "submitExpensiveCar"
let kupdateProfile                     = kbaseURL + "updateProfile"
let kGetExtraPrice                     = kbaseURL + "getExtraPrice"
let kgetVat                            = kbaseURL + "getVat"
let kgetBookings                       = kbaseURL + "getBookings"
let kgetPastBookings                   = kbaseURL + "getPastBookings"
let kgetCollectionReport               = kbaseURL + "getCollectionReport"
let kgetDeliveryReport                 = kbaseURL + "getDeliveryReport"
let kgetServiceSchedule                = kbaseURL + "getServiceSchedule"
let kgetHealthCheckVideo               = kbaseURL + "getHealthCheckVideo"
let kgetInvoice                        = kbaseURL + "getInvoice"
let kgetExtraPartImages                = kbaseURL + "getExtraPartImages"
let klistCards                         = kbaseURL + "listCards"
let kaddUserCard                       = kbaseURL + "addUserCard"
let kapplyCoupons                      = kbaseURL + "applyCoupons"
let kremoveCoupon                      = kbaseURL + "removeCoupon"
let kadditionalService                 = kbaseURL + "additionalService"
let kgetPreviousExtraPartsReport       = kbaseURL + "getPreviousExtraPartsReport"
let kgetExtraParts                     = kbaseURL + "getExtraParts"
let ksubmitExtraPartsApproval          = kbaseURL + "submitExtraPartsApproval"
let kgetUserNewNotification            = kbaseURL + "getUserNewNotification"
let ksubmitBooking                     = kbaseURL + "submitBooking"
let kpayExtraInvoice                   = kbaseURL + "payExtraInvoice"
let ksetSeenNotification               = kbaseURL + "setSeenNotification"
let kgetCountNewNotification           = kbaseURL + "getCountNewNotification"
let kresetCountTotalNotification       = kbaseURL + "resetCountTotalNotification"
let kgetOrderDetailById                = kbaseURL + "getOrderDetailById"
let kpostQuoteRequest                  = kbaseURL + "postQuoteRequest"
let ksubmitOtherBooking                = kbaseURL + "submitOtherBooking"
let kgetUserAddress                    = kbaseURL + "getUserAddress"
let kdeleteUserAddress                 = kbaseURL + "deleteUserAddress"
let kEditUserAddress                   = kbaseURL + "editUserAddress"
let kAddUserAddress                    = kbaseURL + "addUserAddress"
let kgetAllUserNotification            = kbaseURL + "getUserAllNotification"
let kgetAllDocument                    = kbaseURL + "allDocuments"
let kputCheckBlockDays                 = kbaseURL + "checkBlockDays"
let kSetUserLang                       = kbaseURL + "setUserLang"
let kGetCurrentVersion                 = kbaseURL + "currentVersion"
