//
//  AppHelper.swift
//  Deputize Americar
//
//  Created by Darshak Trivedi.
//  Copyright © 2017 Scorch Mobile. All rights reserved.
//

import UIKit
import GoogleMaps


class AppHelper: NSObject
{
    struct Platform
    {
        static var isSimulator: Bool
        {
            return TARGET_OS_SIMULATOR != 0 // Use this line in Xcode 7 or newer
        }
    }
    
    class func showAlert(_ title:String, message:String)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        let defaultAction = UIAlertAction(title: "OK".localized(), style: UIAlertAction.Style.default, handler: nil)
        alertController.addAction(defaultAction)
        
        if var topController = UIApplication.shared.keyWindow?.rootViewController
        {
            while let presentedViewController = topController.presentedViewController
            {
                topController = presentedViewController
            }
            topController.present(alertController, animated: true, completion: nil)
        }
    }
    
    class func isValidEmail(_ testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    
    
    class func makeViewCircular(_ view:UIView,borderColor:UIColor,borderWidth:CGFloat)
    {
        view.layer.borderColor = borderColor.cgColor
        view.layer.borderWidth = borderWidth
        view.layer.cornerRadius = view.frame.size.width/2
        view.layer.masksToBounds = true
    }
    class func makeViewCircularWithRespectToHeight(_ view:UIView,borderColor:UIColor,borderWidth:CGFloat)
    {
        view.layer.borderColor = borderColor.cgColor
        view.layer.borderWidth = borderWidth
        view.layer.cornerRadius = view.frame.size.height/2
        view.layer.masksToBounds = true
    }
    
    class func makeViewCircularWithCornerRadius(_ view:UIView,borderColor:UIColor,borderWidth:CGFloat, cornerRadius: CGFloat)
    {
        view.layer.borderColor = borderColor.cgColor
        view.layer.borderWidth = borderWidth
        view.layer.cornerRadius = cornerRadius
        view.layer.masksToBounds = true
    }
    
    
    
    
    
    
    class func makeCircularWithLeftBottmTwoCornerRadius(_ view:UIView,cornerRadius:CGFloat){
        view.clipsToBounds = true
        view.layer.cornerRadius = cornerRadius
        view.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
    }
    
   class func makeCircularWithRightBottmTwoCornerRadius(_ view:UIView,cornerRadius:CGFloat){
        view.clipsToBounds = true
        view.layer.cornerRadius = cornerRadius
        view.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        
    }
    
    
    class func makeCircularWithLeftTopTwoCornerRadius(_ view:UIView,cornerRadius:CGFloat){
        view.clipsToBounds = true
        view.layer.cornerRadius = cornerRadius
        view.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMinXMinYCorner, .layerMinXMaxYCorner]
    }
    
   class func makeCircularWithRightTopTwoCornerRadius(_ view:UIView,cornerRadius:CGFloat){
        view.clipsToBounds = true
        view.layer.cornerRadius = cornerRadius
        view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner,.layerMinXMinYCorner]
        
    }
    
    
   class func makeCircularWithLeftTwoCornerRadius(_ view:UIView,cornerRadius:CGFloat){
        view.clipsToBounds = true
        view.layer.cornerRadius = cornerRadius
        view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner]
    }
    
    class func makeCircularWithRightTwoCornerRadius(_ view:UIView,cornerRadius:CGFloat){
        view.clipsToBounds = true
        view.layer.cornerRadius = cornerRadius
        view.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMaxXMaxYCorner]
        
    }
    
    
    
   class func makeCircularWithTopTwoCornerRadius(_ view:UIView,cornerRadius:CGFloat){
        view.clipsToBounds = true
        view.layer.cornerRadius = cornerRadius
        view.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMinXMinYCorner]//[.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        
    }
    
    
    
   class func setNumberPlateImage(stateName:String) -> UIImage?{
        var plateImage : UIImage!
        if stateName.lowercased() == "Abu Dhabi".lowercased(){
            plateImage = UIImage(named: "abu_dabi_plate.jpeg")
        }else if stateName.lowercased() == "Dubai".lowercased(){
            plateImage = UIImage(named: "dubai_plate.jpeg")
            
        }else if stateName.lowercased() == "Sharjah".lowercased(){
            plateImage = UIImage(named: "sharjah_plate.jpeg")
            
        }else if stateName.lowercased() == "Umm Al Quwain".lowercased(){
            plateImage = UIImage(named: "umm_al_quwain_plate.jpeg")
            
        }else if stateName.lowercased() == "Fujairah".lowercased(){
            plateImage = UIImage(named: "fujairah_plate.jpeg")
            
        }else if stateName.lowercased() == "Ras Al Khaimah".lowercased(){
            plateImage = UIImage(named: "rasal_khaimah_plate.jpeg")
            
        }else if stateName.lowercased() == "Ajman".lowercased(){
            plateImage = UIImage(named: "ajman_plate.jpeg")
        }else{
            plateImage = UIImage(named: "dubai_plate.jpeg")
    }
        return plateImage
    }
    
    
    class func getDayAndDateFromDateString(_ dateString: String) -> String{
        if dateString.isEmpty{return ""}
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        let loc = Locale(identifier: "\(languageCode)")
        var calendar = Calendar.current
        calendar.locale = loc
        var eta = ""
        let df  = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        df.locale = loc
        df.calendar = calendar
        let date = df.date(from: dateString)!
        
        //df.dateFormat = "EEEE d MMMM HH:mm a"
        let etaDay = df.string(from: date);
        
        eta = "\(etaDay)"
        return eta
    }

    
    
    
    
    
    class func showLoadingView()
    {
        if appDelegate.isLoadingViewVisible == false
        {
            //            SVProgressHUD.setDefaultMaskType(.clear)
            //            SVProgressHUD.setDefaultStyle(.dark)
            //            SVProgressHUD.setDefaultAnimationType(.flat)
            //            SVProgressHUD.show()
            //            SVProgressHUD.setDefaultMaskType(.clear)
            //            SVProgressHUD.setDefaultStyle(.dark)
            //            SVProgressHUD.setDefaultAnimationType(.native)
            //            SVProgressHUD.show()
            
            let window = UIApplication.shared.keyWindow
            let loading = MBProgressHUD.showAdded(to: window, animated: true)
            loading?.mode = MBProgressHUDModeIndeterminate
            //            loading?.labelText = "please wait...";
            
            appDelegate.isLoadingViewVisible = true
        }
    }
    
    class func hideLoadingView()
    {
        let window = UIApplication.shared.keyWindow
        MBProgressHUD.hide(for: window, animated: true)
        appDelegate.isLoadingViewVisible = false
    }
}

public extension UIImage {
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
    
}



extension UINavigationController {
    
    
    func makeAdditionalPageTransPrent(){
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.isTranslucent = true
        self.navigationBar.isOpaque = false
        self.navigationBar.barTintColor = UIColor.white
        self.navigationBar.backgroundColor = .clear
        self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black] //[NSAttributedStringKey.font: fonts.OpenSans.semiBold.font(.xXLarge)]
    }
    
    func makeTransparent(){
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.isTranslucent = true
        self.navigationBar.isOpaque = false
        self.navigationBar.barTintColor = UIColor.white
        self.navigationBar.backgroundColor = .clear
        self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white] //[NSAttributedStringKey.font: fonts.OpenSans.semiBold.font(.xXLarge)]
    }
    
    func clearShadowLine(){
        self.navigationBar.shadowImage = UIImage()
    }
    
    func setupNavigation(){
        self.navigationBar.clipsToBounds = false
        self.navigationBar.isTranslucent = false
        self.navigationBar.barTintColor = backGroundColor
        self.navigationBar.backgroundColor = backGroundColor
        self.navigationBar.isOpaque = false
        self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
        let bounds = self.navigationBar.bounds
        self.navigationBar.setBackgroundImage(UIImage(color: backGroundColor,size:(bounds.size)), for: .default)
        self.navigationBar.shadowImage = UIImage(color: backGroundColor,size:CGSize(width: (bounds.size.width), height: 1))
    }
    
    func setupNavigationBack(){
        UIApplication.shared.statusBarStyle = .default
        self.navigationBar.barTintColor = UIColor.white
        self.navigationBar.backgroundColor = UIColor.white
        self.navigationBar.barTintColor = UIColor.white
        self.navigationBar.isOpaque = false
        self.navigationBar.isTranslucent = false
        self.navigationBar.clipsToBounds = false
        self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black]
        let bounds = self.navigationBar.bounds
        self.navigationBar.setBackgroundImage(UIImage(color: UIColor.white,size:(bounds.size)), for: .default)
        self.navigationBar.shadowImage = UIImage(color: UIColor.white,size:CGSize(width: (bounds.size.width), height: 1))
    }
    
    
}

extension UIView {
    //MARK: - method for UITableView
    func tableViewCell() -> UITableViewCell? {
        var tableViewcell : UIView? = self
        while(tableViewcell != nil)
        {
            if tableViewcell! is UITableViewCell {
                break
            }
            tableViewcell = tableViewcell!.superview
        }
        return tableViewcell as? UITableViewCell
    }
    
    func tableViewIndexPath(_ tableView: UITableView) -> IndexPath? {
        
        if let cell = self.tableViewCell() {
            
            return tableView.indexPath(for: cell) as IndexPath?
        }else {
            return nil
        }
    }
    
    
    
    //MARK: - method for UICollectionView
    func collectionViewCell() -> UICollectionViewCell? {
        
        var collectionViewcell : UIView? = self
        
        while(collectionViewcell != nil)
        {
            if collectionViewcell! is UICollectionViewCell {
                break
            }
            collectionViewcell = collectionViewcell!.superview
        }
        return collectionViewcell as? UICollectionViewCell
    }
    
    
    func collectionViewIndexPath(_ collectionView: UICollectionView) -> IndexPath? {
        if let cell = self.collectionViewCell(){
            return collectionView.indexPath(for: cell) as IndexPath?
        }else {
            return nil
        }
        
    }
    
    func collectionReusableView() -> UICollectionReusableView? {
        var collectionReusableView : UIView? = self
        while(collectionReusableView != nil)
        {
            if collectionReusableView! is UICollectionReusableView {
                break
            }
            collectionReusableView = collectionReusableView!.superview
        }
        return collectionReusableView as? UICollectionReusableView
    }
    
    func addShadowView(_ width:CGFloat=0.2, height:CGFloat=0.2, Opacidade:Float=0.7, maskToBounds:Bool=false, radius:CGFloat=0.5){
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: width, height: height)
        self.layer.shadowRadius = radius
        self.layer.shadowOpacity = Opacidade
        self.layer.masksToBounds = maskToBounds
    }
}



extension GMSMapView {
    func mapStyle(withFilename name: String, andType type: String) {
        do {
            if let styleURL = Bundle.main.url(forResource: name, withExtension: type) {
                self.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
            }
        } catch {
        }
    }
}



/*extension UIView {
    
    @IBInspectable var shadow: Bool {
        get {
            return layer.shadowOpacity > 0.0
        }
        set {
            if newValue == true {
                self.addShadow()
            }
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set {
            self.layer.cornerRadius = newValue
            
            // Don't touch the masksToBound property if a shadow is needed in addition to the cornerRadius
            if shadow == false {
                self.layer.masksToBounds = true
            }
        }
    }
    
    
    func addShadow(_ shadowColor: CGColor = UIColor.black.cgColor,
                   shadowOffset: CGSize = CGSize(width: 1.0, height: 2.0),
                   shadowOpacity: Float = 0.4,
                   shadowRadius: CGFloat = 3.0) {
        layer.shadowColor = shadowColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
    }
}*/

