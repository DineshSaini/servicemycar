//
//  Constant.swift
//  Deputize America
//
//  Created by Darshak Trivedi.
//  Copyright © 2017 Scorch Mobile. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

let appDelegate = UIApplication.shared.delegate as! AppDelegate
//MARK:- Font
let MontserratSemiBold = "Montserrat-SemiBold"
let MontserratRegular   = "Montserrat-Regular"
let Regular = "Raleway-Regular"
let FontBold = "Raleway-Bold"
let FontMedium = "Raleway-Medium"
let FontLight = "Raleway-Light"
let MerriweatherBlack = "Merriweather-Black"
//MARK:- Color
let ColorLightgray = UIColor(hexString: "9b9b9b")
let ColorDarkblack = UIColor(hexString: "4a4a4a")
let Colorblack = UIColor(hexString: "000000")
let Colorblack2 = UIColor(hexString: "484848")
let ColorOrange = UIColor(hexString: "E49434")
let colorGreen =  UIColor(hexString: "009B01")
let colorlight =  UIColor(hexString: "f4f4f4")
let ColorStarEmpty = UIColor(hexString: "f8f8f8")
let ColorBgNavigation =  UIColor(hexString: "194366")
let Colorblue =  UIColor(hexString: "194366")
let ColorLightGray = UIColor(hexString: "4a4a4a")
let colorRed =  UIColor(hexString: "F5161A")
let colorLine =  UIColor(hexString: "d8d8d8")
let coloLightPink =  UIColor(hexString: "FFB6C1")
let colorBackGroundColor = UIColor.init(red: 244.0/255.0, green: 245.0/255.0, blue: 248.0/255.0, alpha: 1.0)


//MARK:- Storyboards Objects
let objLogin_SignUpSB = UIStoryboard(name: "Login_SignUpSB", bundle: nil)
let objMain = UIStoryboard(name: "Main", bundle: nil)
let objHomeSB = UIStoryboard.init(name: "HomeSB", bundle: nil)
let objServiceSB = UIStoryboard.init(name: "ServiceSB", bundle: nil)



//Instance Create

//telr payment Testing Key

//live pass "0" and Testing pass "1"
let Istesting = isLive ? "0" : "1"

let isLive = true

//MARK:- General Constant
let  kisLogin       = "Login"
//Login Success
let kUserLoginUserId        = "UserLoginUserId"
let kUserLoginPhone        = "user_id"
let kUserName       = "display_name"
let klatitude       = "latitude"
let kLongitude      = "longitude"
let kProfile_pic    = "profile_pic"
let thirdparty_id   = "thirdparty_id"
let SignOutKey   = "isSignOut"
let KUserLoginEmail   = "UserLoginEmail"
let kAppLanguage = "appLanguage"
let kFirstTime = "firstTime"

let kDeviceToken = "deviceToken"

let kUserShortLoginPhone = "sortPhone"

let appID = "1478263365"

//
//let getUserId = UserDefaults.standard.object(forKey: kUser_id)
let getuserName = UserDefaults.standard.object(forKey: kUserName)
let getLatitude = UserDefaults.standard.object(forKey: klatitude)
let getLongitude = UserDefaults.standard.object(forKey: kLongitude)
let getProfile_Pic = UserDefaults.standard.object(forKey: kProfile_pic)
let getFBId = UserDefaults.standard.object(forKey: thirdparty_id)
let getIsSignOut = UserDefaults.standard.object(forKey: SignOutKey)

var isInstalled: Bool{
    var result = false
    if let r = UserDefaults.standard.bool(forKey:kFirstTime) as Bool?{
        result = r
    }
    return result//kUserDefaults.bool(forKey: kIsLoggedIN)
}

/*//MARK:- Color
let backGroundColor = UIColor(red: 255.0/255.0, green: 255/255, blue: 255/255, alpha: 1.0)
let darkGrayColor = UIColor(red: 85/255.0, green: 85/255, blue: 85/255, alpha: 1.0)
let PinkColor = UIColor(red: 158/255.0, green: 1/255, blue: 92/255, alpha: 1.0)
let whiteColor = UIColor(red: 255/255.0, green: 255/255, blue: 255/255, alpha: 1.0)
let fontColor = UIColor(red: 64/255.0, green: 64/255, blue: 64/255, alpha: 1.0)
let textviewFont = UIColor(red: 64/255.0, green: 64/255, blue: 64/255, alpha: 1.0)
//MARK:- Fonts
let FontSemibold = "Montserrat-SemiBold"
let Regular = "OpenSans"
let FontBold = "OpenSans-Bold"

//MARK:- Dev API
let kbaseURL = "http://52.59.5.17/CoFly/public/api/"

//MARK:- Webservice API list
let kLogin                           = kbaseURL + "login"
let kStripepay                       = kbaseURL + "stripepay?"
let kJourney                         = kbaseURL + "journey"
let kSearch                          = kbaseURL + "journey/search"
let kGetfriendrequest                = kbaseURL + "getfriendrequest"
let kUpdate                          = kbaseURL + "profile/update"
let kCategory                        = kbaseURL + "journey/category"
let kJourney_Add                     = kbaseURL + "journey/add"
let kUPdateprofilepic                = kbaseURL + "profile/updateprofilepic"
let kProfile                         = kbaseURL + "profile"
let kChangepassword                  = kbaseURL + "profile/changepassword"
let kSignup                          = kbaseURL + "signup"
let kfriend_update                   = kbaseURL + "friend/update"
let kGetFriend                       = kbaseURL + "getfriend"
let kReviewADD                       = kbaseURL + "review/add"
let kReviewGet                       = kbaseURL + "review/get"


enum ConstantsStripe {
    static let publishableKey = "pk_test_2qbPoTrTYmDpLI8Qyd4lHhar"
    static let defaultCurrency = ""
    static let defaultDescription = ""
}

//0-pending , 1=friends , 2=reject , 3=block , 4=unblock
enum FreindStatus {
    static let pending = "0"
    static let friends = "1"
    static let reject  = "2"
    static let block   = "3"
    static let unblock = "4"
}
class Constants {
    
    struct NotificationKeys {
        static let SignedIn = "onSignInCompleted"
    }
    
    // DBProvider
    static let FriendList = "FriendList"
    static let Messages = "messages";
    
    static let USER_MESSAGES_ID = "User_Messages";
    
    //FriendList Key
    static let FriendDate = "Date"
    static let FriendName = "FriendName"
    static let FriendUserId = "FriendUserId"
    static let IsBlock = "IsBlock"
    static let LastMsg = "LastMsg"
    static let PhotoUrl = "PhotoUrl"
    static let Remarks1 = "Remarks1"
    static let Remarks2 = "Remarks2"
    static let TokenId = "TokenId"
    
    
    // Messages Key
    
    static let name = "name"
    
    static let MsgDate = "Date"
    static let IsUnRead = "IsUnRead"
    static let Remarks = "Remarks"
    static let Status = "Status"
    static let Textmessage = "message"
    static let user_Receiver = "user_Receiver"
    static let user_Sender = "user_Sender"
    
}  */ // class
//extension UIImage {
//    enum JPEGQuality: CGFloat {
//        case lowest  = 0
//        case low     = 0.25
//        case medium  = 0.5
//        case high    = 0.75
//        case highest = 1
//    }
//    
//    /// Returns the data for the specified image in JPEG format.
//    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
//    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
//    func jpeg(_ jpegQuality: JPEGQuality) -> Data? {
//        return jpegData(compressionQuality: jpegQuality.rawValue)
//    }
//}


/*============== NOTIFICATIONS ==================*/
extension NSNotification.Name {
    
    public static let BOOKING_SUCCESS_NOTIFICATION = NSNotification.Name("BookingSuccessNotification")
    
    public static let SHOW_REPORT_NOTIFICATION = NSNotification.Name("ShowReportNotification")
    
    public static let EXTRA_PART_APPROVAL_NOTIFICATION = NSNotification.Name("ExtraPartApprovalNotification")
    
    public static let EXTRA_INVOICE_NOTIFICATION = NSNotification.Name("ExtraInvoiceNotification")

    
    
    
}

