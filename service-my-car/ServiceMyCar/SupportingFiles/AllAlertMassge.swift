//
//  AllAlertMassge.swift
//  Deputize America
//
//  Created by Darshak Trivedi.
//  Copyright © 2017 Scorch Mobile. All rights reserved.
//

import Foundation

//MARK:- internet connected

let  internetConnectedTitle  = "Please make sure you are connected to the internet.".localized()
let  internetConnected  = "We could not detect an internet connection.".localized()


//MARK:- SignUp Login  & Forgot Password

let  signUpEmail = "Please enter Email"
let  signUpValidEmail = "Please enter valid email id."
let  signUpReEnterPassword = "Sorry, your Passwords were not matching."
let  signUpPassword = "Please enter Password"
let signUpMinSize = "Please enter minimum 6 characters"
let SignUpNumber = "Please enter Phone Number"
let SignUpNumberRequird = "The Phone number Field is required or invalid."
let validDate = "Please Select Valid Date."
let SignUpfName = "Please enter First Name"
let SignUplName = "Please enter Last Name"
let oldPassword = "Please enter your old password"
let newPassword = "Please enter your new password"
let confirmPassword = "Please enter your confirm password"
let Profession = "Please enter Profession"
let NotMetchPassword = "New password does not match the confirm password."
let Required = "Please enter All Deteils"
let DepartureORArrivale = "Please enter Departure Or Arrival."
let SelectCar = "Please select car."
let SelectState = "Please select State."
let SelectCarYear = "Please select the year."
let SelectEngineType = "Please select the engine type."
let SelectCarModel = "Please select car model."
let enterPlateCode = "Please enter plate code."
let enterPlatenumber = "Please enter plate number."
let pleaseSearchAdress = "Please Search Address."
let pleasebuldingNameNo = "Please Enter building Name/Number."
let pleaseCardNumber = "Please Enter Card Number."
let pleaseMonth = "Please select Month."
let pleaseYear = "Please select Year."
let pleaseEnterHolderName = "Please Enter Card Holder Name."
let pleaseEnterCountryRegion = "Please Enter Country or Region."
let pleaseEnterPromoCode = "Please Enter Promocode !"
let pleaseSelectTransport = "Please select transport Option."
let PleaseEnterQuote = "Please enter Quote".localized()
let RegisterNumber = "Please Fill Register Number"


















