//
//  AppDelegate.swift
//  ServiceMyCar
//
//  Created by baps on 21/01/19.
//  Copyright © 2019 baps. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Alamofire
import SJSwiftSideMenuController
import GoogleMaps
import GooglePlaces
import Firebase
import UserNotifications
import TelrSDK
let googleApiKey = "AIzaSyC4_bhTW7L0-uoV35GxuZy2yEc-XsI-fV0"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {

    
    let gcmMessageIDKey = "gcm.message_id"
    var window: UIWindow?
    var isFromNotification = false
    var isPayExtraInvoice = "1"
    var ExtraInvoiceOderId:String = String()
   var Selectedplacemark: CLLocationCoordinate2D?
    var selectedAddress:String = String()
    var isLoadingViewVisible:Bool = false
    var DataDic:NSDictionary!
    var net:NetworkReachabilityManager = NetworkReachabilityManager()!
    var isDropSelection = false
    var selectedPackagePrice:Float = Float()
    var StrVat:String  = String()
    var navLogin:UINavigationController! = nil
    var Bookings = BookingDetails()
    var CarRepair = CarRepairQuete()
    var deviceUDID:String = "0"
    var NCount = "0"
    var timer: Timer?
    var arrCategorylist:NSMutableArray = NSMutableArray()
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.addObserver(self, selector: #selector(AppDelegate.tokenRefreshNotification(_:)), name: .MessagingRegistrationTokenRefreshed, object: nil)
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.disabledToolbarClasses = [TelrController.self]
        UIApplication.shared.applicationIconBadgeNumber = 0
        GMSServices.provideAPIKey(googleApiKey)
        GMSPlacesClient.provideAPIKey(googleApiKey)
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
      
        if UserDefaults.standard.object(forKey: kAppLanguage) == nil {
           UserDefaults.standard.set("en", forKey: kAppLanguage)
        }
        
        print(UserDefaults.standard.object(forKey: kAppLanguage) as! String)
        //Push Notification
       /* if application.responds(to: #selector(UIApplication.registerUserNotificationSettings(_:))){
            
            if #available(iOS 10.0, *) {
                let center = UNUserNotificationCenter.current()
                center.delegate = self
                center.requestAuthorization(options: [.badge, .sound, .alert], completionHandler: {(grant, error)  in
                    if error == nil {
                        if grant {
                            DispatchQueue.main.async {
                                application.registerForRemoteNotifications()
                            }
                        } else {
                        }
                    } else {
                        print("error: ",error!)
                    }
                })
            } else {
                let notificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
                application.registerUserNotificationSettings(notificationSettings)
            }
            
            let types: UIUserNotificationType = [UIUserNotificationType.alert, UIUserNotificationType.badge, UIUserNotificationType.sound]
            
            let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: types, categories: nil)
            
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
        }*/
        
       
        
      //  application.registerForRemoteNotifications()
        startTimer()
        self.registerForRemoteNotification()

        return true
    }

   
    func startTimer() {
        timer = Timer.scheduledTimer(timeInterval: 10,
                                     target: self,
                                     selector: #selector(eventWith(timer:)),
                                     userInfo: [ "foo" : "bar" ],
                                     repeats: true)
    }
    
    // Timer expects @objc selector
    @objc func eventWith(timer: Timer!) {
        let info = timer.userInfo as Any
        print(info)
        getNotificationCount()
    }
    
    func loginSuccess(){
    let nav = objHomeSB.instantiateViewController(withIdentifier: "MainNavigationController") as! UINavigationController
        let languageCode = UserDefaults.standard.object(forKey: kAppLanguage) as! String
        if languageCode == "en" {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }else {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Done".localized()
        IQKeyboardManager.shared.disabledToolbarClasses = [TelrController.self,TelrResponseController.self]
    (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController = nav
        self.window?.makeKeyAndVisible()
   }
   
        func logout(){
            navLogin = (objLogin_SignUpSB.instantiateViewController(withIdentifier: "NavLogin") as! UINavigationController)
                if self.window != nil {
                    UIView.transition(with: self.window!, duration: 0.5, options: .transitionCrossDissolve, animations: {
                        let oldState: Bool = UIView.areAnimationsEnabled
                        UIView.setAnimationsEnabled(false)
                        self.window!.rootViewController = self.navLogin
                        UIView.setAnimationsEnabled(oldState)
                    }, completion: { (finished: Bool) -> () in
                    })
                }
        }
    func LogInfromTutorial() {
        navLogin = objMain.instantiateViewController(withIdentifier: "MainNav") as? UINavigationController
        if self.window != nil {
            UIView.transition(with: self.window!, duration: 0.5, options: .transitionCrossDissolve, animations: {
                let oldState: Bool = UIView.areAnimationsEnabled
                UIView.setAnimationsEnabled(false)
                self.window!.rootViewController = self.navLogin
                UIView.setAnimationsEnabled(oldState)
            }, completion: { (finished: Bool) -> () in
            })
        }
    }
    
    //MARK:- SET Date & Time Format
    
    func SetDateFormat(_ strDate: String)-> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let yourDate = dateFormatter.date(from: strDate)!
        dateFormatter.dateFormat = "MMM dd, yyyy"
        let convertdate: String = dateFormatter.string(from: yourDate)
        
        return convertdate
    }
    
    func SetDateFormat1Convert(_ strDate: String)-> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "mm/dd/yy"
        let yourDate = dateFormatter.date(from: strDate)!
        dateFormatter.dateFormat = "MMM dd, yyyy"
        let convertdate: String = dateFormatter.string(from: yourDate)
        
        return convertdate
    }
    
    func SetTimeFormat(_ strTime: String)-> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        let yourDate = dateFormatter.date(from: strTime)!
        dateFormatter.dateFormat = "h:mm a"
        let convertdate: String = dateFormatter.string(from: yourDate)
        
        return convertdate
    }
    
    func SetTimeFormatConvert(_ strTime: String)-> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        
        let yourDate = dateFormatter.date(from: strTime)!
        dateFormatter.dateFormat = "HH:mm"
        let convertdate: String = dateFormatter.string(from: yourDate)
        
        return convertdate
    }
    
    
    func getNotificationCount(){
    
        if let nUserId = UserDefaults.standard.value(forKey: kUserLoginUserId) as? String
        {
            let params : Parameters = [
                "userId": nUserId
            ]
            var Count = "0"
            print("param is \(params)")
            
            let url = URL(string: kgetCountNewNotification)!
            print("Url printed \(url)")
            
            let manager = Alamofire.SessionManager.default
            let headers = header
            manager.session.configuration.timeoutIntervalForRequest = 120
            manager.request(url, method: .get, parameters: params, headers: headers as? HTTPHeaders)
                .responseJSON
                {
                    response in
                    
                    switch (response.result)
                    {
                    case .success:
                        
                        //do json stuff
                        let jsonResponse = response.result.value as! NSDictionary
                        print("JsonResponse printed \(jsonResponse)")
                        
                        if let data = jsonResponse.value(forKey: "data") as? NSDictionary
                        {
                            if let Count = data.value(forKey: "count") as? String
                            {
                                self.NCount = Count
                            }
                            else if let Count = data.value(forKey: "count") as? NSNumber
                            {
                                self.NCount = "\(Count)"
                                
                            }
                            NotificationCenter.default.post(
                                name: Notification.Name(rawValue: "ToggleAuthUINotification"),
                                object: nil,
                                userInfo: ["statusText": self.NCount])
                            print(self.NCount)
                        }
                        else
                        {
                            self.NCount = "0"
                            NotificationCenter.default.post(
                                name: Notification.Name(rawValue: "ToggleAuthUINotification"),
                                object: nil,
                                userInfo: ["statusText": self.NCount])
                        }
                        
                        break
                        
                    case .failure(let error):
                        
                        if error._code == NSURLErrorTimedOut {
                            //HANDLE TIMEOUT HERE
                        }
                        
                        
                        
                        print("\n\nAuth request failed with error:\n \(error)")
                        break
                    }
            }
            
        }
        
       
    }
    
   
    
    ///////////*************** NOTIFICATION HANDLING START ***************//////////////
    
    func registerForRemoteNotification() {
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For iOS 10 data message sent via FCM
            Messaging.messaging().delegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        if let userInfoDict = notification.request.content.userInfo as? NSDictionary{
            print(" Notification data dict \(userInfoDict)")
        }
        
        //         print("User Info = ",notification.request.content.userInfo)
        completionHandler([.alert, .badge, .sound])
    }
    

    
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        if let token = Messaging.messaging().fcmToken{
            UserDefaults.standard.set(token, forKey: kDeviceToken)
            deviceUDID = token
           // kUserDefaults.set(token, forKey: kDeviceToken)
            print("registered to firebase with token : \(Messaging.messaging().fcmToken ?? "--")")
        }else{
            deviceUDID = "\(UserDefaults.standard.value(forKey: kDeviceToken) ?? "--")"
            print("already registered to firebase with token : \(UserDefaults.standard.value(forKey: kDeviceToken) ?? "--")")
        }
    }
    
    
    
    private func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("didFailToRegisterForRemoteNotificationsWithError")
        print(error.localizedDescription)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        if let userInfoDict = response.notification.request.content.userInfo as? NSDictionary /*Dictionary<String,AnyObject>*/{
            print("userInfo: \(userInfoDict)")
            self.openNotification(notificationInfo: userInfoDict)
        }
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("userInfo: \(userInfo)")
        if let userInfoDict = userInfo as? NSDictionary{
            self.openNotification(notificationInfo: userInfoDict)
        }
    }
    
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print("userInfo: \(userInfo)")
        if let userInfoDict = userInfo as? NSDictionary{
            self.openNotification(notificationInfo: userInfoDict)
        }
    }
    
    // Push notification Click navigation Method
    func NavigateToClickOnNotification(NavTo:UIViewController){
        isFromNotification = true
        self.window?.rootViewController = NavTo
    }

    
    
    func openNotification(notificationInfo:NSDictionary){
        self.DataDic = notificationInfo
        if let dics2 = DataDic.object(forKey: "aps") as? NSDictionary{
            
            if let varMsgType = DataDic.object(forKey: "type") as? String {
                if varMsgType == "BOOKING_SUCCESS"{
//                    let nav = objHomeSB.instantiateViewController(withIdentifier: "MainNavigationController") as! MainNavigationController
//
//                    let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyServiceViewController") as! MyServiceViewController
//                    // nextViewController.isHistory = false
//                    NavigateToClickOnNotification(NavTo: nextViewController)
                    
                    NotificationCenter.default.post(name: .BOOKING_SUCCESS_NOTIFICATION, object: nil, userInfo: ["booking":"success"])
                    
                   // goToMyServiceScreen()
                    
                }else if varMsgType == "COLLECTION_REPORT"{
                    let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "webview_Vc") as! webview_Vc
                    var params = Dictionary<String,String>()
                    var name:String  = ""
                    var redirectUrl:String = ""
                    var orderID:String = ""
                    if let _name = DataDic.object(forKey: "type") as? String{
                        nextViewController.strName  = name
                        name = _name
                        params.updateValue(_name, forKey: "type")
                        
                    }
                    if let _redirectUrl = DataDic.object(forKey: "url") as? String{
                         nextViewController.URL  = name
                        redirectUrl = _redirectUrl
                        params.updateValue(_redirectUrl, forKey: "url")

                    }
                    if let intId = DataDic.object(forKey: "orderId") as? String{
                        nextViewController.orderId = intId
                        orderID = intId
                        params.updateValue(intId, forKey: "orderId")
                    }else if let intId = DataDic.object(forKey: "orderId") as? NSNumber{
                        nextViewController.orderId = "\(intId)"
                        orderID = "\(intId)"
                        params.updateValue("\(intId)", forKey: "orderId")
                        
                    }

                    NotificationCenter.default.post(name: .SHOW_REPORT_NOTIFICATION, object: nil, userInfo: ["report":params])
                   // self.goToShowDetailScreen(name: name, redirectUrl: redirectUrl)
                    //  NavigateToClickOnNotification(NavTo: nextViewController)

                    
                }else if varMsgType == "DELIVERY_REPORT"{
                    let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "webview_Vc") as! webview_Vc
                    var params = Dictionary<String,String>()
                    var name:String  = ""
                    var redirectUrl:String = ""
                    var orderID:String = ""
                    if let _name = DataDic.object(forKey: "type") as? String{
                        nextViewController.strName  = name
                        name = _name
                        params.updateValue(_name, forKey: "type")
                        
                    }
                    if let _redirectUrl = DataDic.object(forKey: "url") as? String{
                        nextViewController.URL  = name
                        redirectUrl = _redirectUrl
                        params.updateValue(_redirectUrl, forKey: "url")
                        
                    }
                    if let intId = DataDic.object(forKey: "orderId") as? String{
                        nextViewController.orderId = intId
                        orderID = intId
                        params.updateValue(intId, forKey: "orderId")
                    }else if let intId = DataDic.object(forKey: "orderId") as? NSNumber{
                        nextViewController.orderId = "\(intId)"
                        orderID = "\(intId)"
                        params.updateValue("\(intId)", forKey: "orderId")
                        
                    }

                    NotificationCenter.default.post(name: .SHOW_REPORT_NOTIFICATION, object: nil, userInfo: ["report":params])

                    //self.goToShowDetailScreen(name: name, redirectUrl: redirectUrl)
                  //  NavigateToClickOnNotification(NavTo: nextViewController)
                    
                }else if varMsgType == "INVOICE_RECEIVED"{
                  
                    let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "CategoryDocViewController") as! CategoryDocViewController
                    
                    
                    var params = Dictionary<String,String>()
                    var name:String  = ""
                    var redirectUrl:String = ""
                    var orderID:String = ""
                    var service:String = ""
                    if let _name = DataDic.object(forKey: "type") as? String{
                       // nextViewController.strName  = name
                        name = _name
                        params.updateValue(_name, forKey: "type")
                        
                    }
                    if let _redirectUrl = DataDic.object(forKey: "url") as? String{
                      //  nextViewController.URL  = name
                        redirectUrl = _redirectUrl
                        params.updateValue(_redirectUrl, forKey: "url")
                        
                    }
                    if let intId = DataDic.object(forKey: "orderId") as? String{
                        //nextViewController.orderId = intId
                        orderID = intId
                        params.updateValue(intId, forKey: "orderId")
                    }else if let intId = DataDic.object(forKey: "orderId") as? NSNumber{
                     //   nextViewController.orderId = "\(intId)"
                        orderID = "\(intId)"
                        params.updateValue("\(intId)", forKey: "orderId")
                        
                    }else if let servicet = DataDic.object(forKey: "serviceType") as? String{
                        //   nextViewController.orderId = "\(intId)"
                        service = servicet
                        params.updateValue(servicet, forKey: "serviceType")
                        
                    }

                    
                    NotificationCenter.default.post(name: .EXTRA_INVOICE_NOTIFICATION, object: nil, userInfo:["invoice":params])
                   
                    
                }else if varMsgType == "INVOICE_PAID"{
                    let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "webview_Vc") as! webview_Vc
                    var params = Dictionary<String,String>()
                    var name:String  = ""
                    var redirectUrl:String = ""
                    var orderID:String = ""
                    if let _name = DataDic.object(forKey: "type") as? String{
                        nextViewController.strName  = name
                        name = _name
                        params.updateValue(_name, forKey: "type")
                        
                    }
                    if let _redirectUrl = DataDic.object(forKey: "url") as? String{
                        nextViewController.URL  = name
                        redirectUrl = _redirectUrl
                        params.updateValue(_redirectUrl, forKey: "url")
                        
                    }
                    if let intId = DataDic.object(forKey: "orderId") as? String{
                        nextViewController.orderId = intId
                        orderID = intId
                        params.updateValue(intId, forKey: "orderId")
                    }else if let intId = DataDic.object(forKey: "orderId") as? NSNumber{
                        nextViewController.orderId = "\(intId)"
                        orderID = "\(intId)"
                        params.updateValue("\(intId)", forKey: "orderId")
                        
                    }

                    NotificationCenter.default.post(name: .SHOW_REPORT_NOTIFICATION, object: nil, userInfo: ["report":params])

                    
                    //self.goToShowDetailScreen(name: name, redirectUrl: redirectUrl)
                   // NavigateToClickOnNotification(NavTo: nextViewController)
                    
                    
                }else if varMsgType == "HEALTH_CHECK_VIDEO"{
                    let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "webview_Vc") as! webview_Vc
                    var params = Dictionary<String,String>()
                    var name:String  = ""
                    var redirectUrl:String = ""
                    var orderID:String = ""
                    if let _name = DataDic.object(forKey: "type") as? String{
                        nextViewController.strName  = name
                        name = _name
                        params.updateValue(_name, forKey: "type")
                        
                    }
                    if let _redirectUrl = DataDic.object(forKey: "url") as? String{
                        nextViewController.URL  = name
                        redirectUrl = _redirectUrl
                        params.updateValue(_redirectUrl, forKey: "url")
                        
                    }
                    if let intId = DataDic.object(forKey: "orderId") as? String{
                        nextViewController.orderId = intId
                        orderID = intId
                        params.updateValue(intId, forKey: "orderId")
                    }else if let intId = DataDic.object(forKey: "orderId") as? NSNumber{
                        nextViewController.orderId = "\(intId)"
                        orderID = "\(intId)"
                        params.updateValue("\(intId)", forKey: "orderId")
                        
                    }


                    
                    NotificationCenter.default.post(name: .SHOW_REPORT_NOTIFICATION, object: nil, userInfo: ["report":params])


                    //self.goToShowDetailScreen(name: name, redirectUrl: redirectUrl)
                    //NavigateToClickOnNotification(NavTo: nextViewController)
                    
                    
                }else if varMsgType == "EXTRAPART_APPROVAL"{
                   // let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "ExtraParts_Vc") as! ExtraParts_Vc
                    let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "AdditionRepairsViewController") as! AdditionRepairsViewController
//                    var orderID:String = ""
//                    if let intId = DataDic.object(forKey: "orderId") as? String{
//                        nextViewController.orderId = intId
//                        orderID = intId
//                    }else if let intId = DataDic.object(forKey: "orderId") as? NSNumber{
//                        nextViewController.orderId = "\(intId)"
//                        orderID = "\(intId)"
//                    }
                    
                    var params = Dictionary<String,String>()
                    var name:String  = ""
                    var redirectUrl:String = ""
                    var orderID:String = ""
                    if let _name = DataDic.object(forKey: "type") as? String{
                        name = _name
                        params.updateValue(_name, forKey: "type")
                        
                    }
                    if let _redirectUrl = DataDic.object(forKey: "url") as? String{
                        redirectUrl = _redirectUrl
                        params.updateValue(_redirectUrl, forKey: "url")
                        
                    }
                    if let intId = DataDic.object(forKey: "orderId") as? String{
                        nextViewController.orderId = intId
                        orderID = intId
                        params.updateValue(intId, forKey: "orderId")
                    }else if let intId = DataDic.object(forKey: "orderId") as? NSNumber{
                        nextViewController.orderId = "\(intId)"
                        orderID = "\(intId)"
                        params.updateValue("\(intId)", forKey: "orderId")
                        
                    }

                    
                    

                    NotificationCenter.default.post(name: .EXTRA_PART_APPROVAL_NOTIFICATION, object: nil, userInfo: ["extrapart":params])
                    //self.goToExtraApprovalPartScreen(orderID: orderID)
                    //NavigateToClickOnNotification(NavTo: nextViewController)
                    
                    
                }else if varMsgType == "SERVICE_SCHADULE"{
                    let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "webview_Vc") as! webview_Vc
                    var params = Dictionary<String,String>()
                    var name:String  = ""
                    var redirectUrl:String = ""
                    var orderID:String = ""

                    if let _name = DataDic.object(forKey: "type") as? String{
                        nextViewController.strName  = name
                        name = _name
                        params.updateValue(_name, forKey: "type")
                        
                    }
                    if let _redirectUrl = DataDic.object(forKey: "url") as? String{
                        nextViewController.URL  = name
                        redirectUrl = _redirectUrl
                        params.updateValue(_redirectUrl, forKey: "url")
                        
                    }
                    
                    if let intId = DataDic.object(forKey: "orderId") as? String{
                        nextViewController.orderId = intId
                        orderID = intId
                        params.updateValue(intId, forKey: "orderId")
                    }else if let intId = DataDic.object(forKey: "orderId") as? NSNumber{
                        nextViewController.orderId = "\(intId)"
                        orderID = "\(intId)"
                        params.updateValue("\(intId)", forKey: "orderId")

                    }
                    NotificationCenter.default.post(name: .SHOW_REPORT_NOTIFICATION, object: nil, userInfo: ["report":params])


                   // self.goToShowDetailScreen(name: name, redirectUrl: redirectUrl)
                   // NavigateToClickOnNotification(NavTo: nextViewController)
                }
            }
        }
    }
    
    
    func goToMyServiceScreen() {
        let vc = objHomeSB.instantiateViewController(withIdentifier: "DashboardTabbarController") as! DashboardTabbarController
        var nav : UINavigationController!
        if let appNav = self.window?.rootViewController as? UINavigationController{
            nav = appNav
            nav.dismiss(animated: false, completion: nil)
            nav.popToRootViewController(animated: true)
            
        }else{
            nav = proccedDashboard()
        }
        self.window?.rootViewController = nav
        vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        let vcCNav = UINavigationController(rootViewController: vc)
        self.window?.rootViewController = vcCNav
        
    }
    
    
    func goToShowDetailScreen(name:String, redirectUrl:String) {
        let vc = objHomeSB.instantiateViewController(withIdentifier: "webview_Vc") as! webview_Vc
       //  let vc = objHomeSB.instantiateViewController(withIdentifier: "DashboardTabbarController") as! DashboardTabbarController
        vc.strName = name
        vc.URL = redirectUrl
        var nav : UINavigationController!
        if let appNav = self.window?.rootViewController as? UINavigationController{
            nav = appNav
            nav.dismiss(animated: false, completion: nil)
            nav.popToRootViewController(animated: true)
            
        }else{
            nav = proccedDashboard()
        }
        self.window?.rootViewController = nav
        vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        let vcCNav = UINavigationController(rootViewController: vc)
        self.window?.rootViewController = vcCNav

        
    }
    
    
    func goToExtraInvoiceScreen(name:String, redirectUrl:String) {
       //  let vc = objHomeSB.instantiateViewController(withIdentifier: "DashboardTabbarController") as! DashboardTabbarController
        let vc = objHomeSB.instantiateViewController(withIdentifier: "MyInvoice_Vc") as! MyInvoice_Vc
//        vc.strName = name
//        vc.URL = redirectUrl
        var nav : UINavigationController!
        if let appNav = self.window?.rootViewController as? UINavigationController{
            nav = appNav
            nav.dismiss(animated: false, completion: nil)
            nav.popToRootViewController(animated: true)
            
        }else{
            nav = proccedDashboard()
        }
        self.window?.rootViewController = nav
        vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        let vcCNav = UINavigationController(rootViewController: vc)
        self.window?.rootViewController = vcCNav
        
        
    }
    
    
    func goToExtraApprovalPartScreen(orderID:String) {
        // let vc = objHomeSB.instantiateViewController(withIdentifier: "DashboardTabbarController") as! DashboardTabbarController
       // let vc = objHomeSB.instantiateViewController(withIdentifier: "ExtraParts_Vc") as! ExtraParts_Vc
         let vc = objHomeSB.instantiateViewController(withIdentifier: "AdditionRepairsViewController") as! AdditionRepairsViewController
        vc.orderId = orderID
        var nav : UINavigationController!
        if let appNav = self.window?.rootViewController as? UINavigationController{
            nav = appNav
            nav.dismiss(animated: false, completion: nil)
            nav.popToRootViewController(animated: true)
            
        }else{
            nav = proccedDashboard()
        }
        self.window?.rootViewController = nav
        vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        let vcCNav = UINavigationController(rootViewController: vc)
        self.window?.rootViewController = vcCNav
        
        
    }

    
    
    
    func proccedDashboard() -> UINavigationController{
            let navVC = objHomeSB.instantiateViewController(withIdentifier: "MainNavigationController") as!UINavigationController
            return navVC
    }


    
    
    ///////////****************** NOTIFICATION HANDLING END *************/////////
    
   

//    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//
//        var token: String = ""
//        for i in 0..<deviceToken.count {
//            token += String(format: "%02.2hhx", deviceToken[i] as CVarArg)
//        }
//        print(token)
//        deviceUDID = token
//    }
    
 
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
   
    
    
    // Push notification received

    
   /* func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any]) {
        
        DataDic = (data as NSDictionary?)!
        
        
        print(DataDic)
        
        self.openNotification(notificationInfo: DataDic)
        if(application.applicationState ==  UIApplication.State.active) {
            
             print("active")
        }
        else if(application.applicationState == UIApplication.State.background){
            
            print("background")
            if let dics2 = DataDic.object(forKey: "aps") as? NSDictionary{
                
                if let varMsgType = dics2.object(forKey: "type") as? String {
                    
                    //            BOOKING_SUCCESS = > Redirect To My Services
                    //            COLLECTION_REPORT = > Redirect to collection Report
                    //            DELIVERY_REPORT = > Redirect to delivery address
                    //            INVOICE_RECEIVED = > Redirect to invoice
                    //            INVOICE_PAID = > Redirect to invoice
                    //            EXTRAPART_APPROVAL = > Redirect to Additional Repair
                    //            SERVICE_SCHADULE => Redirect to service schedule
                    if varMsgType == "BOOKING_SUCCESS"
                    {
                        
                        
                        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyServices_Vc") as! MyServices_Vc
                        nextViewController.isHistory = false
                       
                        NavigateToClickOnNotification(NavTo: nextViewController)
                        
                    }
                    else if varMsgType == "COLLECTION_REPORT"
                    {
                        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "webview_Vc") as! webview_Vc
                        
                        if let name = dics2.object(forKey: "title") as? String
                        {
                            nextViewController.strName  = name
                        }
                        
                        if let name = dics2.object(forKey: "redirectUrl") as? String
                        {
                            nextViewController.URL  = name
                        }
                        
                        NavigateToClickOnNotification(NavTo: nextViewController)
                    }
                    else if varMsgType == "DELIVERY_REPORT"
                    {
                        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "webview_Vc") as! webview_Vc
                        
                        if let name = dics2.object(forKey: "title") as? String
                        {
                            nextViewController.strName  = name
                        }
                        
                        if let name = dics2.object(forKey: "redirectUrl") as? String
                        {
                            nextViewController.URL  = name
                        }
                        
                        
                       NavigateToClickOnNotification(NavTo: nextViewController)
                    }
                    else if varMsgType == "INVOICE_RECEIVED"
                    {
                        let nextViewController = objMain.instantiateViewController(withIdentifier: "MyInvoice_Vc") as! MyInvoice_Vc
                        
                       NavigateToClickOnNotification(NavTo: nextViewController)
                    }
                    else if varMsgType == "INVOICE_PAID"
                    {
                        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "webview_Vc") as! webview_Vc
                        
                        if let name = dics2.object(forKey: "title") as? String
                        {
                            nextViewController.strName  = name
                        }
                        
                        if let name = dics2.object(forKey: "redirectUrl") as? String
                        {
                            nextViewController.URL  = name
                        }
                        
                        
                        NavigateToClickOnNotification(NavTo: nextViewController)
                    }
                    else if varMsgType == "HEALTH_CHECK_VIDEO"
                    {
                        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "webview_Vc") as! webview_Vc
                        
                        if let name = dics2.object(forKey: "title") as? String
                        {
                            nextViewController.strName  = name
                        }
                        
                        if let name = dics2.object(forKey: "redirectUrl") as? String
                        {
                            nextViewController.URL  = name
                        }
                        
                        
                        NavigateToClickOnNotification(NavTo: nextViewController)
                    }
                    else if varMsgType == "EXTRAPART_APPROVAL"
                    {
                        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "ExtraParts_Vc") as! ExtraParts_Vc
                        if let intId = dics2.object(forKey: "orderId") as? String
                        {
                            nextViewController.orderId = intId
                        }
                        else if let intId = dics2.object(forKey: "orderId") as? NSNumber
                        {
                            nextViewController.orderId = "\(intId)"
                        }
                        
                        NavigateToClickOnNotification(NavTo: nextViewController)
                    }
                    else if varMsgType == "SERVICE_SCHADULE"
                    {
                        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "webview_Vc") as! webview_Vc
                        
                        if let name = dics2.object(forKey: "title") as? String
                        {
                            nextViewController.strName  = name
                        }
                        
                        if let name = dics2.object(forKey: "redirectUrl") as? String
                        {
                            nextViewController.URL  = name
                        }
                        
                        NavigateToClickOnNotification(NavTo: nextViewController)
                    }
                    
                    
                    
                }
            }
            
        }
        else if(application.applicationState == UIApplication.State.inactive ){
           
            print("inactive")
            if let dics2 = DataDic.object(forKey: "aps") as? NSDictionary{
                
                if let varMsgType = dics2.object(forKey: "type") as? String {
                    
                    //            BOOKING_SUCCESS = > Redirect To My Services
                    //            COLLECTION_REPORT = > Redirect to collection Report
                    //            DELIVERY_REPORT = > Redirect to delivery address
                    //            INVOICE_RECEIVED = > Redirect to invoice
                    //            INVOICE_PAID = > Redirect to invoice
                    //            EXTRAPART_APPROVAL = > Redirect to Additional Repair
                    //            SERVICE_SCHADULE => Redirect to service schedule
                    if varMsgType == "BOOKING_SUCCESS"
                    {
                        
                        
                        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "MyServiceViewController") as! MyServiceViewController
                       // nextViewController.isHistory = false
                        
                        NavigateToClickOnNotification(NavTo: nextViewController)
                        
                    }
                    else if varMsgType == "COLLECTION_REPORT"
                    {
                        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "webview_Vc") as! webview_Vc
                        
                        if let name = dics2.object(forKey: "title") as? String
                        {
                            nextViewController.strName  = name
                        }
                        
                        if let name = dics2.object(forKey: "redirectUrl") as? String
                        {
                            nextViewController.URL  = name
                        }
                        
                        NavigateToClickOnNotification(NavTo: nextViewController)
                    }
                    else if varMsgType == "DELIVERY_REPORT"
                    {
                        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "webview_Vc") as! webview_Vc
                        
                        if let name = dics2.object(forKey: "title") as? String
                        {
                            nextViewController.strName  = name
                        }
                        
                        if let name = dics2.object(forKey: "redirectUrl") as? String
                        {
                            nextViewController.URL  = name
                        }
                        
                        
                        NavigateToClickOnNotification(NavTo: nextViewController)
                    }
                    else if varMsgType == "INVOICE_RECEIVED"
                    {
                        let nextViewController = objMain.instantiateViewController(withIdentifier: "MyInvoice_Vc") as! MyInvoice_Vc
                        
                        NavigateToClickOnNotification(NavTo: nextViewController)
                    }
                    else if varMsgType == "INVOICE_PAID"
                    {
                        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "webview_Vc") as! webview_Vc
                        
                        if let name = dics2.object(forKey: "title") as? String
                        {
                            nextViewController.strName  = name
                        }
                        
                        if let name = dics2.object(forKey: "redirectUrl") as? String
                        {
                            nextViewController.URL  = name
                        }
                        
                        
                        NavigateToClickOnNotification(NavTo: nextViewController)
                    }
                    else if varMsgType == "HEALTH_CHECK_VIDEO"
                    {
                        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "webview_Vc") as! webview_Vc
                        
                        if let name = dics2.object(forKey: "title") as? String
                        {
                            nextViewController.strName  = name
                        }
                        
                        if let name = dics2.object(forKey: "redirectUrl") as? String
                        {
                            nextViewController.URL  = name
                        }
                        
                        
                        NavigateToClickOnNotification(NavTo: nextViewController)
                    }
                    else if varMsgType == "EXTRAPART_APPROVAL"
                    {
                        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "ExtraParts_Vc") as! ExtraParts_Vc
                        if let intId = dics2.object(forKey: "orderId") as? String
                        {
                            nextViewController.orderId = intId
                        }
                        else if let intId = dics2.object(forKey: "orderId") as? NSNumber
                        {
                            nextViewController.orderId = "\(intId)"
                        }
                        
                        NavigateToClickOnNotification(NavTo: nextViewController)
                    }
                    else if varMsgType == "SERVICE_SCHADULE"
                    {
                        let nextViewController = objHomeSB.instantiateViewController(withIdentifier: "webview_Vc") as! webview_Vc
                        
                        if let name = dics2.object(forKey: "title") as? String
                        {
                            nextViewController.strName  = name
                        }
                        
                        if let name = dics2.object(forKey: "redirectUrl") as? String
                        {
                            nextViewController.URL  = name
                        }
                        
                        NavigateToClickOnNotification(NavTo: nextViewController)
                    }
                }
            }
           
            
        }
        
    }*/
    

}
/*@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    
 
    //receive local notification when app in foreground
    func application(_ application: UIApplication, didReceive notificationMedicine: UILocalNotification) {
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "TodoListShouldRefreshMedicines"), object: self)
        
        let Dic:NSDictionary = notificationMedicine.userInfo! as NSDictionary
        print(Dic)
        if let StrTemp = Dic.object(forKey: "titleMedicine") as? String {
            
            if(StrTemp != ""){
//                let RootObj:Notification_Schedule_Hendal_Vc = Notification_Schedule_Hendal_Vc(nibName:appDelegate.CheckDeviceType("Notification_Schedule_Hendal_Vc"),bundle: nil)
//                RootObj.DataDic = Dic
//                let navigates = UINavigationController(rootViewController: RootObj)
//                window?.rootViewController?.present(navigates, animated: true, completion: nil)
            }
        }
    }
    
    func applicationDidBecomeActive(_ application: UIApplication, didReceive notificationMedicine: UILocalNotification) {
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "TodoListShouldRefreshMedicines"), object: self)
        
        let Dic:NSDictionary = notificationMedicine.userInfo! as NSDictionary
        print(Dic)
        if let StrTemp = Dic.object(forKey: "titleMedicine") as? String {
            
            if(StrTemp != ""){
               
            }
        }
    }
    
    
    
    //snooze notification handler when app in background
    func application(_ application: UIApplication, handleActionWithIdentifier identifier: String?, for notification: UILocalNotification, completionHandler: @escaping () -> Void) {
        var index: Int = -1
        var soundName: String = ""
        if let userInfo = notification.userInfo {
            soundName = userInfo["soundName"] as! String
            index = userInfo["index"] as! Int
        }
        
        completionHandler()
    }
    
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("didFailToRegisterForRemoteNotificationsWithError")
        print(error.localizedDescription)
    }
    
}*/

extension AppDelegate : MessagingDelegate{
    //MARK: - Device Token
    
    
    
    @objc func tokenRefreshNotification(_ notification: NSNotification) {
        if let refreshedToken = Messaging.messaging().fcmToken{
            deviceUDID = refreshedToken
            UserDefaults.standard.set(refreshedToken, forKey: kDeviceToken)

            print("InstanceID token: \(refreshedToken)")
        }
        
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        
    }

    
    func application(received remoteMessage: MessagingRemoteMessage) {
        print("remote message received: \(remoteMessage)")
    }
    
    
//    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
//        print("Firebase registration token: \(fcmToken)")
//
//        let dataDict:[String: String] = ["token": fcmToken]
//        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
//        // TODO: If necessary send token to application server.
//        // Note: This callback is fired at each app startup and whenever a new token is generated.
//    }
//
//    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
//        print("MEssage",remoteMessage.appData)
//    }
}
extension String {
    
    func localized(bundle: Bundle = .main, tableName: String = "Localizable") -> String {
        return NSLocalizedString(self, tableName: tableName, value: "**\(self)**", comment: "")
    }
}







class MainNavigationController: UINavigationController {
    var statusBarView : UIView!
    
    
    override init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // self.makeTransparent()
    }
    
    
    
    override var shouldAutorotate : Bool {
        return false
    }
    
    override var prefersStatusBarHidden : Bool {
        return false
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }
    
    override var preferredStatusBarUpdateAnimation : UIStatusBarAnimation {
        return .none
    }
}
